<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\service;

class ServiceArchives {

	public static function doUnzip($zipfile, $targetDir, $delete = true) {

		$zip = new \ZipArchive();
		$res = $zip->open($zipfile);
		if ($res !== true) return false;
		$ok = $zip->extractTo($targetDir);
		$zip->close();
		if ($ok && $delete) {
			unlink($zipfile);
		}
		return $ok;

	}

}