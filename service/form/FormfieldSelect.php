<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\service\form;

use tt\service\ServiceStrings;

class FormfieldSelect extends Formfield {

	private $options;

	public function __construct($name, $options, $title = null, $value = null, $val_from_request = true, $more_params = array()) {
		parent::__construct($name, $title, $value, $val_from_request, $more_params);
		$this->options = $options;
		$this->cssClasses[] = "form-control";
	}

	public function inner_html() {
		$options_html = array();
		foreach ($this->options as $key => $value) {
			$selected = $key == $this->value ? " selected" : "";
			$options_html[] = "<option value='" . ServiceStrings::escape_value_html($key) . "' $selected>$value</option>";
		}
		$html = "<select " . $this->getParams_inner(false) . " >" . implode("\n", $options_html) . "</select>";
		return $html;
	}

}
