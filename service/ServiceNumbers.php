<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\service;

class ServiceNumbers {

	/**
	 * "0"         -> 0
	 * ""          -> 0
	 * "1"         -> 1
	 * "11"        -> 11
	 * ".1"        -> 0.1
	 * ".11"       -> 0.11
	 * ".111"      -> 0.111
	 * "1."        -> 1
	 * "1.2"       -> 1.2
	 * "1.22"      -> 1.22
	 * "1.222"     -> 1.222    !!
	 * "1.222,"    -> 1222
	 * "1.222,3"   -> 1222.3
	 * "1.222,33"  -> 1222.33
	 * "1.222,333" -> 1222.333
	 * ",1"        -> 0.1
	 * ",11"       -> 0.11
	 * ",111"      -> 0.111
	 * "1,"        -> 1
	 * "1,2"       -> 1.2
	 * "1,22"      -> 1.22
	 * "1,222"     -> 1.222    !!
	 * "1,222."    -> 1222
	 * "1,222.3"   -> 1222.3
	 * "1,222.33"  -> 1222.33
	 * "1,222.333" -> 1222.333
	 */
	public static function parseNumber($string) {
		preg_match("/^(\\-?[0-9.,]*?)(?:[.,]([0-9]*))?\$/", $string, $matches);
		$r = preg_replace("/[,.]/", "", $matches[1]) . (isset($matches[2]) ? "." . $matches[2] : "");
		return floatval($r);
	}

}
