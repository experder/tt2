<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\service;

class ServiceAccounting {

	public static function format_euro($cents){
		$vorzeichen=$cents>0?"+":($cents<0?"-":"");
		$euro=abs($cents);
		$trenner=strlen($euro)-2;
		if($trenner<0){
			$euro="0,0".$euro;
		}else if($trenner<1){
			$euro="0,".$euro;
		}else{
			$euro=substr($euro, 0, $trenner).",".substr($euro, $trenner);
		}
		$tausend=strlen($euro)-6;
		if($tausend>0){
			$euro=substr($euro, 0, $tausend).".".substr($euro, $tausend);
		}
		return $vorzeichen.$euro;
	}

}