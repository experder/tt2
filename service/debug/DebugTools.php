<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\service\debug;

class DebugTools {

	/**
	 * @deprecated [A__DE]
	 */
	public static function backtrace($cut_from_the_start = 0) {
		//...
	}

	/**
	 * Examples:
	 * $backtrace_plain = implode("\n", DebugTools::backtrace2());
	 * $backtrace_html = implode("<br>", DebugTools::backtrace2());
	 * @param int  $highlight Number of steps after the error occured.
	 * @param bool $isCli
	 * @return string[]
	 */
	public static function backtrace2($highlight = 0, $isCli=false) {
		$caller = array();
		$backtrace = debug_backtrace();
		if (!$backtrace || !is_array($backtrace)) {
			return array("unknown caller");
		}
		foreach ($backtrace as $row) {
			if (!isset($row["file"]) || !isset($row["line"])) continue;
			$caller[] = $row["file"] . ':' . $row["line"]
				. " (" . $row["function"] . ")";
		}
		if ($highlight && isset($caller[$highlight])) {
			if($isCli){
				$caller[$highlight] = "===>" . $caller[$highlight];
			}else{
				$caller[$highlight] = "<b>" . $caller[$highlight] . "</b>";
			}
		}
		if (!$caller) {
			return array("unknown_caller");
		}

		return $caller;
	}

	/**
	 * @param string $dump
	 * @return string|false
	 */
	public static function getCompiledQueryFromDebugDump($dump) {
		$compiled_query = false;

		/** https://github.com/experder/TToolbox/blob/master/help/dev/regex.md */
		// /.../s PCRE_DOTALL ("...a dot metacharacter in the pattern matches all characters, including newlines.")
		$pattern = "/^SQL: \\[[0-9]*?\\] (.*?)\nParams:  0$/s";

		if (preg_match($pattern, $dump, $matches)) {
			$compiled_query = $matches[1];
		} else {
			preg_match("/\\nSent SQL: \\[([0-9]*?)\\] /", $dump, $matches);
			if (isset($matches[1])) {
				$count = $matches[1];
				preg_match("/\\nSent SQL: \\[$count\\] (.{{$count}})\nParams:/s", $dump, $matches);
				if (isset($matches[1])) {
					$compiled_query = $matches[1];
				}
			}
		}
		return $compiled_query;
	}

}