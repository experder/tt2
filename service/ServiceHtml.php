<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\service;

use tt\alias\CFG;
use tt\components\Html;

class ServiceHtml {

	/**
	 * Creates key-value pairs as used by HTML tags.
	 * @param array $params
	 * @return string
	 */
	public static function tag_keyValues($params) {
		if (!is_array($params)) {
			return "";
		}
		$html = "";
		foreach ($params as $key => $value) {
			if(is_null($value))$value="";
			$html .= " $key=\"" . htmlentities($value) . "\"";
		}
		return $html;
	}

	public static function A($content, $href, $class = null, $params = array()) {
		$params["href"] = $href;
		if ($class) {
			$params["class"] = $class;
		}
		return new Html("a", $content, $params);
	}

	/**
	 * @deprecated [AB__E]
	 */
	public static function DIV($content, $class = null, $params = array(), $children = null) {
		if ($class) {
			$params["class"] = $class;
		}
		return new Html("div", $content, $params, $children);
	}

	public static function H1($content, $id = null) {
		return new Html("h1", $content, array("id" => $id));
	}

	public static function H2($content, $id = null) {
		return new Html("h2", $content, array("id" => $id));
	}

	public static function H3($content, $id = null) {
		return new Html("h3", $content, array("id" => $id));
	}

	public static function H4($content, $id = null) {
		return new Html("h4", $content, array("id" => $id));
	}

	public static function BUTTON($value, $js = null, $params = array()) {
		$params["type"] = "button";
		if ($js) {
			$params["onclick"] = $js;
		}
		return new Html("button", $value, $params);
	}

	public static function PRE($content, $classes = array(), $params = array()) {
		$params["class"] = implode(" ", $classes);
		return new Html("pre", $content, $params);
	}

	/**
	 * @deprecated [AB__E]
	 */
	public static function PRE_console($content, $id = null, $outer_id = null) {
		$params = array("class" => "console_inner");
		if ($id) {
			$params["id"] = $id;
		}
		$outer_params = array();
		if ($outer_id) {
			$outer_params["id"] = $outer_id;
		}
		return self::PRE(new Html("div", $content, $params), array("console"), $outer_params);
	}

	/**
	 * @deprecated [AB__E]
	 */
	public static function TEXTAREA_console($content, $id = null) {
		$params = array("class" => "console");
		if ($id) {
			$params["id"] = $id;
		}
		return new Html("textarea", $content, $params);
	}

	public static function UL($children = array(), $params = null) {
		return self::list_builder("ul", $children, $params);
	}

	public static function list_builder($elem, $children, $params) {
		$html = new Html($elem, "", $params, null, false);
		foreach ($children as $child) {
			if (!($child instanceof Html) || strtolower($child->get_tag()) != 'li') {
				$child = new Html("li", null, null, $child);
			}
			$html->addChild($child);
		}
		return $html;
	}

	/**
	 * @deprecated [AB__E]
	 */
	public static function A_button($content, $href, $classes = array(), $params = array()) {
		$html = self::A($content, $href, "abutton", $params);
		$html->addClasses($classes);
		return $html;
	}

	/**
	 * @deprecated [AB__E]
	 */
	public static function P($content, $children = null, $params = array()) {
		return new Html("p", $content, $params, $children);
	}

	public static function A_external($content, $href, $params = array()) {
		$params['href'] = $href;
		$params['target'] = '_blank';
		return new Html("a", $content, $params);
	}

	public static function htmlScript($src, $srcId = null) {
		return "<script"
			. (CFG::DEVMODE() && $srcId ? " srcId='" . ServiceStrings::escape_value_html($srcId) . "'" : "")
			. " src=\"$src\"></script>";
	}

}
