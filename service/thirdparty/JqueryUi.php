<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\service\thirdparty;

use tt\install\ThirdpartyInstaller;
use tt\service\ServiceFiles;

class JqueryUi extends ThirdpartyInstaller {

	protected $id = "jqueryui113";

	/**
	 * https://jqueryui.com/
	 * Latest stable (01.2022) v1.13.0 (jQuery 1.8+)
	 * Compatibility with recent jQuery versions (up to 3.6): Usage of deprecated jQuery APIs have been removed. jQuery
	 * UI 1.13 triggers no jQuery Migrate warnings when running its test suite against jQuery 3.6.0 with jQuery Migrate
	 * 3.3.2, i.e. the latest versions at the moment of its release.
	 */
	protected $resourceUrl = "https://jqueryui.com/resources/download/jquery-ui-1.13.0.zip";
	private $extract_dir = "jquery-ui-1.13.0";
	protected $checksum = "755849a55e702bf915a8f3c26cd383e1";

	protected $includeJs = "jquery-ui.min.js";
	protected $includeCss = "jquery-ui.min.css";

	protected function hookAfterDownload(){
		rename($this->getTargetDir().'/'.$this->extract_dir.'/'.$this->includeJs,$this->getTargetDir().'/'.$this->includeJs);
		rename($this->getTargetDir().'/'.$this->extract_dir.'/'.$this->includeCss,$this->getTargetDir().'/'.$this->includeCss);
		ServiceFiles::unlinkDirectoryRecursively($this->getTargetDir().'/'.$this->extract_dir);
	}

}