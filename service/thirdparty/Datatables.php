<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\service\thirdparty;

use tt\install\ThirdpartyInstaller;

class Datatables extends ThirdpartyInstaller {

	protected $id = "datatables_1_11_3";

	/**
	 * https://datatables.net/
	 * https://datatables.net/download
	 * With extension FixedHeader (3.2.1) included.
	 */
	protected $resourceUrl = "https://cdn.datatables.net/v/dt/dt-1.11.3/fh-3.2.1/datatables.min.js";
	protected $checksum = "e1645082cea20f46e0b2459a58e6d589";

}