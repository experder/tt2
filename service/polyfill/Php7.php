<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\service\polyfill;

/**
 * Features that have been introduced in PHP 7
 */
class Php7 {

	/**
	 * Example: $session_id = bin2hex(\tt\service\polyfill\Php7::random_bytes(10));
	 *
	 * Syntax since PHP 7: $session_id = bin2hex(random_bytes(10));
	 *
	 * @see http://php.net/manual/en/function.random-bytes.php (Since 7.0)
	 *
	 * @param int $length
	 * @return string
	 */
	public static function random_bytes($length) {
		$string = "";
		for ($i = 0; $i < $length; $i++) {
			$string .= chr(rand(0, 255));
		}

		//Syntax since PHP 7:
//		try {
//			$string = random_bytes($length);
//		} catch (\Exception $e) {
//			\tt\core\ErrorDev::fromException($e);
//		}

		return $string;
	}

	/**
	 * Example: $objects = \tt\service\polyfill\Php7::call_function_by_variable($this->model_class, "getObjects", 1,
	 * $this->where_string);
	 *
	 * Syntax since PHP 7: $result = $variable::$function($arg1);
	 *
	 * @see https://wiki.php.net/rfc/uniform_variable_syntax (Since PHP 7)
	 *
	 * @param string     $variable
	 * @param string     $function
	 * @param int        $number_of_args
	 * @param null|mixed $arg1
	 * @param null|mixed $arg2
	 * @param null|mixed $arg3
	 * @param null|mixed $arg4
	 * @param null|mixed $arg5
	 * @param null|mixed $arg6
	 * @param null|mixed $arg7
	 * @param null|mixed $arg8
	 * @return mixed
	 */
	public static function call_function_by_variable($variable, $function, /** @noinspection PhpUnusedParameterInspection */$number_of_args=0, $arg1=null, $arg2=null, $arg3=null, $arg4=null, $arg5=null, $arg6=null, $arg7=null, $arg8=null) {

		$result = call_user_func("$variable::$function", $arg1, $arg2, $arg3, $arg4, $arg5, $arg6, $arg7, $arg8);

		//Syntax since PHP 7:
		#$result = $variable::$function($arg1, $arg2, $arg3, $arg4, $arg5, $arg6, $arg7, $arg8);

		return $result;
	}

}