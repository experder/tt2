<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\core;

use Exception;
use tt\service\debug\DebugTools;
use tt\service\ServiceEnv;

class ErrorDev {

	public static $recursion_protection = true;

	public function __construct($message, $highlightBacktrace = 0) {

		if (!self::$recursion_protection) {
			self::errorInErrorHandling($message);
		}
		self::$recursion_protection = false;

		$isCli = ServiceEnv::isSapiCLI();

		$backtrace = DebugTools::backtrace2($highlightBacktrace + 1, $isCli);

		if ($isCli) {

			echo "\n" . str_repeat('=', 40) . "\n$message\n" . str_repeat('-', 40) . "\n" . implode("\n", $backtrace) . "\n" . str_repeat('=', 40) . "\n";
			exit;

		}

		echo "<pre>$message<hr>" . implode("\n", $backtrace) . "</pre>";

		exit;
	}

	private static function errorInErrorHandling($message) {
		echo "\n<hr>ERROR IN ERROR HANDLING! " . $message;
		exit;
	}

	public static function fromException(Exception $e, $highlightBacktrace = 0) {
		return new ErrorDev($e->getMessage(), $highlightBacktrace + 1);
	}

}
