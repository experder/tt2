<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\core\view;

use tt\coremodule\dbmodell\core_routes;

/**
 * @deprecated
 */
abstract class View {

	public function __construct(){}
	protected $title = null;

	/**
	 * @param InputData2 $inputData
	 * @return HtmlPage
	 */
	abstract public function runWeb(InputData2 $inputData);

	/**
	 * return \tt\service\polyfill\Php5::get_class();
	 * @return string
	 */
	abstract public function getClass_();

	/**
	 * @return string VARCHAR(200) NOT NULL, UNIQUE
	 * @deprecated TODO non-abstract
	 */
	abstract public function getViewId();

	/**
	 * @return string
	 */
	public function getViewTitle(){
		if($this->title===null){
			$this->title = $this->getViewId();
		}
		return $this->title;
	}

	/**
	 * @deprecated [_____]
	 */
	public function get_navi_entry_sql($title, $parent = null, $visible = true, $orderby = null) {
		$view = $this->get_navi_entry($title, $parent, $visible, $orderby);
		return $view->getSqlInsert();
	}

	/**
	 * @param string|null $parent parent route_id in navigation / VARCHAR(200)
	 * @param boolean     $visible
	 * @param int|null    $orderby global int for navigation order
	 * @return string SQL query
	 */
	public function get_navi_entry_sql2($parent = null, $visible = true, $orderby = null) {
		$view = $this->get_navi_entry($this->getViewTitle(), $parent, $visible, $orderby);
		return $view->getSqlInsert();
	}

	/**
	 * @param string      $title
	 * @param string|null $parent parent route_id in navigation / VARCHAR(200)
	 * @param boolean     $visible
	 * @param int|null    $orderby global int for navigation order
	 * @return core_routes
	 */
	public function get_navi_entry($title, $parent = null, $visible = true, $orderby = null) {
		$view = new core_routes(array(
			"route_id" => $this->getViewId(),
			"title" => $title,
			"parent" => $parent,
			"visible" => $visible ? '1' : '0',
			"target" => $this->getClass_(),
			"orderby" => $orderby,
		));
		return $view;
	}

}
