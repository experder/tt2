<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\core\view;

use tt\alias\S;
use tt\components\Component;
use tt\core\Config;
use tt\core\page\Message;
use tt\core\page\Node;
use tt\core\page\Page;
use tt\core\Response;
use tt\service\ServiceHtml;
use tt\service\ServiceStrings;
use tt\service\thirdparty\Jquery3;

class HtmlPage extends Response {

	/**
	 * @var string VARCHAR(200), UNIQUE
	 */
	private $route_id = null;

	/**
	 * @var string|array|Component $body
	 */
	private $body;

	/**
	 * @var bool|string|null $focus TRUE,FALSE or selector. Example: "#name"
	 */
	private $focus = null;

	private $stylesheets = array();

	private $javascripts;
	private $javascript_inline = "";

	/**
	 * @var Message[] $messages
	 */
	private $messages2 = array();

	private static $next_global_id = 1;

	/**
	 * @param string|array|Component $body
	 * @param int                    $status Response::STATUS_
	 */
	public function __construct($body, $status = Response::STATUS_OK) {
		parent::__construct($status);
		$this->body = $body;
		$this->javascripts = Jquery3::getSingleton()->getJsLink();
		$this->javascripts['coreJs'] = Config::get(Config::HTTP_TTROOT).'/service/js/core.js';
	}

	public static function getNextGlobalId($prefix = "id") {
		return $prefix . (self::$next_global_id++);
	}

	/**
	 * @param string $type Message::TYPE_
	 * @param string $message
	 */
	public function addMessage2($type, $message) {
		$this->messages2[] = new Message($type, $message);
	}

	public function setRouteId($routeId) {
		$this->route_id = $routeId;
	}

	public function deliver() {

//		echo $this->getHtml(1);
//		exit;

		Page::init($this->route_id, null);

		$response = $this->body;

		Page::getInstance()->add($response);

		Page::getInstance()->setFocus($this->focus, true);

		foreach ($this->stylesheets as $stylesheet){
			Page::getInstance()->addCss($stylesheet);
		}

		foreach ($this->javascripts as $key=>$url){
			Page::getInstance()->addJs($url,$key);
		}

		foreach ($this->messages2 as $message){
			Page::getInstance()->addMessage($message);
		}

		if($this->javascript_inline)
		Page::getInstance()->addHeadAdditionalHtml("\n<script>$this->javascript_inline</script>");

		Page::getInstance()->deliver();
		exit;
	}

	/**
	 * @param string $js
	 */
	public function addJavascriptInline($js) {
		$this->javascript_inline .= $js;
	}

	public function getHtml($cutBacktrace = 0) {

		//Parsing the body adds JS and CSS to the page.
		$body = $this->getHtmlBody(1, $cutBacktrace +1);

		$html = "<!DOCTYPE html>"
			. "\n<html>"
			. "\n" . $this->getHtmlHead(1)
			. "\n" . $body
			. "\n</html>";
		return $html;
	}

	private function getHtmlHead($levelIndent) {
		return S::TAB($levelIndent)."<head>"
			."\n".$this->getJsHtml($levelIndent+1)
			.S::LN($levelIndent)."</head>";
	}

	private function getJsHtml($levelIndent) {
		$html = array();
		foreach ($this->getJsScripts() as $key=>$script) {
			if (Config::get(Config::CSS_NOCACHE, false)) $script .= "?" . time();
			$html[] = ServiceHtml::htmlScript($script, $key);
		}
		return S::TAB($levelIndent).implode(S::LN($levelIndent), $html);
	}

	public function getJsScripts() {
		return $this->javascripts;
	}

	private function getHtmlBody($levelIndent, $cutBacktrace = 0) {
		$body = S::TAB($levelIndent)."<body"
			.$this->getFocus()
			." onunload='if(typeof tt_tools!==\"undefined\")tt_tools.bodyOnUnload();'"
			." onload='if(typeof tt_tools!==\"undefined\")tt_tools.bodyOnLoad();'"
			.">"
			. "\n" . $this->getComponentHtml($this->body, $levelIndent+1, $cutBacktrace +1)
			. S::LN($levelIndent) . "</body>";
		return $body;
	}

	private function getComponentHtml($component, $levelIndent, $cutBacktrace = 0) {
		if (is_array($component)){
			$html = array();
			foreach ($component as $comp){
				$html[] = $this->getComponentHtml($comp, $levelIndent+1, $cutBacktrace +1);
			}
			return implode("\n",$html);
		}

		Node::check_type($component, $cutBacktrace+1);

		if($component instanceof Component){
			if($component->getJsLink()){
				foreach ($component->getJsLink() as $key=>$url){
					$this->addJsLink($url, $key);
				}
			}
			if($component->getCssLink())$this->addCss($component->getCssLink());
			if($component->getStartJs())$this->addJavascriptInline("$(function(){".$component->getStartJs()."});");
		}

		return S::TAB($levelIndent).$component;
	}

	private function getFocus() {
		$focus = $this->focus;
		if ($focus === null || $focus === true) {
			$focus = ":input:enabled:visible:first";
		}
		return $focus===false?"":(" focusSelector='".ServiceStrings::escape_value_html($focus)."'");
	}

	/**
	 * @param bool|string $focus TRUE,FALSE or selector. Example: "#name"
	 * @param bool        $override If focus is already set this command is ignored unless $override is set to TRUE.
	 */
	public function setFocus($focus, $override = false) {
		if (!$override && $this->focus !== null) return;
		$this->focus = $focus;
	}

	public function addCss($cssUrlFromHttpRoot, $key = null) {
		if(!is_array($cssUrlFromHttpRoot)){
			$cssUrlFromHttpRoot = array($key => $cssUrlFromHttpRoot);
		}
		$ok = true;
		foreach ($cssUrlFromHttpRoot as $key => $url){
			$ok = $ok && $this->addCssAbsolute(Config::getChecked(Config::HTTP_ROOT) . $url, $key);
		}
		return $ok;
	}

	public function addCssAbsolute($cssUrl, $key = null) {
		$ok = true;
		if ($key === null) {
			$this->stylesheets[] = $cssUrl;
		} else {
			if (isset($this->stylesheets[$key])) $ok = false;
			$this->stylesheets[$key] = $cssUrl;
		}
		return $ok;
	}

	public function addJsLink($jsUrl, $key = null) {
		$ok = true;
		if ($key === null) {
			$this->javascripts[] = $jsUrl;
		} else {
			if (isset($this->javascripts[$key])) $ok = false;
			$this->javascripts[$key] = $jsUrl;
		}
		return $ok;
	}

}