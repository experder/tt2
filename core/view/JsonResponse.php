<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\core\view;

use tt\alias\CFG;
use tt\core\Response;
use tt\service\debug\Stats;

class JsonResponse extends Response {

	/**
	 * @var array
	 */
	private $data;

	/**
	 * @param array $data Understood by TT API:
	 * <pre>
	 * "ok" (bool) - deprecated!
	 * "error_msg" (string)
	 * "html" (string)
	 * "msg_type" (Message::TYPE_)
	 * "tt_stats" (trasmitted in dev-mode)
	 * </pre>
	 * @param int   $status Response::STATUS_
	 */
	public function __construct($data, $status = Response::STATUS_OK) {
		parent::__construct($status);
		$this->data = $data;
	}

	public function deliver() {

		if (!isset($this->data['ok'])) {
			if ($this->status === Response::STATUS_OK) {
				$this->data['ok'] = true;
			} else {
				$this->data['ok'] = false;
				if (!isset($this->data['error_msg'])) {
					$this->data['error_msg'] = $this->getErrorMessage();
				}
			}
		}

		if (CFG::DEVMODE() && !isset($this->data["tt_stats"])) {
			$this->data["tt_stats"] = Stats::getAllStatsJson();
		}

		echo json_encode($this->data);
		exit;
	}

}