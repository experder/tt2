<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\core\view;

/**
 * Processes and provides data from:
 * - GET-request
 * - POST-data
 * - Input-streams
 * - CLI-args
 * Possible extensions (to do):
 * - Cookies
 * - Not JSON-formatted input-streams
 */
class InputData2 {

	private $accumulatedData = array();

	public function __construct($parse_all_streams = true) {
		if($parse_all_streams){
			$this->parseStream_get();
			$this->parseStream_post();
			$this->parseStream_input_json();
			$this->parseStream_cliArgs();
		}
	}

	/**
	 * IN (GET):
	 *     my.php?foo=bar&empty=&multiple[]=one&multiple[]=two&multiple[3]=foo&multiple[three]=bar&multiple["3"]=foobar
	 *     ( "multiple[]" is urlencoded: "multiple%5B%5D" )
	 * OUT: Array(
	 *     [foo] => bar
	 *     [empty] =>
	 *     [multiple] => Array(
	 *             [0] => one
	 *             [1] => two
	 *             [3] => foo
	 *             [three] => bar
	 *             ["3"] => foobar
	 *         )
	 * )
	 */
	public function parseStream_get() {
		foreach ($_GET as $key => $value) {
			$this->accumulatedData[$key] = $value;
		}
	}

	/**
	 * IN (POST):
	 *     foo=bar&empty=&multiple[]=one&multiple[]=two
	 *     ( "multiple[]" is urlencoded: "multiple%5B%5D" )
	 * OUT: Array(
	 *     [foo] => bar
	 *     [empty] =>
	 *     [multiple] => Array(
	 *             [0] => one
	 *             [1] => two
	 *         )
	 * )
	 */
	public function parseStream_post() {
		foreach ($_POST as $key => $value) {
			$this->accumulatedData[$key] = $value;
		}
	}

	/**
	 * IN: POST,Content-Type:application/json
	 *
	 * IN: $payload = '"string"';
	 * OUT: Array( [0] => string )
	 *
	 * IN: $payload = '["string"]';
	 * OUT: Array( [0] => string )
	 *
	 * IN: $payload = '{"0":"string"}';
	 * OUT: Array( [0] => string )
	 *
	 * IN: $payload = '{"foo":"string"}';
	 * OUT: Array( ["foo"] => string )
	 *
	 * IN: $payload = '1.50';
	 * OUT: Array( [0] => 1.5 )
	 *
	 * @return bool TRUE for success,
	 * FALSE if input-stream is empty or "null" or invalid JSON
	 */
	public function parseStream_input_json() {

		//POST uses input-stream as well. Input-stream can't be valid POST AND JSON:
		if($_POST)return false;

		$inputStream = file_get_contents("php://input");
		$inputJsonDecoded = json_decode($inputStream, true);
		if($inputJsonDecoded===null)return false;
		if(!is_array($inputJsonDecoded))$inputJsonDecoded=array($inputJsonDecoded);
		foreach ($inputJsonDecoded as $key => $value) {
			$this->accumulatedData[$key] = $value;
		}
		return true;
	}

	/**
	 * IN (CLI):
	 *     php my.php a "b c" ""
	 * OUT: Array(
	 *     [0] => my.php
	 *     [1] => a
	 *     [2] => b c
	 *     [3] =>
	 * )
	 * @return bool TRUE for success or FALSE if $argv ($_SERVER['argv']) is not available
	 */
	public function parseStream_cliArgs() {
		if(!isset($_SERVER)||!is_array($_SERVER)||!isset($_SERVER['argv'])||!is_array($_SERVER['argv']))return false;
		foreach ($_SERVER['argv'] as $key => $value) {
			$this->accumulatedData[$key] = $value;
		}
		return true;
	}

	/**
	 * @return array ONE associative array
	 */
	public function getData() {
		return $this->accumulatedData;
	}

	/**
	 * @deprecated [A__DE]
	 */
	public function getPostData() {
		return $_POST;
	}

}