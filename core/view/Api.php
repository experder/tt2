<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\core\view;

use tt\core\ErrorDev;
use tt\coremodule\dbmodell\core_routes;

abstract class Api {

	/**
	 * @param InputData2 $inputData
	 * @return JsonResponse
	 */
	public abstract function run(InputData2 $inputData);

	/**
	 * @return string VARCHAR(200) NOT NULL, UNIQUE
	 */
	abstract public function getApiId();

	abstract public function getClass_();

	/**
	 * @return string SQL query
	 */
	public function get_navi_entry_sql() {
		$view = $this->get_navi_entry();
		return $view->getSqlInsert();
	}

	/**
	 * @return core_routes
	 */
	public function get_navi_entry() {
		$view = new core_routes(array(
			"route_id" => $this->getApiId(),
			"visible" => 0,
			"target" => $this->getClass_(),
		));
		return $view;
	}

	protected function requiredFieldsFromData($data, $fieldlist, $return_associative = true) {
		$fields = array();
		foreach ($fieldlist as $key) {
			if (!isset($data[$key])) {
				new ErrorDev(get_class($this) . ": Required data not received: '$key'", 1);
			}
			if ($return_associative) {
				$fields[$key] = $data[$key];
			} else {
				$fields[] = $data[$key];
			}
		}
		return $fields;
	}

}