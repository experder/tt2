<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\core;

use tt\alias\CFG;
use tt\service\debug\DebugTools;
use tt\service\ServiceEnv;

class ErrorUser {

	public static $recursion_protection = true;

	public function __construct($message, $highlightBacktrace = 0) {

		if (!self::$recursion_protection) {
			self::errorInErrorHandling($message);
		}
		self::$recursion_protection = false;

		$isCli = ServiceEnv::isSapiCLI();

		$backtrace = DebugTools::backtrace2($highlightBacktrace + 1, $isCli);

		$message="Error! ".$message;

		if ($isCli) {

			echo "\n" . str_repeat('=', 40) . "\n$message\n";
			if(CFG::DEVMODE()){
				echo str_repeat('-', 40) . "\n" . implode("\n", $backtrace) . "\n";
			}
			echo str_repeat('=', 40) . "\n";
			exit;

		}

		echo "<pre>$message</pre>";

		if(CFG::DEVMODE()){
			echo "<hr><pre>" . implode("\n", $backtrace) . "</pre>";
		}

		exit;
	}

	private static function errorInErrorHandling($message) {
		echo "\nERROR IN ERROR HANDLING! " . $message;
		exit;
	}

}
