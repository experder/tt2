<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\core;

class ModelDatafield {

	const TYPE_DATETIME     = 1;
	const TYPE_DATE         = 2;
	const TYPE_VARCHAR      = 3;
	const TYPE_TEXT         = 4;
	const TYPE_INT          = 5;
	const TYPE_FLOAT        = 6;

	private $type;
	private $length;
	private $nullable;
	private $signed;

	/**
	 * @param int $type ModelDatafield::TYPE_
	 * @param int $length
	 * @param bool $nullable
	 * @param bool $signed
	 */
	public function __construct($type, $length=255, $nullable=false, $signed=true) {
		$this->type = $type;
		$this->length = $length;
		$this->nullable = $nullable;
		$this->signed = $signed;
	}

	/**
	 * @return int
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * @return int
	 */
	public function getLength(){
		return $this->length;
	}

	/**
	 * @return bool
	 */
	public function isNullable(){
		return $this->nullable;
	}

	/**
	 * @return bool
	 */
	public function isSigned(){
		return $this->signed;
	}

}