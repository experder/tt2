<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\core;

class Response {

	const STATUS_OK = 200;
	const STATUS_BAD_REQUEST = 400;
	const STATUS_INTERNAL_SERVER_ERROR = 500;

	protected $status;

	protected static $STATUS_STRINGS = array(
		self::STATUS_OK => "OK",
		self::STATUS_BAD_REQUEST => "Bad Request",
		self::STATUS_INTERNAL_SERVER_ERROR => "Internal Server Error",
	);

	/**
	 * @param int $status Response::STATUS_
	 */
	public function __construct($status = Response::STATUS_OK) {
		$this->status = $status;
	}

	/**
	 * @param string $unknown
	 * @return string
	 */
	public function getErrorMessage($unknown = "Unknown State/Error") {
		if (isset(Response::$STATUS_STRINGS[$this->status])) {
			return $this->status . ' ' . Response::$STATUS_STRINGS[$this->status];
		}
		return $this->status . ' ' . $unknown;
	}

}