<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\core;

use tt\alias\DB;
use tt\coremodule\dbmodell\core_config;
use tt\core\database\Database;
use tt\core\page\Page;
use tt\install\Installer;
use tt\service\ServiceEnv;
use ttcfg\ConfigProject;

Config::$startTimestamp = microtime(true);

class Config {

	public static $startTimestamp;

	private static $config_cache = array();

	private static $settings = array();

	const DBCFG_DB_VERSION = "DB_VERSION";

	const DEFAULT_VALUE_NOT_FOUND = "!TTDEFVALNOTFOUND!";

	const PLATFORM_UNKNOWN = 0;
	const PLATFORM_WINDOWS = 1;
	const PLATFORM_LINUX = 2;

	//==============================================

	const PROJECT_TITLE = 'PROJECT_TITLE';
	const NAMESPACE_PROJECT_ROOT = 'NAMESPACE_PROJECT_ROOT';

	const DIR_PROJECT_ROOT = 'DIR_PROJECT_ROOT';
	const DIR_CFG = 'DIR_CFG';
	const DIR_TT = 'DIR_TT';
	const DIR_3RDPARTY = 'DIR_3RDPARTY';

	const CFG_SERVER_INIT_FILE = 'CFG_SERVER_INIT_FILE';

	//----------------------------------------------

	const DEVMODE = 'DEVMODE';
	const DB_CORE_PREFIX = 'DB_CORE_PREFIX';

	const HTTP_ROOT = 'HTTP_ROOT';

	const CFG_PLATFORM = 'CFG_PLATFORM';
	const CSS_NOCACHE = 'CSS_NOCACHE';

	//----------------------------------------------

	const HTTP_TTROOT = 'HTTP_TTROOT';
	const HTTP_SKIN = 'HTTP_SKIN';
	const HTTP_3RDPARTY = 'HTTP_3RDPARTY';

	const SKIN_NAVI_HEIGHT = "SKIN_NAVI_HEIGHT";

	const RUN_ALIAS_API = 'RUN_ALIAS_API';
	const RUN_ALIAS = 'RUN_ALIAS';

	//==============================================

	public static function set($cfgId, $value) {
		self::$settings[$cfgId] = $value;
	}

	public static function get($cfgId, $else = null) {

		if (isset(self::$settings[$cfgId])) {
			return self::$settings[$cfgId];
		}

		return $else;
	}

	public static function getChecked($cfgId, $depth = 0) {

		if (!isset(self::$settings[$cfgId])) {
			new ErrorDev("Config value not set: '$cfgId'", $depth + 1);
			return false;
		}

		return self::$settings[$cfgId];
	}

	public static function getValue($id, $module, $user = null, $default_value = null) {
		$value = self::recallVal($module, $id, $user);
		if ($value !== false) {
			return $value;
		}

		$database = Database::getPrimary();

		$data = $database->_query("SELECT " . core_config::ROW_content . " FROM " . core_config::getSingleton()->getTableName() . " WHERE " . core_config::ROW_idstring . "=:ID AND " . core_config::ROW_module . "=:MOD AND " . core_config::ROW_userid . "<=>:USR LIMIT 1;", array(
			":ID" => $id,
			":USR" => $user,
			":MOD" => $module,
		), Database::RETURN_ASSOC);

		if (!$data || !is_array($data) || count($data) < 1) {
			return $default_value;
		}

		if (count($data) > 1) {
			new ErrorDev(
				"Config database corrupt! Multiple entries found for \"$id\" (module '$module'" . ($user ? ", user #$user" : "") . ")."
			);
		}

		$value = $data[0][core_config::ROW_content];

		self::storeVal($value, $module, $id, $user);

		return $value;
	}

	private static function recallVal($module, $key, $user = null) {
		$user_index = 'u' . ($user ?: 0);
		if (!isset(self::$config_cache[$module][$user_index][$key])) {
			return false;
		}
		return self::$config_cache[$module][$user_index][$key];
	}

	private static function storeVal($value, $module, $key, $user = null) {
		$user_index = 'u' . ($user ?: 0);
		self::$config_cache[$module][$user_index][$key] = $value;
	}

	public static function setValue($value, $key, $module, $user = null) {

		DB::updateOrInsert(core_config::getSingleton()->getTableName(), array(
			core_config::ROW_idstring => $key,
			core_config::ROW_module => $module,
			core_config::ROW_userid => $user,
		), array(
			core_config::ROW_content => $value,
		), core_config::ROW_id);

		self::storeVal($value, $module, $key, $user);
	}

	public static function init_cli() {

		//Autoloader
		require_once dirname(__DIR__) . '/core/Autoloader.php';
		Autoloader::init();

		//Modules
		Modules::init();

		//Server specific configuration, including database settings
		require_once Config::get(Config::CFG_SERVER_INIT_FILE);

	}

	public static function init_api() {

		self::initCore();

		//API calls return JSON
		ServiceEnv::$response_is_expected_to_be_json = true;

		//Modules
		Modules::init();

		//Server specific configuration, including database settings
		require_once Config::get(Config::CFG_SERVER_INIT_FILE);

		//Session, Authentication
		User::initSession();

	}

	public static function initCli() {

		self::initCore();

		if(!ServiceEnv::isSapiCLI()){
			new ErrorUser("This can't be viewed via browser!", 1);
		}

	}

	public static function initCore() {

		//Project-specific configuration, part 1
		require_once dirname(__DIR__) . '/config_pointer.php';
		ConfigProject::ConfigPt1();

		//Autoloader
		require_once dirname(__DIR__) . '/core/Autoloader.php';
		Autoloader::init();

		//Server specific configuration (Config Pt.2), including database settings
		Installer::requireConfigServer();

		//Project-specific configuration (Config Pt.3)
		ConfigProject::ConfigPt3();

	}

	/**
	 * @param string $pid unique page id
	 * @return Page
	 */
	public static function init_web($pid) {

		self::initCore();

		//Modules
		Modules::init();

		//Server specific configuration, including database settings
		Installer::requireConfigServer();

		//Session, Authentication
		$token = User::initSession();

		//Breadcrumbs, HTML
		$page = Page::init($pid, $token);

		return $page;
	}

}