<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\core\database;

use Exception;
use PDO;
use PDOException;
use PDOStatement;
use tt\alias\CFG;
use tt\core\ErrorDev;
use tt\install\Installer;
use tt\service\debug\DebugQuery;
use tt\service\debug\DebugTools;
use tt\service\debug\Stats;
use tt\service\ServiceArrays;
use tt\service\ServiceStrings;

class Database {

	/**
	 * Returns id value of the INSERTed set of data.
	 */
	const RETURN_LASTINSERTID = 1;
	/**
	 * Returns result of the SELECT query as an associative array.
	 */
	const RETURN_ASSOC = 2;
	/**
	 * Returns the number of rows affected by the last query (UPDATE or DELETE).
	 */
	const RETURN_ROWCOUNT = 3;

	/**
	 * @var PDO $pdo
	 */
	private $pdo;

	/**
	 * @var string $dbname
	 */
	private $dbname;
	private $host;

	/** @var Database $primary */
	static private $primary = null;

	public static function isPrimarySet() {
		return self::$primary !== null;
	}

	public static function getPrimary() {
		if (self::$primary === null) {
			new ErrorDev("ERROR_DB_NOT_INITIALIZED");
		}
		return self::$primary;
	}

	/**
	 * @param string $host
	 * @param string $dbname
	 * @param string $user
	 * @param string $password
	 */
	public function __construct($host, $dbname, $user, $password) {
		$this->host = $host;
		$this->dbname = $dbname;
		$this->initPdo($user, $password);
	}

	private function initPdo($user, $password) {
		try {
			$this->pdo = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->dbname, $user, $password);
			$this->pdo->query('SET NAMES utf8;');
		} catch (Exception $e) {
			if ($e instanceof PDOException) {
				if ($e->getCode() === 2002/*php_network_getaddresses: getaddrinfo failed*/) {
					new ErrorDev("Host unknown! $this->host");
				} else if ($e->getCode() === 1045/*Access denied*/) {
					new ErrorDev("Access denied! $user@$this->host");
				} else if ($e->getCode() === 1044/*Access denied for user to database*/) {
					new ErrorDev("Access denied to database '$this->dbname'!");
				} else if ($e->getCode() === 1049/*Unknown database*/) {
					Installer::initDatabaseGui($this->dbname, $this->host, $user, $password);
				} else if ($e->getMessage() === "could not find driver") {
					new ErrorDev("Please install MySQL!\nextension=pdo_mysql\nextension=\"C:/Programs/PHP/ext/php_pdo_mysql.dll\"");
				}
			}
			ErrorDev::fromException($e);
		}
	}

	public static function init($host, $dbname, $user, $password) {

		if (self::$primary !== null) {
			new ErrorDev("Database is already initialized!");
		}

		self::$primary = new Database($host, $dbname, $user, $password);

		return self::$primary;
	}

	/**
	 * @param string $query
	 * @param array  $substitutions
	 * @return int
	 */
	public function insert($query, $substitutions = null) {
		return $this->_query($query, $substitutions, self::RETURN_LASTINSERTID);
	}

	public function select($query, $substitutions = null) {
		return $this->_query($query, $substitutions, self::RETURN_ASSOC);
	}

	public function update($query, $substitutions = null) {
		return $this->_query($query, $substitutions, self::RETURN_ROWCOUNT);
	}

	public function delete($query, $substitutions = null) {
		return $this->_query($query, $substitutions, self::RETURN_ROWCOUNT);
	}

	public function selectColumnAsArray($columnName, $query, $substitutions = null) {
		$data = self::select($query, $substitutions);
		$columnArray = array();
		foreach ($data as $row){
			$columnArray[] = $row[$columnName];
		}
		return $columnArray;
	}

	/**
	 * @deprecated [AB__E]
	 */
	public function select_single($query, $substitutions = null) {
		return $this->selectSingle($query, $substitutions);
	}
	public function selectSingle($query, $substitutions = null) {
		$data = $this->select($query, $substitutions);
		if (!$data) return false;
		return $data[0];
	}

	public function quote($string) {
		if ($string === null) return "NULL";
		if (is_numeric($string)) return $string;
		if (is_bool($string)) return $string ? "TRUE" : "FALSE";
		return $this->getPdo()->quote($string);
	}

	public function insertAssoc($table, $data_set) {

		$keys_array = array_keys($data_set);
		$keys_prefixed = ServiceArrays::prefixValues(':', $keys_array);

		$substitutions = array();
		foreach ($data_set as $key => $value) {
			$substitutions[':' . $key] = $value;
		}

		$keys = implode(",", $keys_array);
		$values = implode(",", $keys_prefixed);
		return $this->insert("INSERT INTO $table ($keys) VALUES ($values);", $substitutions);
	}

	/**
	 * @param string $query
	 * @param array  $substitutions
	 * @param int    $return_type Database::RETURN_...
	 * @param int    $highlightBacktrace
	 * @return string|array|int|null
	 */
	public function _query($query, $substitutions = null, $return_type = 0, $highlightBacktrace = 0) {
		$statement = $this->pdo->prepare($query);
		$ok = @$statement->execute($substitutions);
		$compiledQuery = $this->debuginfo($statement, $query);
		if (!$ok) {
			$this->error_handling($statement, $highlightBacktrace + 1, $compiledQuery);
			return null;
		}
		switch ($return_type) {
			case self::RETURN_LASTINSERTID:
				return $this->pdo->lastInsertId();
			case self::RETURN_ASSOC:
				return $statement->fetchAll(PDO::FETCH_ASSOC);
			case self::RETURN_ROWCOUNT:
				return $statement->rowCount();
			default:
				return null;/*No return type specified*/
		}
	}

	private function debuginfo(PDOStatement $statement, $query) {
		if (!CFG::DEVMODE()) {
			return null;
		}
		if (ob_get_status()) {
			ob_flush();
		}
		ob_start();
		$statement->debugDumpParams();
		$debugDump = ob_get_clean();
		$compiled_query = DebugTools::getCompiledQueryFromDebugDump($debugDump);
		if (!$compiled_query) {
			$compiled_query = ($debugDump ?: $query);
		}

		Stats::getSingleton()->addQuery(new DebugQuery($compiled_query
			#."\n\t".implode("\n\t",DebugTools::backtrace2())
		));
		return $compiled_query;
	}

	private function error_handling(PDOStatement $statement, $highlightBacktrace, $compiledQuery) {
		$eInfo = $statement->errorInfo();
		$errorCode = $eInfo[0];
		$errorInfo = "[$errorCode] " . $eInfo[2];

		if ($errorCode == "HY093") {
			new ErrorDev("Invalid parameter number: parameter was not defined", $highlightBacktrace + 1);
		}

		new ErrorDev($errorInfo . "\n-----------\n" . $compiledQuery, $highlightBacktrace + 1);
	}

	public function getPdo() {
		return $this->pdo;
	}

	/**
	 * @return string
	 */
	public function getDbname() {
		return $this->dbname;
	}

	public function updateOrInsert($table, $where_assoc, $update_assoc=null, $idKey = 'id', $allowMultiple = false) {

		$where_array = array();
		$subs = array();
		$idCounter = 1;
		foreach ($where_assoc as $key=>$value){
			$id = ":ID".($idCounter++);
			$where_array[] = "`$key`<=>".$id;
			$subs[$id] = $value;
		}
		$where_sql = "WHERE ".implode(" AND ",$where_array);

		//Will we have to UPDATE or INSERT?
		$query = "SELECT `$idKey` FROM $table " .$where_sql;
		$data_exists = $this->select($query,$subs);

		if ($data_exists) {
			//UPDATE
			$ids = array();
			foreach ($data_exists as $row){
				$ids[] = $row[$idKey];
			}
			if (count($data_exists) > 1 && !$allowMultiple){
				new ErrorDev("Database is corrupt! Duplicate entry! FROM $table WHERE ".print_r($where_assoc,1)."[".implode(",",$ids)."]");
			}
			if($update_assoc){
				$this->updateAssocIds($table, $update_assoc, $ids, $idKey);
			}
			return (count($data_exists)==1?$ids[0]:$ids);
		} else {
			//INSERT
			$update_and_where = array_merge($where_assoc, $update_assoc?:array());
			return $this->insertAssoc($table, $update_and_where);
		}

	}


	public function updateAssocIds($table, $data_set, $ids, $idKey='id') {
		if(!is_array($ids))$ids=array($ids);
		$where_sql = "WHERE `$idKey` IN (".implode(",",$ids).")";
		return self::updateAssocWhere($table, $data_set, $where_sql);
	}
	public function updateAssocWhere($table, $data_set, $where_sql) {
		if (!$where_sql || !ServiceStrings::startsWith("WHERE ", $where_sql, false))
			new ErrorDev("\$where_sql should start with 'WHERE'!", 1);

		$set_assoc = array();
		$subs = array();
		$idCounter = 1;
		foreach ($data_set as $key=>$value){
			$id = ":ID".($idCounter++);
			$set_assoc[] = "`$key`=".$id;
			$subs[$id] = $value;
		}
		$set_sql = implode(",",$set_assoc);

		#echo("UPDATE $table $set_sql $where_sql\n".print_r($subs,1));exit;
		return $this->update("UPDATE $table SET $set_sql $where_sql", $subs);
	}

}