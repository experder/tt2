<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\core;

class Classloader {

	/**
	 * @param string            $classnameOriginal
	 * @param array|string|null $args
	 * @return mixed
	 */
	public static function loadClass($classnameOriginal, $args = null) {

		$classname = $classnameOriginal;

		$class = basename(str_replace("\\", "/", $classname));

		if (file_exists(Config::get(Config::DIR_CFG) . "/custom/$class.php")) {
			$classname = "ttcfg\\custom\\$class";
		}

		if (!class_exists($classname)) {
			new ErrorDev("Could not load $classnameOriginal" . ($classnameOriginal == $classname ? "" : " / $classname") . "!");
		}

		if ($args == null) {
			$instance = new $classname;
		} else {
			if (!is_array($args)) {
				$args = array($args);
			}
			try {
				$r = new \ReflectionClass($classname);
			} catch (\ReflectionException $e) {
				new ErrorDev("Could not reflect $classnameOriginal" . ($classnameOriginal == $classname ? "" : " / $classname") . "!");
				exit;
			}
			$instance = $r->newInstanceArgs($args);
		}

		if (!($instance instanceof $classnameOriginal)) {
			new ErrorDev("$classname should extend $classnameOriginal!");
		}

		return $instance;
	}

	/**
	 * @return \tt\components\PageWrapper
	 */
	public static function loadClassPageWrapper() {
		return Classloader::loadClass('tt\components\PageWrapper');
	}

	/**
	 * @return \tt\core\page\Page
	 */
	public static function loadClassPage() {
		return Classloader::loadClass('tt\core\page\Page');
	}

	/**
	 * @return \tt\core\navigation\Navigation
	 */
	public static function loadClassNavigation() {
		return Classloader::loadClass('tt\core\navigation\Navigation');
	}

}