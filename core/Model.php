<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\core;

use tt\alias\DB;
use tt\components\tables\TableEditModel;
use tt\service\ServiceStrings;

/**
 * @deprecated
 */
abstract class Model {

	protected $id = null;

	private static $singletons = array();

	public static function getClassModel() {
		/** @noinspection PhpFullyQualifiedNameUsageInspection */
		return \tt\service\polyfill\Php5::get_class();
	}

	/**
	 * @param array $data
	 */
	public function __construct($data = null) {
		if (!is_array($data)) return;
		$this->setData($data);
	}

	/**
	 * @param string $whereString
	 * @return $this
	 */
	public function fromDatabaseByWhere($whereString) {
		if (!ServiceStrings::startsWith("WHERE ", $whereString, false)) new ErrorDev("\$whereString should start with 'WHERE'!");

		$sql = self::getSingleton()->getSqlSelect();

		$data = DB::select_single("$sql $whereString LIMIT 1;");

		if (!$data) return $this;

		$this->setData($data);

		return $this;
	}

	/**
	 * @param string $whereString
	 * @return $this|null
	 * @noinspection PhpUnused
	 */
	public static function fromDatabaseByWhere_($whereString) {
		$classname = get_called_class();
		$class = new $classname();
		if (!($class instanceof Model)) return null;
		return $class->fromDatabaseByWhere($whereString);
	}

	/**
	 * @param string $where
	 * @param string $orderBy
	 * @param string $limit
	 * @return array|null
	 * @see TableEditModel::setData_()
	 */
	public static function multipleFromDatabaseByWhere($where = null, $orderBy = null, $limit = null) {

		if ($where!==null && !ServiceStrings::startsWith("WHERE ", $where, false))
			new ErrorDev("\$where should start with 'WHERE'!", 1);

		if ($orderBy!==null && !ServiceStrings::startsWith("ORDER BY ", $orderBy, false))
			new ErrorDev("\$orderBy should start with 'ORDER BY'!", 1);

		if ($limit!==null && !ServiceStrings::startsWith("LIMIT ", $limit, false))
			new ErrorDev("\$limit should start with 'LIMIT'!", 1);

		$sql = self::getSingleton()->getSqlSelect();

		$data = DB::select("$sql $where $orderBy $limit;");

		if (!$data) return array();

		$models = array();

		$classname = get_called_class();

		foreach ($data as $row){
			$models[] = new $classname($row);
		}

		return $models;
	}

	/**
	 * @return Model
	 */
	public static function getSingleton(){

		$classname = get_called_class();

		if(!isset(self::$singletons[$classname])){
			self::$singletons[$classname] = new $classname();
		}

		return self::$singletons[$classname];
	}

	public function getSqlSelect() {
		$class = self::getSingleton();
		if (!($class instanceof Model)) return null;//Hint for the IDE

		$table = $class->getTableName();

		return "SELECT * FROM $table ";
	}

	public function getObjectVars($unset_id = true) {
		$fields = get_object_vars($this);
		if ($unset_id) unset($fields['id']);
		return $fields;
	}

	/**
	 * @deprecated [A__DE]
	 */
	public function sql_insert() {
		return $this->getSqlInsert();
	}
	public function getSqlInsert() {
		$fields = $this->getObjectVars();
		$keys = array();
		$values = array();
		foreach ($fields as $key => $value) {
			$keys[] = "`$key`";
			$values[] = DB::quote($value);
		}
		return "INSERT INTO " . $this->getTableName() . " ("
			. implode(",", $keys)
			. ") VALUES ("
			. implode(",", $values)
			. ");";
	}

	public function fromDatabaseById($id = null) {
		if ($id === null) {
			if ($this->id === null) new ErrorDev("ID not set!");
			$id = $this->id;
		}

		return $this->fromDatabaseByWhere("WHERE id=" . DB::quote($id));
	}

	public static function fromDatabaseById_($id = null) {
		$classname = get_called_class();
		$class = new $classname();
		if (!($class instanceof Model)) return null;
		return $class->fromDatabaseById($id);
	}

	/**
	 * @return string
	 */
	public abstract function getTableName();

	public function setData($dataArray, $check = true) {
		if (!is_array($dataArray)) return;

		$allFields = get_object_vars($this);

		foreach ($dataArray as $key => $value) {
			if ($check && !array_key_exists($key, $allFields)) {
				new ErrorDev("Skipped key '$key' when setting data for: " . get_class($this));
			} else {
				$this->$key = $value;
			}
		}

	}

	public function persistInsert($nullValues = true) {
		$table = $this->getTableName();
		$data = $this->getDataArray($nullValues);
		unset($data["id"]);
		return DB::insertAssoc($table, $data);
	}

	/**
	 * @param bool $nullValues
	 * @return array
	 */
	public function getDataArray($nullValues = true) {
		$data = get_object_vars($this);
		if (!$nullValues) {
			foreach ($data as $key => $value) {
				if (!isset($this->$key)) {
					unset($data[$key]);
				}
			}
		}
		return $data;
	}

	/**
	 * @return int|null
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param int $format_type
	 * @return array
	 */
	public function getDataFormatted(/** @noinspection PhpUnusedParameterInspection */
		$format_type) {
		return $this->getDataArray();
	}

}