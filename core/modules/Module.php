<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\core\modules;

abstract class Module {

	/**
	 * @var UpdateDatabase
	 */
	private $updateDatabase = null;

	/**
	 * @return string /[a-z_0-9]/ maxlen:40 chars
	 * @see \tt\core\Modules
	 */
	abstract public function getModuleId();

	abstract public function doUpdateDatabase();

	public function __construct() {
	}

	/**
	 * @return UpdateDatabase
	 */
	public function getUpdateDatabase() {
		if ($this->updateDatabase === null) {
			$this->updateDatabase = new UpdateDatabase($this);
		}
		return $this->updateDatabase;
	}

	protected function q($ver, $query, $highlight_backtrace = 0) {
		$this->getUpdateDatabase()->q($ver, $query, $highlight_backtrace + 1);
	}

}