<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\core\modules;

use tt\core\Config;
use tt\core\database\Database;

class UpdateDatabase {

	private $ver_old = null;
	private $ver = null;

	/**
	 * @var Module
	 */
	private $module;

	public function __construct(Module $module) {
		$this->module = $module;
	}

	public function q($ver, $query, $highlight_backtrace = 0) {
		if ($this->ver + 1 == $ver) {
			Database::getPrimary()->_query($query, null, 0, $highlight_backtrace + 1);
			$ver_new = $this->ver + 1;
			$this->setVersion($ver_new);
			$this->ver = $this->getVersion();
		}
	}

	public function startUpdate() {
		$this->ver_old = $this->getVersion();
		$this->ver = $this->ver_old;

		$this->module->doUpdateDatabase();

		if ($this->ver_old == $this->ver) {
			return "<i>Module '" . $this->module->getModuleId() . "': Nothing new. Current version: " . $this->ver."</i>";
		}
		return "<b>Module '" . $this->module->getModuleId() . "' updated from version $this->ver_old to $this->ver.</b>";
	}

	private function setVersion($ver) {
		Config::setValue($ver, Config::DBCFG_DB_VERSION, $this->module->getModuleId());
	}

	private function getVersion() {
		$ver = Config::getValue(Config::DBCFG_DB_VERSION, $this->module->getModuleId(), null, 0);
		return $ver;
	}

}