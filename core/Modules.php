<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\core;

use tt\coremodule\CoreModule2;
use ttcfg\ConfigProject;

class Modules {

	const MODULE_ID_MAXLENGTH = 40;//chars

	/**
	 * @var Modules $instance
	 */
	private static $instance = null;

	/**
	 * @var \tt\core\modules\Module[] $modules
	 */
	private $modules = null;

	private function __construct() {
	}

	public static function getInstance() {
		if (self::$instance === null) {
			self::$instance = new Modules();
		}
		return self::$instance;
	}

	/**
	 * return \tt\core\modules\Module[]
	 */
	public static function getAllModules() {
		$modules = self::getInstance()->modules;
		if ($modules === null) {
			Modules::init();
			$modules = self::getInstance()->modules;
		}
		return $modules;
	}

	public static function updateAll() {
		$responses = array();
		foreach (\tt\core\Modules::getAllModules() as $module) {
			if($module instanceof \tt\core\modules\Module){
				$responses[] = $module->getUpdateDatabase()->startUpdate();
			}else{
				new ErrorDev("Type error!");
			}
		}
		return $responses;
	}

	public function register2(\tt\core\modules\Module $module) {
		$module_id = $module->getModuleId();

		if (preg_replace("/[^a-z_0-9]/", "", $module_id) !== $module_id
			|| strlen($module_id) > Modules::MODULE_ID_MAXLENGTH) {
			new ErrorDev("Invalid module ID: '$module_id'");
		}

		$this->modules[$module_id] = $module;
	}

	public function getModule($module_id) {
		if (!isset($this->modules[$module_id])) {
			new ErrorDev("Module not registered: '$module_id'");
		}
		$module = $this->modules[$module_id];
		return $module;
	}

	public static function init() {

		$m = Modules::getInstance();

		$m->modules = array();

		$m->register2(new CoreModule2());

		ConfigProject::registerModules($m);

	}

}