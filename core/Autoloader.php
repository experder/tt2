<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\core;

use tt\install\Installer;
use tt\service\ServiceEnv;
use tt\service\ServiceStrings;
use ttcfg\ConfigProject;

class Autoloader {

	private static $initialized = false;
	private static $abort_on_error = true;

	private static $all_namespace_roots = null;

	public static function init() {
		if (self::$initialized) return;

		$autloloader = new Autoloader();
		$autloloader->register();

		self::$initialized = true;
	}

	public static function multipleAutoloader() {
		self::$abort_on_error = false;
	}

	private function register() {
		require_once dirname(__DIR__) . '/service/ServiceStrings.php';
		#require_once dirname(__DIR__) . '/core/Config.php';
		spl_autoload_register(function ($class_name) {
			$class_name = ServiceStrings::classnameSafe($class_name);

			//Namespace: TT\API
			//@deprecated [A__DE]
			//Autoloader,Navigation,PageWrapper,RegisterModules,Session
			if (self::loadApiClass($class_name)) return true;

			foreach (self::getAllNamespaceRoots() as $namespace => $folder) {
				if (self::requireFileInNamespace($class_name, $namespace, $folder)) return true;
			}

			return self::notFound($class_name, 1);
		});

	}

	private static function loadApiClass($class_name) {
		if (!preg_match("/^tt\\\\api\\\\(.*)\$/", $class_name, $matches)) return false;
		if (Config::get(Config::CFG_API_DIR, false) === false) return false;
		require_once dirname(__DIR__) . '/service/ServiceEnv.php';

		$name_api = $matches[1];

		$file_api = Config::getChecked(Config::CFG_API_DIR) . '/' . $name_api . ".php";
		if (!file_exists($file_api)) {
			//Installer: Create API file stubs
			Installer::initApiClass($name_api, $file_api);
		}

		require_once $file_api;

		if (!ServiceEnv::reflectionInstanceof($class_name, "tt\\core\\api_default\\$name_api")) {
			require_once __DIR__ . '/ErrorDev.php';
			new ErrorDev(
				"TT API class '$class_name' ($file_api) does not extend '\\tt\\core\\api_default\\$name_api'!"
			);
		}

		return true;
	}

	private static function requireFileInNamespace($classname, $namespace_root, $folder) {
		$file = self::classnameMatchesNamespaceRoot($classname, $namespace_root, $folder);

		if (!$file) return false;

		if (!file_exists($file)) return false;

		require_once $file;

		return true;
	}

	public static function classnameMatchesAnyNamespaceRoot($classname) {
		foreach (self::getAllNamespaceRoots() as $namespace => $folder) {
			if (($file = self::classnameMatchesNamespaceRoot($classname, $namespace, $folder)) !== false) {
				return $file;
			}
		}
		return false;
	}

	public static function getAllNamespaceRoots($force_reload = false) {
		if (self::$all_namespace_roots === null || $force_reload) {

			self::$all_namespace_roots = array(
				"tt" => dirname(__DIR__),
			);

			if (($d = Config::get(Config::DIR_CFG, false)) !== false) {
				self::$all_namespace_roots["ttcfg"] = $d;
			}

			if (($r = Config::get(Config::NAMESPACE_PROJECT_ROOT, false)) !== false) {
				self::$all_namespace_roots[$r] = Config::get(Config::DIR_PROJECT_ROOT);
			}

			if(class_exists('\ttcfg\ConfigProject', false)){
				$additionalNamespaceRoots = ConfigProject::registerNamespaceRoots();
				if (is_array($additionalNamespaceRoots)) {
					foreach ($additionalNamespaceRoots as $namespace => $folder) {
						self::$all_namespace_roots[$namespace] = $folder;
					}
				}
			}

		}
		return self::$all_namespace_roots;
	}

	private static function classnameMatchesNamespaceRoot($classname, $namespace_root, $folder) {
		if (!preg_match("/^$namespace_root\\\\(.*)/", $classname, $matches)) return false;

		$name = $matches[1];

		$file = str_replace('\\', '/', $folder . '/' . $name . '.php');

		return $file;
	}

	private static function notFound($class, $cutBacktrace = 0) {
		if (!Autoloader::$abort_on_error) return false;
		require_once dirname(__DIR__) . '/service/Error.php';
		require_once dirname(__DIR__) . '/service/ServiceEnv.php';
		#new Error("Can't autoload \"$class\"!", $cutBacktrace + 1);
		new ErrorDev("Can't autoload \"$class\"!", $cutBacktrace + 1);
		return null;
	}

}