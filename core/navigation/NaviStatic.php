<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\core\navigation;

use tt\coremodule\CoreModule2;
use tt\coremodule\dbmodell\core_routes;

class NaviStatic {

	/**
	 * @return core_routes[]
	 */
	public function getStaticNavi2() {
		$navi = array();

		foreach (CoreModule2::getRoutes() as $route) {
			$navi[] = $route;
		}

		//DemoModule
		//1
		$navi[] = new core_routes(array(
			"route_id" => "core_demos",
			"title" => "Core demos",
		));
		//2
		$view = new \myproject\demo_module\views\CssDemo();
		$navi[] = $view->get_navi_entry("CSS demo", "core_demos");

		//ExampleModule
		//1
		$navi[] = new core_routes(array(
			"route_id" => "example_root",
			"title" => "Example module",
		));
		//2
		$view = new \myproject\new_module\views\ExampleView();
		$navi[] = $view->get_navi_entry("Example view", "example_root");

		return $navi;
	}
}