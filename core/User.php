<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\core;

use tt\core\auth\Token;
use tt\core\view\HtmlPage;
use tt\service\form\Form;
use tt\service\form\FormfieldPassword;
use tt\service\form\FormfieldText;
use tt\service\ServiceEnv;
use tt\service\ServiceHtml;

class User {

	private static $singleton = null;

	/**
	 * @return User
	 */
	public static function getSingleton(){
		if(self::$singleton===null){
			self::$singleton = new User();
		}
		return self::$singleton;
	}

	public static function initSession($api = false) {
		return new Token();//disabled for the moment
		if (!ServiceEnv::isSapiCLI() && !$api) {
			$page = new HtmlPage(User::getSingleton()->getLoginHtml());
			$page->deliver();
		}
		return new Token();
	}

	/**
	 * @return string HTML
	 */
	protected function getLoginHtml() {
		$html = "";

		$form = new Form();
		$form->addField(new FormfieldText("name", "User"));
		$form->addField(new FormfieldPassword("pass", "Password"));

		$html .= ServiceHtml::H1("Login");
		$html .= $form->toHtml();

		return $html;
	}

}