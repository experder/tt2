
Apache
======

Linux
-----

```
sudo apt-get install apache2
```


Windows
-------

[ [Hints](install_windows.md) ]

https://www.php.net/manual/en/install.windows.apache2.php

### Install as a Service
```
httpd.exe -k install -n "Apache HTTP Server"
```

### `httpd.conf`

	LoadModule rewrite_module
	
	#LoadModule php7_module "C:/Programme/php-7.4.22-Win32-vc15-x64/php7apache2_4.dll"
	LoadModule php_module "C:/Programme/php-8.0.8-Win32-vs16-x64/php8apache2_4.dll"
	
	<IfModule dir_module>
	    DirectoryIndex index.html index.php
	</IfModule>
	
	Alias /ttdemo "C:/Workspace/ttDemo"
	<Directory "C:/Workspace/ttDemo">
	    Require all granted
	    AllowOverride All
	</Directory>
	
	<FilesMatch \.php$>
	    SetHandler application/x-httpd-php
	</FilesMatch>
	
	# configure the path to php.ini
	#PHPIniDir "C:/Programme/php-7.4.22-nts-Win32-vc15-x64"
	PHPIniDir "C:/Programme/php-8.0.8-Win32-vs16-x64"

