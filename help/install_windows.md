
Windows
=======

When installing LAMP on Windows (=WAMP), keep following in mind:
* Download PHP 7 **Thread Safe** (contains `php7apache2_4.dll`)
* Paths in `httpd.conf` and `php.ini` contain **slashes** instead of backslashes (e.g. `C:/Workspace/ttDemo`)
