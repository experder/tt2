
Troubleshooting
===============

### Warning: fopen(): Unable to find the wrapper "https" - did you forget to enable it when you configured PHP? Could not open URL

#### `php.ini`:

Since PHP 7.4:

	extension=openssl
Older versions:

	extension=php_openssl
