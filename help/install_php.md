
PHP
===

Linux
-----
```
sudo apt update
sudo apt upgrade
sudo apt install ca-certificates apt-transport-https software-properties-common
sudo add-apt-repository ppa:ondrej/php
sudo apt install php8.0 libapache2-mod-php8.0
sudo systemctl restart apache2
```

Windows
-------

[ [Hints](install_windows.md) ]

https://windows.php.net/download#php-7.4  
https://windows.php.net/downloads/releases/php-7.4.22-Win32-vc15-x64.zip

`php.ini`
---------

	extension=mbstring
	extension=openssl
	extension=pdo_mysql
    extension=pdo_pgsql

If necessary, use absolute paths!
