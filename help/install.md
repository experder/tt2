
Installation
============

Repository
----------

#### Create new project:  
`git init`  
`git add .`  
`git commit -m "Initial commit."`

#### Clone submodule into project:  
`git submodule add https://gitlab.com/experder/tt2.git TToolbox`  
`git commit -m "Add TT as submodule 'TToolbox'."`

#### When cloning an existing project with submodule(s):
`git clone --recurse-submodules https://github.com/experder/ttDemo.git .`

Requirements
------------

TT is currently downward compatible to PHP 5.3.

[ [Windows](install_windows.md) ]
[ [Apache](install_apache.md) ]
[ [MySQL](install_mysql.md) ]
[ [PHP](install_php.md) ]

And for downloading the repo:

[ [Git](install_git.md) ]

Troubleshooting
---------------

[ [Troubleshooting](faq.md) ]
