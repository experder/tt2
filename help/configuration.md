Configuration
=============

![configuration_architecture.png](configuration_architecture.png)

Custom class override (`custom`)
--------------------------------

Your skin (`css`)
-----------------

Project specific configuration (`ConfigProject.php`)
-----------------------------------------------------

Server specific configuration (`ConfigServer.php`)
---------------------------------------------------

