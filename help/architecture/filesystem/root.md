Filesystem architecture of a TToolbox project
=============================================
![root.png](root.png)

Repository "Your project"
-------------------------
Example: [ttDemo](https://github.com/experder/ttDemo)

Folder "thirdparty"
-------------------
Main article: [Third-party-packages and -libraries](../../thirdparty/thirdparty.md#filesystem-architecture)

Folder "TTconfig"
-----------------
Main article: [Configuration](../../configuration.md)

Repository "TToolbox"
---------------------
Main article: [Filesystem architecture of TToolbox](ttoolbox.md)

tt\\core\\Model
--------------
Main article: [Model.php](tt/core/model.md)

tt\\core\\view\\View
-------------
Main article: [View.php](tt/core/view/view.md)
