`tt\core`
========
![ttoolbox_core.png](core/ttoolbox_core.png)

`tt\core\admin`
---------------
Example:
* `Admin.php`

`tt\core\auth`
---------------
Example:
* `Token.php`

`tt\core\database`
---------------
Example:
* `Database.php`

`tt\core\dev`
---------------
Examples:
* `DebugQuery.php`
* `DebugTools.php`
* `Stats.php`
* `StatsElement.php`

`tt\core\mail`
---------------
E-mail classes.

`tt\core\modules`
---------------
Examples:
* [`Module.php`](core/module.md)
* `Modules.php`

`tt\core\navigation`
---------------
See [`navigation`](core/navigation.md)


`Autoloader.php`
---------------

`Classloader.php`
---------------

`Config.php`
---------------

`Error.php`
---------------

`Model.php`
---------------
See [`Model.php`](core/model.md)

`view`
---------------
* [`View.php`](core/view/view.md)
* [`Api.php`](https://github.com/experder/TToolbox/blob/main/core/view/Api.php)
* `ResponseJson.php`
