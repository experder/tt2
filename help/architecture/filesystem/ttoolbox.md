Filesystem architecture of TToolbox
===================================
![ttoolbox.png](ttoolbox.png)

`alias`
------

`components`
------

`tt\core`
------
See [`tt\core`](tt/core.md)

`help`
------

`installer`
------

`coremodule`
------
### `model`
### `CoreModule.php`

`run`
------

`services`
------
![ttoolbox_services.png](ttoolbox_services.png)

### `js`
Examples:
* `core.js`
* `Js.php`
### `polyfill`
Examples:
* `Php5.php`
* `Php7.php`
### `thirdparty`
Examples:
* `Jquery.php`
* `LoadJs.php`

### `ServiceArrays.php`
### `ServiceDateTime.php`
### `ServiceEnv.php`
### `ServiceFiles.php`
### `ServiceHtml.php`
### `ServiceStrings.php`
### `ServiceTemplates.php`
### `ServiceUrls.php`

`.gitignore`
------

`config_pointer.php`
------

`README.md`
------
Documentation: [TToolbox](../../../README.md)

