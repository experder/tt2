Architecture of your modules
============================


Module-specific model
---------------------
Database- and object-structure used by this module only.

![](model.png)

See also:
[[Global model](ttoolbox/model.md)]
[[Project-specific model](my_project.mds#project-specific-model)]

