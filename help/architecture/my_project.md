Architecture of your project
============================

Project-specific model
----------------------
Database- and object-structure used by multiple of your modules.

![](model.png)

See also:
[[Global model](ttoolbox/model.md)]
[[Module-specific model](my_module.md#module-specific-model)]

Specific services
-----------------
...
