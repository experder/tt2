[[Architecture](../my_project.md)] -> [[Architecture of TToolbox](architecture.md)]

Global model
============

Database- and object-structure used by your modules and core functions.

![](../model.png)

See also:
[[Project-specific model](../my_project.mds#project-specific-model)]
[[Module-specific model](../my_module.md#module-specific-model)]


