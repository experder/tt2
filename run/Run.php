<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\run;

use tt\core\Autoloader;
use tt\alias\CFG;
use tt\core\Config;
use tt\core\ErrorDev;
use tt\core\navigation\Navigation;
use tt\core\view\Api;
use tt\core\view\HtmlPage;
use tt\core\view\InputData2;
use tt\core\view\JsonResponse;
use tt\core\view\View;
use tt\service\debug\Stats;
use tt\service\ServiceStrings;

class Run {

	public static function getWebUrl($routeId, $getParams = array()) {
		$runAlias = Config::getChecked(Config::RUN_ALIAS);
		$routeUrl = $runAlias . $routeId;
		if($getParams){
			$runUsesGet = strpos($runAlias, '?')!==false;
			$routeUrl.=ServiceStrings::buildGetString($getParams, ($runUsesGet?'&':'?'));
		}
		return $routeUrl;
	}

	public static function getWebLink2($routeId, $getParams = null, $linkTitle = null) {
		if ($linkTitle === null) $linkTitle = $routeId;
		return "<a href='" . self::getWebUrl($routeId, $getParams) . "'>" . $linkTitle . "</a>";
	}

	public static function doRun() {
		$input = new InputData2();
		$accumulatedInputData = $input->getData();

		$classname = false;

		//@deprecated! [A__DE]
		if(isset($accumulatedInputData['c']))$classname=$accumulatedInputData['c'];

		//@deprecated! [A__DE]
		if(isset($accumulatedInputData['class']))$classname=$accumulatedInputData['class'];

		if(isset($accumulatedInputData['tt_route']))$classname=$accumulatedInputData['tt_route'];

		if ($classname===false) {
			new ErrorDev("TT/run: No route specified!");
		}

		$class = self::loadRunner($classname);

		if ($class instanceof View) {
			Config::init_web(null);
			$response = $class->runWeb($input);
			if (!($response instanceof HtmlPage)) {
				new ErrorDev(get_class($class) . ": runWeb is supposed to return HtmlPage!");
			}
			$response->setRouteId($class->getViewId());
			$response->deliver();
			exit;
		}

		if ($class instanceof Api) {
			Config::init_api();
			$response = $class->run($input);
			if (!($response instanceof JsonResponse)) {
				new ErrorDev(get_class($class) . ": runApi(): Invalid return value!");
			}
			$response->deliver();
			exit;
		}

		new ErrorDev("Should not happen.75");

	}

	private static function checkRunner($controllerClass) {

		if (!ServiceStrings::classnameSafeCheck($controllerClass)) {
			new ErrorDev("No qualified controller classname given!");
		}

		$file = Autoloader::classnameMatchesAnyNamespaceRoot($controllerClass);

		if ($file === false) {
			new ErrorDev("Controller not found: '$controllerClass'");
		}

		return true;
	}

	/**
	 * @param string $controllerClass
	 * @param bool   $isAlias
	 * @return View|Api
	 */
	private static function loadRunner($controllerClass, $isAlias = true) {
		if ($isAlias) {
			$navi = Navigation::getInstance();
			$naviEntry = $navi->getEntryById($controllerClass);
			if ($naviEntry !== false) {
				$controllerClass = $naviEntry->getTarget();
			} else {
				new ErrorDev("Route not found: '$controllerClass'");
				$controllerClass = str_replace('/', '\\', $controllerClass);
			}
		}

		if (CFG::DEVMODE()) {
			Stats::$apiClass = $controllerClass;
		}

		self::checkRunner($controllerClass);

		$class = new $controllerClass();

		if (!($class instanceof View) && !($class instanceof Api)) {
			new ErrorDev("Controller class '$controllerClass' does not extend 'tt\\core\\view\\View' or 'tt\\core\\view\\Api'!");
		}

		return $class;
	}

}