<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\run;

use tt\core\Config;
use tt\install\Installer;

require_once dirname(__DIR__) . '/install/Installer.php';
Installer::requireConfigPointer();

Config::initCore();

require_once __DIR__ . '/Run.php';
Run::doRun();
