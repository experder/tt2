<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\coremodule\dbmodell;

use tt\alias\CFG;
use tt\alias\DB;
use tt\core\Config;
use tt\core\ErrorDev;
use tt\core\Model;
use tt\core\navigation\Navigation;
use tt\run\Run;
use tt\service\ServiceStrings;

/**
 * https://github.com/experder/TToolbox/blob/main/help/architecture/filesystem/tt/core/view/view.md
 */
class core_routes extends Model {

	private static $table_name = null;

	/**
	 * @var string $route_id VARCHAR(200) NOT NULL, UNIQUE
	 */
	protected $route_id;

	/**
	 * @var string|null $title VARCHAR(80) DEFAULT NULL, html-title / menu-entry
	 */
	protected $title;

	/**
	 * @var string|null $parent VARCHAR(200) DEFAULT NULL,
	 *                          FOREIGN KEY (`parent`) REFERENCES (`route_id`),
	 *                          parent route_id in navigation
	 */
	protected $parent;

	/**
	 * @var boolean $visible TINYINT DEFAULT 1 NOT NULL, (navigation)
	 */
	protected $visible = true;

	/**
	 * @var string|null $target VARCHAR(200) View class, Api class or URL
	 */
	protected $target;

	/**
	 * @var int|null $orderby INT NULL, global int for navigation order
	 */
	protected $orderby;


	/**
	 * @var core_routes $parentEntry
	 */
	private $parentEntry = null;
	/**
	 * @var core_routes[] $childEntries
	 */
	private $childEntries = array();

	private static $limit_depth = 99;

	/**
	 * @param string $where SQL WHERE-Statement beginning with "WHERE"
	 * @return core_routes[]
	 */
	public static function sql_select2($where = "") {
		$data = DB::select("SELECT "
			. " `route_id`,"
			. " `title`,"
			. " `parent`,"
			. " `visible`,"
			. " `target`,"
			. " `orderby`"
			. " FROM " . self::getSingleton()->getTableName() . " " . $where . " ORDER BY IFNULL(`orderby`,0);");
		return $data;
	}

	/**
	 * @return string
	 */
	public function getRouteId() {
		return $this->route_id;
	}

	/**
	 * @return string|null
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @deprecated [A__DE]
	 */
	public function getLink() {
		return $this->target;
	}
	public function getTarget() {
		return $this->target;
	}

	public static function sql_001_create() {
		return "CREATE TABLE " . self::getSingleton()->getTableName() . " (
				`id` INT(11) NOT NULL AUTO_INCREMENT,
				`route_id` VARCHAR(200) NOT NULL,
				`title` VARCHAR(80) DEFAULT NULL,
				`parent` VARCHAR(200) DEFAULT NULL,
				`visible` TINYINT DEFAULT 1 NOT NULL,
				`target` VARCHAR(200) DEFAULT NULL,
				`orderby` INT NULL,
				PRIMARY KEY (`id`),
				UNIQUE KEY `route_id` (`route_id`),
				KEY `parent` (`parent`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	}

	public static function sql_002_constraint1() {
		return "ALTER TABLE " . self::getSingleton()->getTableName() . " ADD CONSTRAINT `tt_core_routes_ibfk_1`
			FOREIGN KEY (`parent`) REFERENCES " . self::getSingleton()->getTableName() . " (`route_id`);";
	}

	/**
	 * @return core_routes|false
	 */
	public function getParentEntry() {
		if ($this->parentEntry === null) {
			$this->parentEntry = Navigation::getInstance()->getEntryById($this->parent);
		}
		return $this->parentEntry;
	}

	/**
	 * @param core_routes $child
	 */
	public function addChildEntry($child) {
		$this->childEntries[] = $child;
	}

	public function getBreadcrumbs() {
		if (self::$limit_depth-- < 1) {
			new ErrorDev("Maximum depth of navigation exeeded! Possible recursion. " . $this->route_id);
		}
		$parent = $this->getParentEntry();
		if ($parent !== false) {
			$breads = $parent->getBreadcrumbs();
		} else {
			$breads = array();
		}
		$breads[] = $this;
		return $breads;
	}

	/**
	 * @param bool $title
	 * @return string|null
	 */
	public function getHtmlInner($title) {
		if ($this->title === null || !$this->visible) return null;

		if ($this->target) {
			if (class_exists($this->target)) {
				$url = Run::getWebUrl($this->route_id);
			} else {
				$url = $this->target;
			}
		} else {
			$url = false;
		}

		$title_ = htmlentities($this->title);

		$titleTag = ($title ? $title_ : "");
		if (CFG::DEVMODE()) $titleTag .= " [id_" . ServiceStrings::cssClassSafe($this->route_id) . "]";

		$titleVal = $titleTag ? "title='$titleTag'" : "";

		if ($url !== false) {
			$targetBlank = "";//($this->type == self::TYPE_ext ? "target='_blank' " : "");
			$link = "<a {$targetBlank}href='" . htmlentities($url) . "' $titleVal>$title_</a>";
		} else {
			$link = "<span class='pseudo_a' $titleVal>$title_</span>";
		}

		return $link;
	}

	public function getHtml($highlighted_id) {
		if ($this->title === null && !$this->childEntries) return false;

		//Link:
		$inner = $this->getHtmlInner($this->parent === null);

		//Children:
		$html = array($inner);
		if ($this->childEntries) {
			$childsHtml = array();

			foreach ($this->childEntries as $entry) {
				$childHtml = $entry->getHtml($highlighted_id);
				if ($childHtml === false && $this->title === false) return false;
				$childsHtml[] = $childHtml;
			}

			$html[] = "<ul>" . implode("\n", $childsHtml) . "</ul>";
		}

		//Highlighting:
		$crumbs = Navigation::getInstance()->getBreadcrumbs($highlighted_id);
		$high = ($crumbs && in_array($this, $crumbs));
		$highclass = $high ? " high" : "";

		//Ouput:
		$cssId = "id_" . ServiceStrings::cssClassSafe($this->route_id);
		return "<li class='$cssId$highclass'>" . implode("\n", $html) . "</li>";
	}

	/**
	 * @return core_routes
	 */
	public function getTopEntry() {
		$parent = $this->getParentEntry();
		if ($parent === false) return $this;
		return $parent->getTopEntry();
	}

	/**
	 * @return core_routes[]
	 */
	public function getChildEntries() {
		return $this->childEntries;
	}

	/**
	 * @inheritdoc
	 */
	public function getTableName() {
		if (self::$table_name === null) {
			self::$table_name = Config::getChecked(Config::DB_CORE_PREFIX) . '_routes';
		}
		return self::$table_name;
	}
}