<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\coremodule\views;

use tt\core\Modules;
use tt\core\page\Message;
use tt\core\view\Api;
use tt\core\view\InputData2;
use tt\core\view\JsonResponse;

class AdminApi extends Api {

	const API_ID = "core/adminapi";

	public static function getClass() {
		return \tt\service\polyfill\Php5::get_class();
	}

	public function getClass_() {
		return self::getClass();
	}

	/**
	 * @inheritdoc
	 */
	public function run(InputData2 $inputData) {
		$responses = Modules::updateAll();
		$response = "Updated database.<pre class='module_updates'>" . print_r($responses, 1) . "</pre>";

		return new JsonResponse(array(
			"html" => $response,
			"msg_type" => Message::TYPE_CONFIRM,
		));
	}

	/**
	 * @inheritdoc
	 */
	public function getApiId() {
		return self::API_ID;
	}

}