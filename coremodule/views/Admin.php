<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\coremodule\views;

use tt\core\view\HtmlPage;
use tt\core\view\InputData2;
use tt\core\view\View;
use tt\service\form\Form;
use tt\service\js\Js;
use tt\service\ServiceStrings;

class Admin extends View {

	const VIEW_ID = "core/admin";

	const CMD_updateDb = "updateDb";

	public static function getClass() {
		return \tt\service\polyfill\Php5::get_class();
	}

	/**
	 * @inheritdoc
	 */
	public function runWeb(InputData2 $inputData) {
		$html = "";

		$form = new Form(null, "", "Update DB");
		$form->onSubmit .= (Js::ajaxPostToMessages(null, null, "{
			tt_route:'" . ServiceStrings::escape_value_js(AdminApi::API_ID) . "',
			cmd:'" . self::CMD_updateDb . "',
		}"))
			. "return false;";

		$html .= $form;

		return new HtmlPage($html);
	}

	public function getClass_() {
		return self::getClass();
	}

	public function getViewId() {
		return self::VIEW_ID;
	}

}