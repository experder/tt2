<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\coremodule;

use tt\core\database\Database;
use tt\core\modules\Module;
use tt\coremodule\dbmodell\core_config;
use tt\coremodule\dbmodell\core_routes;

class CoreModule2 extends Module {

	const moduleId = "core_module";

	/**
	 * @inheritdoc
	 */
	function getModuleId() {
		return self::moduleId;
	}

	/**
	 * @return core_routes[]
	 */
	public static function getRoutes(){
		$routes = array();

		$view = new \tt\coremodule\views\Admin();
		$routes["1"] = $view->get_navi_entry("TT Admin", null, true, -1000);

		$api = new \tt\coremodule\views\AdminApi();
		$routes["2"]=$api->get_navi_entry();

		$api = new \tt\install\InstallerApi();
		$routes["3"]=$api->get_navi_entry();

		return $routes;
	}

	public function doUpdateDatabase() {
		$routes = self::getRoutes();

		$this->q(1, $routes["1"]->getSqlInsert());
		$this->q(2, $routes["2"]->getSqlInsert());
		$this->q(3, $routes["3"]->getSqlInsert());

	}

	public static function init($host, $dbname, $user, $password) {

		$db = Database::init($host, $dbname, $user, $password);

		$db->_query(core_config::sql_001_create());

		$db->_query(core_routes::sql_001_create());
		$db->_query(core_routes::sql_002_constraint1());

		return true;
	}

}