<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\alias;

use tt\service\ServiceStrings;

class S {

	public static function LN($level_of_indention) {
		return ServiceStrings::returnAndIndent($level_of_indention);
	}
	public static function TAB($level_of_indention) {
		return str_repeat("\t", $level_of_indention);
	}

}