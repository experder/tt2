<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\components\tables;

use tt\components\Html;

class TableCell extends Html {

	public function __construct($data) {
		parent::__construct("td", $data);
	}

	public function toHtml(){
		return $this->__toString();
	}

	public function setSortValue($sortValue){
		$this->params["data-order"] = $sortValue;
	}

	public function setSearchValue($sortValue){
		$this->params["data-search"] = $sortValue;
	}

}