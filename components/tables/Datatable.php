<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\components\tables;

use tt\core\Config;
use tt\core\view\HtmlPage;
use tt\service\thirdparty\Datatables;

class Datatable extends Table {

	private $paging = false;

	private $fixedHeader = true;
	/**
	 * @var string JS expression. e.g.: "$('nav.menu').outerHeight()" or "50"
	 */
	private $fixedHeader_offset;

	public function __construct(array $data) {
		parent::__construct($data);
		$this->id = HtmlPage::getNextGlobalId();

		$this->js_link = Datatables::getSingleton()->getJsLink();

		$this->fixedHeader_offset = Config::get(Config::SKIN_NAVI_HEIGHT, "$('nav.menu').outerHeight()");
	}

	public function getStartJs(){
		if($this->js_start===null){
			$this->js_start="$('#".$this->id."').DataTable({"
				. $this->getJsPaging()
				. $this->getJsFixedHeader()
				. "});";
		}
		return $this->js_start;
	}

	private function getJsPaging(){
		return "\"paging\":".($this->paging?"true":"false").",";
	}
	private function getJsFixedHeader(){
		if(!$this->fixedHeader)return "";
		return "fixedHeader:{"
			. "headerOffset:" . $this->fixedHeader_offset . ","
			. "},";
	}

}