<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\components\tables;

use tt\alias\CFG;
use tt\components\Component;
use tt\service\ServiceStrings;

class Table extends Component {

	public static $empty_message_ = "No data";
	private $empty_message = true;

	protected $id = null;

	/**
	 * @var array[] $data
	 */
	private $data;

	/**
	 * @var array $head
	 */
	private $head = null;

	public function __construct(array $data) {
		parent::__construct();
		$this->data = $data;
	}

	public function getEmptyMessage() {
		if ($this->empty_message === true) {
			$this->empty_message = self::$empty_message_;
		}
		return $this->empty_message;
	}

	/**
	 * @param array $head
	 */
	public function setHead($head) {
		$this->head = $head;
	}

	private function setDefaultHead() {
		$this->head = array();
		if (!$this->data) return;
		$row1 = $this->data[0];
		foreach ($row1 as $key => $dummy) {
			$this->head[$key] = $key;
		}
	}

	public function __toString() {
		return $this->toHtml();
	}

	private function getHead() {
		if ($this->head === null) {
			$this->setDefaultHead();
		}
		return $this->head;
	}

	private function getHeadHtml($head) {
		$html_head = array();

		foreach ($head as $key=>$header){
			$html_head[] = "<th"
				.(CFG::DEVMODE()?" title='".ServiceStrings::escape_value_html($key)."'":"")
				.">$header</th>";
		}

		$html = "<thead><tr>" . implode("\n", $html_head) . "</tr></thead>";
		return $html;
	}

	public function toHtml() {
		if (!$this->data) return $this->getEmptyMessage();

		$id_val = $this->id?" id=\"".ServiceStrings::escape_value_html($this->id)."\" ":"";

		$head = $this->getHead();

		$html_head = $this->getHeadHtml($head);

		$html_body = "<tbody>" . $this->getBody($head) . "</tbody>";

		$html_table = "<table $id_val>$html_head\n$html_body</table>";

		return $html_table;
	}

	private function getBody($header) {

		$rows = array();

		foreach ($this->data as $row_data) {

			$row_html = array();

			foreach ($header as $key => $dummy) {

				$cell = isset($row_data[$key]) ? $row_data[$key] : "";

				if($cell instanceof TableCell){
					$cell = $cell->toHtml();
				}else{
					$cell = "<td>$cell</td>";
				}

				$row_html[] = $cell;

			}

			$rows[] = "<tr>" . implode("", $row_html) . "</tr>";

		}

		return implode("\n", $rows);
	}

	/**
	 * @param array[] $data
	 */
	public function setData($data) {
		$this->data = $data;
	}

}