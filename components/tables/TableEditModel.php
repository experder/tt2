<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\components\tables;

use tt\core\ErrorDev;
use tt\core\Model;

class TableEditModel extends Datatable {

	private $model_class;
	private $where;
	private $orderBy;
	private $limit;

	public $format_type = 0;

	public function __construct($model_class, $where = null, $orderBy = null, $limit = null) {
		parent::__construct(array());

		$this->model_class = $model_class;
		if (!is_subclass_of($this->model_class, \tt\core\Model::getClassModel()))
			new ErrorDev("\$model_class must be instanceof tt\\core\\Model");

		$this->where = $where;
		$this->orderBy = $orderBy;
		$this->limit = $limit;

	}

	private function setData_() {
		/** @see Model::multipleFromDatabaseByWhere() */
		$objects = call_user_func("$this->model_class::multipleFromDatabaseByWhere",
			$this->where,
			$this->orderBy,
			$this->limit
		);

		$data = array();
		foreach ($objects as $model){
			if(!($model instanceof Model))continue;
			$row = $model->getDataFormatted($this->format_type);
			$data[] = $row;
		}

		$this->setData($data);
	}

	public function getHtml() {
		return $this->toHtml();
	}

	public function toHtml() {
		$this->setData_();
		return parent::toHtml();
	}

}