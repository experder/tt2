<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\components;

use tt\install\ThirdpartyInstaller;

class ThirdpartyJs extends Component {

	public function __construct(ThirdpartyInstaller $thirdpartyObject) {
		parent::__construct();
		$this->js_link = $thirdpartyObject->getJsLink();
		$this->css_link = $thirdpartyObject->getCssLink();
	}

	public function __toString() {
		return "";
	}

}