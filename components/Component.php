<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\components;

abstract class Component {

	/**
	 * @var null|array $js_link
	 */
	protected $js_link = null;
	/**
	 * @var null|string $js_start
	 */
	protected $js_start = null;

	/**
	 * @var null|array $css_link
	 */
	protected $css_link = null;

	public function __construct() {
	}

	/**
	 * @return null|array
	 */
	public function getJsLink() {
		return $this->js_link;
	}

	/**
	 * @return null|string
	 */
	public function getStartJs(){
		return $this->js_start;
	}

	/**
	 * @return null|array
	 */
	public function getCssLink() {
		return $this->css_link;
	}

	public abstract function __toString();

}