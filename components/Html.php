<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\components;

use tt\core\ErrorDev;
use tt\service\ServiceHtml;

class Html {

	protected $tag;
	protected $content;
	private $void;
	protected $params;
	private static $dev_beautify = false;
	/**
	 * @var Html[] $children
	 */
	private $children = array();

	/**
	 * Html constructor.
	 * @param string     $tag e.g. DIV, P, A, BUTTON
	 * @param string     $content
	 * @param array|null $params Key-Value pairs of HTML-Attributes
	 * @param mixed      $children
	 * @param boolean    $void
	 */
	public function __construct($tag, $content, $params = null, $children = null, $void = false) {
		$this->tag = $tag;
		$this->content = $content;
		$this->void = $void;
		$this->addParams($params);
		if ($children !== null) {
			self::addChildren($children);
		}
	}

	public static function setDevBeautify() {
		self::$dev_beautify = true;
	}

	/**
	 * @param Html|string $child
	 */
	public function addChild($child) {
		$this->children[] = $child;
	}

	/**
	 * @param array $childs
	 */
	public function addChildren($childs) {
		if (!is_array($childs)) {
			/** @noinspection PhpParamsInspection */
			self::addChild($childs);
			return;
		}
		foreach ($childs as $child) {
			$this->addChild($child);
		}
	}

	public function addClasses($classes) {
		if (is_array($classes)) {
			foreach ($classes as $class) {
				$this->addClass($class);
			}
		}
	}

	public function addClass($class) {
		new ErrorDev("TODO!");//add css class: Check, if class already exists
		if ($class === null) {
			return;
		}
		if (isset($this->params["class"])) {
			$this->params["class"] .= ' ' . $class;
		} else {
			$this->params["class"] = $class;
		}
	}

	public function set_param($key, $value) {
		$key = strtolower($key);
		if ($value === null) {
			unset($this->params[$key]);
		}
		$this->params[$key] = $value;
	}

	public function set_id($value) {
		$this->set_param("id", $value);
	}

	/**
	 * @return string
	 */
	public function get_tag() {
		return $this->tag;
	}

	public function addParams($array) {
		if (!is_array($array)) {
			return;
		}
		foreach ($array as $key => $value) {
			$this->set_param($key, $value);
		}
	}

	public function __toString() {
		$params = ServiceHtml::tag_keyValues($this->params);
		$html = "<" . $this->tag . $params;
		if ($this->void) {
			$html .= " />"
				. $this->children_to_string();
		} else {
			$html .= ">"
				. $this->content . $this->children_to_string()
				. "</$this->tag>";
		}
		if (self::$dev_beautify) {
			$html .= "\n";
		}
		return $html;
	}

	private function children_to_string() {
		if (self::$dev_beautify && $this->children) {
			return "\n\t" . implode("\t", $this->children);
		}
		return implode("", $this->children);
	}

}
