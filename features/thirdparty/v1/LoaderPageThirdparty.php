<?php

namespace tt\features\thirdparty\v1;

use tt\features\htmlpage\view\v1\LoaderPage;
use tt\features\htmlpage\view\v1\ViewHtmlNew;
use tt\features\messages\v2\MsgConfirm;
use tt\services\ServiceEnv;

class LoaderPageThirdparty extends ViewHtmlNew
{
	public $prefixWithTitle = false;

	/**
	 * @var string $title
	 */
	private $title;
	/**
	 * @var string $msg
	 */
	private $msg;
	/**
	 * @var Thirdpartyasset $asset
	 */
	private $asset;

	/**
	 * @param Thirdpartyasset $asset
	 * @param string $title
	 * @param string $msg
	 */
	public function __construct(Thirdpartyasset $asset, $title=true, $msg=true)
	{
		if($msg===true){
			$msg = "Loading...";
		}
		if($title===true){
			$title="Loading...";
		}
		$this->asset = $asset;
		$this->title = $title;
		$this->msg = $msg;
	}

	/**
	 * @return string
	 */
	function getHtml() {
		$this->asset->doInstall();
		new MsgConfirm($this->asset->getAssetName()." installed successfully!");
		return "[ <a href='".ServiceEnv::updateUrlParams(array())."'>OK</a> ]";
	}

	/**
	 * @return string
	 */
	function getTitle() {
		return $this->title;
	}

	/**
	 * @return LoaderPage|null
	 */
	public function getLoadingMessage()
	{
		return new LoaderPage($this->title, $this->msg);
	}

}