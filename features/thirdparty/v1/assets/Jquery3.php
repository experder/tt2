<?php

namespace tt\features\thirdparty\v1\assets;

use tt\features\thirdparty\AssetAtom;
use tt\features\thirdparty\v1\Thirdpartyasset;
use tt\services\ServiceFiles;

class Jquery3 extends Thirdpartyasset
{

	const ASSET_NAME = 'JQuery';
	protected $asset_name = self::ASSET_NAME;

	const DIR = 'jquery_3_6_3';
	protected $dir = self::DIR;

	/*
	 * https://jquery.com/
	 * https://code.jquery.com/jquery-3.6.3.min.js
	 */
	const FILE_JS = 'jquery-3.6.3.min.js';
	const DOWNLOAD = 'https://code.jquery.com';

	/**
	 * @return AssetAtom[]
	 */
	function newAtoms()
	{
		return array(
			new AssetAtom($this, AssetAtom::DELIVER_TYPE_JS, self::FILE_JS, 'cf2fbbf84281d9ecbffb4993203d543b'),
			#new AssetAtom($this, AssetAtom::DELIVER_TYPE_, '', ''),
		);
	}

	/**
	 * @return void
	 */
	function doInstall()
	{
		foreach ($this->getAtoms() as $atom){
			ServiceFiles::download(self::DOWNLOAD.'/'.$atom->getFilenameRel(), $atom->getFilenameAbs());
		}
	}

}