<?php

namespace tt\features\thirdparty\v1\assets;

use tt\features\config\v1\ConfigServer;
use tt\features\css\Stylesheet;
use tt\features\thirdparty\AssetAtom;
use tt\features\thirdparty\v1\Thirdpartyasset;
use tt\services\ServiceArchives;
use tt\services\ServiceFiles;

class Chosen1 extends Thirdpartyasset
{

	const ASSET_NAME = 'Chosen';
	protected $asset_name = self::ASSET_NAME;

	const DIR = 'chosen_1_8_7';
	protected $dir = self::DIR;

	/*
	 * https://harvesthq.github.io/chosen/
	 * https://github.com/harvesthq/chosen
	 * https://github.com/harvesthq/chosen/releases
	 */
	const DOWNLOAD = 'https://github.com/harvesthq/chosen/releases/download/v1.8.7/chosen_v1.8.7.zip';

	/**
	 * @return string
	 */
	public function getJsHead(){
		return "\$(function () {\$('select.chosen').chosen();});";
	}

	/**
	 * @return Thirdpartyasset[]
	 */
	public function getDependencies()
	{
		return array(
			new Jquery3()
		);
	}

	/**
	 * @return string[]
	 */
	public function getCssLinks(){
		return array_merge(parent::getCssLinks(),array(
			new Stylesheet(
				null,
				ConfigServer::$HTTP_TT_ROOT.'/features/thirdparty/v1/assets/chosen1_darkmode.css',
			)
		));
	}

	/**
	 * @return AssetAtom[]
	 */
	function newAtoms()
	{
		return array(
			new AssetAtom($this, AssetAtom::DELIVER_TYPE_JS, 'chosen.jquery.min.js', '3e9f1dcb9cc75169765265133fb815a7'),
			new AssetAtom($this, AssetAtom::DELIVER_TYPE_CSS, 'chosen.min.css', 'd7ca5ca9441ef9e10f9ea5d90002690a'),
			new AssetAtom($this, AssetAtom::DELIVER_TYPE_NONE, 'chosen-sprite.png', '8b55a822e72b8fd5e2ee069236f2d797'),
			new AssetAtom($this, AssetAtom::DELIVER_TYPE_NONE, 'chosen-sprite@2x.png', '614fad616d014daf5367e068505cad35'),
			#new AssetAtom($this, AssetAtom::DELIVER_TYPE_, '', ''),
		);
	}

	/**
	 * @return void
	 */
	function doInstall()
	{
		$tempfile = ConfigServer::$DIR_THIRDPARTY_ROOT."/temp_".$this->getDir().".zip";
		$tempdir = ConfigServer::$DIR_THIRDPARTY_ROOT.'/temp';

		ServiceFiles::download(self::DOWNLOAD, $tempfile);

		ServiceFiles::checkHashOfFile($tempfile, '3409a8c09b8c7ad51629fefccd4c63d3', true);

		ServiceArchives::doUnzip($tempfile, $tempdir);

		foreach ($this->getAtoms() as $atom){
			ServiceFiles::rename($tempdir.'/'.$atom->getFilenameRel(), $atom->getFilenameAbs());
		}

		if(!$this->doCheck(true))return;
		ServiceFiles::unlinkDirectoryRecursively($tempdir);
	}

}
