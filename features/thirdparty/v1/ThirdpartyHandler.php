<?php

namespace tt\features\thirdparty\v1;

class ThirdpartyHandler
{

	/**
	 * @var Thirdpartyasset[] $globalAssets
	 */
	private static $globalAssets = array();

	/**
	 * @param Thirdpartyasset $asset
	 * @return bool
	 */
	public static function addGlobalAsset(Thirdpartyasset $asset){
		$type = get_class($asset);
		if(isset(self::$globalAssets[$type]))return false;
		$parents = $asset->getDependencies();
		self::$globalAssets[$type] = $asset;
		foreach ($parents as $dependency){
			ThirdpartyHandler::addGlobalAsset($dependency);
		}
		return true;
	}

	/**
	 * @return Thirdpartyasset[]
	 */
	public static function getGlobalAssets(){
		return array_reverse(self::$globalAssets);
	}

}