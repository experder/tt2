<?php

namespace tt\features\thirdparty\v1;

use tt\features\config\v1\CFG_S;
use tt\features\debug\errorhandler\v1\Error;
use tt\features\debug\errorhandler\v2\Warning;
use tt\features\htmlpage\view\v1\PageHtml;
use tt\features\thirdparty\AssetAtom;
use tt\services\ServiceEnv;

abstract class Thirdpartyasset
{

	const LOADER_POST_ID = "TT_ASSET_LOADING_ID";

	protected $atoms = null;
	private $check_passed=false;

	/**
	 * @var string $asset_name
	 */
	protected $asset_name = true;

	/**
	 * @var string $dir
	 */
	protected $dir = true;

	/**
	 * @return string
	 */
	public function getAssetName()
	{
		if($this->asset_name===true)$this->asset_name=basename(get_class($this));
		return $this->asset_name;
	}

	/**
	 * @return string
	 */
	public function getDir()
	{
		if($this->dir===true)$this->dir=basename(get_class($this));
		return $this->dir;
	}

	/**
	 * @return string[]
	 */
	public function getJsLinks(){
		if(!$this->doCheckR())return array();
		$links = array();
		foreach ($this->getAtoms() as $atom){
			if(!$atom->isTypeJs())continue;
			$links[] = $atom->getFilenameHttp();
		}
		return $links;
	}

	/**
	 * @return string
	 */
	public function getJsHead(){
		return "";
	}

	/**
	 * @return string
	 */
	public function getJsHeadWithCheck(){
		if(!$this->doCheckR())return "";
		return $this->getJsHead();
	}

	/**
	 * @return string[]
	 */
	public function getCssLinks(){
		if(!$this->doCheckR())return array();
		$links = array();
		foreach ($this->getAtoms() as $atom){
			if(!$atom->isTypeCss())continue;
			$links[] = $atom->getFilenameHttp();
		}
		return $links;
	}

	public function doCheckR()
	{
		foreach ($this->getDependencies() as $dependency){
			if(!$dependency->doCheck())return false;
		}
		return $this->doCheck();
	}

	/**
	 * @param boolean $retry_and_terminate
	 * @return boolean
	 */
	public function doCheck($retry_and_terminate=false, $hashCheckMandatory=false)
	{
		if($this->check_passed&&!$retry_and_terminate)return true;
		foreach ($this->getAtoms() as $atom){
			if(!$atom->isCheckFile())continue;
			if(!$atom->fileExists()){
				if($retry_and_terminate){
					new Error($this->getAssetName()." installation failed:"
						." File '".basename($atom->getFilenameAbs())."' is missing.");
				}
				return false;
			}
			if(!CFG_S::$DEVMODE && !$hashCheckMandatory)continue;
			$atom->checkHash($hashCheckMandatory);
		}
		$this->check_passed=true;
		return true;
	}

	/**
	 * @return AssetAtom[]
	 */
	abstract function newAtoms();

	public function getAtoms(){
		if($this->atoms===null){
			$this->atoms = $this->newAtoms();
		}
		return $this->atoms;
	}

	/**
	 * @return void
	 */
	abstract function doInstall();

	/**
	 * @return Thirdpartyasset[]
	 */
	public function getDependencies()
	{
		return array();
	}

	/**
	 * @return AssetAtom|null
	 */
	public function getAtomByKey($key){
		$atoms = $this->getAtoms();
		return isset($atoms[$key])?$atoms[$key]:null;
	}

	public function checkInstallDoublecheck($hashCheckMandatory = true){
		if(!$this->doCheck(false, $hashCheckMandatory)){
			if(ServiceEnv::isSapiCLI()){
				echo "\nInstalling ".$this->getAssetName()."...";
				$this->doInstall();
				echo "OK\n";
			}else{
				$view = new LoaderPageThirdparty($this);
				new PageHtml($view);
			}
//			//TODO:Same code as used by PageHtml
			$this->doCheck(true, $hashCheckMandatory);
		}
	}

}