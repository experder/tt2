<?php

namespace tt\features\thirdparty;

use tt\features\config\v1\CFG_S;
use tt\features\config\v1\ConfigServer;
use tt\features\debug\errorhandler\v1\Error;
use tt\features\debug\errorhandler\v2\Warning;
use tt\features\thirdparty\v1\Thirdpartyasset;

class AssetAtom
{

	const DELIVER_TYPE_NONE = 0;
	const DELIVER_TYPE_JS = 1;
	const DELIVER_TYPE_CSS = 2;

	/**
	 * @var bool $file_check
	 */
	private $file_check = true;
	/**
	 * @var int $file_deliver_type AssetAtom::DELIVER_TYPE_...
	 */
	private $file_deliver_type;
	/**
	 * @var string $filename_relative
	 */
	private $filename_relative;
	/**
	 * @var string|false FALSE = Don't perform hash check.
	 */
	private $file_hash_target;

	private $file_hash_current = null;

	/**
	 * @var Thirdpartyasset $asset
	 */
	private $asset;

	/**
	 * @param Thirdpartyasset $asset
	 * @param int $file_deliver_type
	 * @param string $filename_relative
	 * @param false|string $file_hash_target
	 */
	public function __construct(Thirdpartyasset $asset, $file_deliver_type, $filename_relative, $file_hash_target=false)
	{
		$this->asset=$asset;
		$this->file_deliver_type = $file_deliver_type;
		$this->filename_relative = $filename_relative;
		$this->file_hash_target = $file_hash_target;
	}

	/**
	 * @return bool
	 */
	public function isCheckFile()
	{
		return $this->file_check;
	}

	/**
	 * @return bool
	 */
	public function fileExists()
	{
		return file_exists($this->getFilenameAbs());
	}

	/**
	 * @return string
	 */
	public function getFilenameAbs()
	{
		return ConfigServer::$DIR_THIRDPARTY_ROOT.'/'.$this->asset->getDir().'/'.$this->filename_relative;
	}
	public function getFilenameHttp()
	{
		return ConfigServer::$HTTP_THIRDPARTY_DIR.'/'.$this->asset->getDir().'/'.$this->filename_relative;
	}

	/**
	 * @return string
	 */
	public function getFilenameRel()
	{
		return $this->filename_relative;
	}

	/**
	 * @return string|false
	 */
	public function getHashTarget()
	{
		return $this->file_hash_target;
	}

	/**
	 * @return string
	 */
	public function getHashCurrent()
	{
		if($this->file_hash_current===null){
			$this->file_hash_current = md5_file($this->getFilenameAbs());
		}
		return $this->file_hash_current;
	}

	public function isTypeJs()
	{
		return $this->file_deliver_type===self::DELIVER_TYPE_JS;
	}

	public function isTypeCss()
	{
		return $this->file_deliver_type===self::DELIVER_TYPE_CSS;
	}

	public function checkHash($exitOnFailure=false) {
		$hash = $this->getHashTarget();
		if($hash===false)return;
		if($hash!==$this->getHashCurrent()){
			$msg = "Hash for '".$this->filename_relative."' does not match!"
				.(CFG_S::$DEVMODE?" ".$this->getHashCurrent():"");
			if($exitOnFailure){
				new Error($msg);
			}else{
				new Warning($msg);
			}
		}
	}

}