<?php

namespace tt\features\navigation\bycode;

use tt\features\htmlpage\components\Html_A;
use tt\features\htmlpage\components\Html_LI;
use tt\features\htmlpage\components\Html_UL;
use tt\features\htmlpage\components\HtmlDiv;

class NavigationEntry
{

	private $nameId;
	private $nameGui;
	private $link;
	/**
	 * @var NavigationEntry[] $children
	 */
	private $children = array();

	/**
	 * @param string $nameId
	 * @param string $nameGui
	 * @param string $link
	 */
	public function __construct($nameId, $nameGui=true, $link=false)
	{
		if($nameGui===true)$nameGui=$nameId;

		$this->nameId = $nameId;
		$this->nameGui = $nameGui;
		$this->link = $link;
	}

	public function addEntry(NavigationEntry $entry)
	{
		$this->children[] = $entry;
	}

	public function getChildrenDiv(){
		$ul = new Html_UL();
		foreach ($this->children as $child){
			$ul->add($child->toLiComp());
		}
		$div = new HtmlDiv();
		$div->addClass("tt_navList");
		$div->add($ul);
		return $div;
	}

	public function toLiComp()
	{
		if($this->link!==false){
			$child = new Html_A($this->link, $this->nameGui);
		}else{
			$child = new HtmlDiv($this->nameGui);
			$child->addClass("tt_navEntry");
		}

		$list = new Html_LI();
		$list->add($child);
		$list->setId($this->nameId);

		if($this->children)$list->add($this->getChildrenDiv());
		return $list;
	}

}