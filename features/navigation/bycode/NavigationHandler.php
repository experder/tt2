<?php

namespace tt\features\navigation\bycode;

use tt\features\modulehandler\v1\ModulehandlerV1;
use tt\features\modulehandler\v1\RegistrationHandler;

class NavigationHandler
{

	/**
	 * @return NavigationEntry
	 */
	public function getNavigation(){
		$modules = ModulehandlerV1::getModules();
		if(!$modules)return new NavigationEntry("");
		$navi = new NavigationEntry("tt_navi_root");
		foreach ($modules as $regHandler){
			if(!($regHandler instanceof RegistrationHandler))continue;
			$entries = $regHandler->getNavigation();
			foreach ($entries as $entry){
				$navi->addEntry($entry);
			}
		}
		return $navi;
	}

}