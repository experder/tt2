<?php

namespace tt\features\database\schema;

class Key
{

	const UNIQUE = true;
	const FULLTEXT = true;

	/**
	 * @var string $name
	 */
	private $name;
	private $tableName;
	/**
	 * @var Column[] $columns
	 */
	private $columns;
	/**
	 * @var bool $unique
	 */
	private $unique;
	/**
	 * @var bool $fulltext
	 */
	private $fulltext = false;

	/**
	 * @param string $tableName
	 * @param string $keyName
	 * @param Column[] $columns
	 * @param bool $unique
	 */
	public function __construct($tableName, $keyName, array $columns, $unique = false)
	{
		$this->tableName = $tableName;
		$this->name = $keyName;
		$this->columns = $columns;
		$this->unique = $unique;
	}

	/**
	 * @return string
	 */
	public function getTableName()
	{
		return $this->tableName;
	}

	public function setFulltext()
	{
		$this->fulltext = true;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return Column[]
	 */
	public function getColumns()
	{
		return $this->columns;
	}

	/**
	 * @return bool
	 */
	public function isUnique()
	{
		return $this->unique;
	}

	/**
	 * @return bool
	 */
	public function isFulltext()
	{
		return $this->fulltext;
	}

	public function compareToKey(Key $other){
		$colsAreEqual = implode(",",ServiceSchema::columnNames($this->columns))
			===implode(",",ServiceSchema::columnNames($other->getColumns()));
		return $this->name===$other->getName()
			&& $this->tableName===$other->getTableName()
			&& $colsAreEqual
			&& $this->unique===$other->isUnique()
			&& $this->fulltext===$other->isFulltext();
	}

}