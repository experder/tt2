<?php /** @noinspection PhpUnused TODO */

namespace tt\features\database\schema;

use tt\features\database\v1\Model;

class ForeignKey
{

	const CONSTRAINTACTION_NOACTION = "tt_noaction";
	const CONSTRAINTACTION_CASCADE = "tt_cascade";
	const CONSTRAINTACTION_RESTRICT = "tt_restrict";
	const CONSTRAINTACTION_SETNULL = "tt_setnull";
	const CONSTRAINTACTION_SETDEFAULT = "tt_setdefault";

	/**
	 * @var string $name
	 */
	private $name;
	private $tableName;
	/**
	 * @var Column[] $columns
	 */
	private $columns;
	/**
	 * @var string $refTable
	 */
	private $refTable;
	/**
	 * @var string[] $refColumns
	 */
	private $refColumns;
	/**
	 * @var string $onUpdate ForeignKey::CONSTRAINTACTION_...
	 */
	private $onUpdate = ForeignKey::CONSTRAINTACTION_RESTRICT;
	/**
	 * @var string $onUpdate ForeignKey::CONSTRAINTACTION_...
	 */
	private $onDelete = ForeignKey::CONSTRAINTACTION_RESTRICT;

	/**
	 * @param string $tableName
	 * @param string $keyName
	 * @param Column[] $columns
	 * @param string $refTable
	 * @param string[] $refColumns
	 */
	public function __construct($tableName, $keyName, array $columns, $refTable, array $refColumns = array(Model::FIELD_id))
	{
		$this->name = $keyName;
		$this->tableName = $tableName;
		$this->columns = $columns;
		$this->refTable = $refTable;
		$this->refColumns = $refColumns;
	}

	/**
	 * @return string
	 */
	public function getTableName()
	{
		return $this->tableName;
	}

	/**
	 * @param string $onUpdate
	 * @param string $onDelete
	 * @return $this
	 */
	public function setActions($onUpdate, $onDelete)
	{
		$this->onUpdate = $onUpdate;
		$this->onDelete = $onDelete;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return Column[]
	 */
	public function getColumns()
	{
		return $this->columns;
	}

	/**
	 * @return string
	 */
	public function getRefTable()
	{
		return $this->refTable;
	}

	/**
	 * @return string[]
	 */
	public function getRefColumns()
	{
		return $this->refColumns;
	}

	/**
	 * @return string
	 */
	public function getOnUpdate()
	{
		return $this->onUpdate;
	}

	/**
	 * @return string
	 */
	public function getOnDelete()
	{
		return $this->onDelete;
	}

	public function compareToForeignKey(ForeignKey $other){
		$colsAreEqual = implode(",",ServiceSchema::columnNames($this->columns))
			===implode(",",ServiceSchema::columnNames($other->getColumns()));
		return $this->name===$other->getName()
			&& $colsAreEqual
			&& $this->refTable===$other->getRefTable()
			&& implode(",",$this->refColumns)===implode(",",$other->getRefColumns())
			&& $this->onUpdate===$other->getOnUpdate()
			&& $this->onDelete===$other->getOnDelete()
			;
	}

}