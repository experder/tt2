<?php

namespace tt\features\database\schema;

use tt\features\config\v1\CFG_S;
use tt\features\database\v1\Model;
use tt\features\debug\errorhandler\v2\Warning;

class Table
{

	/**
	 * @var Column[] $columns
	 */
	private $columns = array();
	private $columnNames = null;
	/**
	 * @var Column[] $primaryKey
	 */
	private $primaryKey = true;
	/**
	 * @var Key[] $keys
	 */
	private $keys = array();
	/**
	 * @var ForeignKey[] $foreignKeys
	 */
	private $foreignKeys = array();
	/**
	 * @var string $name
	 */
	private $name;
	/**
	 * @var string $comment
	 */
	private $comment = "";

	/**
	 * @param string $name
	 */
	public function __construct($name)
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param Column $column
	 * @return $this
	 */
	public function addColumn(Column $column){
		if(isset($this->columns[$column->getName()]) && CFG_S::$DEVMODE){
			new Warning("Same column defined twice! ".$column->getName());
		}
		$this->columns[$column->getName()] = $column;
		return $this;
	}

	public function removeColumn($columnName){
		if(!isset($this->columns[$columnName])){
			return false;
		}
		unset($this->columns[$columnName]);
		return true;
	}

	/**
	 * @param ForeignKey $key
	 * @return $this
	 */
	public function addForeignKey(ForeignKey $key){
		if(isset($this->foreignKeys[$key->getName()]) && CFG_S::$DEVMODE){
			new Warning("Foreign key already exists! ".$key->getName());
		}
		$this->foreignKeys[$key->getName()] = $key;
		return $this;
	}

	/**
	 * @param Column[] $columns
	 * @param bool $unique
	 * @param string|true $name
	 * @param bool $fulltext
	 * @return $this
	 */
	public function addIndex(array $columns, $unique = false, $name = true, $fulltext = false){
		if($name===true){
			$name = ServiceSchema::keyDefaultName($this, $columns)."_idx";
		}
		if(isset($this->keys[$name]) && CFG_S::$DEVMODE){
			new Warning("Same key defined twice! ".$name);
		}
		$key = new Key($this->name, $name, $columns, $unique);
		if($fulltext)$key->setFulltext();
		$this->keys[$name] = $key;
		return $this;
	}

	/**
	 * @param Column[] $columns
	 * @return $this
	 */
	public function addIndexUnique(array $columns){
		return $this->addIndex($columns, Key::UNIQUE);
	}

	/**
	 * @param Column[] $columns
	 * @return $this
	 */
	public function addIndexFulltext(array $columns){
		return $this->addIndex($columns, false, true, Key::FULLTEXT);
	}

	/**
	 * @param Column[] $columns
	 * @return $this
	 */
	public function addColumns(array $columns){
		foreach ($columns as $column){
			$this->addColumn($column);
		}
		return $this;
	}

	public static function fromModel(Model $model){
		$table = new Table($model->getTableName());
		$table->setComment($model->getTableComment());

		$fields = $model->getColumns(false);
		foreach ($fields as $field){
			$table->addColumn(new Column($field, Column::DATATYPE_STRING));
		}

		return $table;
	}

	/**
	 * @param $comment
	 * @return $this
	 */
	public function setComment($comment)
	{
		$this->comment = $comment;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getComment()
	{
		return $this->comment;
	}

	/**
	 * @return Column[]
	 */
	public function getColumns()
	{
		return $this->columns;
	}

	/**
	 * @return Key[]
	 */
	public function getKeys()
	{
		return $this->keys;
	}

	/**
	 * @param string $name
	 * @return Key|false
	 */
	public function getKey($name)
	{
		if(!isset($this->keys[$name]))return false;
		return $this->keys[$name];
	}

	/**
	 * @param string $name
	 * @return Column|false
	 */
	public function getColumn($name)
	{
		if(!isset($this->columns[$name]))return false;
		return $this->columns[$name];
	}

	/**
	 * @return ForeignKey[]
	 */
	public function getForeignKeys()
	{
		return $this->foreignKeys;
	}

	/**
	 * @return Column[]
	 */
	public function getPrimaryKey()
	{
		if($this->primaryKey===true){
			$this->setPrimaryKey(Model::FIELD_id);
		}
		return $this->primaryKey;
	}

	public function setPrimaryKey($keyName)
	{
		$this->primaryKey = array(
			new Column($keyName, Column::DATATYPE_INTEGER, Column::NOT_NULLABLE)
		);
	}

	/**
	 * @return string[]
	 */
	public function getColumnNames($id = false)
	{
		if($this->columnNames===null){
			$this->columnNames=array();
			if($id)$this->columnNames[] = Model::FIELD_id;
			foreach ($this->getColumns() as $col){
				$this->columnNames[] = $col->getName();
			}
		}
		return $this->columnNames;
	}


}