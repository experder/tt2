<?php /** @noinspection PhpUnusedPrivateFieldInspection TODO */
/** @noinspection PhpUnused TODO */

namespace tt\features\database\schema;

use tt\features\database\v1\Model;

class Column
{

	const DATATYPE_STRING = "tt_string";
	const DATATYPE_TEXT = "tt_text";
	const DATATYPE_INTEGER = "tt_integer";
	const DATATYPE_NUMERIC = "tt_numeric";
	const DATATYPE_DATE = "tt_date";
	const DATATYPE_DATETIME = "tt_datetime";
	const DATATYPE_BOOLEAN = "tt_boolean";
	const DATATYPE_BLOB = "tt_blob";
	const DATATYPE_ENUM = "tt_enum";

	const NOT_NULLABLE = false;
	const NULLABLE = true;

	const STRING_DEFAULT_LENGTH = 200;

	/**
	 * @var string $name
	 */
	private $name;
	/**
	 * @var string $guiName
	 */
	private $guiName = true;
	/**
	 * @var string $data_type Column::DATATYPE_...
	 */
	private $data_type;
	private $datatypeString_maxlength = 0;
	private $datatypeEnum_options = array();
	/**
	 * @var string|null $default
	 */
	private $default;
	/**
	 * @var string $comment
	 */
	private $comment = "";
	/**
	 * @var bool $nullable
	 */
	private $nullable;
	/**
	 * @var bool $signed
	 */
	private $signed = true;

	/**
	 * @param string $name
	 * @param string $data_type Column::DATATYPE_...
	 * @param bool $nullable
	 * @param string|null $default
	 */
	public function __construct($name, $data_type, $nullable=Column::NULLABLE, $default=null)
	{
		$this->name = $name;
		$this->data_type = $data_type;
		$this->default = $default;
		$this->nullable = $nullable;
	}

	/**
	 * @param $comment
	 * @return $this
	 */
	public function setComment($comment)
	{
		$this->comment = $comment;
		return $this;
	}

	/**
	 * @param string $data_type
	 * @return $this
	 */
	public function setDataType($data_type)
	{
		$this->data_type = $data_type;
		return $this;
	}
	/**
	 * @param int $maxlength
	 * @return $this
	 */
	public function setDataTypeString($maxlength = null)
	{
		$this->data_type = Column::DATATYPE_STRING;
		$this->setStringMaxLength($maxlength);
		return $this;
	}
	/**
	 * @return $this
	 */
	public function setDataTypeInteger()
	{
		$this->data_type = Column::DATATYPE_INTEGER;
		return $this;
	}

	/**
	 * @return $this
	 */
	public function setNotNullable()
	{
		$this->nullable = Column::NOT_NULLABLE;
		return $this;
	}

	/**
	 * @return $this
	 */
	public function setUnsigned()
	{
		$this->signed = false;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isSigned()
	{
		return $this->signed;
	}

	/**
	 * @return string
	 */
	public function getComment()
	{
		return $this->comment;
	}

	/**
	 * @param string $default
	 */
	public function setDefault($default)
	{
		$this->default = $default;
		return $this;
	}

	/**
	 * @param int $maxlength
	 * @return $this
	 */
	public function setStringMaxLength($maxlength){
		$this->datatypeString_maxlength = $maxlength;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isNullable()
	{
		return $this->nullable;
	}

	/**
	 * @return string|null
	 */
	public function getDefault()
	{
		return $this->default;
	}

	/**
	 * @return int
	 */
	public function getStringMaxlength()
	{
		return $this->datatypeString_maxlength;
	}

	public function addIndex(Table $table, $unique = false, $name = true, $fulltext = false){
		$table->addIndex(array($this), $unique, $name, $fulltext);
		return $this;
	}
	public function addIndexUnique(Table $table){
		return $this->addIndex($table, Key::UNIQUE);
	}
	public function addIndexFulltext(Table $table){
		return $this->addIndex($table, false, true, Key::FULLTEXT);
	}

	/**
	 * @param Table $table
	 * @param string $refTable
	 * @param string[] $refColumns
	 * @param string|true $name TRUE for default
	 * @return $this
	 */
	public function addForeignKey(Table $table, $refTable, $refColumns=array(Model::FIELD_id), $name = true){
		$columns = array($this);
		if($name===true){
			$name = ServiceSchema::keyDefaultName($table, $columns)."_fk";
		}
		$table->addForeignKey(new ForeignKey($table->getName(), $name, $columns, $refTable, $refColumns));
		return $this;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getDataType()
	{
		return $this->data_type;
	}

	/**
	 * @param string $guiName
	 * @return Column
	 */
	public function setGuiName($guiName)
	{
		$this->guiName = $guiName;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getGuiName()
	{
		if($this->guiName===true){
			$this->guiName=$this->name;
		}
		return $this->guiName;
	}

}