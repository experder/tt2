<?php

namespace tt\features\database\schema;

class ServiceSchema
{

	/**
	 * @param Table $table
	 * @param Column[] $columns
	 * @return string
	 */
	public static function keyDefaultName(Table $table, array $columns){
		return $table->getName()."_".implode("_",ServiceSchema::columnNames($columns));
	}

	/**
	 * @param Column[] $columns
	 * @return array
	 */
	public static function columnNames(array $columns){
		$colnames = array();
		foreach ($columns as $column){
			$colnames[] = $column->getName();
		}
		return $colnames;
	}

	public static function columnsByNames(array $colNames){
		$columns = array();
		foreach ($colNames as $colname){
			$columns[] = new Column($colname, null);
		}
		return $columns;
	}

}