<?php

namespace tt\features\database\v2;

class SchemaConstraintUniquekey extends SchemaConstraint
{

	protected $unique = true;

}