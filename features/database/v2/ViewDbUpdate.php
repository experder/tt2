<?php

namespace tt\features\database\v2;

use tt\features\config\v1\ConfigProject;
use tt\features\config\v1\ConfigServer;
use tt\features\css\Stylesheet;
use tt\features\debug\errorhandler\v1\Error;
use tt\features\htmlpage\components\Form;
use tt\features\htmlpage\components\FormDiv;
use tt\features\htmlpage\view\v1\ViewHtmlNew;
use tt\features\i18n\Trans;
use tt\features\messages\v2\MsgConfirm;
use tt\features\messages\v2\MsgError;
use tt\services\polyfill\Php5;
use tt\services\ServiceEnv;

class ViewDbUpdate extends ViewHtmlNew
{

	const CMD_EXECUTE = "execute";
	const POSTVAL_PREVIEW_HASH = "previewhash";

	public static function getClass() {
		return Php5::get_class();
	}

	/**
	 * @return string
	 */
	function getHtml()
	{
		$cmd = ServiceEnv::valueFromPost(Form::COMMAND_STRING);
		if($cmd===self::CMD_EXECUTE){
			return $this->htmlExecute();
		}
		return $this->htmlPreview();
	}

	/**
	 * @return string
	 */
	function getTitle()
	{
		return self::title();
	}

	static function title(){
		return Trans::late("Update database schema");
	}

	private function htmlPreview()
	{
		$schema = Schema::createGlobalSchema();
		$preview = Database::getGlobalDatabase()->syncWithModel($schema, true);
		if(!$preview){
			return $this->OK();
		}
		$form = new Form(self::CMD_EXECUTE, Trans::late("Execute!"));
		$form->addHiddenField(self::POSTVAL_PREVIEW_HASH, md5($preview));
		$form->add(new FormDiv(false, "<pre class='db_commands'>$preview</pre>"));
		return $form->toHtml();
	}

	public function getCss()
	{
		return array(
			new Stylesheet(
				ConfigServer::$HTTP_PROJ_SKIN_DIR .'/'.ConfigProject::$SKIN_ID.'/syncdb.css',
				ConfigServer::$HTTP_PROJ_SKIN_DIR .'/'.ConfigProject::$SKIN_ID.'/darkmode_syncdb.css'
			)
		);
	}

	private function htmlExecute()
	{
		$schema = Schema::createGlobalSchema();
		$preview = Database::getGlobalDatabase()->syncWithModel($schema);

		$hash = md5($preview);
		$checksum = ServiceEnv::valueFromPost(self::POSTVAL_PREVIEW_HASH);
		if($hash!==$checksum){
			new Error("Database has changed!");
		}

		Database::getGlobalDatabase()->syncWithModel($schema, false, false);

		$check = Database::getGlobalDatabase()->syncWithModel($schema);
		if($check){
			new MsgError("Sync failed!");
			return "";
		}

		return $this->OK();
	}

	private function OK()
	{
		new MsgConfirm("Everything in sync!");
		return "";
	}

}