<?php
/** @noinspection PhpUnusedPrivateMethodInspection TODO */
/** @noinspection PhpUnused TODO */
/** @noinspection PhpPropertyOnlyWrittenInspection TODO */

namespace tt\features\database\v2;

use tt\features\debug\errorhandler\v1\Error;
use tt\features\modulehandler\v1\ModulehandlerV1;

abstract class Schema
{

	/**
	 * @var Schema[] $simgletons KEY = classname
	 */
	private static $simgletons = array();
	/**
	 * @var SchemaColumn[]
	 */
	private $columns = null;
	/**
	 * @var SchemaColumn
	 */
	private $columnPrimary = null;
	/**
	 * @var SchemaConstraint[]
	 */
	private $constraints = array();
	/**
	 * @var string $comment
	 */
	private $comment = null;

	const COL_id = 'id';

	private function __construct()
	{
	}

	/**
	 * @return SchemaColumn[]
	 */
	public function getTableCols(){
		if($this->columns===null){
			$this->columns = $this->createTableCols();
		}
		return $this->columns;
	}

	public function getTableColNames($primary=true){
		$cols = $this->getTableCols();
		if($primary)$cols[] = $this->getTableColPrimary();
		$all_names = array();
		foreach ($cols as $col){
			$all_names[$col->getColName()]=true;
		}
		return $all_names;
	}

	/**
	 * @return SchemaColumn|false
	 */
	public function getTableColByName($name){
		foreach ($this->getTableCols() as $col){
			if($col->getColName()===$name) return $col;
		}
		if($this->getTableColPrimary()->getColName()===$name)return $this->getTableColPrimary();
		return false;
	}

	/**
	 * @return SchemaColumn
	 */
	public function getTableColPrimary(){
		if($this->columnPrimary===null){
			$this->columnPrimary = $this->createTableColPrimary();
		}
		return $this->columnPrimary;
	}

	/**
	 * @return SchemaConstraint[]
	 */
	public function getConstraints()
	{
		return $this->constraints;
	}

	/**
	 * @param $classname
	 * @return Schema
	 */
	public static function getSingleton($classname){
		if(!isset(self::$simgletons[$classname])){
			self::$simgletons[$classname] = new $classname();
			if(!(self::$simgletons[$classname] instanceof Schema))new Error("!42!");
		}
		return self::$simgletons[$classname];
	}

	/**
	 * @return string[]
	 */
	public static function getSingletonsKeys()
	{
		return array_keys(self::$simgletons);
	}

	/**
	 * @return string
	 */
	public abstract function getTableName();

	/**
	 * @return string|null
	 */
	public function getTableComment(){
		return $this->comment;
	}

	/**
	 * @param string $comment
	 * @return Schema
	 */
	public function setComment($comment) {
		$this->comment = $comment;
		return $this;
	}

	/**
	 * @return SchemaColumn
	 */
	protected function createTableColPrimary(){
		/** @noinspection PhpUnusedLocalVariableInspection */
		return ($x=new SchemaColumn($this, self::COL_id, SchemaColumn::DATATYPE_INTEGER))
			->setNotNullable()
			->setAutoIncrement()
			;
	}

	/**
	 * @return SchemaColumn[]
	 */
	protected abstract function createTableCols();

	public function addConstraint(SchemaConstraint $constraint){
		$this->constraints[] = $constraint;
	}

	/**
	 * @return Schema[]
	 */
	public static function createGlobalSchema() {
		$moduleHandlers = ModulehandlerV1::$modulesRegistration;
		$schema = array();
		foreach ($moduleHandlers as $moduleHandler) {
			$models = $moduleHandler->getModelSchema();
			$modelSchema = self::getGlobalSchemaForModel($models);
			$schema = array_merge($schema, $modelSchema);
		}
		return $schema;
	}

	private static function getGlobalSchemaForModel(array $models)
	{
		$schema = array();
		foreach ($models as $model) {
			if(!($model instanceof Schema)) {
				if($model instanceof \tt\features\database\v1\Model) continue;
				new Error("ModelSchema corrupt!");
			}
			$schema[] = $model;
		}
		return $schema;
	}

	/**
	 * @param string $name
	 * @return false|SchemaConstraint
	 */
	public function getConstraintByName($name){
		foreach ($this->getConstraints() as $constraint){
			if($constraint->getName()===$name) return $constraint;
		}
		return false;
	}

}