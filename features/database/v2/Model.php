<?php

namespace tt\features\database\v2;

use tt\features\database\v2\querybuilder\FromTable;
use tt\features\database\v2\querybuilder\WhereColumn;

abstract class Model
{

	public function __construct() {
	}

	/**
	 * @return string
	 */
	abstract function getSchemaClass();

	/**
	 * @param string[] $row
	 * @return $this
	 */
	abstract public function fromDbRow(array $row);

	/**
	 * @param int $id
	 * @return $this
	 */
	public function fromDbById($id, $db = null){
		if($db===null)$db = Database::getGlobalDatabase();
		$primary=$this->getSchema()->getTableColPrimary();
		$result = $db->selectGeneral1(
			array_merge(array($primary),$this->getSchema()->getTableCols()),
			new FromTable($this->getSchema()),
			array(),
			array(
				new WhereColumn(WhereColumn::TYPE_EQUALS, $primary, $id)
			)
		);
		if(!$result)return $this;
		$this->fromDbRow($result[0]);
		return $this;
	}

	/**
	 * @return Schema
	 */
	public function getSchema(){
		return Schema::getSingleton($this->getSchemaClass());
	}

}