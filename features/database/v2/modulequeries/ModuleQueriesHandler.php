<?php

namespace tt\features\database\v2\modulequeries;

use tt\features\database\v2\Database;
use tt\features\database\v2\engines\mysql\DatabaseMySql;
use tt\features\database\v2\engines\sqlite\DatabaseSqlite;
use tt\features\debug\errorhandler\v1\Error;

abstract class ModuleQueriesHandler
{

	/**
	 * @var ModuleQueriesDatabase[] $mqs
	 */
	private static $mqs = array();

	/**
	 * @var Database $db
	 */
	protected $db;

	/**
	 * @param Database $db
	 */
	private function __construct(Database $db) {
		$this->db = $db;
	}

	/**
	 * @param string $classname ModuleQueriesHandler
	 * @param Database|null $db
	 * @return ModuleQueriesDatabase
	 */
	public static function getModuleQueries($classname, Database $db=null){
		if($db===null)$db=Database::getGlobalDatabase();
		$key = md5($classname.spl_object_hash($db));
		if(!isset(self::$mqs[$key])){
			$module = new $classname($db);
			if(!($module instanceof ModuleQueriesHandler))new Error("!!!30!!!");
			self::$mqs[$key] = $module->createModuleQueries();
		}
		return self::$mqs[$key];
	}

	private function createModuleQueries(){
		if($this->db instanceof DatabaseMySql){
			$queries = $this->createModuleQueriesMysql();
			if($queries!==null) return $queries;
		}
		if($this->db instanceof DatabaseSqlite){
			$queries = $this->createModuleQueriesSqlite();
			if($queries!==null) return $queries;
		}
		new Error("'".get_class($this)."': No ModuleQueries for '".basename(get_class($this->db))."'!");
	}

	/**
	 * @return ModuleQueriesDatabase|null
	 */
	protected function createModuleQueriesMysql() {
		return null;
	}

	/**
	 * @return ModuleQueriesDatabase|null
	 */
	protected function createModuleQueriesSqlite(){
		return null;
	}

}