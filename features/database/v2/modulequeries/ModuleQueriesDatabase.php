<?php

namespace tt\features\database\v2\modulequeries;

use tt\features\database\v2\Database;

abstract class ModuleQueriesDatabase
{

	/**
	 * @var Database $db
	 */
	protected $db;

	/**
	 * @param Database $db
	 */
	public function __construct(Database $db) {
		$this->db = $db;
	}

}