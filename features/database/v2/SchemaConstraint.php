<?php

namespace tt\features\database\v2;

use tt\features\debug\errorhandler\v1\Error;

abstract class SchemaConstraint
{

	/**
	 * @var SchemaColumn[] $columns
	 */
	protected $columns;
	/**
	 * @var string|null
	 */
	private $name = null;
	/**
	 * @var bool $unique
	 */
	protected $unique = false;

	/**
	 * @param SchemaColumn[] $columns
	 */
	public function __construct(array $columns)
	{
		$this->columns = $columns;
	}

	public function getColumn()
	{
		if(count($this->columns)!==1)new Error("Constraint does not refer to one column!");
		return reset($this->columns);
	}

	/**
	 * @return SchemaColumn[]
	 */
	public function getColumns()
	{
		return $this->columns;
	}

	/**
	 * @return string[]
	 */
	public function getColumnNames()
	{
		$names = array();
		foreach ($this->columns as $column){
			$names[] = $column->getColName();
		}
		return $names;
	}

	/**
	 * @return string|null
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 * @return SchemaConstraint
	 */
	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isUnique()
	{
		return $this->unique;
	}

	/**
	 * @return SchemaConstraint
	 */
	public function setUnique()
	{
		$this->unique = true;
		return $this;
	}

}