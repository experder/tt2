<?php

namespace tt\features\database\v2\engines\sqlite;

use Exception;
use PDOStatement;
use tt\features\database\v2\engines\DbErrorHandler;
use tt\features\debug\errorhandler\v1\Error;

class SqLiteErrorHandler extends DbErrorHandler
{

	/**
	 * @param Exception $e
	 * @param string $debugInfo
	 * @return void
	 */
	public function handle_exception(Exception $e, $debugInfo = "") {
		new Error($e->getMessage()."\n".$debugInfo);
	}

	/**
	 * @param string|PDOStatement $dump
	 * @return string
	 */
	public function getCompiledQueryFromDebugDump($dump) {
		if ($dump instanceof PDOStatement) {
			return $dump->queryString;
			#$dump = ServiceDebug::debugDumpParams($dump);
		}
//		if (preg_match("/^SQL: \\[\\d+] (.*?)\\nParams:  ?\\d+/s", $dump, $matches)) {
//			return $matches[1];
//		}
		new Error("Could not parse SQLite query!\n" . $dump);
		return $dump;
	}
}