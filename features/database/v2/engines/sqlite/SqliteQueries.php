<?php

namespace tt\features\database\v2\engines\sqlite;

use tt\features\database\v2\engines\Queries;
use tt\features\database\v2\Schema;
use tt\features\database\v2\SchemaColumn;
use tt\features\database\v2\SchemaConstraintForeignkey;
use tt\features\database\v2\SchemaConstraintIndex;
use tt\features\database\v2\SchemaConstraintUniquekey;
use tt\features\debug\errorhandler\v2\Warning;

class SqliteQueries extends Queries
{

	/**
	 * @var DatabaseSqlite
	 */
	protected $database;

	public function qAllTables(){
		return $this->database->selectSqlite(
			"SELECT tbl_name FROM SQLITE_SCHEMA WHERE type='table' and tbl_name not in ('sqlite_sequence');");
	}

	public function getCreateTable(Schema $schema)
	{
		$name = $schema->getTableName();

		$indexes = array();
		$desc = array();

		//Primary key
		$pk = $schema->getTableColPrimary();
		$desc[] = $this->getCreateColumn($pk, true);

		//Columns
		foreach ($schema->getTableCols() as $column) {
			$desc[] = $this->getCreateColumn($column);
		}

		//Keys
		foreach ($schema->getConstraints() as $key) {
			if($key instanceof SchemaConstraintUniquekey){
				$desc[] = $this->getCreateUniquekey($key);
			}else if($key instanceof SchemaConstraintIndex) {
				$indexes[] = $this->getCreateIndex($name, $key);
			}else if($key instanceof SchemaConstraintForeignkey) {
				$desc[] = $this->getCreateForeignkey($key);
			}else{
				new Warning("Unknown key constraint!49");
			}
		}

		$cols_and_keys = implode(",\n\t", $desc);

		return array_merge(array("CREATE TABLE $name (\n\t$cols_and_keys\n);"),$indexes);
	}

	function getCreateColumn(SchemaColumn $column, $isPrimaryKey=false)
	{
		$name = $column->getColName();

		$type = $this->database->mapDataTypeOut($column->getDataType(), $column);

		$nullable = $column->isNullable() ? "" : "NOT NULL";

		$default = $column->getDefault() !== null
			? "DEFAULT " . $this->database->mapDefaultValueOut($column)
			: "";

		$primary = "";
		if($isPrimaryKey){
			if($column->getDataType()===SchemaColumn::DATATYPE_INTEGER){
				$primary = "PRIMARY KEY";
			}else{
				new Warning("Primary key has to be of type integer!");
			}
		}

		if($column->getAutoIncrement()){
			if($isPrimaryKey){
				$primary.=" AUTOINCREMENT";
			}else{
				new Warning("AutoIncrement only on primary key!");
			}
		}

		return "$name $type $default $nullable $primary";
	}

	private function getCreateUniquekey(SchemaConstraintUniquekey $key)
	{
		$name = $this->database->generateKeyName($key);
		$columns = implode(",", $key->getColumnNames());
		return "CONSTRAINT $name UNIQUE ($columns)";
	}

	private function getCreateIndex($table_name, SchemaConstraintIndex $key)
	{
		$unique = $key->isUnique()?"UNIQUE":"";
		$name = $this->database->generateKeyName($key);
		$columns = implode(",", $key->getColumnNames());
		return "CREATE $unique INDEX $name ON $table_name ($columns);";
	}

	private function getCreateForeignkey(SchemaConstraintForeignkey $key)
	{
		$name = $this->database->generateKeyName($key);
		$colname = implode(",", $key->getColumnNames());
		$refTable = $key->getRefTable()->getTableName();
		$refCol = $key->getRefColumn()->getColName();
		$ondelete = $this->database->mapConstraintTypeOut("ON DELETE", $key->getOnDelete());
		$onupdate = $this->database->mapConstraintTypeOut("ON UPDATE", $key->getOnUpdate());
		return "CONSTRAINT $name FOREIGN KEY ($colname) REFERENCES $refTable($refCol) $ondelete $onupdate";
	}

}