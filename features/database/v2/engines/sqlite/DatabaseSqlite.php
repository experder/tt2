<?php

namespace tt\features\database\v2\engines\sqlite;

use Exception;
use PDO;
use tt\features\database\v2\Database;
use tt\features\database\v2\engines\DbErrorHandler;
use tt\features\database\v2\engines\ServicePdo;
use tt\features\database\v2\Schema;
use tt\features\database\v2\SchemaColumn;
use tt\features\database\v2\SchemaConstraint;
use tt\features\database\v2\SchemaConstraintForeignkey;
use tt\features\debug\errorhandler\v1\Error;
use tt\features\messages\v2\MsgConfirm;
use tt\features\messages\v2\MsgError;
use tt\services\ServiceArrays;
use tt\services\ServiceStrings;

class DatabaseSqlite extends Database
{

	/**
	 * @var string $file
	 */
	private $file;
	/**
	 * @var SqliteQueries $queries
	 */
	private $queries;
	/**
	 * @var PDO $pdo
	 */
	private $pdo;

	public function __construct($file)
	{
		$this->file = $file;

		$this->initialize();

		$this->queries = new SqliteQueries($this);
	}

	public function initialize(){
		try {
			$initialize = !file_exists($this->file);
			$this->pdo = new PDO("sqlite:".$this->file);
			if($initialize){
				new MsgConfirm("Empty SQLite database '$this->file' created.");
			}
			$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->pdo->query('PRAGMA foreign_keys = ON;');
		}catch (Exception $e) {
			$this->getErrorHandler()->handle_exception($e);
		}
	}

	public function getSchema()
	{
		$name = basename($this->file);
		return preg_replace("/\\.sqlite\$/i", "", $name);
	}
	public function getFilename() {
		return $this->file;
	}

	public function getAllTables()
	{
		return ServiceArrays::keyValueArray($this->queries->qAllTables(),true,"{tbl_name}");
	}

	/**
	 * @param Schema $table
	 * @param bool $showWarnings
	 * @return string[]
	 */
	public function syncTableWithModel(Schema $table, $showWarnings){
		$commands = array();

		$commands[] = "sync: ".$table->getTableName();

		return $commands;
	}

	/**
	 * @return SqliteQueries
	 */
	function getQueries()
	{
		return $this->queries;
	}

	/**
	 * @param string $query
	 * @param array|null $placeholders
	 * @param int $type
	 * @return array|bool|string|int SELECT:array|false, INSERT:string, UPDATE/DELETE:int, VOID:true
	 */
	protected function query($query, $placeholders, $type)
	{
		return ServicePdo::query($this->pdo, $query, $placeholders, $type, $this->getErrorHandler());
	}

	/**
	 * @param string $dataType SchemaColumn::DATATYPE_
	 * @param SchemaColumn|null $column
	 * @return string
	 */
	public function mapDataTypeOut($dataType, SchemaColumn $column = null)
	{
		if ($dataType === SchemaColumn::DATATYPE_STRING) {
			if(!($column instanceof SchemaColumn)){new Error("113");}
			$maxlength = $column->getMaxLength()?"(".$column->getMaxLength().")":"";
			return "TEXT".$maxlength;
		}
		if ($dataType === SchemaColumn::DATATYPE_INTEGER
			||$dataType === SchemaColumn::DATATYPE_BOOLEAN
		) {
			return "INTEGER";
		}
		if ($dataType === SchemaColumn::DATATYPE_TEXT
			||$dataType === SchemaColumn::DATATYPE_DATETIME
		) {
			return "TEXT";
		}
		if ($dataType === SchemaColumn::DATATYPE_DATE) {
			return "TEXT(10)";
		}
		new MsgError("168: Unknown data type '$dataType'!");
		return "/*!!!!168!!!!!*/";
	}

	public function mapDefaultValueOut(SchemaColumn $column){
		$type = $column->getDataType();
		$default = $column->getDefault();
		if($type===SchemaColumn::DATATYPE_BOOLEAN){
			if($default===true||$default===false){
				return $default?"1":"0";
			}
		}
		new MsgError("140: Unknown default for '$type'!");
		return "/*!!!!140!!!!!*/";
	}

	public function generateKeyName(SchemaConstraint $key)
	{
		$name = $key->getName();
		if($name!==null)return $name;

		//Get schema
		$cols = $key->getColumns();
		$col0 = reset($cols);
		$schema = $col0->getSchema();

		$name = $schema->getTableName();

		$suffix = $key instanceof SchemaConstraintForeignkey?"FK"
			:($key->isUnique()?"UN":"IDX");

		//Next free counter
		$keyName = $name."_".$suffix;
		$generatedName = $keyName;
		$counter=1;
		while ($schema->getConstraintByName($generatedName)!==false){
			$generatedName = $keyName.(++$counter);
		}

		$key->setName($generatedName);
		return $generatedName;
	}

	public function mapConstraintTypeOut($prefix, $constraintType_tt)
	{
		if ($constraintType_tt === SchemaConstraintForeignkey::CONSTRAINTACTION_RESTRICT) {
			return "$prefix RESTRICT";
		}
		if ($constraintType_tt === SchemaConstraintForeignkey::CONSTRAINTACTION_CASCADE) {
			return "$prefix CASCADE";
		}
		if ($constraintType_tt === SchemaConstraintForeignkey::CONSTRAINTACTION_SETNULL) {
			return "$prefix SET NULL";
		}
		if ($constraintType_tt === SchemaConstraintForeignkey::CONSTRAINTACTION_SETDEFAULT) {
			return "$prefix SET DEFAULT";
		}
		if ($constraintType_tt === SchemaConstraintForeignkey::CONSTRAINTACTION_NOACTION) {
			return "";
		}
		new MsgError("215: Unknown action '$constraintType_tt'!");
		return "/*!!!!215!!!!*/";
	}

	public function selectSqlite($query, $placeholders=null) {
		return $this->select($query, $placeholders);
	}

	public function disconnect() {
		$this->pdo = null;
	}

	/**
	 * @return DbErrorHandler
	 */
	protected function createErrorHandler() {
		return new SqLiteErrorHandler();
	}

	/**
	 * @param string $tableName
	 * @param array $data {"foo":"bar",...}
	 * @return string
	 */
	public function insertByArray($tableName, $data) {
		$keys_array = array_keys($data);
		$keys_prefixed = ServiceStrings::prefixValues(':', $keys_array);
		$keys_quoted = ServiceStrings::prefixValues('"', $keys_array, '"');

		$substitutions = array();
		foreach ($data as $key => $value) {
			$substitutions[':' . $key] = $value;
		}

		$keys = implode(",", $keys_quoted);
		$values = implode(",", $keys_prefixed);
		$query = "INSERT INTO $tableName ($keys)\nVALUES ($values);";
		return $this->insert($query, $substitutions);
	}

}