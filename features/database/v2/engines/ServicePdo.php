<?php

namespace tt\features\database\v2\engines;

use Exception;
use PDO;
use tt\features\config\v1\CFG_S;
use tt\features\database\v2\Database;
use tt\features\debug\DebugHandler;
use tt\features\debug\errorhandler\v1\Error;

class ServicePdo
{

	/**
	 * @param PDO $pdo
	 * @param string $query
	 * @param array|null $placeholders
	 * @param int $type
	 * @param DbErrorHandler $errorHandler
	 * @return array|bool|string|int SELECT:array|false, INSERT:string, UPDATE/DELETE:int, VOID:true
	 */
	public static function query(PDO $pdo, $query, $placeholders, $type, DbErrorHandler $errorHandler)
	{
		$statement = $pdo->prepare($query);
		try {
			@$statement->execute($placeholders);
		} catch (Exception $e) {
			$errorHandler->handle_exception($e, $errorHandler->getCompiledQueryFromDebugDump($statement));
		}
		if (CFG_S::$DEVMODE && CFG_S::$DEBUGINFO_SHOWQUERIES) {
			$compiledQuery = $errorHandler->getCompiledQueryFromDebugDump($statement);
			DebugHandler::addQuery($compiledQuery
			#."\n".implode("\n",ServiceDebug::backtrace())
			);
		}
		if ($type === Database::QUERY_TYPE_SELECT) {
			return $statement->fetchAll(PDO::FETCH_ASSOC);
		}
		if ($type === Database::QUERY_TYPE_INSERT) {
			return $pdo->lastInsertId();
		}
		if ($type === Database::QUERY_TYPE_UPDATE || $type === Database::QUERY_TYPE_DELETE) {
			return $statement->rowCount();
		}
		if ($type === Database::QUERY_TYPE_VOID) {
			return true;
		}
		new Error("Unknown Query Type!");
		return false;
	}

}