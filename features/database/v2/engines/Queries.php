<?php

namespace tt\features\database\v2\engines;

use tt\features\database\v2\Database;
use tt\features\database\v2\Schema;

abstract class Queries
{

	/**
	 * @var Database
	 */
	protected $database;

	/**
	 * @param Database $database
	 */
	public function __construct(Database $database)
	{
		$this->database = $database;
	}

	/**
	 * @param Schema $schema
	 * @return string[] commands
	 */
	abstract public function getCreateTable(Schema $schema);

}