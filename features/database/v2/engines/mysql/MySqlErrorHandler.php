<?php

namespace tt\features\database\v2\engines\mysql;

use Exception;
use PDOStatement;
use tt\features\database\v2\engines\DbErrorHandler;
use tt\features\debug\errorhandler\v1\Error;
use tt\services\ServiceDebug;

class MySqlErrorHandler extends DbErrorHandler
{

	/**
	 * @param Exception $e
	 * @param string $debugInfo
	 * @return void
	 */
	public function handle_exception(Exception $e, $debugInfo = "") {
		new Error($e->getMessage()."\n".$debugInfo);
	}

	/**
	 * @param string|PDOStatement $dump
	 * @return string
	 */
	public function getCompiledQueryFromDebugDump($dump) {
		if ($dump instanceof PDOStatement) {
			$dump = ServiceDebug::debugDumpParams($dump);
		}
		if (preg_match("/\\nSent SQL: \\[\\d+] (.*?)\\nParams:  ?\\d+\\n/s", $dump, $matches)) {
			return $matches[1];
		}
		if (preg_match("/^SQL: \\[\\d+] (.*?)\\nParams: {2}0$/s", $dump, $matches)) {
			return $matches[1];
		}
		if (preg_match("/^SQL: \\[\\d+] (.*?)\\nParams:  ?\\d+\\n/s", $dump, $matches)) {
			return $matches[1];
		}
		new Error("Could not parse MySQL query!\n" . $dump);
		return "?";
	}
}