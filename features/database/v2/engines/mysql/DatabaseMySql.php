<?php

namespace tt\features\database\v2\engines\mysql;

use Exception;
use PDO;
use tt\features\config\v1\ConfigProject;
use tt\features\database\v1\db_mysql\DbErrorHandling;
use tt\features\database\v1\db_mysql\DbSchemaHandler;
use tt\features\database\v2\Database;
use tt\features\database\v2\engines\DbErrorHandler;
use tt\features\database\v2\engines\ServicePdo;
use tt\features\database\v2\querybuilder\From;
use tt\features\database\v2\querybuilder\FromTable;
use tt\features\database\v2\querybuilder\Groupby;
use tt\features\database\v2\querybuilder\GroupbyColumn;
use tt\features\database\v2\querybuilder\Join;
use tt\features\database\v2\querybuilder\JoinLeft;
use tt\features\database\v2\querybuilder\Limit;
use tt\features\database\v2\querybuilder\Orderby;
use tt\features\database\v2\querybuilder\OrderbyColumn;
use tt\features\database\v2\querybuilder\Select;
use tt\features\database\v2\querybuilder\SelectColumn;
use tt\features\database\v2\querybuilder\SelectStar;
use tt\features\database\v2\querybuilder\Where;
use tt\features\database\v2\querybuilder\WhereAND;
use tt\features\database\v2\querybuilder\WhereColumn;
use tt\features\database\v2\querybuilder\WhereOR;
use tt\features\database\v2\Schema;
use tt\features\database\v2\SchemaColumn;
use tt\features\database\v2\SchemaConstraint;
use tt\features\database\v2\SchemaConstraintForeignkey;
use tt\features\debug\errorhandler\v1\Error;
use tt\features\messages\v2\MsgError;
use tt\features\messages\v2\MsgInfo;
use tt\services\DEBUG;
use tt\services\polyfill\Php8;
use tt\services\ServiceArrays;

class DatabaseMySql extends Database
{

	const DATATYPE_VARCHAR_DEFAULT_MAXLENGTH = 200;

	private $tableAliasses = array();

	private $schema;
	/**
	 * @var MySqlQueries $queries
	 */
	private $queries;

	/**
	 * @var PDO $pdo
	 */
	private $pdo;

	private static $relations = array(
		WhereColumn::TYPE_EQUALS => " <=> ",
		WhereColumn::TYPE_GREATER_EQUAL => " >= ",
		WhereColumn::TYPE_SMALLER => " < ",
	);

	public function __construct($host, $schema, $username, $password)
	{
		$this->schema = $schema;
		try {
			$this->pdo = new PDO(
				"mysql:host=" . $host
				. ";dbname=" . $schema,
				$username,
				$password
			);
			Php8::pdo_init($this->pdo);
		} catch (Exception $e) {
			DbErrorHandling::handle_exception($e);
		}
		$this->queries = new MySqlQueries($this);
	}

	public function getSchema()
	{
		return $this->schema;
	}

	public function getAllTables()
	{
		return ServiceArrays::keyValueArray($this->queries->qShowTableStatus(), true, "{Name}");
	}

	/**
	 * @param Schema $table
	 * @param bool $showWarnings
	 * @return string[] commands
	 */
	public function syncTableWithModel(Schema $table, $showWarnings){
		$commands = array();

		$createTable = $this->queries->queryShowCreate($table->getTableName());
#(new DbSchemaHandler())->getTableSchemaFromDb();
#(new DbSchemaHandler())->compareSchema();
#if($table->getTableName()==="core_settings")DEBUG::out_continue($createTable);

		if (!preg_match("/^CREATE TABLE `(.*?)` \\(\\n(.*?)\\n\\) ([^\\n]+)\$/s", $createTable, $matches)) {
			new Error("Couldn't parse SQL response!");
		}
		$name = $matches[1];
		$desc = $matches[2];
		$tblInfo = $matches[3];

		if (!$this->syncCheckName($name, $table)){
			new Error("Error when fetching table create query.");
		}

		//Build Schema:
		$table->getTableCols();

		//Comment
		if (preg_match("/ COMMENT='(.*?)'\$/", $tblInfo, $matches)) {
			$comment = $matches[1];
			/**
			 * This is for later extensions, where engine and collation are considered.
			 * @noinspection PhpUnusedLocalVariableInspection
			 */
			$tblInfo = substr($tblInfo, 0, strlen($tblInfo) - strlen($comment) - 11);
			$comment = $this->unquoteString($comment);
		}else{
			$comment=null;
		}
		$more_commands = $this->syncComment($table, $comment);
		$commands = array_merge($commands, $more_commands);

		//Columns,Keys
		$all_columns = $table->getTableColNames();
		foreach (explode("\n", $desc) as $row) {
			$more_commands = $this->syncColKey($table, trim($row, "\t\r ,"), $showWarnings, $all_columns);
			$commands = array_merge($commands, $more_commands);
		}
		foreach ($all_columns as $column=>$dummy){
			$commands[]="ALTER TABLE `".$table->getTableName()
				."` ADD ".$this->queries->getCreateColumn($table->getTableColByName($column)).";";
		}

		return $commands;
	}

	/**
	 * @return MySqlQueries
	 */
	function getQueries()
	{
		return $this->queries;
	}

	public function selectMysql($query, $placeholders=null) {
		return $this->select($query, $placeholders);
	}

	/**
	 * @param string $query
	 * @param array|null $placeholders
	 * @param int $type
	 * @return array|bool|string|int SELECT:array|false, INSERT:string, UPDATE/DELETE:int, VOID:true
	 */
	protected function query($query, $placeholders, $type)
	{
		return ServicePdo::query($this->pdo, $query, $placeholders, $type, $this->getErrorHandler());
	}

	public function quoteString($string) {
		$string = str_replace("'", "''", $string);
		$string = str_replace("\\", "\\\\", $string);
		$string = str_replace("\r", "\\r", $string);
		$string = str_replace("\n", "\\n", $string);
		return "'$string'";
	}

	private function unquoteString($string) {
		$string = str_replace("''", "'", $string);
		$string = str_replace("\\r", "\r", $string);
		$string = str_replace("\\n", "\n", $string);
		return str_replace("\\\\", "\\", $string);
	}

	/**
	 * @param string $dataType SchemaColumn::DATATYPE_
	 * @param SchemaColumn|null $column
	 * @return string
	 */
	public function mapDataTypeOut($dataType, SchemaColumn $column = null)
	{
		if ($dataType === SchemaColumn::DATATYPE_STRING) {
			if(!($column instanceof SchemaColumn)){new Error("188");}
			$maxlength = $column->getMaxLength()?:self::DATATYPE_VARCHAR_DEFAULT_MAXLENGTH;
			return "varchar($maxlength)";
		}
		if ($dataType === SchemaColumn::DATATYPE_INTEGER) {
			return "int";
		}
		if ($dataType === SchemaColumn::DATATYPE_TEXT) {
			return "text";
		}
		if ($dataType === SchemaColumn::DATATYPE_DATE) {
			return "date";
		}
		if ($dataType === SchemaColumn::DATATYPE_DATETIME) {
			return "datetime";
		}
		if ($dataType === SchemaColumn::DATATYPE_BOOLEAN) {
			return "tinyint";
		}
		new MsgError("203: Unknown data type '$dataType'!");
		return "/*!!!!203!!!!!*/";
	}

	public function mapDefaultValueOut(SchemaColumn $column){
		$type = $column->getDataType();
		$default = $column->getDefault();
		if($type===SchemaColumn::DATATYPE_BOOLEAN){
			if($default===true||$default===false){
				return $default?"'1'":"'0'";
			}
		}
		new MsgError("214: Unknown default for '$type'!");
		return "/*!!!!214!!!!!*/";
	}

	public function generateKeyName(SchemaConstraint $key)
	{
		$name = $key->getName();
		if($name!==null)return $name;
		$cols = $key->getColumns();
		$col0 = reset($cols);
		$schema = $col0->getSchema();
		$name = $schema->getTableName();
		$suffix = $key instanceof SchemaConstraintForeignkey?"fk"
			:($key->isUnique()?"un":"idx");
		$keyName = $name."_".$suffix;
		$generatedName = $keyName;
		$counter=1;
		while ($schema->getConstraintByName($generatedName)!==false){
			$generatedName = $keyName.(++$counter);
		}
		$key->setName($generatedName);
		return $generatedName;
	}

	public function mapConstraintTypeOut($constraintType_tt)
	{
		if ($constraintType_tt === SchemaConstraintForeignkey::CONSTRAINTACTION_RESTRICT) {
			return "RESTRICT";
		}
		if ($constraintType_tt === SchemaConstraintForeignkey::CONSTRAINTACTION_CASCADE) {
			return "CASCADE";
		}
		if ($constraintType_tt === SchemaConstraintForeignkey::CONSTRAINTACTION_SETNULL) {
			return "SET NULL";
		}
		new MsgError("242: Unknown action '$constraintType_tt'!");
		return "/*!!!!242!!!!*/";
	}

	/**
	 * @return DbErrorHandler
	 */
	protected function createErrorHandler() {
		return new MySqlErrorHandler();
	}

	/**
	 * @param string $tableName
	 * @param array $data {"foo":"bar",...}
	 * @return string
	 */
	public function insertByArray($tableName, $data) {
		new Error("Not implemented.197");
		return "";
	}

	/**
	 * @param Select[]|SchemaColumn[] $select
	 * @param From $from
	 * @param Join[] $join
	 * @param Where[] $where
	 * @param Groupby[] $groupBy
	 * @param Orderby[] $orderBy
	 * @param Limit|null $limit
	 * @return array|false
	 */
	public function selectGeneral1(array $select, From $from, array $join, array $where, array $groupBy = array(),
								   array $orderBy = array(), $limit = null) {

		$placeholders = array();

		$select_sql = $this->querybuilderSelect($select);
		$from_sql = $this->querybuilderFrom($from);
		$join_sql = $this->querybuilderJoins($join);
		$where_sql = $this->querybuilderWhere($where, $placeholders);
		$group_sql = $this->querybuilderGroupby($groupBy);
		$order_sql = $this->querybuilderOrder($orderBy);
		$limit_sql = $this->querybuilderLimit($limit);

		$query = $select_sql
			."\n\t".$from_sql
			.($join_sql?("\n\t".$join_sql):"")
			."\n\t".$where_sql
			.($group_sql?("\n\t".$group_sql):"")
			.($order_sql?("\n\t".$order_sql):"")
			.($limit_sql?("\n\t".$limit_sql):"")
		;

		return $this->select($query, $placeholders);
	}

	private function table_alias($tableName){
		if(!isset($this->tableAliasses[$tableName])){
			$this->tableAliasses[$tableName] = "t".count($this->tableAliasses);
		}
		return $this->tableAliasses[$tableName];
	}

	/**
	 * @param Select[]|SchemaColumn[] $select
	 * @return string
	 */
	private function querybuilderSelect(array $select){
		$select_sql = array();
		foreach ($select as $sel){
			if($sel instanceof SchemaColumn){
				$sel = new SelectColumn($sel);
			}
			$sql = "";
			if($sel instanceof SelectColumn) {
				$tblAlias = $this->table_alias($sel->getColumn()->getSchema()->getTableName());
				$sql = "$tblAlias.`" . $sel->getColumnName() . "`";
			}else if($sel instanceof SelectStar){
				$sql = "*";
				if($sel->getAlias()!==false){
					new Error("SELECT * AS ???");
				}
			}else{
				new Error("Unknown SELECT-Type ".get_class($sel));
			}
			if($sel->getAlias()!==false){
				$sql.=" AS \"".$sel->getAlias()."\"";
			}
			$select_sql[] = $sql;
		}
		return "/*General1*/SELECT ".implode(", ",$select_sql);
	}

	private function querybuilderFrom(From $from) {
		$sql = "";
		$tblAlias = "?";
		/** @noinspection PhpConditionAlreadyCheckedInspection */
		if($from instanceof FromTable){
			$sql= "`".$from->getSchemaName()."`";
			$tblAlias = $this->table_alias($from->getSchemaName());
		}else{
			new Error("Unknown FROM type ".get_class($from));
		}
		return "FROM $sql AS $tblAlias";
	}

	private function extendPlaceholders(array &$placeholders, $value) {
		$key = ":p".count($placeholders)."p";
		$placeholders[$key] = $value;
		return $key;
	}

	private function querybuilderWhere(array $where, array &$placeholders) {
		$where_sql = $this->querybuilderWhereArray($where, $placeholders);
		return "WHERE ".implode("\n\t\tAND ",$where_sql);
	}

	private function querybuilderJoins(array $joins) {
		if(!$joins)return "";
		$sql = array();
		foreach ($joins as $join){
			if ($join instanceof JoinLeft){
				$table1 = $this->table_alias($join->getTable1Column()->getSchema()->getTableName());
				$table2 = $join->getTable2Column()->getSchema()->getTableName();
				$alias = $this->table_alias($table2);
				$col1 = $join->getTable1Column()->getColName();
				$col2 = $join->getTable2Column()->getColName();
				$sql[] = "LEFT JOIN $table2 $alias ON $alias.`$col2` = $table1.`$col1`";
			}else{
				new Error("Unknown Join Statement '".basename(get_class($join))."'!");
			}
		}
		return implode("\n\t",$sql);
	}

	private function querybuilderGroupby(array $groupby) {
		if(!$groupby)return "";
		$sql = array();
		foreach ($groupby as $g){
			if($g instanceof GroupbyColumn){
				$column = $g->getColumn();
				$tblAlias = $this->table_alias($column->getSchema()->getTableName());
				$sql[] = $tblAlias.".`".$column->getColName()."`";
			}else{
				new Error("Unknown GroupBy '".basename(get_class($g))."'!");
			}
		}
		return "GROUP BY ".implode(",",$sql);
	}

	private function querybuilderWhereArray(array $where, array &$placeholders) {
		$where_sql = array();

		foreach ($where as $wh){
			if($wh instanceof WhereColumn) {
				if (!isset(self::$relations[$wh->getType()])) {
					new Error("Unknown relation " . $wh->getType() . "!");
				}
				$relation = self::$relations[$wh->getType()];
				$tblAlias = $this->table_alias($wh->getColumn()->getSchema()->getTableName());
				$name = $wh->getColumn()->getColName();
				$where_sql[] = "$tblAlias.`$name`" . $relation . $this->extendPlaceholders($placeholders, $wh->getValue());
			}else if($wh instanceof WhereOR){
				$where1 = $this->querybuilderWhereArray(array($wh->getWhere1()), $placeholders);
				$where2 = $this->querybuilderWhereArray(array($wh->getWhere2()), $placeholders);
				$where_sql[] = "( ".$where1[0]." OR ".$where2[0]." )";
			}else if($wh instanceof WhereAND){
				$where1 = $this->querybuilderWhereArray(array($wh->getWhere1()), $placeholders);
				$where2 = $this->querybuilderWhereArray(array($wh->getWhere2()), $placeholders);
				$where_sql[] = "( ".$where1[0]." AND ".$where2[0]." )";
			}else{
				new Error("Unknown WHERE type '".basename(get_class($wh))."'!");
			}
		}

		return $where_sql;
	}

	/**
	 * @param Limit $limit
	 * @return string
	 */
	private function querybuilderLimit($limit) {
		if ($limit===null) return "";
		return "LIMIT ".$limit->getLimit();
	}

	private function querybuilderOrder(array $orderBy) {
		if(!$orderBy)return "";
		$order_sql = array();
		foreach ($orderBy as $ord){
			if($ord instanceof OrderbyColumn){
				$tblAlias = $this->table_alias($ord->getColumn()->getSchema()->getTableName());
				$order_sql[] = $tblAlias.".`".$ord->getColumn()->getColName()."`"
				.($ord->isAsc()?"":" DESC")
				;
			}else{
				new Error("Unknown ORDER BY: ".basename(get_class($ord)));
			}
		}
		return "ORDER BY ".implode(", ",$order_sql);
	}

	/**
	 * @param string $name
	 * @param Schema $table
	 * @return bool
	 */
	private function syncCheckName($name, Schema $table) {
		return $name===$table->getTableName();
	}

	/**
	 * @param Schema $table
	 * @param string $comment
	 * @return string[] commands
	 */
	private function syncComment(Schema $table, $comment) {
		$commands=array();
		if($table->getTableComment()===$comment)return $commands;

		$name = $table->getTableName();
		$comment_new = $this->quoteString($table->getTableComment()===null?"":$table->getTableComment());
		$commands[] = "ALTER TABLE $name COMMENT=$comment_new;";

		return $commands;
	}

	/**
	 * @param Schema $table
	 * @param string $row
	 * @param bool $showWarnings
	 * @param string[] $all_columns
	 * @return string[] commands
	 */
	private function syncColKey(Schema $table, $row, $showWarnings, &$all_columns) {
		$commands = array();
#(new DbSchemaHandler())->getTableSchemaFromDb();
#(new DbSchemaHandler())->compareSchema();

		//Column
		/*If there is no rest after the type:*/if (preg_match("/^`.*?` [^ ]+\$/", $row)) {$row.=" ";}
		if (preg_match("/^`(.*?)` (.*?) (.*)/", $row, $matches)) {
			$name = $matches[1];
			unset($all_columns[$name]);
			$more_commands = $this->syncColumn($table, $name, $matches[2], $matches[3], $showWarnings);
			$commands = array_merge($commands, $more_commands);
//TODO:
//		} //Primary key
//		else if (preg_match("/^PRIMARY KEY \\(`(.*?)`\\)\$/", $row, $matches)) {
//			$this->getTableSchemaFromDb_desc_primaKey($table, explode('`,`', $matches[1]));
//		} //Key
//		else if (preg_match("/^(UNIQUE|FULLTEXT)? ?KEY `(.*?)` \\(`(.*?)`\\)\$/", $row, $matches)) {
//			$type = $matches[1];
//			$name = $matches[2];
//			$colnames = explode('`,`', $matches[3]);
//			$columns = ServiceSchema::columnsByNames($colnames);
//			$table->addIndex($columns, $type === 'UNIQUE', $name, $type === 'FULLTEXT');
//		} //Constraint
//		else if (preg_match("/^CONSTRAINT `(.*?)` FOREIGN KEY \\(`(.*?)`\\)"
//			. " REFERENCES `(.*?)` \\(`(.*?)`\\)"
//			. " ON DELETE (.*?) ON UPDATE (.*?)\$/", $row, $matches)) {
//			$name = $matches[1];
//			$colnames = explode('`,`', $matches[2]);
//			$refTable = $matches[3];
//			$refColnames = explode('`,`', $matches[4]);
//			$onDelete = $this->mapConstraintTypeTT($matches[5]);
//			$onUpdate = $this->mapConstraintTypeTT($matches[6]);
//
//			$key = new ForeignKey(
//				$table->getName(),
//				$name,
//				ServiceSchema::columnsByNames($colnames),
//				$refTable,
//				$refColnames
//			);
//			$key->setActions($onUpdate, $onDelete);
//			$table->addForeignKey($key);
		} else {
#TODO			if($showWarnings)new Warning($table->getTableName().": Ignored line: $row");
		}

		return $commands;
	}


	/**
	 * @param Schema $table
	 * @param string $name
	 * @param string $type
	 * @param string $rest
	 * @param bool $showWarnings
	 * @return string[] commands
	 */
	private function syncColumn(Schema $table, $name, $type, $rest, $showWarnings) {
		$commands = array();

		$col = $table->getTableColByName($name);
		if($col===false){
			if(ConfigProject::$DB_SYNC_COMPLETE){
				$commands[] = "ALTER TABLE `".$table->getTableName()."` DROP COLUMN `".$name."`;";
			}else{
				if($showWarnings)new MsgInfo($table->getTableName().".$name: Column not modelled!");
			}
		}

//TODO
#(new DbSchemaHandler())->getTableSchemaFromDb();
#(new DbSchemaHandler())->compareSchema();

		return $commands;
	}

}