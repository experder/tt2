<?php

namespace tt\features\database\v2\engines\mysql;

use tt\features\database\v2\engines\Queries;
use tt\features\database\v2\Schema;
use tt\features\database\v2\SchemaColumn;
use tt\features\database\v2\SchemaConstraint;
use tt\features\database\v2\SchemaConstraintForeignkey;

class MySqlQueries extends Queries
{

	/**
	 * @var DatabaseMySql
	 */
	protected $database;

	public function qShowTableStatus(){
		return $this->database->selectMysql("SHOW TABLE STATUS;");
	}

	public function queryShowCreate($table){
		$q = "SHOW CREATE TABLE `".$this->database->getSchema()."`.`$table`;";
		$result = $this->database->selectMysql($q);
		return $result[0]['Create Table'];
	}

	public function getCreateTable(Schema $schema) {
		$name = $schema->getTableName();

		$engine = "InnoDB";
		$charset = "utf8mb4";
		$collation = "utf8mb4_general_ci";

		$desc = array();

		//Primary key, pt.1
		$pk = $schema->getTableColPrimary();
		$desc[] = $this->getCreateColumn($pk);

		//Columns
		foreach ($schema->getTableCols() as $column) {
			$desc[] = $this->getCreateColumn($column);
		}

		//Primary key, pt.2
		$desc[] = "PRIMARY KEY (`" . $pk->getColName() . "`)";

		//Keys
		foreach ($schema->getConstraints() as $key) {
			$desc[] = $this->getCreateKey($key);
			if($key instanceof SchemaConstraintForeignkey){
				$desc[] = $this->getCreateForeignkey($key);
			}
		}

		$table_sql = implode(",\n\t", $desc);

		$comment = $schema->getTableComment()
			?" COMMENT=" . $this->database->quoteString($schema->getTableComment())
			: "";

		return array("CREATE TABLE `$name` (\n\t$table_sql\n)"
			. " ENGINE=$engine"
//			. " DEFAULT CHARSET=$charset"
//			. " COLLATE=$collation"
			. $comment
			. ";");
	}

	function getCreateColumn(SchemaColumn $column) {
		$name = $column->getColName();

		$type = $this->database->mapDataTypeOut($column->getDataType(), $column);

		$sign = $column->isSigned() ? "" : "unsigned";

		$nullable = $column->isNullable() ? "" : "NOT NULL";

		$default = $column->getDefault() !== null ?
			"DEFAULT " . $this->database->mapDefaultValueOut($column)
			: ($column->isNullable() ? "DEFAULT NULL" : "");

		$autoIncrement = $column->getAutoIncrement() ? "AUTO_INCREMENT" : "";

		$comment = $column->getComment()
			? "COMMENT " . $this->database->quoteString($column->getComment())
			: "";

		return "`$name` $type $sign $nullable $default $autoIncrement $comment";
	}

	private function getCreateKey(SchemaConstraint $key)
	{
		$type = $key->isUnique() ? "UNIQUE " : "";//($key->isFulltext() ? "FULLTEXT " : "");
		$name = $this->database->generateKeyName($key);
		$columns = "`" . implode("`,`", $key->getColumnNames()) . "`";
		return $type . "KEY `$name` ($columns)";
	}

	private function getCreateForeignkey(SchemaConstraintForeignkey $key)
	{
		$name = $this->database->generateKeyName($key);
		$colname = implode("`,`", $key->getColumnNames());
		$refTable = $key->getRefTable()->getTableName();
		$refCol = $key->getRefColumn()->getColName();
		$ondelete = $this->database->mapConstraintTypeOut($key->getOnDelete());
		$onupdate = $this->database->mapConstraintTypeOut($key->getOnUpdate());
		return "CONSTRAINT `$name` FOREIGN KEY (`$colname`)"
			. " REFERENCES `$refTable` (`$refCol`)"
			. " ON DELETE $ondelete ON UPDATE $onupdate";

	}

}