<?php

namespace tt\features\database\v2\engines;

use Exception;
use PDOStatement;

abstract class DbErrorHandler
{

	/**
	 * @param Exception $e
	 * @param string $debugInfo
	 * @return void
	 */
	abstract public function handle_exception(Exception $e, $debugInfo = "");

	/**
	 * @param string|PDOStatement $dump
	 * @return string
	 */
	abstract public function getCompiledQueryFromDebugDump($dump);

}