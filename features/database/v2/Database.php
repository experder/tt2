<?php

namespace tt\features\database\v2;

use tt\features\config\v1\ConfigProject;
use tt\features\database\v2\engines\DbErrorHandler;
use tt\features\database\v2\engines\Queries;
use tt\features\database\v2\querybuilder\From;
use tt\features\database\v2\querybuilder\Groupby;
use tt\features\database\v2\querybuilder\Join;
use tt\features\database\v2\querybuilder\Limit;
use tt\features\database\v2\querybuilder\Orderby;
use tt\features\database\v2\querybuilder\Select;
use tt\features\database\v2\querybuilder\Where;
use tt\features\debug\errorhandler\v1\Error;
use tt\features\debug\errorhandler\v2\Warning;
use tt\services\ServiceHtml;

abstract class Database
{

	const QUERY_TYPE_VOID = 0;
	const QUERY_TYPE_SELECT = 1;
	const QUERY_TYPE_INSERT = 2;
	const QUERY_TYPE_UPDATE = 3;
	const QUERY_TYPE_DELETE = 4;

	/**
	 * @return Database|null|false
	 */
	private static $globalDatabase = null;

	/**
	 * @var DbErrorHandler[] $errorHandlerSingletons
	 */
	private static $errorHandlerSingletons = array();

	/**
	 * @return Database|false
	 * @noinspection PhpReturnDocTypeMismatchInspection
	 */
	public static function getGlobalDatabase(){
		if(self::$globalDatabase===null){new Error("Database not initialized!");}
		return self::$globalDatabase;
	}

	public static function setGlobalDatabase(Database $database){
		if(self::$globalDatabase!==null)new Error("Database already initialized!");
		self::$globalDatabase = $database;
	}

	/**
	 * @return string
	 */
	abstract public function getSchema();

	/**
	 * @return true[] Keys: Table names
	 */
	abstract public function getAllTables();

	/**
	 * @param Schema $table
	 * @param bool $showWarnings
	 * @return string[] commands
	 */
	abstract public function syncTableWithModel(Schema $table, $showWarnings);

	/**
	 * @param Schema[] $model
	 * @param boolean $showWarnings
	 * @param boolean $dryrun
	 * @return string|void
	 */
	public function syncWithModel(array $model, $showWarnings = false, $dryrun = true)
	{
		$all_tables = $this->getAllTables();

		$commands = array();

		foreach ($model as $schema){
			$tableName = $schema->getTableName();
			if(isset($all_tables[$tableName])){
				$commands = array_merge($commands, $this->syncTableWithModel($schema, $showWarnings));
				unset($all_tables[$tableName]);
			}else{
				$commands = array_merge($commands, $this->getQueries()->getCreateTable($schema));
			}
		}
		if($showWarnings && ConfigProject::$DB_SYNC_COMPLETE){
			foreach ($all_tables as $key=>$dummy){
				new Warning("Table not modelled: $key");
			}
		}
		if(!$commands)return "";

		if($dryrun)return ServiceHtml::arrayToList_($commands);

		foreach ($commands as $command){
			$this->query($command, null, self::QUERY_TYPE_VOID);
		}
	}

	/**
	 * @return Queries
	 * @deprecated
	 */
	abstract public function getQueries();

	/**
	 * @param Select[]|SchemaColumn[] $select
	 * @param From $from
	 * @param Join[] $join
	 * @param Where[] $where
	 * @param Groupby[] $groupBy
	 * @param Orderby[] $orderBy
	 * @param Limit|null $limit
	 * @return array|false
	 */
	abstract public function selectGeneral1(array $select, From $from, array $join, array $where,
											array $groupBy=array(), array $orderBy=array(), $limit=null);

	/**
	 * @param string $query
	 * @param array|null $placeholders
	 * @return array|false
	 */
	protected function select($query, $placeholders = null)
	{
		return $this->query($query, $placeholders, self::QUERY_TYPE_SELECT);
	}

	/**
	 * @param string $query
	 * @param array|null $placeholders
	 * @return string lastInsertId
	 */
	protected function insert($query, $placeholders = null)
	{
		return $this->query($query, $placeholders, self::QUERY_TYPE_INSERT);
	}

	/**
	 * @param string $tableName
	 * @param array $data {"foo":"bar",...}
	 * @return string
	 */
	public abstract function insertByArray($tableName, $data);

	/**
	 * @param string $query
	 * @param array|null $placeholders
	 * @return int rowCount
	 */
	protected function update($query, $placeholders = null)
	{
		return $this->query($query, $placeholders, self::QUERY_TYPE_UPDATE);
	}

	/**
	 * @param string $query
	 * @param array|null $placeholders
	 * @return int rowCount
	 */
	protected function delete($query, $placeholders = null)
	{
		return $this->query($query, $placeholders, self::QUERY_TYPE_DELETE);
	}

	/**
	 * @param string $query
	 * @param array|null $placeholders
	 * @param int $type
	 * @return array|bool|string|int SELECT:array|false, INSERT:string, UPDATE/DELETE:int, VOID:true
	 */
	abstract protected function query($query, $placeholders, $type);

	public function getErrorHandler(){
		$classname = get_class($this);
		if(!isset(self::$errorHandlerSingletons[$classname])){
			self::$errorHandlerSingletons[$classname] = $this->createErrorHandler();
		}
		return self::$errorHandlerSingletons[$classname];
	}

	/**
	 * @return DbErrorHandler
	 */
	abstract protected function createErrorHandler();

}