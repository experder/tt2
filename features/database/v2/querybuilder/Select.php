<?php

namespace tt\features\database\v2\querybuilder;

abstract class Select
{

	/**
	 * @var string|false $alias
	 */
	protected $alias = false;

	/**
	 * @return false|string
	 */
	public function getAlias() {
		return $this->alias;
	}

}