<?php

namespace tt\features\database\v2\querybuilder;

abstract class Orderby
{

	const DESC = false;

	/**
	 * @var bool $asc
	 */
	protected $asc = true;

	/**
	 * @return bool
	 */
	public function isAsc() {
		return $this->asc;
	}

}