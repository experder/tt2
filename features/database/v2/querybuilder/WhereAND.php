<?php

namespace tt\features\database\v2\querybuilder;

class WhereAND extends Where
{

	/**
	 * @var Where $where1
	 */
	private $where1;
	/**
	 * @var Where $where2
	 */
	private $where2;

	/**
	 * @param Where $where1
	 * @param Where $where2
	 */
	public function __construct(Where $where1, Where $where2) {
		$this->where1 = $where1;
		$this->where2 = $where2;
	}

	/**
	 * @return Where
	 */
	public function getWhere1() {
		return $this->where1;
	}

	/**
	 * @return Where
	 */
	public function getWhere2() {
		return $this->where2;
	}

}