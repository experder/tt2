<?php

namespace tt\features\database\v2\querybuilder;

use tt\features\database\v2\Schema;
use tt\features\database\v2\SchemaColumn;

class JoinLeft extends Join
{

	/**
	 * @var SchemaColumn $target_column
	 */
	private $table2_column;
	/**
	 * @var SchemaColumn $source_column
	 */
	private $table1_column;

	/**
	 * @param SchemaColumn|Schema $table2_column
	 * @param SchemaColumn $table1_column
	 * @param string|false $alias
	 */
	public function __construct($table2_column, SchemaColumn $table1_column, $alias = false) {
		$this->alias = $alias;
		if($table2_column instanceof Schema)$table2_column=$table2_column->getTableColPrimary();
		$this->table2_column = $table2_column;
		$this->table1_column = $table1_column;
	}

	/**
	 * @return SchemaColumn
	 */
	public function getTable2Column() {
		return $this->table2_column;
	}

	/**
	 * @return SchemaColumn
	 */
	public function getTable1Column() {
		return $this->table1_column;
	}

}