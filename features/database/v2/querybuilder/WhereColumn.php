<?php

namespace tt\features\database\v2\querybuilder;

use tt\features\database\v2\SchemaColumn;

class WhereColumn extends Where
{

	const TYPE_EQUALS = "tt_equals";
	const TYPE_GREATER_EQUAL = "tt_greater_equal";
	const TYPE_SMALLER = "tt_smaller";

	/**
	 * @var string $type WhereColumn::TYPE_
	 */
	protected $type;

	/**
	 * @var SchemaColumn $column
	 */
	protected $column = null;

	/**
	 * @var mixed $value
	 */
	protected $value;

	/**
	 * @param string $type
	 * @param SchemaColumn $column
	 * @param $value
	 */
	public function __construct($type, $column, $value) {
		$this->type = $type;
		$this->column = $column;
		$this->value = $value;
	}

	/**
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * @return mixed
	 */
	public function getValue() {
		return $this->value;
	}

	/**
	 * @return SchemaColumn
	 */
	public function getColumn() {
		return $this->column;
	}

}