<?php

namespace tt\features\database\v2\querybuilder;

use tt\features\database\v2\SchemaColumn;

class SelectColumn extends Select
{

	/**
	 * @var SchemaColumn $column
	 */
	private $column;

	/**
	 * @param SchemaColumn $column
	 * @param string|false $alias
	 */
	public function __construct($column, $alias=false) {
		$this->column = $column;
		$this->alias = $alias;
	}

	/**
	 * @return string
	 */
	public function getColumnName() {
		return $this->column->getColName();
	}

	/**
	 * @return SchemaColumn
	 */
	public function getColumn() {
		return $this->column;
	}

}