<?php

namespace tt\features\database\v2\querybuilder;

class Limit
{

	/**
	 * @var int $limit
	 */
	private $limit;

	/**
	 * @param int $limit
	 */
	public function __construct($limit) {
		$this->limit = $limit;
	}

	/**
	 * @return int
	 */
	public function getLimit() {
		return $this->limit;
	}

}