<?php

namespace tt\features\database\v2\querybuilder;

use tt\features\database\v2\SchemaColumn;

class OrderbyColumn extends Orderby
{

	/**
	 * @var SchemaColumn $column
	 */
	private $column;

	/**
	 * @param bool $asc
	 * @param SchemaColumn $column
	 */
	public function __construct(SchemaColumn $column, $asc = true) {
		$this->asc = $asc;
		$this->column = $column;
	}

	/**
	 * @return SchemaColumn
	 */
	public function getColumn() {
		return $this->column;
	}

}