<?php

namespace tt\features\database\v2\querybuilder;

abstract class Join
{

	/**
	 * @var string|false $alias
	 */
	protected $alias = false;

}