<?php

namespace tt\features\database\v2\querybuilder;

use tt\features\database\v2\Schema;

class FromTable extends From
{

	/**
	 * @var Schema $schema
	 */
	private $schema;

	/**
	 * @param Schema $schema
	 */
	public function __construct(Schema $schema) {
		$this->schema = $schema;
	}

	/**
	 * @return string
	 */
	public function getSchemaName() {
		return $this->schema->getTableName();
	}

}