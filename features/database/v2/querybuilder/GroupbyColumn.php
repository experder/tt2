<?php

namespace tt\features\database\v2\querybuilder;

use tt\features\database\v2\SchemaColumn;

class GroupbyColumn extends Groupby
{

	/**
	 * @var SchemaColumn $column
	 */
	private $column;

	/**
	 * @param SchemaColumn $column
	 */
	public function __construct(SchemaColumn $column) {
		$this->column = $column;
	}

	/**
	 * @return SchemaColumn
	 */
	public function getColumn() {
		return $this->column;
	}

}