<?php

namespace tt\features\database\v2;

class SchemaColumn
{

	const DATATYPE_STRING = "tt_string";
	const DATATYPE_TEXT = "tt_text";
	const DATATYPE_INTEGER = "tt_integer";
	const DATATYPE_NUMERIC = "tt_numeric";
	const DATATYPE_DATE = "tt_date";
	const DATATYPE_DATETIME = "tt_datetime";
	const DATATYPE_BOOLEAN = "tt_boolean";
	const DATATYPE_BLOB = "tt_blob";
	const DATATYPE_ENUM = "tt_enum";

	/**
	 * @var string $comment
	 */
	private $comment = "";

	/**
	 * @var bool $signed
	 */
	private $signed = true;

	/**
	 * @var mixed $default
	 */
	private $default = null;

	/**
	 * @var int $maxLength
	 */
	private $maxLength = 0;

	/**
	 * @var string $dataType SchemaColumn::DATATYPE_...
	 */
	private $dataType;

	/**
	 * @var string $colName
	 */
	private $colName;

	/**
	 * @var Schema $schema
	 */
	private $schema;

	/**
	 * @var bool $nullable
	 */
	private $nullable = true;

	/**
	 * @var bool
	 */
	private $autoIncrement = false;

	/**
	 * @param Schema $schema
	 * @param string $colName
	 * @param string $dataType
	 */
	public function __construct(Schema $schema, $colName, $dataType)
	{
		$this->schema = $schema;
		$this->colName = $colName;
		$this->dataType = $dataType;
	}

	/**
	 * @param SchemaColumn|string $refColumn reference column or classname
	 * @return $this
	 */
	public function addForeignKey($refColumn)
	{
		if(is_string($refColumn)){
			$refColumn = Schema::getSingleton($refColumn)->getTableColPrimary();
		}
		$this->schema->addConstraint(new SchemaConstraintForeignkey(array($this), $refColumn));
		return $this;
	}

	public function addIndexUnique()
	{
		$this->schema->addConstraint(new SchemaConstraintUniquekey(array($this)));
		return $this;
	}

	/**
	 * @return $this
	 */
	public function setNotNullable()
	{
		$this->nullable = false;
		return $this;
	}

	/**
	 * @return SchemaColumn
	 */
	public function setAutoIncrement()
	{
		$this->autoIncrement = true;
		return $this;
	}

	/**
	 * @param string $comment
	 * @return SchemaColumn
	 */
	public function setComment($comment)
	{
		$this->comment = $comment;
		return $this;
	}

	/**
	 * @return SchemaColumn
	 */
	public function setUnsigned()
	{
		$this->signed = false;
		return $this;
	}

	/**
	 * @param mixed $default
	 * @return SchemaColumn
	 */
	public function setDefault($default)
	{
		$this->default = $default;
		return $this;
	}

	/**
	 * @param int $maxLength
	 * @return SchemaColumn
	 */
	public function setMaxLength($maxLength)
	{
		$this->maxLength = $maxLength;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getComment()
	{
		return $this->comment;
	}

	/**
	 * @return bool
	 */
	public function isSigned()
	{
		return $this->signed;
	}

	/**
	 * @return mixed
	 */
	public function getDefault()
	{
		return $this->default;
	}

	/**
	 * @return int
	 */
	public function getMaxLength()
	{
		return $this->maxLength;
	}

	/**
	 * @return string
	 */
	public function getDataType()
	{
		return $this->dataType;
	}

	/**
	 * @return string
	 */
	public function getColName()
	{
		return $this->colName;
	}

	/**
	 * @return Schema
	 */
	public function getSchema()
	{
		return $this->schema;
	}

	/**
	 * @return bool
	 */
	public function isNullable()
	{
		return $this->nullable;
	}

	/**
	 * @return bool
	 */
	public function getAutoIncrement()
	{
		return $this->autoIncrement;
	}

}