<?php
/**  @noinspection PhpUnused TODO */
/**  @noinspection PhpPropertyOnlyWrittenInspection TODO */

namespace tt\features\database\v2;

class SchemaConstraintForeignkey extends SchemaConstraint
{

	const CONSTRAINTACTION_NOACTION = "tt_noaction";
	const CONSTRAINTACTION_CASCADE = "tt_cascade";
	const CONSTRAINTACTION_RESTRICT = "tt_restrict";
	const CONSTRAINTACTION_SETNULL = "tt_setnull";
	const CONSTRAINTACTION_SETDEFAULT = "tt_setdefault";

	/**
	 * @var SchemaColumn
	 */
	private $refColumn;

	/**
	 * @var string $onUpdate SchemaConstraintForeignkey::CONSTRAINTACTION_...
	 */
	private $onUpdate = self::CONSTRAINTACTION_RESTRICT;
	/**
	 * @var string $onDelete SchemaConstraintForeignkey::CONSTRAINTACTION_...
	 */
	private $onDelete = self::CONSTRAINTACTION_RESTRICT;

	/**
	 * @param SchemaColumn[] $columns
	 * @param SchemaColumn $refColumn
	 */
	public function __construct(array $columns, $refColumn)
	{
		parent::__construct($columns);
		$this->refColumn = $refColumn;
	}

	/**
	 * @param string $onUpdate
	 * @param string $onDelete
	 * @return $this
	 */
	public function setActions($onUpdate, $onDelete)
	{
		$this->onUpdate = $onUpdate;
		$this->onDelete = $onDelete;
		return $this;
	}

	/**
	 * @return SchemaColumn
	 */
	public function getRefColumn()
	{
		return $this->refColumn;
	}

	/**
	 * @return Schema
	 */
	public function getRefTable()
	{
		return $this->refColumn->getSchema();
	}

	/**
	 * @return string
	 */
	public function getOnUpdate()
	{
		return $this->onUpdate;
	}

	/**
	 * @return string
	 */
	public function getOnDelete()
	{
		return $this->onDelete;
	}

}