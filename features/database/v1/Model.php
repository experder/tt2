<?php

namespace tt\features\database\v1;

use tt\core\database\Database;
use tt\features\config\v1\CFG_S;
use tt\features\css\Stylesheet;
use tt\features\database\schema\Column;
use tt\features\database\schema\ForeignKey;
use tt\features\database\schema\Table;
use tt\features\debug\errorhandler\v1\Error;
use tt\features\debug\errorhandler\v2\Warning;
use tt\features\htmlpage\components\Form;
use tt\features\htmlpage\components\HtmlCompFormInput;
use tt\features\htmlpage\components\HtmlComponent;
use tt\features\modulehandler\v1\ModulehandlerV1;
use tt\services\ServiceArrays;

/**
 * @deprecated
 */
abstract class Model
{

	const VALUE_DB_DEFAULT = "tt_!VAL_NOT_SET!_";

	public function __construct()
	{
	}

	public static function getClassModel()
	{
		/** @noinspection PhpFullyQualifiedNameUsageInspection */
		return \tt\services\polyfill\Php5::get_class();
	}

	/**
	 * @var string $tableName
	 */
	protected $tableName = null;
	/**
	 * @var string $tableComment
	 */
	protected $tableComment = "";

	/**
	 * @var int $id
	 */
	protected $id = null;
	const FIELD_id = 'id';

	/**
	 * @var Table $schema
	 */
	private $schema = null;
	/**
	 * @var Form $editForm
	 */
	private $editForm = null;

	/**
	 * @param string $table_name
	 * @return Model|false
	 */
	public static function getModelByTableName($table_name)
	{
		foreach (ModulehandlerV1::getModules() as $module) {
			foreach ($module->getModelSchema() as $model) {
				if($model instanceof Model){
					if ($model->getTableName() === $table_name) return $model;
				}
			}
		}
		return false;
	}

	/**
	 * @return string
	 */
	public function getTableName()
	{
		if ($this->tableName === null) {
			$this->tableName = basename(get_class($this));
		}
		return $this->tableName;
	}

	abstract function getGuiName();

	/**
	 * @return string
	 */
	public function getTableComment()
	{
		return $this->tableComment;
	}

	public function getDataAll($skip_db_default_values = true)
	{
		$data = $this->getDataArray($this->getSchema()->getColumnNames());
		if (!$skip_db_default_values) return $data;
		foreach ($data as $key => $value) {
			if ($value === Model::VALUE_DB_DEFAULT) {
				unset($data[$key]);
			}
		}
		return $data;
	}

	public function getDataArray(array $keys)
	{
		$values = array();
		foreach ($keys as $key) {
			$values[$key] = $this->getData($key);
		}
		return $values;
	}

	public function getData($key)
	{
		if (!isset($this->$key)) return null;
		return $this->$key;
	}

	public function getDataToReadable($key)
	{
		if (!isset($this->$key)) return null;
		$value = $this->$key;
		$column = $this->getSchema()->getColumn($key);
		if ($column === false) new Error("Key does not exist (get)! " . basename(get_class($this)) . ": " . $key);
		$type = $column->getDataType();
		if ($type === Column::DATATYPE_DATE) {
			$value = date("d.m.Y", strtotime($value));
		}
		return $value;
	}

	public function setDataFromReadable($key, $value)
	{
		$column = $this->getSchema()->getColumn($key);
		if ($column === false) new Error("Key does not exist (set)! " . basename(get_class($this)) . ": " . $key);
		$type = $column->getDataType();
		if ($type === Column::DATATYPE_DATE) {
			$value = date("Y-m-d", strtotime($value));
		}
		if ($value === Form::TT_NULL_VALUE) $value = null;
		$this->setData($key, $value);
	}

	public function setData($key, $value)
	{
		$this->$key = $value;
	}

	public function setDataArray(array $data)
	{
		foreach ($data as $key => $value) {
			$this->setData($key, $value);
		}
	}

	public function getEditForm()
	{
		if ($this->editForm === null) {
			$this->editForm = $this->createEditForm();
			if ($this->editForm === null) {
				$this->editForm = Form::fromModel($this);
				$this->changeDefaultEditForm($this->editForm);
			}
		}
		return $this->editForm;
	}

	private function createEditForm()
	{
		return null;
	}

	/**
	 * @param Form $editForm
	 * @return void
	 */
	public function changeDefaultEditForm(Form $editForm)
	{
	}

	public function getSchema()
	{
		if ($this->schema === null) {
			$this->schema = $this->createSchema();
			if ($this->schema === null) {
				$this->schema = Table::fromModel($this);
				$this->changeDefaultSchema($this->schema);
			}
		}
		return $this->schema;
	}

	/**
	 * @return Table|null
	 */
	public function createSchema()
	{
		return null;
	}

	/**
	 * @param Table $table
	 * @return void
	 */
	abstract function changeDefaultSchema(Table $table);

	/**
	 * @return string[]
	 */
	public function getColumns($id = true)
	{
		$internal_cols = array(
			"tableName",
			"tableComment",
			"schema",
			"editForm",
		);
		if (!$id) $internal_cols[] = Model::FIELD_id;
		return array_diff(array_keys(get_object_vars($this)), $internal_cols);
	}

	/**
	 * @deprecated
	 */
	public function persist(DatabaseGeneral $db = null){
		return $this->persistStrict($db, false);
	}
	/**
	 * @param DatabaseGeneral|null $db
	 * @return false|string|int
	 */
	public function persistStrict(DatabaseGeneral $db = null, $strict=true)
	{
		if ($db === null) $db = DatabaseHandler::getDefaultDb();

		//No id? -> Insert.
		$key = $this->getSchema()->getPrimaryKey();
		if (count($key) > 1) new Error("Not developed. Multi-column primary key. Table: " . $this->getTableName());
		$idkey = $key[0]->getName();
		$id = $this->getData($idkey);
		if ($id === null) {
			$data = $this->getDataAll();
			foreach ($data as $key => $value) {
				if ($value instanceof Model) {
					$data[$key] = $value->getId();
				}
			}
			$id = $db->insertByArray($this->getTableName(), $data);
			$this->setId($id);
			return $id;
		}

		$new = $this->getDataAll();

		$model = new $this();
		if (!($model instanceof Model)) return false;
		$model->fromDbWhereEqualsId($id);
		$old = $model->getDataAll();

		$delta = $this->calcUpdate($old, $new, $strict);
		if (!$delta) return 0;

		/** @noinspection PhpUnnecessaryLocalVariableInspection */
		$cols_updated = $db->updateByArray($this->getSchema()->getName(), $delta,
			array(new WhereEquals($idkey, $id)));
		return $cols_updated;
	}

	/**
	 * @param array $where_array key-value array
	 * @param DatabaseGeneral|null $db
	 * @param string[] $subset
	 * @return bool
	 */
	public function fromDbWhereEquals($where_array, $subset = true, $db = null)
	{
		$joinWhereOrder = array();
		foreach ($where_array as $key => $value) {
			$joinWhereOrder[] = new WhereEquals($key, $value);
		}

		return $this->fromDbWhere($joinWhereOrder, $subset, $db);
	}

	/**
	 * @param array $joinWhereOrder
	 * @param string[] $subset
	 * @param DatabaseGeneral|null $db
	 * @return bool
	 */
	public function fromDbWhere($joinWhereOrder, $subset = true, $db = null)
	{
		if ($db === null) $db = DatabaseHandler::getDefaultDb();

		if ($subset === true) {
			$subset = $this->getColumns();
		}
		$result = $db->generalQuery($subset, $this->getTableName(), $joinWhereOrder);
		if (!$result) return false;
		if (count($result) > 1 && CFG_S::$DEVMODE) {
			new Warning("Multiple entries for " . $this->getTableName() . "!");
		}
		$this->setDataArray($result[0]);
		return true;
	}

	public function fromDbWhereEqualsId($id)
	{
		$this->fromDbWhereEquals(array(Model::FIELD_id => $id));
		return $this;
	}

	/**
	 * @param int $id
	 * @return $this
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param string $modelClass
	 * @param array $joinWhereOrder see WhereCondition, OrderClause
	 * @param Database $db
	 * @return Model[]|false
	 * @see WhereCondition, OrderClause
	 */
	public static function fromDatabase($modelClass, array $joinWhereOrder, Database $db = null)
	{
		if ($db === null) $db = DatabaseHandler::getDefaultDb();
		$model_array = array();

		$model = new $modelClass();
		if (!($model instanceof Model)) return false;

		$data = $db->generalQuery(
			$model->getSchema()->getColumnNames(true),
			$model->getTableName(),
			$joinWhereOrder
		);

		foreach ($data as $row) {
			$instance = new $modelClass();
			if (!($instance instanceof Model)) continue;
			$instance->setDataArray($row);
			$model_array[] = $instance;
		}

		return $model_array;
	}

	private function calcUpdate(array $old, array $new, $strict)
	{
		$delta = array();
		foreach ($new as $key => $value) {
			$set = $strict?$old[$key] !== $value:($old[$key] != $value);
			if ($set) {
				$delta[$key] = $value;
			}
		}
		return $delta;
	}

	public function getDefaultFormat()
	{
		return $this->getGuiName() . " #{" . Model::FIELD_id . "}";
	}

	/**
	 * @param Column $column
	 * @param ForeignKey|null $foreignKey
	 * @return HtmlCompFormInput|null|HtmlCompFormInput[]
	 */
	public function getEditFormField(Column $column, $foreignKey)
	{
		$colname = $column->getName();
		return (HtmlComponent::fromColumn(
			$column,
			$this->getDataToReadable($colname),
			$foreignKey
		));
	}

	/**
	 * @return Stylesheet[]
	 */
	public function getEditFormCss()
	{
		return array();
	}

	public function getEditFormJs()
	{
		return array();
	}

	/**
	 * @param array $whereOrder
	 * @return array Key-Value array usind default format of this model
	 */
	public function getDefaultFormattedList($whereOrder = array())
	{
		$db = DatabaseHandler::getDefaultDb();
		$format = $this->getDefaultFormat();
		preg_match_all("/\\{(.*?)}/", $format, $matches);
		$keys = $matches[1];
		if (!in_array(Model::FIELD_id, $keys)) $keys = array_merge($keys, array(Model::FIELD_id));

		$options_result = $db->generalQuery(
			$keys,
			$this->getTableName(),
			$whereOrder
		);
		return ServiceArrays::keyValueArray($options_result, $format);
	}

	public function fromPost()
	{
		foreach ($this->getSchema()->getColumnNames() as $col_name) {
			if (isset($_POST[$col_name])) {
				$this->setDataFromReadable($col_name, $_POST[$col_name]);
			}
		}
	}

	public function editFormPostProcess()
	{
		$this->fromPost();
		$this->persistStrict();
		return null;
	}

	/**
	 * @param string $FIELD
	 * @return string
	 */
	public function getGuiNameOfColumn($FIELD)
	{
		return $this->getSchema()->getColumn($FIELD)->getGuiName();
	}

}