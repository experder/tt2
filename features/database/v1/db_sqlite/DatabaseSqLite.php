<?php

namespace tt\features\database\v1\db_sqlite;

use Exception;
use PDO;
use tt\features\database\v1\DatabaseGeneral;
use tt\features\database\v1\DatabaseGeneralSchema;
use tt\features\database\v1\Model;
use tt\features\debug\errorhandler\v1\Error;
use tt\features\debug\errorhandler\v1\Warning;
use tt\services\ServiceDebug;

class DatabaseSqLite extends DatabaseGeneral
{

	const QUERY_TYPE_VOID = 0;
	const QUERY_TYPE_SELECT = 1;
	const QUERY_TYPE_INSERT = 2;

	/**
	 * @var PDO
	 */
	private $pdo = null;

	/**
	 * @var DbQueries $queryHandler
	 */
	private $queryHandler;

	/**
	 * @var DatabaseGeneralSchema $schemaHandler
	 */
	private $schemaHandler = null;

	public function __construct($file)
	{
		parent::__construct($file);
		$this->queryHandler = new DbQueries($this);
	}

	/**
	 * @return DatabaseGeneralSchema
	 */
	function getSchemaHandler()
	{
		if($this->schemaHandler===null){
			$this->schemaHandler = new DbSchemaHandler($this);
		}
		return $this->schemaHandler;
	}

	function connect($password)
	{
		try {
			$initialize = !file_exists($this->host);
			$this->pdo = new PDO("sqlite:".$this->host);
			if($initialize){
				new Warning("Empty SQLite database '$this->host' created.");
			}
			$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->pdo->query('PRAGMA foreign_keys = ON;');
		}catch (Exception $e) {
			DbErrorHandling::handle_exception($e);
		}
	}

	/**
	 * @param string|string[] $string
	 * @return true Terminates with an error if execution fails
	 */
	public function execute($string)
	{
		if(is_array($string)){
			foreach ($string as $str){
				$this->execute($str);
			}
			return true;
		}
		$this->query($string, DatabaseSqLite::QUERY_TYPE_VOID);
		return true;
	}

	/**
	 * @param string $sql_query
	 * @param array $placeholders
	 * @return array
	 */
	public function select($sql_query, $placeholders=null){
		return $this->query($sql_query, self::QUERY_TYPE_SELECT, $placeholders);
	}

	/**
	 * @param string $sql_query
	 * @param array $placeholders
	 * @return string
	 */
	public function insert($sql_query, $placeholders=null){
		return $this->query($sql_query, self::QUERY_TYPE_INSERT, $placeholders);
	}

	public function query($sql_query, $type, $placeholders=null){
		try{
			$statement = $this->pdo->prepare($sql_query);
			@$statement->execute($placeholders);
		}catch (Exception $e) {
			DbErrorHandling::handle_exception($e, $sql_query, 1);
		}
		if($type===self::QUERY_TYPE_SELECT){
			return $statement->fetchAll(PDO::FETCH_ASSOC);
		}
		if($type===self::QUERY_TYPE_INSERT){
			return $this->pdo->lastInsertId();
		}
		if($type===self::QUERY_TYPE_VOID){
			return true;
		}
		new Error("Unknown Query Type!");
		return false;
	}

	/**
	 * @param Model $model
	 * @return false|string
	 */
	public function persistInsertModel(Model $model)
	{
		$data = $model->getDataAll();
		return $this->queryHandler->insert_byAssocArray(
			$model->getTableName(),
			$data
		);
	}

	/**
	 * @return DbQueries
	 */
	public function getQueryHandler()
	{
		return $this->queryHandler;
	}

}