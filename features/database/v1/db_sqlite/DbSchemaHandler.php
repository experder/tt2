<?php

namespace tt\features\database\v1\db_sqlite;

use tt\features\database\schema\Column;
use tt\features\database\schema\ForeignKey;
use tt\features\database\schema\Key;
use tt\features\database\schema\ServiceSchema;
use tt\features\database\schema\Table;
use tt\features\database\v1\DatabaseGeneralSchema;
use tt\features\debug\errorhandler\v1\Warning;
use tt\services\DEBUG;

class DbSchemaHandler extends DatabaseGeneralSchema
{

	public $engineSupportsCommentsOnColumns = false;

	private $dbQueries;

	private $redefineTables = array();

	public function __construct(DatabaseSqLite $db)
	{
		parent::__construct($db);
		$this->dbQueries = $db->getQueryHandler();
	}

	/**
	 * @param string $comment
	 * @return string
	 */
	function comment($comment)
	{
		$comment = str_replace("\r","\\r",$comment);
		$comment = str_replace("\n","\\n",$comment);
		return "-- $comment";
	}

	/**
	 * @param string $tableName
	 * @param Column $columnSchema
	 * @return string
	 */
	function cmdAddColumn($tableName, Column $columnSchema)
	{
		$col = $this->cmdCreateColumn($columnSchema);
		return "ALTER TABLE $tableName ADD $col;";
	}

	/**
	 * @param Table $schema
	 * @return string[]
	 */
	function cmdCreateTable(Table $schema)
	{
		$commands = array();
		$name = $schema->getName();
		$desc = array();

		//Primary Key
		$pk = $schema->getPrimaryKey();
		if(count($pk)>1){
			new Warning("Not implemented: Multi column primary key! ($name: ".implode(",",ServiceSchema::columnNames($pk)).")");
		}else{
			$desc[] = $pk[0]->getName()." INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT";
		}

		//Columns
		foreach ($schema->getColumns() as $column){
			$desc[] = $this->cmdCreateColumn($column);
		}

		//Constraints
		foreach ($schema->getForeignKeys() as $foreignKey){
			$desc[] = $this->cmdCreateConstraint($foreignKey);
		}

		$descSql = implode(",\n\t", $desc);
		$commands[] = "CREATE TABLE $name (\n\t$descSql\n);";

		//Keys
		foreach ($schema->getKeys() as $key){
			$commands[] = $this->cmdCreateIndex($key, $name);
		}

		return $commands;
	}

	private function cmdCreateIndex(Key $key, $tableName){
		$name = $key->getName();
		$unique = $key->isUnique()?"UNIQUE":"";
		$cols = implode(",",ServiceSchema::columnNames($key->getColumns()));
		return "CREATE $unique INDEX $name ON $tableName ($cols);";
	}

	private function cmdCreateColumn(Column $column){
		$name = $column->getName();
		$type = $this->mapDataTypeSQL($column->getDataType(), $column);
		$default = $column->getDefault()?"DEFAULT ".$column->getDefault():"";
		$nullable = $column->isNullable()?"":"NOT NULL";
		return "$name $type $default $nullable";
	}

	private function cmdCreateConstraint(ForeignKey $foreignKey){
		$name = $foreignKey->getName();
		$cols = implode(",",ServiceSchema::columnNames($foreignKey->getColumns()));
		$refTable = $foreignKey->getRefTable();
		$refCols = implode(",",$foreignKey->getRefColumns());
		$onDelete = $this->mapConstraintActionSQL($foreignKey->getOnDelete());
		$onUpdate = $this->mapConstraintActionSQL($foreignKey->getOnUpdate());
		return "CONSTRAINT $name FOREIGN KEY ($cols)
    	    REFERENCES $refTable($refCols)
    	    ON DELETE $onDelete ON UPDATE $onUpdate";
	}

	private function mapConstraintActionSQL($action_tt){
		if($action_tt===ForeignKey::CONSTRAINTACTION_RESTRICT){
			return "RESTRICT";
		}
		return DEBUG::invalidState("!UNKNOWN_CONSTRAINT_ACTION_TT_SQLITE '$action_tt'!");
	}

	/*
	 * https://sqlite.org/foreignkeys.html#fk_actions
	 */
	private function mapConstraintActionTT($action_sql){
		if($action_sql==="RESTRICT"){
			return ForeignKey::CONSTRAINTACTION_RESTRICT;
		}
		return DEBUG::invalidState("!UNKNOWN_CONSTRAINT_ACTION_SQLITE '$action_sql'!");
	}

	/**
	 * @param string $datatype_tt
	 * @return string
	 */
	function mapDataTypeDb($datatype_tt)
	{
		return $this->mapDataTypeSQL($datatype_tt);
	}
	private function mapDataTypeSQL($datatype_tt, Column $column=null){
		if($datatype_tt===Column::DATATYPE_STRING){
			$maxlength = $column&&$column->getStringMaxlength()?"(".$column->getStringMaxlength().")":"";
			return "TEXT$maxlength";
		}
		if($datatype_tt===Column::DATATYPE_INTEGER){
			return "INTEGER";
		}
		return DEBUG::invalidState("!UNKNOWN_DATATYPE_TT '$datatype_tt'!");
	}
	private function mapDataTypeTT($datatype_sql, Column $column){
		if($datatype_sql==="INTEGER"){
			$column->setDataTypeInteger();
		}else if($datatype_sql==="TEXT"){
			$column->setDataTypeString();
		}else if(preg_match("/^TEXT\\((\\d+)\\)\$/",$datatype_sql,$matches)){
			$column->setDataTypeString($matches[1]);
		}else{
			new Warning("Unknown data type: $datatype_sql");
		}
	}

	/**
	 * @param string $tableName
	 * @param Column $columnSchema
	 * @return false
	 */
	function cmdUpdateColumn($tableName, Column $columnSchema)
	{
		/*
		 * SQLite does not support Update of columns.
		 * https://gitlab.com/experder/tt2/-/tree/main/features/database/v1/db_sqlite/README.md#complete-alter-table-support
		 */
		$this->redefineTables[$tableName] = true;
		return false;
	}

	/**
	 * @param string $table
	 * @return Table|false FALSE if table does not exist
	 */
	function getTableSchemaFromDb($table)
	{
		$schema = new Table($table);
		$create_cmd = $this->dbQueries->query_show_create($table);
		if($create_cmd===false)return false;

		//Cols and Constraints
		if(preg_match("/^CREATE TABLE $table \\(\\n\\t(.*?)\\)\$/s", $create_cmd, $matches)){
			$sql = explode(",\n\t",$matches[1]);
			foreach ($sql as $col_sql){
				$this->getColFromDb($col_sql, $schema);
			}
		}else{
			new Warning("Couldn't parse create command: $create_cmd");
		}

		//Indexes
		$indexes = $this->dbQueries->query_indexes($table);
		foreach ($indexes as $idx_sql){
			$sql = $idx_sql['sql'];
			if(preg_match("/^CREATE (UNIQUE)? ?INDEX (.*?) ON $table \\((.*?)\\)\$/", $sql, $matches)){
				$unique = $matches[1]==="UNIQUE";
				$name = $matches[2];
				$targetCols = ServiceSchema::columnsByNames(explode(",",$matches[3]));
				$schema->addIndex($targetCols, $unique, $name);
			}else{
				new Warning("Couldn't parse index: $sql");
			}
		}

		return $schema;
	}

	private function getColFromDb($sql, Table $table){
		if(preg_match("/^(.*?) INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT\$/",$sql,$matches)){
			$table->setPrimaryKey($matches[1]);
		}else if(preg_match("/^CONSTRAINT (.*?) FOREIGN KEY \\((.*?)\\)"
			."[\\r\\n \\t]+REFERENCES (.*?)\\((.*?)\\)"
			."[\\r\\n \\t]+ON DELETE (.*?) ON UPDATE (.*?)\$/",$sql,$matches)){
			$name = $matches[1];
			$cols = ServiceSchema::columnsByNames(explode(",",$matches[2]));
			$refTable = $matches[3];
			$refCols = explode(",",$matches[4]);
			$onDelete = $this->mapConstraintActionTT($matches[5]);
			$onUpdate = $this->mapConstraintActionTT($matches[6]);
			/** @noinspection PhpUnusedLocalVariableInspection $temp for compatibility reasons */
			$table->addForeignKey(
				($temp=new ForeignKey($table->getName(), $name,$cols,$refTable,$refCols))
					->setActions($onUpdate,$onDelete)
			);
		}else if(preg_match("/^(.*?) (.*?) ?(?:DEFAULT (.*?))? ?(NOT NULL)?\$/",$sql,$matches)){
			$name = $matches[1];
			$type = $matches[2];
			$default = count($matches)>3?$matches[3]:null;
			$nullable = count($matches)<5;
			$table->addColumn($col=new Column($name,null,$nullable,$default));
			$this->mapDataTypeTT($type,$col);
		}else{
			new Warning("Couldn't parse column: $sql");
		}
	}

	function finishComparsion(Table $actual, Table $target)
	{
		$tableName = $target->getName();
		if(!isset($this->redefineTables[$tableName]))return null;

		$commands = array();

		//Create temporary table
		$tempTable = $tableName."_temp";

		//Drop indexes and rename table
		foreach ($actual->getKeys() as $key){
			$name = $key->getName();
			$commands[] = "DROP INDEX $name;";
		}
		$commands[] = "ALTER TABLE $tableName RENAME TO $tempTable;";

		//Create new table
		foreach ($this->cmdCreateTable($target) as $cmd){
			$commands[] = $cmd;
		}

		//Copy data
		$pks = $target->getPrimaryKey();
		if(!$pks || count($pks)!==1){
			new Warning("Multi columns primary key not supported.");
		}else{
			$keys = array($pks[0]->getName());
			//Determine common columns:
			$actual_keys = ServiceSchema::columnNames($actual->getColumns());
			foreach (ServiceSchema::columnNames($target->getColumns()) as $key){
				if(in_array($key, $actual_keys))$keys[]=$key;
			}
			$keys_sql = implode(",",$keys);
			$commands[] = "INSERT INTO $tableName ($keys_sql)"
				." SELECT $keys_sql"
				." FROM $tempTable";

			//Remove temporary table
			$commands[] = "DROP TABLE $tempTable;";

		}

		unset($this->redefineTables[$tableName]);
		return $commands;
	}

}