

# FAQ
https://www.sqlite.org/faq.html

Foreign keys:
https://sqlite.org/foreignkeys.html

## Complete ALTER TABLE support
https://www.sqlite.org/omitted.html


# PDO
`PHP.ini`:
> ;Windows:  
> extension="C:/Programme/PHP/ext/php_pdo_sqlite.dll"  

> ;Linux:  
> extension=pdo_sqlite

https://www.php.net/manual/en/ref.pdo-sqlite.php

`httpd.conf`:
> LoadFile "C:/Programme/PHP/libsqlite3.dll"


# See also
## Example server configuration
See [ConfigServer.md](../../../config/v1/ConfigServer.md)
