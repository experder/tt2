<?php

namespace tt\features\database\v1\db_sqlite;

use tt\services\ServiceStrings;

class DbQueries
{

	/**
	 * @var DatabaseSqLite $db
	 */
	private $db;

	/**
	 * @param DatabaseSqLite $db
	 */
	public function __construct(DatabaseSqLite $db)
	{
		$this->db = $db;
	}

	/**
	 * @param string $table
	 * @param array $array
	 * @return false|string
	 *
	 * @noinspection DuplicatedCode
	 * @see \tt\features\database\v1\db_mysql\DbQueries::insert_byAssocArray()
	 */
	public function insert_byAssocArray($table, $array){
		$keys_array = array_keys($array);
		$keys_prefixed = ServiceStrings::prefixValues(':', $keys_array);

		$substitutions = array();
		foreach ($array as $key => $value) {
			$substitutions[':' . $key] = $value;
		}

		$keys = implode(",", $keys_array);
		$values = implode(",", $keys_prefixed);
		return $this->db->insert(
			"INSERT INTO $table ($keys) VALUES ($values);",
			$substitutions
		);
	}

	/**
	 * @param string $table
	 * @return false|string
	 */
	public function query_show_create($table){
		$result = $this->db->select(
			"SELECT sql FROM SQLITE_SCHEMA WHERE type='table' AND tbl_name='$table';"
		);
		if(!$result)return false;
		return $result[0]['sql'];
	}

	public function query_indexes($table){
		$result = $this->db->select(
			"SELECT sql FROM sqlite_master WHERE type = 'index' AND tbl_name = '$table';"
		);
		if(!$result)return array();
		return $result;
	}


}