<?php

namespace tt\features\database\v1\db_sqlite;

use Exception;
use PDOException;
use tt\features\config\v1\CFG_S;
use tt\features\debug\errorhandler\v1\Error;
use tt\services\ServiceStrings;

class DbErrorHandling
{

	public static function handle_exception(Exception $e, $debugInfo="", $cut=0){
		if ($e instanceof PDOException) {
			if($e->getMessage()==="could not find driver"){
				new Error("Please install SQLite (PDO) driver!"
					."\nhttps://gitlab.com/experder/tt2/-/blob/main/features/database/v1/db_sqlite/README.md#pdo");
			}

			$error_code = false;
			$error_message = "";

			if(isset($e->errorInfo) && is_array($e->errorInfo) && count($e->errorInfo)>=3){
				$error_code = $e->errorInfo[1];
				$error_message = $e->errorInfo[2];
			}

			if($error_code===1){
				if (ServiceStrings::endsWith(": syntax error", $error_message)){
					self::handle_exception_($e, "Syntax error!");
				}
				if (ServiceStrings::startsWith("no such table: ", $error_message)){
					self::handle_exception_($e, "No such table!");
				}
				if (preg_match("/^table (.*?) already exists\$/", $error_message)){
					self::handle_exception_($e, "Table already exists!");
				}
			}

			new Error("PDO Exception: "
				.$e->getMessage()
				.(CFG_S::$DEVMODE?"\n$debugInfo":"")
				,$cut+1
			);

		}
		Error::fromException($e);
	}

	private static function handle_exception_(Exception $e, $msg){
		if(CFG_S::$DEVMODE){
			$msg.="\n".$e->getMessage();
		}
		new Error("SQL error: ".$msg);
	}

}