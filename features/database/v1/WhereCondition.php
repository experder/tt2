<?php /** @noinspection PhpUnused TODO */

namespace tt\features\database\v1;

class WhereCondition
{

	const RELATION_NOT_EQUALS_NULL = "-not_equals_null-";
	const RELATION_EQUALS = "-equals-";
	const RELATION_NOT_EQUALS = "-not_equals-";
	const RELATION_SMALLER_THAN = "-smaller_than-";
	const RELATION_GREATER_OR_EQUAL_TO = "-greater_or_equal_to-";

	private $key;
	private $relation;
	private $value;

	/**
	 * @param string $key
	 * @param string $relation WhereCondition::RELATION_
	 * @param mixed $value
	 */
	public function __construct($key, $relation, $value = null)
	{
		$this->key = $key;
		$this->relation = $relation;
		$this->value = $value;
	}

	/**
	 * @return string
	 */
	public function getKey()
	{
		return $this->key;
	}

	/**
	 * @return string WhereCondition::RELATION_
	 */
	public function getRelation()
	{
		return $this->relation;
	}

	public function getKRV()
	{
		return array(
			$this->key,
			$this->relation,
			$this->value,
		);
	}

	/**
	 * @return mixed
	 */
	public function getValue()
	{
		return $this->value;
	}

}