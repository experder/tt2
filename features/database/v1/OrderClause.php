<?php

namespace tt\features\database\v1;

class OrderClause
{

	const DESC = false;

	/**
	 * @var string $field
	 */
	private $field;
	/**
	 * @var bool $ascending
	 */
	private $ascending;

	/**
	 * @param string $field
	 * @param bool $ascending
	 */
	public function __construct($field, $ascending = true)
	{
		$this->field = $field;
		$this->ascending = $ascending;
	}

	/**
	 * @return string
	 */
	public function getField()
	{
		return $this->field;
	}

	/**
	 * @return bool
	 */
	public function isAscending()
	{
		return $this->ascending;
	}

}