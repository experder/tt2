<?php

namespace tt\features\database\v1;

class JoinStatement
{

	/**
	 * @var string $target_table
	 */
	private $target_table;
	/**
	 * @var string $target_field
	 */
	private $target_field;
	/**
	 * @var string $source
	 */
	private $source;

	/**
	 * @param string $target_table
	 * @param string $target_field
	 * @param string $source
	 */
	public function __construct($target_table, $source, $target_field = Model::FIELD_id)
	{
		$this->target_table = $target_table;
		$this->target_field = $target_field;
		$this->source = $source;
	}

	/**
	 * @return string
	 */
	public function getTargetTable()
	{
		return $this->target_table;
	}

	/**
	 * @return string
	 */
	public function getTargetField()
	{
		return $this->target_field;
	}

	/**
	 * @return string
	 */
	public function getSource()
	{
		return $this->source;
	}

}