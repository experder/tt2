<?php

namespace tt\features\database\v1\db_mysql;

use Exception;
use PDO;
use PDOStatement;
use tt\features\config\v1\CFG_S;
use tt\features\database\schema\Table;
use tt\features\database\v1\DatabaseGeneral;
use tt\features\database\v1\JoinStatement;
use tt\features\database\v1\LeftJoinStatement;
use tt\features\database\v1\LimitClause;
use tt\features\database\v1\OrderClause;
use tt\features\database\v1\WhereCondition;
use tt\features\debug\DebugHandler;
use tt\features\debug\errorhandler\v1\Error;
use tt\services\ServiceDebug;
use tt\services\ServiceStrings;

/**
 * @deprecated
 */
class DatabaseMySql extends DatabaseGeneral
{

	const QUERY_TYPE_VOID = 0;
	const QUERY_TYPE_SELECT = 1;
	const QUERY_TYPE_INSERT = 2;
	const QUERY_TYPE_UPDATE = 3;
	const QUERY_TYPE_DELETE = 4;

	/**
	 * @var PDO
	 */
	private $pdo = null;

	private $queryHandler;

	/**
	 * @var DbSchemaHandler $schemaHandler
	 */
	private $schemaHandler = null;

	public function __construct($host, $schema, $username, $password)
	{
		parent::__construct($host, $schema, $username, $password);
		$this->queryHandler = new DbQueries($this);
	}

	/**
	 * @return DbQueries
	 */
	public function getQueryHandler()
	{
		return $this->queryHandler;
	}

	function connect($password)
	{
		try {
			$this->pdo = new PDO(
				"mysql:host=" . $this->host
				. ";dbname=" . $this->schema,
				$this->username,
				$password
			);
			$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			#$this->pdo->query('SET NAMES utf8;');
		} catch (Exception $e) {
			DbErrorHandling::handle_exception($e);
		}
	}

	/**
	 * @param string|string[] $string
	 * @return true Ends with an error if execution fails
	 */
	public function execute($string)
	{
		if (is_array($string)) {
			foreach ($string as $str) {
				$this->execute($str);
			}
			return true;
		}
		$this->query($string, null, DatabaseMySql::QUERY_TYPE_VOID);
		return true;
	}

	/**
	 * @return DbSchemaHandler
	 */
	private function getSchemaHandler()
	{
		if ($this->schemaHandler === null) {
			$this->schemaHandler = new DbSchemaHandler($this);
		}
		return $this->schemaHandler;
	}

	/**
	 * @param string $table
	 * @param array $data [{"foo":"bar",...},...]
	 * @return false|string
	 */
	function insertByArray($table, $data)
	{
		return $this->queryHandler->insert_byAssocArray($table, $data);
	}

	function updateByArray($table, $data, array $whereConditions)
	{
		$substitutions = array();
		$param = 1;
		$keyVals_array = array();
		foreach ($data as $key => $value) {
			$p = ':u' . ($param++) . 'u';
			$substitutions[$p] = $value === false ? "0" : $value;
			$keyVals_array[] = $key . "=" . $p;
		}
		$where = $this->buildWhereString($whereConditions, $substitutions);
		$keyVals = implode(",", $keyVals_array);
		$query = "UPDATE $table\nSET $keyVals$where";
		return $this->update($query, $substitutions);
	}

	/**
	 * @param string $sql_query
	 * @param array $placeholders
	 * @return array
	 */
	public function select($sql_query, $placeholders = null)
	{
		return $this->query($sql_query, $placeholders, self::QUERY_TYPE_SELECT);
	}

	/**
	 * @param string $sql_query
	 * @param array $placeholders
	 * @return string
	 */
	public function insert($sql_query, $placeholders = null)
	{
		return $this->query($sql_query, $placeholders, self::QUERY_TYPE_INSERT);
	}

	public function update($sql_query, $placeholders = null)
	{
		return $this->query($sql_query, $placeholders, self::QUERY_TYPE_UPDATE);
	}

	private function query($sql_query, $placeholders, $type)
	{
		$statement = $this->pdo->prepare($sql_query);
		try {
			@$statement->execute($placeholders);
		} catch (Exception $e) {
			DbErrorHandling::handle_exception($e,
				self::getCompiledQueryFromDebugDump($statement),
				1);
		}
		if (CFG_S::$DEVMODE && CFG_S::$DEBUGINFO_SHOWQUERIES) {
			$compiledQuery = self::getCompiledQueryFromDebugDump($statement);
			DebugHandler::addQuery($compiledQuery
			#."\n".implode("\n",ServiceDebug::backtrace())
			);
		}
		if ($type === self::QUERY_TYPE_SELECT) {
			return $statement->fetchAll(PDO::FETCH_ASSOC);
		}
		if ($type === self::QUERY_TYPE_INSERT) {
			return $this->pdo->lastInsertId();
		}
		if ($type === self::QUERY_TYPE_UPDATE || $type === self::QUERY_TYPE_DELETE) {
			return $statement->rowCount();
		}
		if ($type === self::QUERY_TYPE_VOID) {
			return true;
		}
		new Error("Unknown Query Type!");
		return false;
	}

	/**
	 * @param string|PDOStatement $dump
	 * @return string
	 */
	public static function getCompiledQueryFromDebugDump($dump)
	{
		if ($dump instanceof PDOStatement) {
			$dump = ServiceDebug::debugDumpParams($dump);
		}
		if (preg_match("/\\nSent SQL: \\[\\d+] (.*?)\\nParams:  ?\\d+\\n/s", $dump, $matches)) {
			return $matches[1];
		}
		if (preg_match("/^SQL: \\[\\d+] (.*?)\\nParams: {2}0$/s", $dump, $matches)) {
			return $matches[1];
		}
		new Error("Could not parse MySQL query!\n" . $dump);
		return "?";
	}

	/**
	 * @param string $table
	 * @return Table|false FALSE if table does not exist
	 */
	function getTableSchemaFromDb($table)
	{
		return $this->getSchemaHandler()->getTableSchemaFromDb($table);
	}

	/**
	 * @param Table $schema
	 * @return string[]
	 */
	function cmdCreateTable(Table $schema)
	{
		return array($this->getSchemaHandler()->cmdCreateTable($schema));
	}

	/**
	 * @param Table $target
	 * @param Table $actual
	 * @param bool $show_warnings
	 * @return string[] Commands needed to transform $actual into $target
	 */
	function compareSchema(Table $target, Table $actual, $show_warnings = false)
	{
		return $this->getSchemaHandler()->compareSchema($target, $actual, $show_warnings);
	}

	function generalQuery(array $subset, $table_name, array $joinWhereOrder, $debugId=false)
	{
		$joins = array();
		$where_array = array();
		$orderby = array();
		$limit = false;
		foreach ($joinWhereOrder as $statement) {
			if ($statement instanceof JoinStatement) {
				$joins[] = $statement;
				continue;
			}
			if ($statement instanceof WhereCondition) {
				$where_array[] = $statement;
				continue;
			}
			if ($statement instanceof OrderClause) {
				$orderby[] = $statement;
				continue;
			}
			if ($statement instanceof LimitClause) {
				$limit = $statement;
				continue;
			}
			new Error("Unknown statement: " . (is_object($statement)
					? basename(get_class($statement))
					: $statement) . "!");
		}

		$subset0 = array();
		$subset1 = array();
		foreach ($subset as $key) {
			if (strpos($key, '.') === false) {
				$subset0[] = $key;
			} else {
				$subset1[] = $key;
			}
		}
		$subset_sql = implode(",", $this->quoteKeywords(
			array_merge(ServiceStrings::prefixValues('t0.', $subset0), $subset1)));
		$join_sql = $this->buildJoinString($joins);
		$where_sql = $this->buildWhereString($where_array, $substitutions, array($table_name => "t0"));
		$order_sql = $this->buildOrderString($orderby);

		$limit_sql=$limit?"\nLIMIT ".$limit->getRowCount():"";

		$query = "/*".($debugId?:"247")."*/"
			."SELECT $subset_sql\nFROM `$table_name` t0$join_sql$where_sql$order_sql$limit_sql";
		return $this->select($query, $substitutions);
	}

	private function quoteKeywords($name)
	{
		if (is_array($name)) {
			$names = array();
			foreach ($name as $n) {
				$names[] = $this->quoteKeywords($n);
			}
			return $names;
		}

		if (substr($name, 0, 1) === "'") return $name;
		$parts = explode(".", $name);
		$result = array();
		foreach ($parts as $part) {
			if (strcasecmp($part, 'DESC') === 0) {
				$part = "`$part`";
			}
			$result[] = $part;
		}
		return implode(".", $result);
	}

	/**
	 * @param OrderClause[] $orders
	 * @return string ORDER BY ...
	 */
	private function buildOrderString(array $orders)
	{
		if (!$orders) return "";
		$sql = array();
		foreach ($orders as $orderby) {
			$sql[] = $orderby->getField() . " " . ($orderby->isAscending() ? "" : "DESC");
		}
		return "\nORDER BY " . implode(",", $sql);
	}

	/**
	 * @param JoinStatement[] $joins
	 * @return string JOIN ...
	 */
	private function buildJoinString(array $joins)
	{
		if (!$joins) return "";
		$sql = array();
		foreach ($joins as $join) {
			if (!($join instanceof JoinStatement)) new Error("Join statement must be instance of JoinStatement.");

			if ($join instanceof LeftJoinStatement) {
				$table = $join->getTargetTable();
				$source = $join->getSource();
				$source_prefix = (strpos($source, '.') === false ? "t0." : "");
				$field = $join->getTargetField();
				$sql[] = "LEFT JOIN $table ON $source_prefix$source=$table.$field";
				continue;
			}

			new Error("Join statment '" . basename(get_class($join)) . "' not implemented (MySql)!");

		}
		return "\n" . implode("\n", $sql);
	}

	private function sanitizeKey($key, array $table_aliasses = array())
	{
		$parts = explode('.', $key);
		if (count($parts) > 1 && isset($table_aliasses[$parts[0]])) {
			$parts[0] = $table_aliasses[$parts[0]];
		}
		return "`" . implode('`.`', $parts) . "`";
	}

	/**
	 * @param WhereCondition[] $conditions
	 * @param string[] $substitutions
	 * @return string WHERE ...
	 */
	private function buildWhereString(array $conditions, &$substitutions = array(), array $table_aliasses = array())
	{
		if (!$conditions) return "";
		$sql_array = array();
		$param_counter = 0;
		foreach ($conditions as $condition) {
			list($key, $relation, $value) = $condition->getKRV();
			$key_sanitized = $this->sanitizeKey($key, $table_aliasses);
			$param = ':w' . (++$param_counter) . 'w';
			switch ($relation) {
				case WhereCondition::RELATION_NOT_EQUALS_NULL:
					$sql_array[] = "$key_sanitized IS NOT NULL";
					$param = false;
					break;
				case WhereCondition::RELATION_EQUALS:
					$sql_array[] = "$key_sanitized<=>$param";
					break;
				case WhereCondition::RELATION_NOT_EQUALS:
					$sql_array[] = "$key_sanitized != $param";
					break;
				case WhereCondition::RELATION_SMALLER_THAN:
					$sql_array[] = "$key_sanitized < $param";
					break;
				case WhereCondition::RELATION_GREATER_OR_EQUAL_TO:
					$sql_array[] = "$key_sanitized >= $param";
					break;
				default:
					new Error("Unknown Relation for WHERE-statement (MySQL): " . $relation);
					break;
			}
			if ($param !== false) $substitutions[$param] = $value;
		}
		return "\nWHERE " . implode("\n\tAND ", $sql_array);
	}

}