<?php

namespace tt\features\database\v1\db_mysql;

use Exception;
use PDOException;
use tt\features\config\v1\CFG_S;
use tt\features\debug\errorhandler\v1\Error;

class DbErrorHandling
{

	public static function handle_exception(Exception $e, $debugInfo = "", $cut = 0)
	{
		if ($e instanceof PDOException) {
			if ($e->getMessage() === "could not find driver") {
				new Error("Please install MySQL (PDO) driver!"
					. "\nhttps://gitlab.com/experder/tt2/-/blob/main/features/database/v1/db_mysql/README.md#pdo");
			}

			$error_code = false;
			/**
			 * Keep it for future development.
			 * @noinspection PhpUnusedLocalVariableInspection
			 */
			$error_message = "";

			if (isset($e->errorInfo) && is_array($e->errorInfo) && count($e->errorInfo) >= 3) {
				$error_code = $e->errorInfo[1];
				/** @noinspection PhpUnusedLocalVariableInspection */
				$error_message = $e->errorInfo[2];
			}

			/*
			 * Exceptions by (pdo-)errorcode
			 * https://www.fromdual.com/mysql-error-codes-and-messages
			 * https://codeantenna.com/a/m8FYYFX8TI
			 */
			if ($e->getCode() === 1044/*ER_DBACCESS_DENIED_ERROR*/) {
				self::handle_exception_($e, "Access denied to database '" . CFG_S::$DB_SCHEMA . "'!"
					. "\nhttps://gitlab.com/experder/tt2/-/blob/main/features/database/v1/db_mysql/README.md#permissions");
			}
			if ($e->getCode() === 1045/*ER_ACCESS_DENIED_ERROR*/) {
				self::handle_exception_($e, "Access denied for user '" . CFG_S::$DB_USER . "'!"
					. "\nhttps://gitlab.com/experder/tt2/-/blob/main/features/database/v1/db_mysql/README.md#user");
			}
			if ($e->getCode() === 1049/*ER_BAD_DB_ERROR*/) {
				self::handle_exception_($e, "Unknown database '" . CFG_S::$DB_SCHEMA . "'!");
			}
			if ($error_code === 1050/*ER_TABLE_EXISTS_ERROR)*/) {
				self::handle_exception_($e, "Table already exists!");
			}
			if ($error_code === 1826/*ER_FK_DUP_NAME*/) {
				#$s = preg_replace("/^Duplicate foreign key constraint name '(.*?)'\$/", "\$1", $error_message);
				self::handle_exception_($e, "Foreign key constraint name has to be unique!");
			}
			if ($e->getCode() === 2002/*CR_CONNECTION_ERROR*/) {
				self::handle_exception_($e, "Can't connect to MySQL server '" . CFG_S::$DB_HOST . "'!");
			}

			new Error("PDO Exception: "
				. $e->getMessage()
				. (CFG_S::$DEVMODE ? "\n$debugInfo" : "")
				, $cut + 1
			);
		}
		Error::fromException($e, $cut + 1);
	}

	private static function handle_exception_(Exception $e, $msg)
	{
		if (CFG_S::$DEVMODE) {
			$msg .= "\n" . $e->getMessage();
		}
		new Error("SQL error: " . $msg);
	}

}