<?php

namespace tt\features\database\v1\db_mysql;

use tt\services\ServiceStrings;

/**
 * @deprecated TODO reduce these three functions to two and move them to dbsql
 */
class DbQueries
{

	/**
	 * @var DatabaseMySql $db
	 */
	private $db;

	/**
	 * @param DatabaseMySql $db
	 */
	public function __construct(DatabaseMySql $db)
	{
		$this->db = $db;
	}

	/**
	 * @param string $table
	 * @param array $array
	 * @return false|string
	 *
	 * @noinspection DuplicatedCode
	 * @see \tt\features\database\v1\db_sqlite\DbQueries::insert_byAssocArray()
	 */
	public function insert_byAssocArray($table, $array){
		$keys_array = array_keys($array);
		$keys_prefixed = ServiceStrings::prefixValues(':', $keys_array);
		$keys_quoted = ServiceStrings::prefixValues('`', $keys_array, '`');

		$substitutions = array();
		foreach ($array as $key => $value) {
			$substitutions[':' . $key] = $value===false?'0':$value;
		}

		$keys = implode(",", $keys_quoted);
		$values = implode(",", $keys_prefixed);
		$query = "INSERT INTO $table ($keys)\nVALUES ($values);";
		return $this->db->insert($query, $substitutions);
	}

	public function queryInfoTables($table){
		return $this->db->select(
			"SELECT TABLE_COMMENT"
			."\nFROM INFORMATION_SCHEMA.TABLES"
			."\nWHERE TABLE_SCHEMA=:SCHEMA AND TABLE_NAME=:TABLE;",
			array(
				":TABLE"=>$table,
				":SCHEMA"=>$this->db->getSchema(),
			));
	}

	public function queryShowCreate($table){
		$result = $this->db->select(
			"SHOW CREATE TABLE `".$this->db->getSchema()."`.`$table`;"
		);
		return $result[0]['Create Table'];
	}

}