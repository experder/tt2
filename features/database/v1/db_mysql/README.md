### Example server configuration

See [ConfigServer.md](../../../config/v1/ConfigServer.md)

## User

> CREATE USER 'tt_user'@'%' IDENTIFIED BY 'your_password_here';

## Permissions

> GRANT Alter,Create,Delete,Index,Insert,References,Select,Update ON tt_demo.* TO 'tt_user';

# PDO

`PHP.ini`:
> extension=pdo_mysql

https://www.php.net/manual/de/ref.pdo-mysql.php
