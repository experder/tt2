<?php

namespace tt\features\database\v1\db_mysql;

use tt\features\config\v1\CFG_S;
use tt\features\database\schema\Column;
use tt\features\database\schema\ForeignKey;
use tt\features\database\schema\Key;
use tt\features\database\schema\ServiceSchema;
use tt\features\database\schema\Table;
use tt\features\debug\errorhandler\v1\Error;
use tt\features\debug\errorhandler\v2\Warning;
use tt\services\DEBUG;
use tt\services\ServiceArrays;

/**
 * @deprecated
 */
class DbSchemaHandler
{

	/**
	 * @return DbQueries
	 */
	private $dbQueries;

	public function __construct(DatabaseMySql $db)
	{
		$this->dbQueries = $db->getQueryHandler();
	}

	public function cmdAddColumn($tableName, Column $columnSchema)
	{
		return "ALTER TABLE `$tableName` ADD " . $this->cmdCreateColumn($columnSchema) . ";";
	}

	public function cmdUpdateColumn($tableName, Column $columnSchema)
	{
		return "ALTER TABLE `$tableName` MODIFY COLUMN " . $this->cmdCreateColumn($columnSchema) . ";";
	}

	public function comment($comment)
	{
		$comment = str_replace("\r", "\\r", $comment);
		$comment = str_replace("\n", "\\n", $comment);
		return "-- " . $comment;
	}

	/**
	 * @param string $table
	 * @return Table|false FALSE if table does not exist
	 */
	public function getTableSchemaFromDb($table)
	{
		$infoTable = $this->dbQueries->queryInfoTables($table);
		if (!$infoTable) return false;

		$sqlCreate = $this->dbQueries->queryShowCreate($table);

		if (!preg_match("/^CREATE TABLE `(.*?)` \\(\\n(.*?)\\n\\) ([^\\n]+)\$/s", $sqlCreate, $matches)) {
			new Error("Couldn't parse SQL response!");
		}
		$name = $matches[1];
		$desc = $matches[2];
		$tblInfo = $matches[3];

		$table = new Table($name);

		//Comment
		if (preg_match("/ COMMENT='(.*?)'\$/", $tblInfo, $matches)) {
			$comment = $matches[1];
			/**
			 * This is for later extensions, where engine and collation are considered.
			 * @noinspection PhpUnusedLocalVariableInspection
			 */
			$tblInfo = substr($tblInfo, 0, strlen($tblInfo) - strlen($comment) - 11);
			$table->setComment($this->unquoteString($comment));
		}

		//Columns,Keys
		foreach (explode("\n", $desc) as $row) {
			$this->getTableSchemaFromDb_columnsKeys(trim($row, "\t\r ,"), $table);
		}

		return $table;
	}

	private function getTableSchemaFromDb_desc_column(Table $table, $name, $type, $rest)
	{
		$column = new Column($name, $this->mapDataTypeTT($type, $back1));
		if ($column->getDataType() === Column::DATATYPE_STRING) {
			$column->setStringMaxLength($back1);
		}

		//Comment
		if (preg_match("/ COMMENT '(.*?)'\$/", $rest, $matches)) {
			$comment = $matches[1];
			$column->setComment($this->unquoteString($comment));
			$rest = substr($rest, 0, strlen($rest) - strlen($comment) - 11);
		}

		//Nullable
		if (strpos($rest, 'NOT NULL') !== false) {
			$column->setNotNullable();
		}

		//Default
		if (preg_match("/DEFAULT (.*$)\$/", $rest, $matches)) {
			$default = $matches[1];
			if ($default != 'NULL') {
				$column->setDefault($default);
			}
		}

		//Sign
		if (strpos($rest, 'unsigned') !== false) {
			$column->setUnsigned();
		}

		$table->addColumn($column);

	}

	private function getTableSchemaFromDb_desc_primaKey(Table $table, array $colnames)
	{
		if (count($colnames) == 1) {
			$table->setPrimaryKey($colnames[0]);
			$table->removeColumn($colnames[0]);
		} else {
			new Warning("Multi column primary key!");
		}

	}

	private function getTableSchemaFromDb_columnsKeys($row, Table $table)
	{

		//Column
		if (preg_match("/^`(.*?)` (.*?) (.*)/", $row, $matches)) {
			$this->getTableSchemaFromDb_desc_column($table, $matches[1], $matches[2], $matches[3]);
		} //Primary key
		else if (preg_match("/^PRIMARY KEY \\(`(.*?)`\\)\$/", $row, $matches)) {
			$this->getTableSchemaFromDb_desc_primaKey($table, explode('`,`', $matches[1]));
		} //Key
		else if (preg_match("/^(UNIQUE|FULLTEXT)? ?KEY `(.*?)` \\(`(.*?)`\\)\$/", $row, $matches)) {
			$type = $matches[1];
			$name = $matches[2];
			$colnames = explode('`,`', $matches[3]);
			$columns = ServiceSchema::columnsByNames($colnames);
			$table->addIndex($columns, $type === 'UNIQUE', $name, $type === 'FULLTEXT');
		} //Constraint
		else if (preg_match("/^CONSTRAINT `(.*?)` FOREIGN KEY \\(`(.*?)`\\)"
			. " REFERENCES `(.*?)` \\(`(.*?)`\\)"
			. " ON DELETE (.*?) ON UPDATE (.*?)\$/", $row, $matches)) {
			$name = $matches[1];
			$colnames = explode('`,`', $matches[2]);
			$refTable = $matches[3];
			$refColnames = explode('`,`', $matches[4]);
			$onDelete = $this->mapConstraintTypeTT($matches[5]);
			$onUpdate = $this->mapConstraintTypeTT($matches[6]);

			$key = new ForeignKey(
				$table->getName(),
				$name,
				ServiceSchema::columnsByNames($colnames),
				$refTable,
				$refColnames
			);
			$key->setActions($onUpdate, $onDelete);
			$table->addForeignKey($key);
		} else {
			new Warning($table->getName().": Ignored line: $row");
		}
	}

	/**
	 * @param Table $schema
	 * @return string
	 */
	function cmdCreateTable(Table $schema)
	{
		$name = $schema->getName();

		$engine = "InnoDB";
		$charset = "utf8mb4";
		$collation = "utf8mb4_general_ci";

		$desc = array();

		//Primary key, pt.1
		$pk = $schema->getPrimaryKey();
		$auto_increment = count($pk) <= 1;
		foreach ($pk as $column) {
			$desc[] = $this->cmdCreateColumn($column, $auto_increment);
		}

		//Columns
		foreach ($schema->getColumns() as $column) {
			$desc[] = $this->cmdCreateColumn($column);
		}

		//Primary key, pt.2
		$desc[] = "PRIMARY KEY (`" . implode("`,`", ServiceSchema::columnNames($pk)) . "`)";

		//Keys
		foreach ($schema->getKeys() as $key) {
			$desc[] = $this->cmdCreateKey($key);
		}

		//ForeignKeys: Indizes
		foreach ($schema->getForeignKeys() as $key) {
			$idx = $this->cmdCreateConstraintIndex($key, $schema);
			if ($idx !== false) $desc[] = $idx;
		}
		//ForeignKeys: Constraints
		foreach ($schema->getForeignKeys() as $key) {
			$desc[] = $this->cmdCreateConstraint($key);
		}

		$table_sql = implode(",\n\t", $desc);

		$comment = $schema->getComment() ?
			" COMMENT=" . $this->quoteString($schema->getComment())
			: "";

		return "CREATE TABLE `$name` (\n\t$table_sql\n)"
			. " ENGINE=$engine"
			. " DEFAULT CHARSET=$charset"
			. " COLLATE=$collation"
			. $comment
			. ";";
	}

	function cmdCreateConstraintIndex(ForeignKey $key, Table $table)
	{
		$name = $key->getName();

		$existing_key = $table->getKey($name);
		if ($existing_key !== false) {
			return false;
		}

		$columns = implode("`,`", ServiceSchema::columnNames($key->getColumns()));
		return "KEY `$name` (`$columns`)";
	}

	function cmdCreateConstraint(ForeignKey $key)
	{
		$name = $key->getName();
		$colname = implode("`,`", ServiceSchema::columnNames($key->getColumns()));
		$refTable = $key->getRefTable();
		$refCol = implode("`,`", $key->getRefColumns());
		$ondelete = $this->mapConstraintTypeSQL($key->getOnDelete());
		$onupdate = $this->mapConstraintTypeSQL($key->getOnUpdate());
		return "CONSTRAINT `$name` FOREIGN KEY (`$colname`)"
			. " REFERENCES `$refTable` (`$refCol`)"
			. " ON DELETE $ondelete ON UPDATE $onupdate";
	}

	function cmdCreateKey(Key $key)
	{
		$type = $key->isUnique() ? "UNIQUE " : ($key->isFulltext() ? "FULLTEXT " : "");
		$name = $key->getName();
		$columns = "`" . implode("`,`",
				ServiceSchema::columnNames($key->getColumns())
			) . "`";
		return $type . "KEY `$name` ($columns)";
	}

	function cmdCreateColumn(Column $column, $auto_increment = false)
	{
		$name = $column->getName();

		$type = $this->mapDataTypeSQL($column->getDataType(), $column);

		$nullable = $column->isNullable() ? "" : "NOT NULL";

		$default = $column->getDefault() !== null ?
			"DEFAULT " . $column->getDefault()
			: ($column->isNullable() ? "DEFAULT NULL" : "");

		$autoIncrement = $auto_increment ? "AUTO_INCREMENT" : "";

		$comment = $column->getComment()
			? "COMMENT " . $this->quoteString($column->getComment())
			: "";

		$sign = $column->isSigned() ? "" : "unsigned";

		return "`$name` $type $sign $nullable $default $autoIncrement $comment";
	}

	public function mapConstraintTypeTT($constraintType_sql)
	{
		if ($constraintType_sql === 'RESTRICT') {
			return ForeignKey::CONSTRAINTACTION_RESTRICT;
		}
		if ($constraintType_sql === 'CASCADE') {
			return ForeignKey::CONSTRAINTACTION_CASCADE;
		}
		if ($constraintType_sql === 'SET NULL') {
			return ForeignKey::CONSTRAINTACTION_SETNULL;
		}
		return DEBUG::invalidState("!UNKNOWN_ACTIONTYPE_MYSQL '$constraintType_sql'!");
	}

	public function mapConstraintTypeSQL($constraintType_tt)
	{
		if ($constraintType_tt === ForeignKey::CONSTRAINTACTION_RESTRICT) {
			return "RESTRICT";
		}
		if ($constraintType_tt === ForeignKey::CONSTRAINTACTION_CASCADE) {
			return "CASCADE";
		}
		if ($constraintType_tt === ForeignKey::CONSTRAINTACTION_SETNULL) {
			return "SET NULL";
		}
		return DEBUG::invalidState("!UNKNOWN_ACTIONTYPE_TT_MYSQL '$constraintType_tt'!");
	}

	public function mapDataTypeTT($datatype_sql, &$back1 = null)
	{
		if ($datatype_sql === 'int') {
			return Column::DATATYPE_INTEGER;
		}
		if (preg_match("/^varchar\\((\\d*?)\\)\$/", $datatype_sql, $matches)) {
			$back1 = $matches[1];
			return Column::DATATYPE_STRING;
		}
		if ($datatype_sql === 'text') {
			return Column::DATATYPE_TEXT;
		}
		if ($datatype_sql === 'date') {
			return Column::DATATYPE_DATE;
		}
		if ($datatype_sql === 'datetime') {
			return Column::DATATYPE_DATETIME;
		}
		if ($datatype_sql === 'tinyint') {
			return Column::DATATYPE_BOOLEAN;
		}
		return DEBUG::invalidState("!UNKNOWN_DATATYPE_MYSQL '$datatype_sql'!");
	}

	public function mapDataTypeDb($datatype_tt)
	{
		return $this->mapDataTypeSQL($datatype_tt);
	}

	private function mapDataTypeSQL($datatype_tt, Column $column = null)
	{
		if ($datatype_tt === Column::DATATYPE_STRING) {
			$maxlength = $column ? "(" . ($column->getStringMaxlength() ?: Column::STRING_DEFAULT_LENGTH) . ")" : "";
			return "varchar$maxlength";
		}
		if ($datatype_tt === Column::DATATYPE_INTEGER) {
			return "int";
		}
		if ($datatype_tt === Column::DATATYPE_TEXT) {
			return "text";
		}
		if ($datatype_tt === Column::DATATYPE_DATE) {
			return "date";
		}
		if ($datatype_tt === Column::DATATYPE_DATETIME) {
			return "datetime";
		}
		if ($datatype_tt === Column::DATATYPE_BOOLEAN) {
			return "tinyint";
		}
		return DEBUG::invalidState("!UNKNOWN_DATATYPE_TT_MYSQL '$datatype_tt'!");
	}

	private function quoteString($string)
	{
		$string = str_replace("'", "''", $string);
		$string = str_replace("\\", "\\\\", $string);
		$string = str_replace("\r", "\\r", $string);
		$string = str_replace("\n", "\\n", $string);
		return "'$string'";
	}

	private function unquoteString($string)
	{
		$string = str_replace("''", "'", $string);
		$string = str_replace("\\r", "\r", $string);
		$string = str_replace("\\n", "\n", $string);
		return str_replace("\\\\", "\\", $string);
	}

	/**
	 * @param Key $key
	 * @return string
	 */
	function cmdDeleteKey(Key $key)
	{
		$tblName = $key->getTableName();
		$name = $key->getName();
		return "ALTER TABLE $tblName DROP INDEX $name;";
	}

	/**
	 * @param Key $key
	 * @return string
	 */
	function cmdAddKey(Key $key)
	{
		$tableName = $key->getTableName();
		$type = $key->isUnique() ? "UNIQUE " : ($key->isFulltext() ? "FULLTEXT " : "");
		$name = $key->getName();
		$columns = "`" . implode("`,`",
				ServiceSchema::columnNames($key->getColumns())
			) . "`";
		return "CREATE {$type}INDEX `$name` ON $tableName ($columns);";
	}

	function cmdDeleteConstraint(ForeignKey $key)
	{
		$table = $key->getTableName();
		$name = $key->getName();
		return "ALTER TABLE $table DROP FOREIGN KEY $name;";
	}

	function cmdAddConstraint(ForeignKey $key)
	{
		$table = $key->getTableName();
		return "ALTER TABLE $table ADD " . $this->cmdCreateConstraint($key) . ";";
	}

	function cmdUpdateTableComment(Table $table, $comment)
	{
		$name = $table->getName();
		$comment = $this->quoteString($comment);
		return "ALTER TABLE $name COMMENT=$comment;";
	}

	/**
	 * @param Table $target
	 * @param Table $actual
	 * @param bool $show_warnings
	 * @return string[] Commands needed to transform $actual into $target
	 */
	public function compareSchema(Table $target, Table $actual, $show_warnings = false)
	{
		$commands = array();

		//Columns
		$more_commands = $this->compareColumns(
			$target->getColumns(),
			$actual->getColumns(),
			$target->getName(),
			$show_warnings
		);
		$commands = array_merge($commands, $more_commands);

		//PrimaryKey
		$target_pk = ServiceSchema::columnNames($target->getPrimaryKey());
		$actual_pk = ServiceSchema::columnNames($actual->getPrimaryKey());
		if (!ServiceArrays::compareArrays($target_pk, $actual_pk)) {
			$commands[] = $this->comment(
				DEBUG::invalidState(
					"Primary key mismatch. Please resolve.\n" . $target->getName() . ":"
					. " ORM:" . implode(",", $target_pk)
					. " DB:" . implode(",", $actual_pk)
				)
			);
		}

		//Keys
		$more_commands = $this->compareKeys(
			$target->getKeys(),
			$actual->getKeys(),
			$target->getForeignKeys(),
			$actual->getForeignKeys()
		);
		$commands = array_merge($commands, $more_commands);

		//ForeignKeys
		$more_commands = $this->compareConstraints(
			$target->getForeignKeys(),
			$actual->getForeignKeys(),
			$actual->getKeys()
		);
		$commands = array_merge($commands, $more_commands);

		//Comment
		if ($target->getComment() != $actual->getComment()) {
			$commands[] = $this->cmdUpdateTableComment($actual, $target->getComment());
		}

		return $commands;
	}

	/**
	 * @param Key[] $target_keys
	 * @param Key[] $actual_keys
	 * @param ForeignKey[] $target_constraints
	 * @param ForeignKey[] $actual_constraints
	 * @return string[]
	 */
	private function compareKeys(
		array $target_keys,
		array $actual_keys,
		array $target_constraints,
		array $actual_constraints
	)
	{
		$commands = array();

		foreach ($actual_keys as $actualKey) {
			$name = $actualKey->getName();

			//Indexes belonging to constraints are handeled with the constraints:
			if (isset($target_constraints[$name]) || isset($actual_constraints[$name])) continue;

			if (!isset($target_keys[$name])) {
				$commands[] = $this->cmdDeleteKey($actualKey);
			} else {
				if (!$target_keys[$name]->compareToKey($actualKey)) {
					$more_commands = $this->cmdUpdateKey($actualKey, $target_keys[$name]);
					$commands = array_merge($commands, $more_commands);
				}
				unset($target_keys[$name]);
			}
		}
		foreach ($target_keys as $targetKey) {
			$commands[] = $this->cmdAddKey($targetKey);
		}

		return $commands;
	}

	/**
	 * @param Key $key_old
	 * @param Key $key_new
	 * @return string[]
	 */
	private function cmdUpdateKey(Key $key_old, Key $key_new)
	{
		return array(
			$this->cmdDeleteKey($key_old),
			$this->cmdAddKey($key_new)
		);
	}

	private function cmdUpdateConstraint(ForeignKey $key_old, ForeignKey $key_new)
	{
		return array(
			$this->cmdDeleteConstraint($key_old),
			$this->cmdAddConstraint($key_new)
		);
	}

	/**
	 * @param ForeignKey[] $target
	 * @param ForeignKey[] $actual
	 * @return string[]
	 */
	private function compareConstraints(array $target, array $actual, array $actualKeys)
	{
		$commands = array();

		foreach ($actual as $actualKey) {
			$name = $actualKey->getName();
			if (!isset($target[$name])) {
				$commands[] = $this->cmdDeleteConstraint($actualKey);
				if (isset($actualKeys[$name])) {
					$commands[] = $this->cmdDeleteKey($actualKeys[$name]);
				}
			} else {
				if (!$target[$name]->compareToForeignKey($actualKey)) {
					$more_commands = $this->cmdUpdateConstraint($actualKey, $target[$name]);
					$commands = array_merge($commands, $more_commands);
				}
				unset($target[$name]);
			}
		}
		foreach ($target as $targetKey) {
			$commands[] = $this->cmdAddConstraint($targetKey);
		}

		return $commands;
	}

	/**
	 * @param Column[] $targets_indexed
	 * @param Column[] $actual
	 * @param string $tableName
	 * @param bool $show_warnings
	 * @return string[] commands
	 */
	private function compareColumns(array $targets_indexed, array $actual, $tableName, $show_warnings)
	{
		$commands = array();

		$unmodeled_db_entities = array();
		foreach ($actual as $actual_column) {
			if (!isset($targets_indexed[$actual_column->getName()])) {
				$unmodeled_db_entities[] = "Column: $tableName." . $actual_column->getName();
			} else {
				$more_commands = $this->compareColumn($tableName, $targets_indexed[$actual_column->getName()], $actual_column);
				$commands = array_merge($commands, $more_commands);
				unset($targets_indexed[$actual_column->getName()]);
			}
		}
		foreach ($targets_indexed as $missing_column) {
			$commands[] = $this->cmdAddColumn($tableName, $missing_column);
		}

		if ($unmodeled_db_entities && $show_warnings && CFG_S::$DEVMODE) {
			new Warning("Unmodeled database entities:\n* " . implode("\n* ", $unmodeled_db_entities));
		}

		return $commands;
	}

	private function compareColumn($tableName, Column $target, Column $actual)
	{
		$commands = array();
		$changes = array();

		//default value
		if ($target->getDefault() !== $actual->getDefault()) {
			$changes[] = ($actual->getDefault() ? "default=" . $actual->getDefault() : "no default");
		}

		//nullable
		if ($target->isNullable() !== $actual->isNullable()) {
			$changes[] = ($actual->isNullable() ? "nullable" : "not nullable");
		}

		//sign
		if ($target->isSigned() !== $actual->isSigned()) {
			$changes[] = ($actual->isSigned() ? "signed" : "unsigned");
		}

		//type
		if ($target->getDataType() !== $actual->getDataType()) {
			$changes[] = "type=" . $this->mapDataTypeDb($actual->getDataType());
		} else if ($target->getDataType() === Column::DATATYPE_STRING) {
			if ($target->getStringMaxlength() != $actual->getStringMaxlength()
				&& !(!$target->getStringMaxlength() && $actual->getStringMaxlength() == Column::STRING_DEFAULT_LENGTH)
			) {
				$changes[] = "maxLength=" . $actual->getStringMaxlength();
			}
		}

		//comment
		if ($target->getComment() != $actual->getComment()
		) {
			$changes[] = $actual->getComment()
				? "comment=" . $actual->getComment()
				: "no comment";
		}

		if ($changes) {
			$cmd = $this->cmdUpdateColumn($tableName, $target);
			$comment = implode(" / ", $changes);
			$commands[] = $this->comment($comment) . "\n" . $cmd;
		}

		return $commands;
	}

}