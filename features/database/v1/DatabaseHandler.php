<?php

namespace tt\features\database\v1;

use tt\features\config\v1\ConfigProject;
use tt\features\config\v1\ConfigServer;
use tt\features\database\v1\db_mysql\DatabaseMySql;
use tt\features\database\v1\db_sqlite\DatabaseSqLite;
use tt\features\debug\errorhandler\v1\Error;
use tt\features\debug\errorhandler\v2\Warning;

/**
 * @deprecated TODO
 */
class DatabaseHandler
{

	const DB_ENGINE_NONE = "none";
	const DB_ENGINE_MYSQL = "mysql";
	const DB_ENGINE_SQLITE = "sqlite";

	private static $default_db = null;

	public static function init_database_from_cfg()
	{
		self::init_database(
			ConfigProject::$DB_TYPE,
			ConfigServer::$DB_HOST,
			ConfigServer::$DB_SCHEMA,
			ConfigServer::$DB_USER,
			ConfigServer::$DB_PASS
		);
	}

	public static function init_database($type, $host, $schema, $user, $password)
	{
		$database = null;
		if ($type === DatabaseHandler::DB_ENGINE_MYSQL) {
			$database = new DatabaseMySql($host, $schema, $user, $password);
		}
		if ($type === DatabaseHandler::DB_ENGINE_SQLITE) {
			new Error("Not ready for SQLite :-(");
			$filename = $host . ($host && $schema ? "/" : "") . $schema;
			$database = new DatabaseSqLite($filename);
		}
		self::$default_db = $database;
	}

	/**
	 * @return DatabaseGeneral|null
	 */
	public static function getDefaultDb()
	{
		return self::$default_db;
	}

	public static function checkDbDependencies()
	{
		if (DatabaseHandler::getDefaultDb() === null) {
			new Warning("Database not set!");
			return false;
		}
		if (DatabaseHandler::getDefaultDb()->isIntentionallyNotConnected()) {
			new Warning("Database not connected!");
			return false;
		}
		return true;
	}

}