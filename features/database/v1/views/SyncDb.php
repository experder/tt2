<?php

namespace tt\features\database\v1\views;

use tt\features\config\v1\ConfigProject;
use tt\features\config\v1\ConfigServer;
use tt\features\css\Stylesheet;
use tt\features\database\v1\DatabaseHandler;
use tt\features\database\v1\DbUpdater;
use tt\features\htmlpage\view\v1\ViewHtml;

class SyncDb extends ViewHtml
{

	private $title="Sync DB";

	public static function getClass() {
		/** @noinspection PhpFullyQualifiedNameUsageInspection */
		return \tt\services\polyfill\Php5::get_class();
	}

	/**
	 * @return string
	 */
	public function getHtml()
	{
		if(!DatabaseHandler::checkDbDependencies())return "";
		$upd = new DbUpdater();
		return $upd->doUpdate();
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	public function getCss()
	{
		return array(
			new Stylesheet(
				ConfigServer::$HTTP_PROJ_SKIN_DIR .'/'.ConfigProject::$SKIN_ID.'/syncdb.css',
				ConfigServer::$HTTP_PROJ_SKIN_DIR .'/'.ConfigProject::$SKIN_ID.'/darkmode_syncdb.css'
			)
		);
	}

}