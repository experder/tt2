<?php

namespace tt\features\database\v1\views;

use tt\features\config\v1\CFG_S;
use tt\features\database\v1\Model;
use tt\features\debug\errorhandler\v1\Error;
use tt\features\htmlpage\components\Form;
use tt\features\htmlpage\view\v1\ViewHtml;
use tt\services\ServiceEnv;

/**
 * @deprecated
 */
class EditModel extends ViewHtml
{

	const PARAM_classname = 'classname';
	const PARAM_id = 'id';

	const CMD_DO = 'do';

#	protected $showNavigation = false;

	/**
	 * @var Model $instance
	 */
	private $instance;

	public function __construct()
	{
		$class = ServiceEnv::valueFromGet(self::PARAM_classname, ServiceEnv::DEFAULT_QUIT_WITH_ERROR);
		$id = ServiceEnv::valueFromGet(self::PARAM_id, ServiceEnv::DEFAULT_QUIT_WITH_ERROR);

		$this->instance = new $class();
		if(!($this->instance instanceof Model))new Error("Class "
			.(CFG_S::$DEVMODE?get_class($this->instance):basename(get_class($this->instance)))
			." not instance of Model!");
		$this->instance->fromDbWhereEqualsId($id);
		if(!$this->instance->getId()){
			new Error($this->instance->getGuiName()." #$id does not exist!");
		}
	}

	public static function getClass() {
		/** @noinspection PhpFullyQualifiedNameUsageInspection */
		return \tt\services\polyfill\Php5::get_class();
	}

	/**
	 * @return string
	 */
	public function getHtml()
	{
		if(ServiceEnv::valueFromPost(Form::COMMAND_STRING)===self::CMD_DO){
			return $this->instance->editFormPostProcess();
		}
		return $this->htmlInitial();
	}

	private function htmlInitial()
	{
		$title = "<h1>".$this->instance->getGuiName()."</h1>";
		$form = $this->instance->getEditForm();
		$form->addClass('tt_editmodel');
		$form->addHiddenField(Form::COMMAND_STRING, self::CMD_DO);
		$form_html = $form->toHtml();
		return $title.$form_html;
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		return "Edit ".$this->instance->getGuiName()." #".$this->instance->getId();
	}

	public function getCss()
	{
		return $this->instance->getEditFormCss();
	}
	public function getJsUrls(){
		return $this->instance->getEditFormJs();
	}

	public function getHeadJs(){
//		if(ServiceEnv::valueFromPost(Form::COMMAND_STRING)===self::CMD_DO){
//			return "window.close();";
//		}
		return "";
	}

}