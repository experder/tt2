<?php

namespace tt\features\database\v1;

class WhereEquals extends WhereCondition
{

	/**
	 * @param string $key
	 * @param mixed $value
	 */
	public function __construct($key, $value)
	{
		parent::__construct($key, WhereCondition::RELATION_EQUALS, $value);
	}

}