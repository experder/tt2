<?php

namespace tt\features\database\v1;

class LimitClause
{

	private $offset;
	private $rowCount;

	/**
	 * @param $offset
	 * @param $rowCount
	 */
	public function __construct($rowCount, $offset=false)
	{
		$this->offset = $offset;
		$this->rowCount = $rowCount;
	}

	/**
	 * @return mixed
	 */
	public function getRowCount()
	{
		return $this->rowCount;
	}

}