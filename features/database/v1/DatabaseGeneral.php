<?php /** @noinspection PhpUnused TODO */

namespace tt\features\database\v1;

use tt\features\database\schema\Table;

abstract class DatabaseGeneral
{

	const HOST_DO_NOT_CONNECT = "tt/HOST_DO_NOT_CONNECT";

	protected $host;
	protected $schema;
	protected $username;

	/**
	 * @param string $host
	 * @param string $schema
	 * @param string $username
	 * @param string $password
	 */
	public function __construct($host, $schema = null, $username = null, $password = null)
	{
		$this->host = $host;
		$this->schema = $schema;
		$this->username = $username;
		if (!$this->isIntentionallyNotConnected()) {
			$this->connect($password);
		}
	}

	public function isIntentionallyNotConnected()
	{
		return $this->host === DatabaseGeneral::HOST_DO_NOT_CONNECT;
	}

	/**
	 * @return string
	 */
	public function getSchema()
	{
		return $this->schema;
	}

	/**
	 * @param string $table
	 * @return Table|false FALSE if table does not exist
	 */
	abstract function getTableSchemaFromDb($table);

	/**
	 * @param Table $schema
	 * @return string[]
	 */
	abstract function cmdCreateTable(Table $schema);

	/**
	 * @param Table $target
	 * @param Table $actual
	 * @param bool $show_warnings
	 * @return string[] Commands needed to transform $actual into $target
	 */
	abstract function compareSchema(Table $target, Table $actual, $show_warnings = false);

	abstract function connect($password);

	/**
	 * @param string|string[] $string
	 * @return true Terminates with an error if execution fails
	 */
	abstract function execute($string);

	/**
	 * @param string $table
	 * @param array $data [{"foo":"bar",...},...]
	 * @return false|string
	 */
	abstract function insertByArray($table, $data);

	/**
	 * @param string $table
	 * @param array $data [{"foo":"bar",...},...]
	 * @param WhereCondition[] $whereConditions
	 * @return false|int
	 */
	abstract function updateByArray($table, $data, array $whereConditions);

	/**
	 * @param string[] $subset Default: All fields: ['*']
	 * @param string $table_name
	 * @param array $joinWhereOrder array of WhereConditions, JoinStatements and OrderClauses
	 * @return array Example: [{"foo":"bar"}]
	 * @see JoinStatement
	 * @see OrderClause
	 * @see WhereCondition
	 */
	abstract function generalQuery(array $subset, $table_name, array $joinWhereOrder, $debugId=false);

}