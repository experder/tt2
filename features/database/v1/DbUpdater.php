<?php

namespace tt\features\database\v1;

use tt\features\database\v2\Schema;
use tt\features\debug\errorhandler\v1\Error;
use tt\features\debug\errorhandler\v2\Warning;
use tt\features\htmlpage\components\Form;
use tt\features\i18n\Trans;
use tt\features\messages\v2\MsgConfirm;
use tt\features\modulehandler\v1\ModulehandlerV1;
use tt\services\ServiceHtml;
use tt\services\ServiceStrings;

/**
 * Updates DB according to the specified models
 */
class DbUpdater
{

	const CMD_EXECUTE = "execute";
	const POSTVAL_COMMANDS = "commands";

	/**
	 * @var DatabaseGeneral $db
	 */
	private $db;

	/**
	 * @param DatabaseGeneral $db
	 */
	public function __construct(DatabaseGeneral $db = null)
	{
		if ($db === null) {
			$db = DatabaseHandler::getDefaultDb();
			if ($db === null) {
				new Error("No database set!"
					."\nhttps://gitlab.com/experder/tt2/-/blob/main/features/config/v1/ConfigServer.md");
			}
		}
		$this->db = $db;
	}

	public function doUpdate()
	{
		if (isset($_POST[Form::COMMAND_STRING])) {
			$cmd = $_POST[Form::COMMAND_STRING];
			if ($cmd === DbUpdater::CMD_EXECUTE) {
				return $this->htmlExecute();
			}
			new Error("Unknown command '$cmd'!");
			return "";
		} else {
			return $this->htmlInitial();
		}
	}

	private function htmlExecute()
	{
		$commands = $this->getCommandsAll();
		$commands_now = ServiceStrings::eliminate_windows_linebreaks(implode("\n", $commands));
		$commands_before = ServiceStrings::eliminate_windows_linebreaks($_POST[DbUpdater::POSTVAL_COMMANDS]);
		if (md5($commands_now) !== md5($commands_before)) {
			new Error("Database has changed!\n" . $commands_now);
		}
		$this->db->execute($commands);
		$commands_new = $this->getCommandsAll();
		if ($commands_new) {
			new Warning("Execution failed!\n" . implode("\n", $commands_new));
		} else {
			new MsgConfirm("OK!");
		}
		return "";
	}

	private function getCommandsAll($show_warnings = false)
	{
		$moduleHandlers = ModulehandlerV1::$modulesRegistration;
		$commands = array();
		foreach ($moduleHandlers as $moduleHandler) {
			$models = $moduleHandler->getModelSchema();
			$more_commands = $this->getCommands($models, $show_warnings);
			$commands = array_merge($commands, $more_commands);
		}
		return $commands;
	}

	private function htmlInitial()
	{
		$commands = $this->getCommandsAll(true);
		if (!$commands) {
			new MsgConfirm("Everything in sync!");
			return "";
		}

		$preview = "<pre class='db_commands'>" . ServiceHtml::arrayToList_(
				ServiceHtml::htmlentitiesArray($commands)
			) . "</pre>";

		$form = new Form(DbUpdater::CMD_EXECUTE, Trans::late("Execute!"));
		$form->addHiddenField(DbUpdater::POSTVAL_COMMANDS, implode("\n", $commands));

		return $preview . $form->toHtml();
	}

	/**
	 * @param Model[]|Schema[] $models
	 * @param bool $show_warnings
	 * @return string[]
	 */
	private function getCommands(array $models, $show_warnings)
	{
		$commands = array();
		foreach ($models as $model) {
			if($model instanceof Schema)continue;
			if (!($model instanceof Model)) {
				new Error("Class \""
					. get_class($model) . "\" is not instance of \""
					. Model::getClassModel() . "\"!");
			}
			$schema_model = $model->getSchema();
			$schema_db = $this->db->getTableSchemaFromDb($schema_model->getName());
			if ($schema_db === false) {
				$createCommands = $this->db->cmdCreateTable($schema_model);
				if (!is_array($createCommands)) $createCommands = array($createCommands);
				foreach ($createCommands as $cmd) {
					$commands[] = $cmd;
				}
			} else {
				$more_commands = $this->db->compareSchema(
					$schema_model,
					$schema_db,
					$show_warnings
				);
				$commands = array_merge($commands, $more_commands);
			}
		}
		return $commands;
	}

}