Please create a file <code>ConfigLoader.php</code> in the TT root directory (or in the parent directory) pointing to
your config files.

As the project config registeres the root namespace of your project for TT's autoloader you need to include this
file manually. If you store your project config one folder above TT root directory and name it
<code>Config_project.php</code> you can skip this step.

File template: https://gitlab.com/experder/tt2/-/blob/main/features/installer/templates/ConfigLoader.php.template
