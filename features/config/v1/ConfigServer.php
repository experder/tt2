<?php

namespace tt\features\config\v1;

use tt\features\debug\errorhandler\v1\ErrorCore;

abstract class ConfigServer
{

	/*
	 * DEV stuff
	 */
	public static $DEVMODE = false;
	public static $DEBUGINFO_SHOWQUERIES = false;
	public static $CSS_NIGHTMODE = false;
	public static $EMULATE_MOBILE = false;

	/*
	 * Server paths
	 */
	/** @var string $HTTP_TT_ROOT Route that Server maps to TT root */
	public static $HTTP_TT_ROOT = null;
	/** @var string $HTTP_RUN_ALIAS Route that Server maps to TT frontcontroller '/r' */
	public static $HTTP_RUN_ALIAS = null;
	/** @var string[] $HTTP_MODULE_ROOT key=module-ID, value=server path to module root */
	public static $HTTP_MODULE_ROOT = array();
	public static $HTTP_PROJ_SKIN_DIR = null;
	public static $HTTP_THIRDPARTY_DIR = null;

	/*
	 * Directories
	 */
	public static $DIR_TT_ROOT = null;
	public static $DIR_PROJ_ROOT = null;
	public static $DIR_THIRDPARTY_ROOT = null;

	/**
	 * @deprecated
	 */
	public static $DB_HOST = "localhost";
	/**
	 * @deprecated
	 */
	public static $DB_SCHEMA = null;
	/**
	 * @deprecated
	 */
	public static $DB_USER = null;
	/**
	 * @deprecated
	 */
	public static $DB_PASS = null;

	public static function guess_values(){

		if(self::$HTTP_TT_ROOT===null){
			new ErrorCore("Please set ConfigServer::\$HTTP_TT_ROOT !");
		}

		if(self::$HTTP_PROJ_SKIN_DIR==null){
			self::$HTTP_PROJ_SKIN_DIR=self::$HTTP_TT_ROOT."/features/skins";
		}

		if(self::$HTTP_RUN_ALIAS===null){
			self::$HTTP_RUN_ALIAS=self::$HTTP_TT_ROOT.'/r/?tt_route=';
		}

		if(self::$DIR_TT_ROOT===null){
			self::$DIR_TT_ROOT=dirname(dirname(dirname(__DIR__)));
		}

		if(self::$DIR_PROJ_ROOT===null){
			self::$DIR_PROJ_ROOT=dirname(self::$DIR_TT_ROOT);
		}

		if(self::$DIR_THIRDPARTY_ROOT===null){
			self::$DIR_THIRDPARTY_ROOT=dirname(self::$DIR_TT_ROOT).'/thirdparty';
		}

	}

	public function configSetValues(){
		$this->setConfigValues();
		self::guess_values();
	}

	abstract function setConfigValues();

}