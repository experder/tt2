

## Example server configuration
```php
<?php
class Config_server extends ConfigServer{
  function setConfigValues(){
    ConfigProject::$DB_TYPE = DatabaseHandler::DB_ENGINE_MYSQL;
    ConfigServer::$DB_HOST = "localhost";
    ConfigServer::$DB_SCHEMA = "tt_demo";
    ConfigServer::$DB_USER = "tt_user";
    ConfigServer::$DB_PASS = "your_password_here";
    //...
  }
}
```