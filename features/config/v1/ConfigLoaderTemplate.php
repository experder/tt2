<?php

namespace tt\features\config\v1;

use tt\ConfigLoader;
use tt\features\debug\errorhandler\v1\Error;
use tt\features\installer\Installer;
use tt\services\ServiceFiles;

/**
 * Please create a file that points to your config files:
 * https://gitlab.com/experder/tt2/-/blob/main/features/config/v1/ConfigLoader.md
 */
abstract class ConfigLoaderTemplate
{

	private $configProject;
	private $configServer = null;

	private static $configLoaded = false;

	public function __construct()
	{
		self::init();
		$this->configProject = $this->newConfigProject();
	}

	public static function init(){
		//Suggestion for project specific config file:
		ServiceFiles::includeIfExists(dirname(dirname(dirname(dirname(__DIR__)))).'/Config_project.php');
	}

	/**
	 * @return ConfigLoader
	 */
	public static function getConfigs(){
		if(self::$configLoaded){
			new Error("Attention! Config has been loaded already!");
		}
		self::$configLoaded=true;

		//Try loading ConfigLoader from parent directory:
		ServiceFiles::includeIfExists(dirname(dirname(dirname(dirname(__DIR__)))).'/ConfigLoader.php');

		if(!class_exists("tt\ConfigLoader")){

			$configLoader_file = dirname(dirname(dirname(__DIR__))).'/ConfigLoader.php';

			if(!file_exists($configLoader_file)){
				$configLoader_file = Installer::getSingleton()->createConfigPointer();
			}

			require_once $configLoader_file;

			if(!class_exists("tt\ConfigLoader")){
				new Error("Wrong class in ConfigLoader!");
			}

		}

		$configLoader = new ConfigLoader();

		/** @noinspection PhpConditionAlreadyCheckedInspection */
		if(!($configLoader instanceof ConfigLoaderTemplate)){
			new Error("Must inherit ConfigLoaderTemplate!");
		}

		return $configLoader;
	}

	/**
	 * @return ConfigProject
	 */
	public function getConfigProject()
	{
		return $this->configProject;
	}

	/**
	 * @return ConfigServer
	 */
	public function getConfigServer()
	{
		if($this->configServer===null){
			$this->configServer = $this->newConfigServer();
		}
		return $this->configServer;
	}

	/**
	 * @return ConfigProject
	 */
	abstract function newConfigProject();

	/**
	 * @return ConfigServer
	 */
	abstract function newConfigServer();

}