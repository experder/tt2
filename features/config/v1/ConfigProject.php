<?php

namespace tt\features\config\v1;

use tt\features\autoloader\simple1\AutoloaderSimple;
use tt\features\autoloader\simple1\RootNamespace;
use tt\features\database\v1\DatabaseHandler;
use tt\features\modulehandler\v1\Module;

abstract class ConfigProject
{

	public static $PROJECT_TITLE = "MyProject";

	/**
	 * @deprecated
	 */
	public static $DB_TYPE = DatabaseHandler::DB_ENGINE_NONE;
	public static $DB_SYNC_COMPLETE = true;

	public static $WHAT_IS_TOO_LONG_FOR_A_PAGE_TO_LOAD = 0.1/* seconds */;

	public static $SKIN_ID = 'demoskin';
	public static $SKIN_BILDER_SAGEN_MEHR_ALS_WORTE = true;

	abstract function setConfigValues();

	public function configLoadNamespaces(){
		$namespaces = $this->registerNamespaces();
		foreach ($namespaces as $namespace){
			AutoloaderSimple::registerRootNamespace($namespace);
		}
	}

	/**
	 * @return Module[]
	 */
	abstract function registerModules();

	/**
	 * @return RootNamespace[]
	 */
	abstract function registerNamespaces();

}