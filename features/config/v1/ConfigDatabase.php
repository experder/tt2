<?php /** @noinspection PhpUnused TODO */

namespace tt\features\config\v1;

use tt\features\database\schema\Column;
use tt\features\database\schema\Table;
use tt\features\database\v1\Model;

class ConfigDatabase extends Model
{

	/**
	 * @var array $settings Cache. Associative array: Key = '$module_id|$cfg_idstring|$user'
	 */
	private static $settings = array();

	public static $table_name = "core_settings";

	protected $module;
	const FIELD_module = 'module';
	protected $idstring;
	const FIELD_idstring = 'idstring';
	protected $user;
	const FIELD_user = 'user';
	protected $value;
	const FIELD_value = 'value';

	public function getTableName(){
		return self::$table_name;
	}

	/**
	 * @param Table $table
	 * @return void
	 */
	function changeDefaultSchema(Table $table)
	{
		$table->getColumn(self::FIELD_module)
			->setDataTypeString(40)
		;
		$table->getColumn(self::FIELD_idstring)
			->setDataTypeString(80)
			->addIndexUnique($table)
			->setNotNullable()
		;
		$table->getColumn(self::FIELD_user)
			->setDataTypeInteger()
		;
		$table->getColumn(self::FIELD_value)
			->setDataType(Column::DATATYPE_TEXT)
		;
	}

	/**
	 * @param string $module_id
	 * @param string $cfg_idstring
	 * @param int|false $user
	 * @return string|false
	 */
	public static function getValue($module_id, $cfg_idstring, $user=false){
		$key = "$module_id|$cfg_idstring|$user";
		if (isset(self::$settings[$key])) return self::$settings[$key];
		$model = new ConfigDatabase();
		$ok = $model->fromDbWhereEquals(array(
			self::FIELD_module=>$module_id,
			self::FIELD_idstring=>$cfg_idstring,
			self::FIELD_user=>$user===false?null:$user,
		),array(ConfigDatabase::FIELD_value));
		$value = $ok===false?false:$model->value;
		self::$settings[$key] = $value;
		return $value;
	}

	function getGuiName()
	{
		return "ConfigDatabase.php";
	}

}