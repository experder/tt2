<?php /** @noinspection PhpUnusedLocalVariableInspection */

namespace tt\features\config\v1;

use tt\features\database\v2\Schema;
use tt\features\database\v2\SchemaColumn;
use tt\services\polyfill\Php5;

class ConfigDatabase2 extends Schema
{

	const COL_module = 'module';
	const COL_idstring = 'idstring';
	const COL_user = 'user';
	const COL_value = 'value';

	public static function getClass() {
		return Php5::get_class();
	}

	/**
	 * @return string
	 */
	public function getTableName() {
		return "core_settings";
	}

	/**
	 * @return SchemaColumn[]
	 */
	protected function createTableCols() {
		return array(
			($x=new SchemaColumn($this, self::COL_module, SchemaColumn::DATATYPE_STRING))
				->setMaxLength(40)
			,
			($x=new SchemaColumn($this, self::COL_idstring, SchemaColumn::DATATYPE_STRING))
				->setNotNullable()
				->setMaxLength(80)
				->addIndexUnique()
			,
			($x=new SchemaColumn($this, self::COL_user, SchemaColumn::DATATYPE_INTEGER))
			,
			($x=new SchemaColumn($this, self::COL_value, SchemaColumn::DATATYPE_TEXT))
			,
		);
	}
}