<?php

namespace tt\features\debug;

use tt\features\config\v1\CFG_S;
use tt\features\config\v1\ConfigProject;
use tt\features\database\v2\Database;
use tt\features\database\v2\engines\mysql\DatabaseMySql;
use tt\features\database\v2\engines\sqlite\DatabaseSqlite;
use tt\services\polyfill\Php7;
use tt\services\ServiceEnv;
use tt\services\ServiceHtml;

class DebugHandler
{

	/**
	 * @var float|null $starttime
	 */
	private static $starttime = null;
	/**
	 * @var string[] $queries
	 */
	private static $queries = array();

	public static function setStart(){
		self::$starttime = microtime(true);
	}

	public static function getEnd($round = 3){
		if(self::$starttime===null) return null;
		return round(microtime(true)-self::$starttime,$round);
	}

	public static function addQuery($query){
		self::$queries[] = $query;
	}

	/**
	 * @return string[]
	 */
	public static function getQueries()
	{
		return self::$queries;
	}

	public static function getHtmlComponent($head, $innerHtml, $outerCssClass=""){
		#$id='ttid'.PageHtml::getNextGlobalCounter();
		#$id = uniqid('ttuid_');
		$id = 'ttuuid_'.bin2hex(Php7::random_bytes(4));
		$head = "<div class='tt_debug_comp_head' onclick='tt.toggleHide(\"#$id\");'>$head</div>";
		$body = "<div class='tt_debug_comp_body tt_hide' id='$id'>$innerHtml</div>";
		return "<div class='tt_debug_comp $outerCssClass'>$head$body</div>";
	}

	public function getHtmlDebugInfo_queries(){
		if(!CFG_S::$DEBUGINFO_SHOWQUERIES)return "";
		$queries = DebugHandler::getQueries();
		if(!$queries)return "";
		$queries_highlighted = array();
		foreach ($queries as $query){
			$query = htmlentities($query);
			$query = preg_replace("/(SELECT \\*)([ \\n]FROM)/i", "<b class='warn'>$1</b>$2", $query);
			$queries_highlighted[] = $query;
		}
		$db = Database::getGlobalDatabase();
		$class = ($db instanceof DatabaseMySql?"db_mysql":($db instanceof DatabaseSqlite?"db_sqlite":"db_unknown"));
		$head = "<span class='$class'>".$db->getSchema()."</span> ".count($queries_highlighted);
		return self::getHtmlComponent($head, "<pre class='debug_queries'>"
			.ServiceHtml::arrayToList_($queries_highlighted)."</pre>", "dbdebug");
	}

	public function getHtmlDebugInfo_duration(){
		$duration = DebugHandler::getEnd();
		$duration_html = $duration.'"';
		if($duration>=ConfigProject::$WHAT_IS_TOO_LONG_FOR_A_PAGE_TO_LOAD){
			$duration_html = "<b class='warn'>$duration_html</b>";
		}

		$content = ServiceEnv::isSapiCLI()?"[[CLI]]":$_SERVER["HTTP_USER_AGENT"];

		return self::getHtmlComponent($duration_html, $content);
	}

	public function getHtmlDebugInfo_post(){
		if(!$_POST)return "";
		$html = array();
		foreach ($_POST as $key=>$value){
			$value = htmlentities($value);
			$html[] = "[$key] => $value";
		}
		$head="POST";
		return self::getHtmlComponent($head, "<pre>".implode("\n",$html)."</pre>");
	}

}