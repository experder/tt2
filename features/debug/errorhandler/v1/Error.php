<?php

namespace tt\features\debug\errorhandler\v1;

use Exception;
use tt\features\config\v1\CFG_S;
use tt\features\config\v1\ConfigServer;
use tt\features\htmlpage\view\v1\PageHtml;
use tt\features\messages\v2\MsgError;
use tt\services\DEBUG;
use tt\services\polyfill\Php7;
use tt\services\ServiceEnv;
use tt\services\ServiceResponse;
use tt\services\ServiceStrings;
use tt\services\UnicodeIcons;

class Error
{

	private static $recursion_protection_errorhandler = 0;
	protected $coreError = false;

	public function __construct($message, $cut=0)
	{
		self::$recursion_protection_errorhandler++;
		if(self::$recursion_protection_errorhandler>2){
			echo "!!! Error::\$recursion_protection_errorhandler";
			exit;
		}
		if(ServiceEnv::isSapiCLI()){
			echo "\n-----------------\n$message\n-----------------\n";
			if(ConfigServer::$DEVMODE){
				echo "Backtrace:\n* ".implode("\n* ",DEBUG::backtrace());
			}
			exit;
		}
		if(ServiceEnv::isRequestAjax()){
			$response = array(
				"ok"=>false,
				"errormsg"=>$message,
			);
			if(ConfigServer::$DEVMODE){
				$response["backtrace"] = DEBUG::backtrace();
			}
			ServiceResponse::respondJson($response);
		}
		$backtrace = ConfigServer::$DEVMODE?DEBUG::backtraceHtml($cut+1):"";

		if(self::$recursion_protection_errorhandler>1 || $this->coreError){
			echo "<hr><pre style='"
				."white-space: pre-wrap;"
				."color:darkred;"
				."'>"
				.Php7::mb_chr(UnicodeIcons::No_Entry)." "
				.ServiceStrings::ttMarkup(htmlentities($message))
				."</pre><hr>";
			echo $backtrace;
			exit;
		}

		$view = new ViewError($backtrace);
		new MsgError("<pre class='msgOnlyPre'>".ServiceStrings::ttMarkup(htmlentities($message))."</pre>");
		new PageHtml($view);
		exit;
	}

	public static function fromException(Exception $exception, $cut=0){
		$type = CFG_S::$DEVMODE&&get_class($exception)!=="Exception"?" (".get_class($exception).")":"";
		$message = "EXCEPTION$type! ".$exception->getMessage();
		#$message.="\n---------------\n".print_r($exception,1);
		new Error($message, $cut+1);
	}

}