<?php

namespace tt\features\debug\errorhandler\v1;

use tt\features\htmlpage\view\v1\ViewHtmlNew;

class ViewError extends ViewHtmlNew
{

	public $prefixWithTitle = false;

	private $html;

	public function __construct($html)
	{
		$this->html=$html;
	}

	/**
	 * @return string
	 */
	function getHtml()
	{
		return $this->html;
	}

	/**
	 * @return string
	 */
	function getTitle()
	{
		return "ERROR";
	}

}