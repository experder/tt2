<?php

namespace tt\features\debug\errorhandler\v1;

use tt\features\config\v1\ConfigServer;

class Error404 extends Error
{

	public function __construct($message="")
	{
		parent::__construct($message, 1);
		http_response_code(404);
		$referrer=ConfigServer::$DEVMODE&&isset($_SERVER['HTTP_REFERER'])?
			" (Referrer: ".$_SERVER['HTTP_REFERER']." )":"";
		parent::__construct("404: Not Found$referrer. ".$message);
	}

}