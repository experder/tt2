<?php

namespace tt\features\debug\errorhandler\v2;

use tt\features\config\v1\CFG_S;
use tt\features\messages\v2\MsgWarning;
use tt\services\DEBUG;
use tt\services\ServiceEnv;
use tt\services\ServiceStrings;

class Warning
{

	/**
	 * @param string $message
	 */
	public function __construct($message)
	{
		new MsgWarning("<pre class='msgOnlyPre'>".ServiceStrings::ttMarkup(htmlentities($message))."</pre>");
		if(!ServiceEnv::isSapiCLI())return;
		echo "\n-----------------\n$message\n-----------------\n";
		if (CFG_S::$DEVMODE){
			echo "Backtrace:\n* ".implode("\n* ",DEBUG::backtrace());
			echo "\n-----------------\n";
		}
	}

}