<?php

namespace tt\features\htmlpage\components;

use tt\services\ServiceHtml;

class FormHiddenField extends HtmlComponent
{

	/**
	 * @var string $key
	 */
	private $key;
	/**
	 * @var string $value
	 */
	private $value;

	/**
	 * @param string $key
	 * @param string $value
	 */
	public function __construct($key, $value)
	{
		$this->key = $key;
		$this->value = $value;
	}

	/**
	 * @return string
	 */
	function toHtml()
	{
		return "<input type='hidden' name='" . ServiceHtml::escape_value_html($this->key) . "'"
			. " value='" . ServiceHtml::escape_value_html($this->value) . "'>";
	}

}