<?php

namespace tt\features\htmlpage\components;

class FormInputTextarea extends HtmlCompFormInput
{

	/**
	 * @var TextareaFunction[] $functions
	 */
	private $functions = array();

	/**
	 * @param string $name
	 * @param string|bool $label TRUE: Label equals name, FALSE: No label
	 * @param string $value
	 */
	public function __construct($name, $label = true, $value = "")
	{
		$this->name = $name;
		$this->label = $label;
		$this->value = $value;
	}

	public function addFunction(TextareaFunction $function)
	{
		$this->functions[] = $function;
	}

	/**
	 * @return string
	 */
	function toHtml()
	{
		$functionsHtml = array();
		foreach ($this->functions as $function){
			$functionsHtml[] = $function->getHtml($this->getId());
		}
		$textarea = $this->buildNode("textarea", array(
			"type" => "text",
			"name" => $this->name,
		), $this->getValue($this->value));
		return $this->getLabel() . "\n<div class='formInput'>"
			.$textarea
			.($functionsHtml?"<br>".implode("\n",$functionsHtml):"")
			."</div>"
		;
	}

}