<?php

namespace tt\features\htmlpage\components;

class Html_UL extends HtmlComponent
{

	/**
	 * @return string
	 */
	function toHtml()
	{
		return self::buildNode("ul", array());
	}

}