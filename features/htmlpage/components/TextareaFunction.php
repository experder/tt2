<?php

namespace tt\features\htmlpage\components;

abstract class TextareaFunction
{

	abstract function getHtml($reference_id);

}