<?php

namespace tt\features\htmlpage\components;

class Html_LI extends HtmlComponent
{

	/**
	 * @var string $inner_before
	 */
	private $inner_before;

	/**
	 * @param string $inner_before
	 */
	public function __construct($inner_before="")
	{
		$this->inner_before = $inner_before;
	}

	/**
	 * @return string
	 */
	function toHtml()
	{
		return self::buildNode("li", array(), $this->inner_before);
	}

}