<?php

namespace tt\features\htmlpage\components\table;

use tt\features\htmlpage\components\HtmlComponent;

class SimpleTableRow extends HtmlComponent
{

	/**
	 * @param string[] $data
	 */
	public function __construct(array $data=array())
	{
		foreach ($data as $string){
			$this->add(new SimpleTableCell($string));
		}
	}

	/**
	 * @return string
	 */
	function toHtml()
	{
		return $this->buildNode("tr", array());
	}
}