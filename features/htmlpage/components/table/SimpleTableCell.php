<?php

namespace tt\features\htmlpage\components\table;

use tt\features\htmlpage\components\HtmlComponent;

class SimpleTableCell extends HtmlComponent
{

	protected $cell_type = "td";

	/**
	 * @var string $content
	 */
	private $content;

	/**
	 * @param string $content
	 */
	public function __construct($content)
	{
		$this->content = $content;
	}

	/**
	 * @return string
	 */
	function toHtml()
	{
		return $this->buildNode($this->cell_type, array(), $this->content);
	}

}