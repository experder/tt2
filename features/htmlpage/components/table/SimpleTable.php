<?php

namespace tt\features\htmlpage\components\table;

use tt\features\htmlpage\components\HtmlComponent;

class SimpleTable extends HtmlComponent
{

	/**
	 * @param string[][] $data
	 */
	public function __construct(array $data, $header=true)
	{
		//First child: Header
		$this->children[0] = null;
		foreach ($data as $row){
			$this->addRow($row);
		}
		if($header!==false){
			$this->setHeader($header===true?array_keys($data[0]):$header);
		}
	}

	/**
	 * @return string
	 */
	function toHtml()
	{
		return $this->buildNode("table", array());
	}

	public function setHeader($array)
	{
		$header = new SimpleTableRow();
		foreach ($array as $key){
			$cell = new SimpleTableHeader($key);
			$header->add($cell);
		}
		$this->children[0] = $header;
	}

	private function addRow(array $rowArray)
	{
		$rowComp = new SimpleTableRow();
		foreach ($rowArray as $col){
			$cell = new SimpleTableCell($col);
			$rowComp->add($cell);
		}
		$this->add($rowComp);
	}

}