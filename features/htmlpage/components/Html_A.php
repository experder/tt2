<?php /** @noinspection PhpUnused TODO */

namespace tt\features\htmlpage\components;

class Html_A extends HtmlComponent
{

	const CLASS_BUTTON = 'button';

	private $display_text;
	/**
	 * @var string|false
	 */
	private $href;

	/**
	 * @param string|false $href
	 * @param string|true $display_text
	 */
	public function __construct($href, $display_text = true)
	{
		$this->display_text = $display_text === true ? $href : $display_text;
		$this->href = $href;
	}

	public function setTargetBlank()
	{
		$this->addKeyVal(HtmlComponent::KEY_TARGET, '_blank');
	}

	public function addClassButton()
	{
		$this->addClass(Html_A::CLASS_BUTTON);
	}

	public function removeHref()
	{
		$value = $this->href;
		$this->href = false;
		return $value;
	}

	/**
	 * @return string
	 */
	function toHtml()
	{
		$params = array();
		if ($this->href !== false) {
			$params['href'] = $this->href;
		}
		return self::buildNode("a", $params, $this->display_text);
	}
}