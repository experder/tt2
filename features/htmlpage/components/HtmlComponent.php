<?php

namespace tt\features\htmlpage\components;

use tt\features\database\schema\Column;
use tt\features\database\schema\ForeignKey;
use tt\features\database\v1\Model;
use tt\features\debug\errorhandler\v1\Error;
use tt\features\htmlpage\view\v1\PageHtml;
use tt\services\ServiceHtml;

abstract class HtmlComponent
{

	const CLASS_HIDE = 'tt_hide';

	/**
	 * @var HtmlComponent[] $children
	 */
	protected $children = array();

	/**
	 * @var string[]
	 */
	private $css_classes = array();

	const KEY_ONCHANGE = 'onchange';
	const KEY_ONKEYUP = 'onkeyup';
	const KEY_ONCLICK = 'onclick';
	const KEY_TARGET = 'target';
	const KEY_TITLE = 'title';
	private $keysVals = array();

	protected $id = null;

	/**
	 * @param Column $column
	 * @param string $value
	 * @param ForeignKey|null $foreignKey
	 * @return HtmlCompFormInput|null
	 */
	public static function fromColumn(Column $column, $value = "", $foreignKey = null)
	{
		if ($foreignKey !== null) {
			return FormInputDropdown::fromModel(
				Model::getModelByTableName($foreignKey->getRefTable()),
				$column,
				$value
			);
		}
		$type = $column->getDataType();
		if ($type === Column::DATATYPE_DATE
			|| $type === Column::DATATYPE_INTEGER
		) {
			return new FormInputText($column->getName(), $column->getGuiName(), $value);
		}
		if ($type === Column::DATATYPE_TEXT) {
			return new FormInputTextarea($column->getName(), $column->getGuiName(), $value);
		}
		if ($type === Column::DATATYPE_BOOLEAN) {
			return new FormInputCheckbox($column->getName(), $column->getGuiName(), $value);
		}
		new Error("Unknown column type: " . $type);
		return null;
	}

	/**
	 * @param string $key HtmlComponent::KEY_
	 * @param string $val
	 * @return HtmlComponent
	 */
	public function addKeyVal($key, $val)
	{
		$this->keysVals[$key] = $val;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getId()
	{
		if ($this->id === null) $this->id = "ttid" . PageHtml::getNextGlobalCounter();
		return $this->id;
	}

	/**
	 * @param string $id
	 * @return HtmlComponent
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return string
	 */
	abstract function toHtml();

	public function __toString(){
		return $this->toHtml();
	}

	/**
	 * @param HtmlComponent|null|HtmlComponent[] $component
	 * @return $this
	 */
	public function add($component)
	{
		if ($component === null) return $this;
		if (is_array($component)) {
			foreach ($component as $comp) {
				$this->add($comp);
			}
			return $this;
		}
		$this->children[] = $component;
		return $this;
	}

	/**
	 * @param string $css_class
	 * @return $this
	 */
	public function addClass($css_class)
	{
		$this->css_classes[] = $css_class;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getChildrenHtml()
	{
		$html = array();
		foreach ($this->children as $child) {
			if(!($child instanceof HtmlComponent))continue;
			$html[] = $child->toHtml();
		}
		return implode("\n\t", $html);
	}

	public function getCssHtml()
	{
		if (!$this->css_classes) return "";
		$classes = implode(" ", $this->css_classes);
		return " class='$classes'";
	}

	public function buildNode($name, $keyVals, $inner_before = "", $inner_after = "")
	{
		$keyVals = $this->keysVals + $keyVals;
		return ServiceHtml::buildNode($name, $keyVals, $this, $inner_before, $inner_after);
	}

	public function buildNodeVoid($name, $keyVals)
	{
		$keyVals = $this->keysVals + $keyVals;
		return ServiceHtml::buildNode($name, $keyVals, $this, "", "", true);
	}

}