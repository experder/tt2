<?php

namespace tt\features\htmlpage\components;

class FormSubset extends HtmlComponent
{

	/**
	 * @var false|string $legend
	 */
	private $legend;

	/**
	 * @param false|string $legend
	 */
	public function __construct($legend = false)
	{
		$this->legend = $legend;
	}

	/**
	 * @return string
	 */
	function toHtml()
	{
		return $this->buildNode("fieldset", array(),
			$this->legend === false ? "" : "<legend>$this->legend</legend>");
	}

	/**
	 * @return string
	 */
	public function getChildrenHtml()
	{
		$html = array();
		foreach ($this->children as $child) {
			$html[] = "<div class='tt_formfield ttid_" . $child->getId() . "'>\n\t\t" . $child->toHtml() . "\n\t</div>";
		}
		return implode("\n\t", $html);
	}

}