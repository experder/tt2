<?php

namespace tt\features\htmlpage\components;

class FunctionEmoji extends TextareaFunction
{

	/**
	 * @var string $characters
	 */
	private $characters;

	/**
	 * @param string $characters
	 */
	public function __construct($characters)
	{
		$this->characters = $characters;
	}

	function getHtml($reference_id)
	{
		return "<a class='button' onclick=\"tt.addTextTo('$this->characters','$reference_id');\">".$this->characters."</a>";
	}

}