<?php

namespace tt\features\htmlpage\components;

class FormDiv extends HtmlCompFormInput
{

	public function __construct($label, $value)
	{
		$this->label = $label;
		$this->value = $value;
	}

	/**
	 * @return string
	 */
	function toHtml()
	{
		return $this->getLabel() . "\n\t\t" . $this->buildNode("div", array('class' => "formdiv"), $this->value);
	}

}