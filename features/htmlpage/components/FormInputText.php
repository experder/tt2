<?php

namespace tt\features\htmlpage\components;

class FormInputText extends HtmlCompFormInput
{

	/**
	 * @param string $name
	 * @param string|bool $label TRUE: Label equals name, FALSE: No label
	 * @param string $value
	 */
	public function __construct($name, $label = true, $value = "")
	{
		$this->name = $name;
		$this->label = $label;
		$this->value = $value;
	}

	/**
	 * @return string
	 */
	function toHtml()
	{
		return $this->getLabel() . "\n\t\t" . $this->buildNodeVoid("input", array(
				"type" => "text",
				"name" => $this->name,
				"value" => $this->getValue($this->value),
			)) . $this->getHelpHtml();
	}

}