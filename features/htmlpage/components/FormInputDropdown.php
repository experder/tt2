<?php

namespace tt\features\htmlpage\components;

use tt\features\database\schema\Column;
use tt\features\database\v1\Model;
use tt\features\thirdparty\v1\assets\Chosen1;
use tt\features\thirdparty\v1\ThirdpartyHandler;

class FormInputDropdown extends HtmlCompFormInput
{

	/**
	 * @var array $options
	 */
	protected $options;

	private $chosen = true;

	/**
	 * @param string $name
	 * @param array $options
	 * @param string|bool $label TRUE: Label equals name, FALSE: No label
	 * @param string $selected
	 */
	public function __construct($name, $options, $label = true, $selected = null)
	{
		$this->name = $name;
		$this->label = $label;
		$this->options = $options;
		$this->value = $selected;
	}

	public function setChosenNo(){
		$this->chosen=false;
	}

	public static function fromModel(Model $model, Column $column, $value = null)
	{
		$options = $model->getDefaultFormattedList();
		if ($column->isNullable()) $options = array(Form::TT_NULL_VALUE => "") + $options;
		return new FormInputDropdown($column->getName(), $options, $column->getGuiName(), $value);
	}

	/**
	 * @return string
	 */
	function toHtml()
	{
		if($this->chosen){
			ThirdpartyHandler::addGlobalAsset(new Chosen1());
			$this->addClass("chosen");
		}
		return $this->getLabel() . "\n\t\t" . $this->buildNode("select", array(
				"name" => $this->name,
			), "\n\t\t\t" . $this->getOptionsHtml() . "\n\t\t");
	}

	private function getOptionsHtml()
	{
		$html = array();
		$selected_value = $this->getValue($this->value);
		foreach ($this->options as $value => $label) {
			$selected = $selected_value
			==/*SIC! Loose comparsion is necessary as keys might be provided
 				as strings (from GET) OR integers (from PHP array)*/
			$value ? " selected" : "";
			$html[] = "<option value='$value'$selected>" . $label . "</option>";
		}
		return implode("\n\t\t\t", $html);
	}

}