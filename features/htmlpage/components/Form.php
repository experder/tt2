<?php

namespace tt\features\htmlpage\components;

use tt\features\database\v1\Model;
use tt\features\debug\errorhandler\v1\Error;
use tt\features\htmlpage\view\v1\PageHtml;
use tt\features\i18n\Trans;
use tt\services\ServiceHtml;

class Form extends HtmlComponent
{

	const COMMAND_STRING = "cmd";

	const TT_NULL_VALUE = "TT_NULL";

	private $method = "post";
	/**
	 * @var false|string $submitText
	 */
	private $submitText;

	/**
	 * @param string|false $cmd
	 * @param string|false $submitText
	 */
	public function __construct($cmd = false, $submitText = true)
	{
		$this->submitText = $submitText===true?Trans::late("Submit"):$submitText;
		if ($cmd !== false) {
			$this->addHiddenField(Form::COMMAND_STRING, $cmd);
		}
	}

	public static function fromModel(Model $model)
	{
		$form = new Form();
		$foreignKeys = array();
		foreach ($model->getSchema()->getForeignKeys() as $foreignKey) {
			$cols = $foreignKey->getColumns();
			if (!$cols || !is_array($cols) || count($cols) !== 1) {
				new Error("Not implemented: Foreign key with not one reference.");
			}
			$col = $cols[0];
			$foreignKeys[$col->getName()] = $foreignKey;
		}
		foreach ($model->getSchema()->getColumns() as $column) {
			$colname = $column->getName();
			$form->add($model->getEditFormField($column,
				isset($foreignKeys[$colname]) ? $foreignKeys[$colname] : null));
		}

		return $form;
	}

	/**
	 * @return $this
	 */
	public function setMethodGet()
	{
		$this->method = 'get';
		return $this;
	}

	/**
	 * @param false|string $submitText
	 * @return Form
	 */
	public function setSubmitText($submitText)
	{
		$this->submitText = $submitText;
		return $this;
	}

	/**
	 * @param string $name
	 * @param string $value
	 * @return $this
	 */
	public function addHiddenField($name, $value)
	{
		$this->add(new FormHiddenField($name, $value));
		return $this;
	}
	public function addHiddenFieldFromGet($name)
	{
		if(isset($_GET[$name]))
			$this->addHiddenField($name, $_GET[$name]);
		return $this;
	}

	/**
	 * @return string
	 */
	public function toHtml()
	{
		PageHtml::$wait_spinner = true;
		$id = $this->id?" id='$this->id'":"";
		return "<form$id"
			. $this->getCssHtml()
			. " method='" . $this->method . "'>\n"
			. "\t" . $this->getChildrenHtml() . "\n"
			. "\t" . $this->getSubmitButtonHtml() . "\n"
			. "</form>";
	}

	/**
	 * @return string
	 */
	public function getChildrenHtml()
	{
		$html = array();
		foreach ($this->children as $child) {
			if ($child instanceof FormSubset) {
				$html[] = $child->toHtml();
			} else {
				$html[] = "<div class='tt_formfield ttid_" . $child->getId() . "'>"
					."\n\t\t" . $child->toHtml()
					. "\n\t</div>";
			}
		}
		return implode("\n\t", $html);
	}

	private function getSubmitButtonHtml()
	{
		if ($this->submitText === false) return "";
		return "<input type='submit' value='" . ServiceHtml::escape_value_html($this->submitText) . "'>";
	}

}