<?php

namespace tt\features\htmlpage\components;

class HtmlDiv extends HtmlComponent
{

	/**
	 * @var string $inner_before
	 */
	private $inner_before;
	/**
	 * @var string $inner_after
	 */
	private $inner_after;

	/**
	 * @param string $inner_before
	 * @param string $inner_after
	 */
	public function __construct($inner_before = "", $inner_after = "")
	{
		$this->inner_before = $inner_before;
		$this->inner_after = $inner_after;
	}

	/**
	 * @return string
	 */
	function toHtml()
	{
		return $this->buildNode("div", array(), $this->inner_before, $this->inner_after);
	}

}