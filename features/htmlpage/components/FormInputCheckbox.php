<?php

namespace tt\features\htmlpage\components;

class FormInputCheckbox extends HtmlCompFormInput
{

	/**
	 * @param string $name
	 * @param string|bool $label TRUE: Label equals name, FALSE: No label
	 * @param bool $checked
	 */
	public function __construct($name, $label = true, $checked = false)
	{
		$this->name = $name;
		$this->label = $label;
		$this->value = $checked;
	}

	/**
	 * @return string
	 */
	function toHtml()
	{
		$checked_html = $this->getValue($this->value) ? " checked" : "";
		return $this->getLabel() . "\n\t\t" . $this->buildNodeVoid("input$checked_html", array(
				"type" => "checkbox",
				"name" => $this->name,
			));
	}

}