<?php

namespace tt\features\htmlpage\components;

use tt\features\config\v1\CFG_S;
use tt\services\ServiceHtml;
use tt\services\ServiceStrings;

abstract class HtmlCompFormInput extends HtmlComponent
{

	protected $name = null;
	private $value_override_by_get = true;
	protected $label = true;
	protected $value = null;
	protected $helpText = null;

	/**
	 * @return $this
	 * @noinspection PhpUnused
	 */
	public function suppressValueOverrideByGet()
	{
		$this->value_override_by_get = false;
		return $this;
	}

	public function getValue($value)
	{
		if ($this->value_override_by_get && isset($_GET[$this->name])) return $_GET[$this->name];
		return $value;
	}

	/**
	 * @return $this
	 */
	public function setReadonly()
	{
		$this->addKeyVal('readonly', 'yes,please');
		return $this;
	}

	/**
	 * @param string $name
	 * @return HtmlCompFormInput
	 */
	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	protected function getLabel()
	{
		if ($this->label === false) return "";
		$label = $this->label === true ? $this->name : ($this->label ?: "&nbsp;");
		return "<label for='" . $this->getId() . "'"
			. (CFG_S::$DEVMODE ? " title='" . $this->name . "'" : "")
			. ">$label</label>";
	}

	public function setHelp($helpText)
	{
		$this->helpText = $helpText;
	}

	protected function getHelpHtml()
	{
		if ($this->helpText === null) return "";
		return "<span class='tt_help' title='"
			. ServiceHtml::escape_value_html(ServiceStrings::ttMarkup($this->helpText)) . "'>?</span>";
	}

}