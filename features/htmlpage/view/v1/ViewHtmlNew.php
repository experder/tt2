<?php

namespace tt\features\htmlpage\view\v1;

use tt\features\css\Stylesheet;
use tt\features\thirdparty\v1\ThirdpartyHandler;

abstract class ViewHtmlNew
{

	const LOADINGMSG_FORM_ID = "tt_loading_form_id";

	protected $showNavigation = true;
	public $prefixWithTitle = true;
	public $wideContent = false;

	/**
	 * @return bool
	 */
	public function isShowNavigation()
	{
		return $this->showNavigation;
	}

	/**
	 * @return string
	 */
	abstract function getHtml();

	/**
	 * @return string
	 */
	abstract function getTitle();

	/**
	 * @return Stylesheet[]
	 */
	public function getCss(){
		return array();
	}

	/**
	 * @return string[]|null
	 */
	public function getJsUrls(){
		return null;
	}

	public function getHeadJs_preload(){

		$thirdpartyJs = "";
		foreach (ThirdpartyHandler::getGlobalAssets() as $asset){
			$thirdpartyJs .= $asset->getJsHeadWithCheck();
		}

		return $thirdpartyJs .$this->getHeadJs();
	}

	public function getHeadJs(){
		return "";
	}

	/**
	 * @return string|false
	 */
	public function getCssClass(){
		return false;
	}

	public function getMoreHead()
	{
		return "";
	}

	/**
	 * @return LoaderPage|null
	 */
	public function getLoadingMessage()
	{
		return null;
	}

}