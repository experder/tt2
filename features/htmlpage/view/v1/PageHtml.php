<?php

namespace tt\features\htmlpage\view\v1;

use tt\features\config\v1\CFG_S;
use tt\features\config\v1\ConfigProject;
use tt\features\config\v1\ConfigServer;
use tt\features\css\ServiceCss;
use tt\features\css\Stylesheet;
use tt\features\debug\DebugHandler;
use tt\features\installer\Installer;
use tt\features\javascripts\ServiceJs;
use tt\features\messages\v2\Message;
use tt\features\navigation\bycode\NavigationHandler;
use tt\features\thirdparty\v1\ThirdpartyHandler;
use tt\services\ServiceEnv;

class PageHtml
{

	public static $wait_spinner = false;
	public static $global_counter = 0;

	/**
	 * @var ViewHtmlNew $view
	 */
	private $view;

	/**
	 * @param ViewHtmlNew $view
	 * @param bool $renderNow
	 */
	public function __construct(ViewHtmlNew $view, $renderNow=true)
	{
		$this->view = $view;
		if($renderNow)$this->renderViewHtml();
	}

	private function checkAssets(){
		$loader = Installer::getSingleton()->checkAssets($this->view->getLoadingMessage());
		if($loader===false)return;
		$this->view = $loader;
		$this->renderViewHtml(false);
		exit;
	}

	private function renderViewHtml($checkAssets=true){
		if(!isset($_POST[LoaderPage::POSTVAL_TT_LOADINGMSG_ID])){
			$loader = $this->view->getLoadingMessage();
			if($loader!==null){
				$this->view = $loader;
			}
		}
		$view = $this->view;
		$viewHtml = $view->getHtml();
		if($checkAssets)$this->checkAssets();
		$title = $this->getTitle();
		$js="const tt_run = '".ConfigServer::$HTTP_RUN_ALIAS."';".$view->getHeadJs_preload();
		$external_js=$this->getJsLinks();
		$css=$this->getCss();
		$more_head=$view->getMoreHead();
		$navigation=$view->isShowNavigation()?$this->getHtmlNavigation():"";
		$titleHtml = $this->getTitleHtml();
		$waitSpinner="";//"{WAIT_SPINNER:".(PageHtml::$wait_spinner?"true":"false")."}";
		$debugInfo = $this->getHtmlDebugInfo();
		$bodycss = $this->getBodyCss();
		$wap=$this->wap();
		$debugCssHtml = $debugCssBody = "";
		if (ConfigServer::$EMULATE_MOBILE&&!ServiceEnv::isMobileAgent()&&CFG_S::$DEVMODE){
			$debugCssHtml = " style='background:#043d4f;'";
			$debugCssBody = " style='background: white;max-width:360px;min-height:600px;margin: 0 auto;'";
		}

		//Needs to be in the end as above functions may add warnings.
		$messages = Message::getMessagesHtml();

		echo <<<END
<!DOCTYPE html$wap>
<html lang="en"$debugCssHtml>
	<head>
		<title>$title</title>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
		$external_js
		<script>$js</script>
		$css
		$more_head
	</head>
	<body$bodycss$debugCssBody>
		<nav class="tt_main_nav">$navigation</nav>
		$titleHtml
		$messages
		<div class="tt_inner_body">$viewHtml</div>
		$waitSpinner
		$debugInfo
	</body>
</html>
END;
		exit;
	}

	private function getTitleHtml(){
		$view = $this->view;
		return ($view->prefixWithTitle?"<h1 class='tt_title'>".$view->getTitle()."</h1>":"");
	}

	private function getHtmlNavigation(){
		$navHandler = new NavigationHandler();
		return $navHandler->getNavigation()->getChildrenDiv()->toHtml();
	}

	private function getBodyCss(){
		$bodycss = array();

		if($this->view->getCssClass())$bodycss[] = $this->view->getCssClass();
		if(CFG_S::$DEVMODE)$bodycss[] = 'debug';
		if($this->view->wideContent)$bodycss[] = 'contentWide';

		if(!$bodycss)return "";
		return " class='".implode(" ",$bodycss)."'";
	}

	private function getHtmlDebugInfo(){
		if(!ConfigServer::$DEVMODE)return "";
		$debugHandler = new DebugHandler();
		$queries = $debugHandler->getHtmlDebugInfo_queries();
		$duration = $debugHandler->getHtmlDebugInfo_duration();
		$post = $debugHandler->getHtmlDebugInfo_post();
		return "<div id='tt_debug_info'>$duration$queries$post</div>";
	}

	private function getTitle(){
		$pageTitle=$this->view->getTitle();
		return ($pageTitle?"$pageTitle - ":"").ConfigProject::$PROJECT_TITLE;
	}

	/**
	 * @return string
	 */
	private function getCss(){
		$css = array(
			ServiceCss::embedStylesheet(ConfigServer::$HTTP_TT_ROOT."/features/css/core.css"),
		);
		if(CFG_S::$DEVMODE){
			$css[]=ServiceCss::embedStylesheet(ConfigServer::$HTTP_TT_ROOT."/features/css/debug.css");
		}
		$styles = $this->view->getCss();
		if(ConfigProject::$SKIN_ID){
			array_unshift($styles, new Stylesheet(
				ConfigServer::$HTTP_PROJ_SKIN_DIR .'/'.ConfigProject::$SKIN_ID.'/core.css',
				ConfigServer::$HTTP_PROJ_SKIN_DIR .'/'.ConfigProject::$SKIN_ID.'/darkmode_core.css',
				ConfigServer::$HTTP_PROJ_SKIN_DIR .'/'.ConfigProject::$SKIN_ID.'/mobile.css'
			));
		}
		foreach (ThirdpartyHandler::getGlobalAssets() as $asset){
			foreach ($asset->getCssLinks() as $url){
				if ($url instanceof Stylesheet){
					$styles[] = $url;
				}else{
					$css[] = ServiceCss::embedStylesheet($url);
				}
			}
		}
		foreach ($styles as $style){
			$style_css = $style->getHtml();
			if($style_css)$css[] = $style_css;
			$style_css = $style->getHtml_darkmode();
			if($style_css)$css[] = $style_css;
			$style_css = $style->getHtml_mobile();
			if($style_css)$css[] = $style_css;
			$style_css = $style->getHtml_print();
			if($style_css)$css[] = $style_css;
		}
		return implode("\n\t\t", $css);
	}

	private function getJsLinks() {
		$css = array(
			ServiceJs::embedJsResource(ConfigServer::$HTTP_TT_ROOT."/features/javascripts/core.js"),
		);
		foreach (ThirdpartyHandler::getGlobalAssets() as $asset){
			foreach ($asset->getJsLinks() as $jsUrl){
				$css[] = ServiceJs::embedJsResource($jsUrl);
			}
		}
		$jsUrls = $this->view->getJsUrls();
		if($jsUrls)
		foreach ($jsUrls as $jsUrl){
			$css[] = ServiceJs::embedJsResource($jsUrl);
		}
		return implode("\n\t\t", $css);
	}

	/**
	 * @return int
	 */
	public static function getNextGlobalCounter()
	{
		return ++self::$global_counter;
	}

	private function wap() {
		if (ServiceEnv::isMobileAgent()){
			return " PUBLIC \"-//WAPFORUM//DTD XHTML Mobile 1.0//EN\" \"http://www.wapforum.org/DTD/xhtml-mobile10.dtd\"";
		}
		return "";
	}

}