<?php

namespace tt\features\htmlpage\view\v1;

use tt\features\htmlpage\components\Form;
use tt\features\javascripts\ServiceJs;
use tt\features\messages\v2\Message;
use tt\services\polyfill\Php7;
use tt\services\UnicodeIcons;

class LoaderPage extends ViewHtmlNew
{

	const POSTVAL_TT_LOADINGMSG_ID = "TT_LOADINGMSG_ID";

	public $prefixWithTitle = false;

	/**
	 * @var string $title
	 */
	private $title;
	/**
	 * @var string $msg
	 */
	private $msg;

	private $post_id = self::POSTVAL_TT_LOADINGMSG_ID;

	/**
	 * @param string $title
	 * @param string $msg
	 */
	public function __construct($title=true, $msg=true)
	{
		if($msg===true){
			$msg = "Loading...";
		}
		if($title===true){
			$title="Loading...";
		}
		$this->title = $title;
		$this->msg = $msg;
	}

	/**
	 * @return string
	 */
	function getHtml()
	{
		$form = new Form(false,false);
		$form->addHiddenField(self::POSTVAL_TT_LOADINGMSG_ID, "");
		$form->addHiddenField($this->post_id, "");//SIC!
		$form->setId(self::LOADINGMSG_FORM_ID);

		$msg = $this->msg;
		if(!is_array($msg))$msg=array($msg);
		$messages = array();
		foreach ($msg as $m){
			$messages[] = Message::messageHtml("", Php7::mb_chr(UnicodeIcons::hourglass_with_flowing_sand), $m);
		}

		return implode("\n",$messages).$form->toHtml();
	}

	/**
	 * @return string
	 */
	public function getMsg()
	{
		return $this->msg;
	}

	/**
	 * @param string $post_id
	 */
	public function setPostIdForConcurrentLoadingPages($post_id)
	{
		$this->post_id = $post_id;
	}

	/**
	 * @return string
	 */
	function getTitle()
	{
		return $this->title;
	}

	public function getHeadJs(){
		return ServiceJs::onLoad("document.getElementById('".self::LOADINGMSG_FORM_ID."').submit();");
	}

}