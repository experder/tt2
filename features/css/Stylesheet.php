<?php

namespace tt\features\css;

use tt\features\config\v1\ConfigServer;
use tt\services\ServiceEnv;

class Stylesheet
{

	private $url;
	private $url_darkmode;
	private $url_mobile;
	private $url_print;

	/**
	 * @param string $url
	 * @param string $url_darkmode
	 * @param string $url_mobile
	 * @param string $url_print
	 */
	public function __construct($url, $url_darkmode="", $url_mobile="", $url_print="")
	{
		$this->url = $url;
		$this->url_darkmode = $url_darkmode;
		$this->url_mobile = $url_mobile;
		$this->url_print = $url_print;
	}

	public function getHtml(){
		if(!$this->url)return "";
		return ServiceCss::embedStylesheet($this->url);
	}
	public function getHtml_darkmode(){
		if(!$this->url_darkmode)return "";
		return ServiceCss::embedStylesheet($this->url_darkmode,
			ConfigServer::$CSS_NIGHTMODE?false:ServiceCss::MEDIAQUERY_DARK);
	}
	public function getHtml_mobile(){
		if(!$this->url_mobile || (!ServiceEnv::isMobileAgent()&&!ConfigServer::$EMULATE_MOBILE) )return "";
		return ServiceCss::embedStylesheet($this->url_mobile);
	}
	public function getHtml_print(){
		return "";
	}

}