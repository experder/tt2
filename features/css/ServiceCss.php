<?php

namespace tt\features\css;

class ServiceCss
{

	const MEDIAQUERY_PRINT='print';
	const MEDIAQUERY_SCREEN='screen';
	const MEDIAQUERY_NARROW='(max-width: 800px)';
	const MEDIAQUERY_DARK='(prefers-color-scheme: dark)';

	/**
	 * @param string $href
	 * @return string
	 */
	public static function embedStylesheet($href, $mediaQuery=false){
		$extra_mediaQuery = "";
		if($mediaQuery!==false){
			$extra_mediaQuery=" media='$mediaQuery'";
		}
		return "<link href=\"$href\" rel=\"stylesheet\" type=\"text/css\"$extra_mediaQuery />";
	}

}