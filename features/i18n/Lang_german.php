<?php

namespace tt\features\i18n;

class Lang_german extends Language
{

	/**
	 * @return string[]
	 */
	function getWordList()
	{
		return array(
			"Execute!"=>"Ausführen!",
			"Submit"=>"Senden",
			"January"=>"Januar",
			"February"=>"Februar",
			"March"=>"März",
			"April"=>"April",
			"May"=>"Mai",
			"June"=>"Juni",
			"July"=>"Juli",
			"August"=>"August",
			"September"=>"September",
			"October"=>"Oktober",
			"November"=>"November",
			"December"=>"Dezember",
			"Format"=>"Format",
			"File"=>"Datei",
			"Delete file"=>"Datei löschen",
			"Import!"=>"Importieren!",
			"Import"=>"Import",
			"Date"=>"Datum",
			"core"=>"core",
			"Update database schema"=>"Datenbankschema aktualisieren",
		);
	}

	/**
	 * @return string[][]
	 */
	function getWordListWithKontext()
	{
		return array(
//			Language::CONTEXT_=>array(
//				""=>"",
//			),
		);
	}

}