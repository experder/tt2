<?php

namespace tt\features\i18n;

abstract class Language
{

	/**
	 * @return string[]
	 */
	abstract function getWordList();

	/**
	 * @return string[][]
	 */
	abstract function getWordListWithKontext();

}