<?php

namespace tt\features\i18n;

use tt\features\debug\errorhandler\v2\Warning;
use tt\features\modulehandler\v1\ModulehandlerV1;
use tt\features\modulehandler\v1\RegistrationHandler;

class Trans
{

	const CONTEXT_DEFAULT="context_default";

	public static $CFG_VERBOSE = false;

	/**
	 * @var Language|null $language
	 */
	public static $language = null;
	private static $wordlist = null;
	private static $wordlistWithContext = null;

	public static function late($string, $context=self::CONTEXT_DEFAULT){
		if(self::$language===null)return $string;
		if($context!==self::CONTEXT_DEFAULT){
			if(!isset((self::getWordList(true))[$context])){
				if(self::$CFG_VERBOSE)new Warning("Language context not found: '$context'.");
				return $string;
			}
			if(!isset(self::$wordlistWithContext[$context][$string])){
				if(self::$CFG_VERBOSE)new Warning("Missing translation for '$string' in context '$context'.");
				return $string;
			}
			$word = self::$wordlistWithContext[$context][$string];
		}else{
			if(!isset((self::getWordList())[$string])){
				if(self::$CFG_VERBOSE)new Warning("Missing translation for '$string'.");
				return $string;
			}
			$word = self::$wordlist[$string];
		}
		if(self::$CFG_VERBOSE){
			return $word." [".$string."]";
		}
		return $word;
	}

	private static function getWordList($withContext=false){
		if(self::$wordlist===null && self::$language!==null){
			self::$wordlist=self::$language->getWordList();
			self::$wordlistWithContext=self::$language->getWordListWithKontext();
			foreach (ModulehandlerV1::getModules() as $module){
				if(!($module instanceof RegistrationHandler))continue;
				self::$wordlist+=$module->getI18wordlist(get_class(self::$language));
				self::$wordlistWithContext+=$module->getI18wordlist(get_class(self::$language),true);
			}
		}
		if($withContext)return self::$wordlistWithContext;
		return self::$wordlist;
	}

}