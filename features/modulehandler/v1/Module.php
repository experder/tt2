<?php

namespace tt\features\modulehandler\v1;

class Module
{

	private $classname;
	private $folder;

	/**
	 * @param string $classname classname of RegistrationHandler starting with '\'
	 * @see RegistrationHandler
	 * @param string $folder Foldername. Filename is expected to be classname (without namespace) and extension 'php'.
	 */
	public function __construct($classname, $folder)
	{
		$this->classname = $classname;
		$this->folder = $folder;
	}

	/**
	 * @return string
	 */
	public function getClassname()
	{
		return $this->classname;
	}

	/**
	 * @return string
	 */
	public function getFolder()
	{
		return $this->folder;
	}

}