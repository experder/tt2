<?php

namespace tt\features\modulehandler\v1;

use tt\ConfigLoader;
use tt\features\autoloader\simple1\AutoloaderSimple;
use tt\features\debug\errorhandler\v1\Error;
use tt\features\frontcontroller\bycode\FrontcontrollerByCode;

class ModulehandlerV1
{

	/**
	 * @var RegistrationHandler[] $modulesRegistration
	 */
	public static $modulesRegistration = false;

	public static function registerModules(ConfigLoader $configLoader){
		$modules= array(
			/** @see \tt\features\modulehandler\CoreModule */
			new Module("\\tt\\features\\modulehandler\\CoreModule",
				dirname(__DIR__)
			),
		);
		$more_modules = $configLoader->getConfigProject()->registerModules();
		return array_merge($modules, $more_modules);
	}

	/**
	 * @param ConfigLoader $configLoader
	 * @return void
	 */
	public static function LoadModules(ConfigLoader $configLoader){
		$modules = ModulehandlerV1::registerModules($configLoader);
		self::$modulesRegistration = array();
		foreach ($modules as $module){
			self::$modulesRegistration[] = self::RegistrationHandlerFromModule($module);
		}
		foreach (self::$modulesRegistration as $moduleHandler){
			AutoloaderSimple::registerRootNamespace($moduleHandler->getNamespaceRoots());
			FrontcontrollerByCode::addRoute($moduleHandler->getModuleRoutes());
		}
	}

	/**
	 * @param Module $module
	 * @return RegistrationHandler
	 */
	public static function RegistrationHandlerFromModule(Module $module){
		$classname = $module->getClassname();
		$classname_file = preg_replace("/^(.*)\\\\(.*?)\$/", "$2", $classname);
		$filename = $module->getFolder()."/$classname_file.php";

		if(!file_exists($filename) || !is_file($filename)){
			new Error("ModuleHandler nicht gefunden: $filename");
		}

		require_once $filename;

		if(!class_exists($classname)){
			new Error("Modul enthält nicht Klasse '".$classname."': $filename");
		}

		$moduleHandler = new $classname();

		if(!($moduleHandler instanceof RegistrationHandler)){
			new Error("Modul ist kein RegistrationHandler: $filename");
		}

		return $moduleHandler;
	}

	/**
	 * @return RegistrationHandler[]
	 */
	public static function getModules()
	{
		return self::$modulesRegistration;
	}

}