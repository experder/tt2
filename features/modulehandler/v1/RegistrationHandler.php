<?php

namespace tt\features\modulehandler\v1;

use tt\features\autoloader\simple1\RootNamespace;
use tt\features\config\v1\CFG_S;
use tt\features\database\v1\Model;
use tt\features\database\v2\Schema;
use tt\features\frontcontroller\bycode\Route;
use tt\features\i18n\Language;
use tt\features\navigation\bycode\NavigationEntry;

abstract class RegistrationHandler
{

	/**
	 * @var Model[]|Schema[]
	 */
	private $modelSchema = null;

	/**
	 * @return Route[]
	 */
	abstract function getModuleRoutes();

	/**
	 * @return string Maximum 40 chars.
	 */
	abstract function getModuleId();

	/**
	 * @return RootNamespace[]|null
	 */
	abstract function getNamespaceRoots();

	/**
	 * @return Model[]|string[]
	 */
	abstract function createModelSchema();

	/**
	 * @return Model[]|Schema[]
	 */
	public function getModelSchema()
	{
		if($this->modelSchema===null){
			$schemas = array();
			foreach ($this->createModelSchema() as $schema){
				if(is_string($schema)){
					$schemas[] = Schema::getSingleton($schema);
				}else{
					$schemas[] = $schema;
				}
			}
			$this->modelSchema = $schemas;
		}
		return $this->modelSchema;
	}

	public function getI18namespace()
	{
		return false;
	}

	public function getI18wordlist($language_classname, $withContext=false)
	{
		$namespace = $this->getI18namespace();
		if(!$namespace)return array();

		$classname = $namespace.'\\'.basename($language_classname);
		if(!class_exists($classname))return array();
		$instance = new $classname();
		if(!($instance instanceof Language))return array();

		if($withContext)return $instance->getWordListWithKontext();
		return $instance->getWordList();
	}

	/**
	 * @return NavigationEntry[]
	 */
	public function getNavigation()
	{
		$navi = new NavigationEntry($this->getModuleId().'_', $this->getGuiName());
		if(CFG_S::$HTTP_RUN_ALIAS!==null)
		foreach ($this->getModuleRoutes() as $route){
			$name = $route->getGuiName();
			if($name===false)continue;
			$id = $route->getRoute();
			$id = trim($id, '/');
			$id = preg_replace("/[^a-z\\d]/", "_", $id);
			$routeEntry = new NavigationEntry($id,$name,CFG_S::$HTTP_RUN_ALIAS.$route->getRoute());
			$navi->addEntry($routeEntry);
		}
		return array($navi);
	}

	public function getGuiName()
	{
		return $this->getModuleId();
	}

}