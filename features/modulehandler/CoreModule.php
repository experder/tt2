<?php

namespace tt\features\modulehandler;

use tt\features\config\v1\ConfigDatabase2;
use tt\features\config\v1\ConfigProject;
use tt\features\database\v1\Model;
use tt\features\database\v1\views\EditModel;
use tt\features\database\v2\ViewDbUpdate;
use tt\features\frontcontroller\bycode\Route;
use tt\features\i18n\Trans;
use tt\features\modulehandler\v1\RegistrationHandler;
use tt\services\UNI;
use tt\services\UnicodeIcons;

class CoreModule extends RegistrationHandler
{

	public static $module_id = "core";
	const GUI_NAME = "core";

	/**
	 * @return Route[]
	 */
	function getModuleRoutes()
	{
		return array(
#			new Route("/tt_core/database/sync", SyncDb::getClass()),
			new Route("/tt_core/database/editmodel", EditModel::getClass(), false),
			new Route("/tt_core/database/sync2", ViewDbUpdate::getClass(), ViewDbUpdate::title()),
		);
	}

	/**
	 * Core root namespace registration: See AutoloaderSimple
	 * @see \tt\features\autoloader\simple1\AutoloaderSimple::register()
	 */
	function getNamespaceRoots(){ return null; }

	/**
	 * @return Model[]|string[]
	 */
	function createModelSchema()
	{
		return array(
			ConfigDatabase2::getClass(),
		);
	}

	/**
	 * @return string
	 */
	function getModuleId()
	{
		return self::$module_id;
	}

	public function getGuiName()
	{
		if(ConfigProject::$SKIN_BILDER_SAGEN_MEHR_ALS_WORTE){
			return UNI::asEmoji(UnicodeIcons::earth_globe_europe_africa);
		}
		return Trans::late(self::GUI_NAME);
	}

}