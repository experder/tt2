<?php

namespace tt\features\frontcontroller\bycode;

use tt\features\htmlpage\view\v1\ViewHtmlNew;
use tt\features\javascripts\AjaxEndpoint;

class Route
{

	private $route;
	private $target;
	private $gui_name;

	/**
	 * @param string $route Route path beginning with '/'
	 * @param string $target Classname/URL
	 * @param string|bool $gui_name TRUE: classname, FALSE: No Navigation entry
	 * @see ViewHtmlNew, AjaxEndpoint
	 */
	public function __construct($route, $target, $gui_name=true)
	{
		$this->route = $route;
		$this->target = $target;
		$this->gui_name = $gui_name;
	}

	/**
	 * @return bool|string
	 */
	public function getGuiName()
	{
		if($this->gui_name===true)return basename($this->target);
		return $this->gui_name;
	}

	/**
	 * @return string
	 */
	public function getRoute()
	{
		return $this->route;
	}

	/**
	 * @return string
	 */
	public function getTarget()
	{
		return $this->target;
	}

}