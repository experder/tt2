<?php

namespace tt\features\frontcontroller\bycode;

use tt\features\api\ApiResponse;
use tt\features\autoloader\simple1\AutoloaderSimple;
use tt\features\config\v1\CFG_S;
use tt\features\config\v1\ConfigLoaderTemplate;
use tt\features\config\v1\ConfigServer;
use tt\features\database\v1\DatabaseHandler;
use tt\features\debug\errorhandler\v1\Error;
use tt\features\debug\errorhandler\v1\Error404;
use tt\features\htmlpage\view\v1\PageHtml;
use tt\features\htmlpage\view\v1\ViewHtmlNew;
use tt\features\javascripts\AjaxEndpoint;
use tt\features\modulehandler\v1\ModulehandlerV1;
use tt\r\RunFromCli;
use tt\services\ServiceResponse;
use tt\services\ServiceStrings;

class FrontcontrollerByCode
{

	/**
	 * @var Route[] $allRoutes
	 */
	public static $allRoutes = array();

	/**
	 * @param Route|Route[] $route
	 * @return void
	 */
	public static function addRoute($route){
		if(is_array($route)){
			foreach ($route as $r){
				self::addRoute($r);
			}
			return;
		}
		self::$allRoutes[] = $route;
	}

	/**
	 * @param string $routename
	 * @return string
	 */
	private static function targetByRoute($routename){
		$route = self::findRoute($routename);
		if($route===false){
			new Error404(CFG_S::$DEVMODE?"Route not found.":"");
		}
		return $route->getTarget();
	}

	/**
	 * @param string $target
	 * @return Route|false
	 */
	public static function routeByTarget($target){
		foreach (self::$allRoutes as $route){
			if($route->getTarget()===$target){
				return $route;
			}
		}
		return false;
	}

	private static function handleRoute_url($target){
		if(!filter_var($target, FILTER_VALIDATE_URL))return;
		ServiceResponse::permanentRedirect301($target);
		exit;
	}

	private static function handleRoute_classname($target){
		if (!ServiceStrings::isClassname2($target,'\\'))return;
		if(!class_exists($target))return;
		$class = new $target();

		//Valid classes for routing:
		self::handleRoute_class_viewHtml($class);
		self::handleRoute_class_ajax($class);
	}

	private static function handleCliRoute_classname($target, $args){
		if (!ServiceStrings::isClassname2($target,'\\'))return;
		if(!class_exists($target))return;
		$class = new $target();

		//Valid classes for routing:
		self::handleRoute_class_runFromCli($class, $args);
	}

	private static function handleRoute_class_viewHtml($class){
		if(!($class instanceof ViewHtmlNew))return;
		new PageHtml($class);
		exit;
	}

	private static function handleRoute_class_runFromCli($class, $args){
		if(!($class instanceof RunFromCli))return;
		$class->runFromCli($args);
		exit;
	}

	private static function handleRoute_class_ajax($class){
		if(!($class instanceof AjaxEndpoint))return;
		$page = new ApiResponse();
		$page->deliverAjaxEndpoint($class);
		exit;
	}

	public static function callRoute($route){
		self::init();

		$target = self::targetByRoute($route);

		//Valid route targets:
		self::handleRoute_classname($target);
		self::handleRoute_url($target);

		$error="Route target not found: $target";
		if(ConfigServer::$DEVMODE){
			$error.="\n* Classname [ViewHtmlNew, AjaxEndpoint]";
			$error.="\n* URL";
		}
		new Error($error);
	}

	public static function cliCallTarget($target, $args){
		self::init();

		//Valid route targets:
		self::handleCliRoute_classname($target, $args);

		$error="Route target not found: $target";
		if(ConfigServer::$DEVMODE){
			$error.="\n* Classname [implements RunFromCli]";
		}
		new Error($error);
	}

	/**
	 * @param string $name Route
	 * @return Route|false
	 */
	private static function findRoute($name){
		foreach (self::$allRoutes as $route){
			if($route->getRoute()===$name){
				return $route;
			}
		}
		return false;
	}

	public static function init(){

		require_once dirname(dirname(__DIR__)).'/autoloader/simple1/AutoloaderSimple.php';
		AutoloaderSimple::register();

		$configs = ConfigLoaderTemplate::getConfigs();

		$configs->getConfigProject()->setConfigValues();
		$configs->getConfigProject()->configLoadNamespaces();
		ModulehandlerV1::LoadModules($configs);

		$configs->getConfigServer()->configSetValues();

		DatabaseHandler::init_database_from_cfg();
	}

}