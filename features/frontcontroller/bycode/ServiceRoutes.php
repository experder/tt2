<?php

namespace tt\features\frontcontroller\bycode;

use tt\features\config\v1\ConfigServer;
use tt\features\debug\errorhandler\v2\Warning;
use tt\features\htmlpage\components\Html_A;
use tt\services\ServiceStrings;

class ServiceRoutes
{

	/**
	 * @param string $target
	 * @param string|true $title
	 * @param string[]|false $link_params
	 * @return Html_A
	 */
	public static function getLinkComponent($target, $title=true, $link_params=false){
		if($title===true){
			$targetArray=explode("\\",$target);
			$title=end($targetArray);
		}
		$link = self::getLinkHref($target);
		if($link===false){
			if(ConfigServer::$DEVMODE)new Warning("[DEAD_LINK!] Target not registered ($title): $target");
			$link = ConfigServer::$HTTP_RUN_ALIAS."/unknownTarget/$target";
			if(ConfigServer::$DEVMODE){
				$title.=" [DEAD_LINK!]";
			}
		}
		$link.=ServiceStrings::buildQueryString($link_params);
		return new Html_A($link, $title);
	}

	public static function getLinkHref($target){
		$route = FrontcontrollerByCode::routeByTarget($target);
		if ($route===false){ return false; }
		return ConfigServer::$HTTP_RUN_ALIAS.$route->getRoute();
	}

}