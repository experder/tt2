<?php

namespace tt\features\autoloader\simple1;

class RootNamespace
{

	private $namespace;
	private $directory;

	/**
	 * @param string $namespace Namespace prefix without starting '\'
	 * @param string $directory Absolute directory path without trailing '/' (make use of __DIR__)
	 */
	public function __construct($namespace, $directory)
	{
		$this->namespace = $namespace;
		$this->directory = $directory;
	}

	/**
	 * @return string
	 */
	public function getNamespace()
	{
		return $this->namespace;
	}

	/**
	 * @return string
	 */
	public function getDirectory()
	{
		return $this->directory;
	}

}