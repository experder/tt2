<?php

namespace tt\features\autoloader\simple1;

use tt\features\debug\errorhandler\v1\Error;
use tt\services\ServiceStrings;

class AutoloaderSimple
{

	private static $registered = false;

	private static $rootNamespaces = array();

	private static $recursion_protection_autoload = 0;

	public static function register(){
		if(self::$registered)new Error("already registered!");
		self::$registered=true;

		require_once __DIR__.'/RootNamespace.php';
		require_once dirname(dirname(dirname(__DIR__))).'/services/ServiceStrings.php';

		self::registerRootNamespace(new RootNamespace("tt", dirname(dirname(dirname(__DIR__)))));

		spl_autoload_register(function ($class_name) {
			AutoloaderSimple::$recursion_protection_autoload++;
			if(AutoloaderSimple::$recursion_protection_autoload>10){
				echo "!!! AutoloaderSimple::\$recursion_protection_autoload";exit;
			}
			if(!ServiceStrings::isClassname2($class_name, "\\"))new Error("not a valid namespace!");

			foreach (AutoloaderSimple::$rootNamespaces as $rootNamespace){
				if (AutoloaderSimple::loadFile($class_name, $rootNamespace)){
					AutoloaderSimple::$recursion_protection_autoload = 0;
					return;
				}
			}

			AutoloaderSimple::$recursion_protection_autoload = 0;
		});

	}

	/**
	 * @param string $class_name
	 * @param RootNamespace $namespace
	 * @return bool
	 */
	private static function loadFile($class_name, RootNamespace $namespace){
		$namespace_root_escaped = preg_quote($namespace->getNamespace(),'/');
		if (!preg_match("/^$namespace_root_escaped\\\\(.*)/", $class_name, $matches)) return false;

		$name = $matches[1];

		$file = str_replace('\\', '/', $namespace->getDirectory() . '/' . $name . '.php');

		if (!$file) return false;

		if (!file_exists($file)) return false;

		require_once $file;

		return true;
	}

	/**
	 * @param RootNamespace|RootNamespace[] $rootNamespace
	 * @return void
	 */
	public static function registerRootNamespace($rootNamespace){
		if(is_array($rootNamespace)){
			foreach ($rootNamespace as $rns){
				self::registerRootNamespace($rns);
			}
			return;
		}
		if($rootNamespace===null)return;
		$ns_name = $rootNamespace->getNamespace();
		if(isset(self::$rootNamespaces[$ns_name])){
			new Error("Namespace root '$ns_name' already defined!");
		}
		self::$rootNamespaces[$ns_name] = $rootNamespace;
	}

}