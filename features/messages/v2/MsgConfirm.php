<?php

namespace tt\features\messages\v2;

use tt\services\polyfill\Php7;
use tt\services\UnicodeIcons;

class MsgConfirm extends Message
{

	const CSS_TYPE = "tt_msg2type_confirm";

	protected $typeCss = self::CSS_TYPE;

	public function __construct($message)
	{
		parent::__construct($message);
		$this->typeIcon = Php7::mb_chr(UnicodeIcons::White_Heavy_Check_Mark);
	}

}