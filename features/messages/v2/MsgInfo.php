<?php

namespace tt\features\messages\v2;

use tt\services\polyfill\Php7;
use tt\services\UnicodeIcons;

class MsgInfo extends Message
{

	public function __construct($message)
	{
		parent::__construct($message);
		$this->typeIcon = Php7::mb_chr(UnicodeIcons::Open_Mailbox_with_Raised_Flag);
	}

}