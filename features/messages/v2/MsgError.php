<?php

namespace tt\features\messages\v2;

use tt\services\polyfill\Php7;
use tt\services\UnicodeIcons;

class MsgError extends Message
{

	protected $typeCss = "tt_msg2type_error";

	public function __construct($message)
	{
		parent::__construct($message);
		$this->typeIcon = Php7::mb_chr(UnicodeIcons::No_Entry);
	}

}