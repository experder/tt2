<?php

namespace tt\features\messages\v2;

abstract class Message
{

	/**
	 * @var Message[] $messages
	 */
	private static $messages = array();

	/**
	 * @var string $message
	 */
	private $message;

	protected $typeCss = "";
	protected $typeIcon;

	/**
	 * @param string $message
	 */
	public function __construct($message)
	{
		$this->message = $message;
		self::$messages[] = $this;
	}

	private function getMessageHtml(){
		return self::messageHtml($this->typeCss, $this->typeIcon, $this->message);
	}

	public static function getMessagesHtml(){
		if(!self::$messages) return "";
		$html=array();
		foreach (self::$messages as $message){
			if(!($message instanceof Message))continue;
			$html[] = $message->getMessageHtml();
		}
		return "<div class='tt_messages2'>\n".implode("\n",$html)."</div>";
	}

	public static function messageHtml($css, $typeIcon, $message){
		return "<div class='tt_msg $css'>"
			."<span class='msg2icon'>".$typeIcon."</span> ".$message."</div>";
	}

}