<?php

namespace tt\features\messages\v2;

use tt\services\UNI;
use tt\services\UnicodeIcons;

class MsgWarning extends Message
{

	const CSS_TYPE = "tt_msg2type_warning";

	protected $typeCss = self::CSS_TYPE;

	public function __construct($message)
	{
		parent::__construct($message);
		$this->typeIcon = UNI::asEmoji(UnicodeIcons::Warning_Sign);
	}

}