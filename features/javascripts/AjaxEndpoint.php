<?php

namespace tt\features\javascripts;

abstract class AjaxEndpoint
{

	/**
	 * @param array $input Key-value array
	 * @return array Key-value array
	 */
	abstract public function process(array $input);

}