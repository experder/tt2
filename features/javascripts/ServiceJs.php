<?php

namespace tt\features\javascripts;

class ServiceJs
{

	/**
	 * @param string $href
	 * @return string
	 */
	public static function embedJsResource($href){
		return "<script type=\"text/javascript\" src=\"$href\"></script>";
	}

	public static function onLoad($js){
		return 'document.addEventListener("DOMContentLoaded",()=>{'
			. $js
			. '});';
	}
	public static function onLoad2($js){
		return "window.onload = function() { $js };";
	}

}