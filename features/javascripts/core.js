const tt={}

const tt_start_times=[];

tt.ajaxJson = function(endpoint, params, callback){
    const randId = Math.random().toString(36).slice(2);
    tt_start_times[randId] = Date.now();
    const xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", endpoint, true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.setRequestHeader("X-TT-Source", "coreJs/ajaxJson");
    xmlhttp.send(params);
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let response;
            try{
                response = JSON.parse(xmlhttp.responseText);
            } catch (error) {
                console.log("Could not decode answer from endpoint '"+endpoint+"':\n"+xmlhttp.responseText);
                alert('Error. See console for details.');
                return;
            }
            if(response.ok!==true) {
                console.log("API endpoint '" + endpoint + "' returned OK=FALSE for params: " + params);
                if (response.errormsg) {
                    console.log("Errormessage: " + response.errormsg);
                    if (response.backtrace) {console.log(response.backtrace);}
                }
                alert('Error. See console for details.');
                return;
            }
            if(response.ttapidebugcomp){
                tt.debugInfoAjax(response.ttapidebugcomp, tt_start_times[randId]);
            }
            callback(response.data);
        }
    }
}

tt.debugInfoAjax = function(compHtml, startTime){

    const existingMessages = document.getElementsByClassName('ajaxDebugInfo');
    if(existingMessages.length>=8){
        existingMessages[0].remove();
    }

    const compElem = document.createElement("div");

    const hookNode = document.getElementById('tt_debug_info').lastChild;

    //InsertAfter:
    hookNode.parentNode.insertBefore(compElem, hookNode.nextSibling);

    const duration = (Date.now()-startTime)/1000;
    let duration_html = duration+" s";

    if(duration>0.1){
        compHtml=compHtml.replace('>AJAX</div>', "><b class='warn'>AJAX</b></div>");
        duration_html = "<b class='warn'>"+duration_html+"</b>";
    }

    compElem.outerHTML = compHtml.replace('{{OVERALL}}', duration_html);
}

tt.error = function(errormsg, details=false){
    //TODO
}

tt.dialog = function(target, callback=false){
    const mytab=window.open(target, 'tt_dialog_name', 'width=820,height=600');

    //For x-domain: https://stackoverflow.com/questions/9388380/
    mytab.onbeforeunload = function(){ callback(); }
}

tt.addTextTo = function(text, inputId){
    const myField = document.getElementById(inputId);
    if (myField.selectionStart || myField.selectionStart === 0) {
        const startPos = myField.selectionStart;
        const endPos = myField.selectionEnd;
        myField.value = myField.value.substring(0, startPos)
            + text
            + myField.value.substring(endPos, myField.value.length);
    } else {
        myField.value += text;
    }
}

tt.toggleHide=function(selector){
    document.querySelectorAll(selector).forEach(function(elem){
        const list = elem.classList;
        if (list.contains('tt_hide')){
            list.remove('tt_hide');
        }else{
            list.add('tt_hide');
        }
    });
}
