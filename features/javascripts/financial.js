const tt_financial={}

tt_financial.euroToCents = function(euro_string){
    if(!euro_string)return 0;
    const pattern = /^([-+]? ?[.\d]*,?\d+)/;
    const matches = euro_string.match(pattern);
    if (!matches) {
        return false;
    }
    let euro_bereinigt = matches[1];
    euro_bereinigt = euro_bereinigt.replace(".", "");
    euro_bereinigt = euro_bereinigt.replace(",", ".");
    return Math.round(euro_bereinigt*100);
}

tt_financial.centsToEuro = function(cents, posSign=true, suffix=" €"){
    if (!cents) cents = "0";
    if (isNaN(cents)) return false;
    return (posSign && cents > 0 ? "+" : "") +
        (cents/100).toFixed(2).replace(".", ",") +
        suffix;
}
