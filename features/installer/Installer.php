<?php

namespace tt\features\installer;

use tt\features\config\v1\ConfigLoaderTemplate;
use tt\features\debug\errorhandler\v2\Warning;
use tt\features\htmlpage\components\Form;
use tt\features\htmlpage\components\FormInputCheckbox;
use tt\features\htmlpage\components\FormInputText;
use tt\features\htmlpage\view\v1\LoaderPage;
use tt\features\htmlpage\view\v1\PageHtml;
use tt\features\messages\v2\MsgConfirm;
use tt\features\templates\tt_template\ServiceTtTemplate;
use tt\features\thirdparty\v1\Thirdpartyasset;
use tt\features\thirdparty\v1\ThirdpartyHandler;
use tt\services\ServiceEnv;

class Installer
{

	const POSTVAL_project_namespace = "project_namespace";

	const CMD_createConfigPointer = "createConfigPointer";
	const POSTVAL_filename_project = "filename_project";
	const POSTVAL_class_project = "class_project";
	const POSTVAL_class_server = "class_server";

	const CMD_createConfigProject = "createConfigProject";
	const POSTVAL_project_root_dir = "project_root_dir";
	const POSTVAL_project_title = "project_title";

	const CMD_createConfigServer = "createConfigServer";
	const POSTVAL_database = "database";
	const POSTVAL_http_tt_root = "http_tt_root";
	const POSTVAL_devmode = "devmode";

	/**
	 * @var Installer $singleton
	 */
	private static $singleton = null;

	private function __construct(){}

	public static function getSingleton(){
		if(self::$singleton===null)self::$singleton=new Installer();
		return self::$singleton;
	}

	/**
	 * @param LoaderPage|null $loadingMessage
	 * @return false|LoaderPage
	 */
	public function checkAssets($loadingMessage){
		$messages = array();
		foreach (ThirdpartyHandler::getGlobalAssets() as $asset){
			if($asset->doCheck())continue;
			if(isset($_POST[Thirdpartyasset::LOADER_POST_ID])){
				$asset->doInstall();
				$asset->doCheck(true);
				new MsgConfirm($asset->getAssetName()." installed successfully!");
			}else{
				$messages[] = "Installing ".$asset->getAssetName()."...";
			}
		}
		if(!$messages)return false;

		if(isset($_POST[LoaderPage::POSTVAL_TT_LOADINGMSG_ID])){
			array_unshift($messages, $loadingMessage->getMsg());
		}
		$page = new LoaderPage("Installing...please wait", $messages);
		$page->setPostIdForConcurrentLoadingPages(Thirdpartyasset::LOADER_POST_ID);
		return $page;
	}

	public function createConfigPointer(){
		if(ServiceEnv::isCmd(self::CMD_createConfigPointer)){
			$config_pointer = dirname(dirname(dirname(__DIR__))).'/ConfigLoader.php';
			$filename_project = ServiceEnv::valueFromPost(self::POSTVAL_filename_project);
			$require_project = "require_once __DIR__.'/$filename_project';";
			if($filename_project==="Config_project.php")$require_project="// ".$require_project;
			ServiceTtTemplate::createFile($config_pointer, __DIR__.'/templates/ConfigLoader.php.template', array(
				"{{PROJECT_NAMESPACE}}"=>ServiceEnv::valueFromPost(self::POSTVAL_project_namespace),
				"{{FILE_PROJECT}}"=>$filename_project,
				"{{REQUIRE|Example: require_once __DIR__.'/Config_project.php'; }}"=>$require_project,
				"{{CLASS_PROJECT|Example: Config_project }}"=>ServiceEnv::valueFromPost(self::POSTVAL_class_project),
				"{{CLASS_SERVER|Example: Config_server }}"=>ServiceEnv::valueFromPost(self::POSTVAL_class_server),
			));
			new MsgConfirm("ConfigLoader.php created.");
			return $config_pointer;
		}

		$form = new Form(self::CMD_createConfigPointer);
		$form->add(new FormInputText(self::POSTVAL_project_namespace, true, "myproject"));
		$form->add(new FormInputText(self::POSTVAL_filename_project, true, "Config_project.php"));
		$form->add(new FormInputText(self::POSTVAL_class_project, true, "Config_project"));
		$form->add(new FormInputText(self::POSTVAL_class_server, true, "Config_server"));

		new Warning("Please create a file that points to your config files:\nhttps://gitlab.com/experder/tt2/-/blob/main/features/config/v1/ConfigLoader.md");
		new PageHtml(new ViewWizard($form->toHtml()));
		return null;
	}

	public function createConfigFileProject($filename){
		if(ServiceEnv::isCmd(self::CMD_createConfigProject)){
			ServiceTtTemplate::createFile($filename, __DIR__.'/templates/Config_project.php.template', array(
				"{{ROOT_NAMESPACE}}"=>ServiceEnv::valueFromPost(self::POSTVAL_project_namespace),
				"{{ROOT_DIR}}"=>ServiceEnv::valueFromPost(self::POSTVAL_project_root_dir),
				"{{PROJECT_TITLE}}"=>ServiceEnv::valueFromPost(self::POSTVAL_project_title),
			));
			new MsgConfirm("Config_project.php created.");
			ConfigLoaderTemplate::init();
			return;
		}

		$form = new Form(self::CMD_createConfigProject);
		$form->add(new FormInputText(self::POSTVAL_project_namespace, true, ServiceEnv::valueFromPost(self::POSTVAL_project_namespace, "myproject")));
		$form->add(new FormInputText(self::POSTVAL_project_root_dir, true, "__DIR__"));
		$form->add(new FormInputText(self::POSTVAL_project_title, true, "MyProject"));

		new Warning("Please create file '$filename' with project configs.");
		new PageHtml(new ViewWizard($form->toHtml()));
	}

	public function createConfigFileServer($filename){
		if(ServiceEnv::isCmd(self::CMD_createConfigServer)){
			ServiceTtTemplate::createFile($filename, __DIR__.'/templates/Config_server.php.template', array(
				"{{ROOT_NAMESPACE}}"=>ServiceEnv::valueFromPost(self::POSTVAL_project_namespace),
				"{{DATABASE}}"=>ServiceEnv::valueFromPost(self::POSTVAL_database),
				"{{HTTP_TT_ROOT}}"=>ServiceEnv::valueFromPost(self::POSTVAL_http_tt_root),
				"{{DEVMODE}}"=>ServiceEnv::valueFromPost(self::POSTVAL_devmode)?"true":"false",
			));
			new MsgConfirm("Config_server.php created.");
			return;
		}

		$root_namespace = ServiceEnv::valueFromPost(self::POSTVAL_project_namespace, "myproject");
		$database = dirname(dirname(dirname(__DIR__)))."/myproject.sqlite";
		$http_tt_root = "/myproject/tt";

		$form = new Form(self::CMD_createConfigServer);
		$form->add(new FormInputText(self::POSTVAL_project_namespace, true, $root_namespace));
		$form->add(new FormInputCheckbox(self::POSTVAL_devmode, true, true));
		$form->add(new FormInputText(self::POSTVAL_database, true, $database));
		$form->add(new FormInputText(self::POSTVAL_http_tt_root, true, $http_tt_root));

		new Warning("Please create file '$filename' with server configs.");
		new PageHtml(new ViewWizard($form->toHtml()));
	}

}