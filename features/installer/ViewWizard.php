<?php

namespace tt\features\installer;

use tt\features\htmlpage\view\v1\ViewHtmlNew;

class ViewWizard extends ViewHtmlNew
{

	private $html;

	public function __construct($html)
	{
		$this->html=$html;
	}

	/**
	 * @return string
	 */
	function getHtml()
	{
		return $this->html;
	}

	/**
	 * @return string
	 */
	function getTitle()
	{
		return "Installer";
	}

	public function getMoreHead()
	{
		return "<style>"
			.file_get_contents(dirname(__DIR__) . "/skins/demoskin/core.css")
			."\n".file_get_contents(__DIR__ . "/installer.css")
			."</style>";
	}

}