<?php

namespace tt\features\api;

use tt\features\config\v1\CFG_S;
use tt\features\debug\DebugHandler;
use tt\features\javascripts\AjaxEndpoint;
use tt\services\ServiceResponse;

class ApiResponse
{

	public function deliverAjaxEndpoint(AjaxEndpoint $endpoint)
	{
		$input = $_POST;
		$data = $endpoint->process($input);
		$response = array(
			"ok" => true,
			"data" => $data,
		);
		if (CFG_S::$DEVMODE) {
			#sleep(1);
			$debug_html =
				"<h3>Endpoint</h3>" . get_class($endpoint)
				. "<h3>Post</h3>" . print_r($input, 1)
				. "<h3>Duration</h3>Server: " . DebugHandler::getEnd() . " s / Overall: {{OVERALL}}"
				. "<h3>Response</h3>" . htmlentities(print_r($data, 1));
			$debug_html = "<div class='tt_ajax_debug'>$debug_html</div>";
			$response['ttapidebugcomp'] = DebugHandler::getHtmlComponent("AJAX", $debug_html, "ajaxDebugInfo");
		}
		ServiceResponse::respondJson($response);
	}

}