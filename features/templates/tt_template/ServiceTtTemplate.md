## Date and time
### {Ymd}
Current date. E.g. "20230113"
```php
date('Ymd')
```
