<?php

namespace tt\features\templates\tt_template;

use tt\features\debug\errorhandler\v1\Error;
use tt\services\ServiceFiles;
use tt\services\ServiceStrings;

/**
 * @see https://gitlab.com/experder/tt2/-/blob/main/features/templates/tt_template/ServiceTtTemplate.md
 */
class ServiceTtTemplate
{

	public static function template_dateTime($string){

		//{Ymd}
		$string = str_replace("{Ymd}", date('Ymd'), $string);

        $string = str_replace("{d-m-Y}", date('d-m-Y'), $string);

		return $string;
	}

	public static function load($file, $replacements = array()) {
		if (!file_exists($file)) {
			new Error("Template file \"$file\" not found!");
		}

		//Read template file:
		$content = file_get_contents($file);

		if ($content === false) {
			new Error("Template file \"$file\" could not be loaded.");
		}

		//Replacements:
		return ServiceStrings::replace_byArray($content, $replacements);
	}

	/**
	 * @param string   $target_file
	 * @param string   $template_file
	 * @param string[] $keyVals
	 * @param bool     $override
	 * @return bool
	 */
	public static function createFile($target_file, $template_file, $keyVals, $override = false) {
		if (!$override && file_exists($target_file)) {
			new Error("Couldn't store file \"$target_file\". File already exists!");
		}
		$content = self::load($template_file, $keyVals);
		return ServiceFiles::save($target_file, $content);
	}
}