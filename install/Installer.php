<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\install;

use tt\core\Autoloader;
use tt\core\Config;
use tt\core\ErrorDev;
use tt\core\Modules;
use tt\core\page\Message;
use tt\core\view\JsonResponse;
use tt\coremodule\CoreModule2;
use tt\service\form\Form;
use tt\service\form\FormfieldPassword;
use tt\service\form\FormfieldRadio;
use tt\service\form\FormfieldRadioOption;
use tt\service\form\FormfieldText;
use tt\service\js\Js;
use tt\service\ServiceArchives;
use tt\service\ServiceEnv;
use tt\service\ServiceFiles;
use tt\service\ServiceHtml;
use tt\service\ServiceStrings;
use tt\service\ServiceTemplates;

/**
 * Installer handles:
 * - Creation of "config_pointer.php"
 * - Creation of "ConfigProject.php"
 * - Creation of "ConfigServer.php" (name specified in ConfigProject.php)
 * - Creation of database (name specified in ConfigServer.php)
 * - Download of third party packages
 */
class Installer {

	const PAGEID = "core/installer";

	const DIVID_download_status_div = 'download_status_div';

	const AJAXDATA_warning = "warning";

	const CMD_GetExternalFile = 'cmdGetExternalFile';

	public static $WizardHeadJquerySource = "";

	public static function getClass() {
		return \tt\service\polyfill\Php5::get_class();
	}

	public static function requireConfigPointer($init = true) {
		$file = dirname(__DIR__) . '/config_pointer.php';

		if (!file_exists($file)) {
			self::promptConfigPointer($file);
		}

		require_once $file;

		if($init)Config::initCore();
	}

	private static function promptConfigPointer($file) {
		require_once dirname(__DIR__) . '/core/Autoloader.php';
		Autoloader::init();

		if (!ServiceEnv::requestCmd('createConfigPointer')) {
			$form = new Form("createConfigPointer", "", "Create config_pointer.php");
			$suggest = "dirname(__DIR__).'/TTconfig/ConfigProject.php'";
			$form->addField(new FormfieldText("val_webpath", "Path to ConfigProject.php", $suggest, true, array(
				"id" => "focus"
			)));
			$m = new Message(Message::TYPE_INFO,
				"The file <b>$file</b> (excluded from the repo) points to <b>Config_project.php</b>."
			);
			self::startWizard(
				$m->toHtml()
				. $form
			);
		}

		ServiceTemplates::create_file($file, __DIR__ . '/templates/config_pointer.php', array(
			"'#INIT_WEB_PATH'" => $_REQUEST["val_webpath"],
		));

		$form = new Form(null, "", false);
		$form->addButton(
			"<input type='submit' id='focus' value='OK'>"
		);
		self::startWizard(
			Message::messageToHtml(Message::TYPE_CONFIRM,
				"File '<b>$file</b>' has been created."
			)
			. $form
		);

	}

	public static function getExternalFileChecked($url, $toFile, $onSuccessJs = "", $checksum = false, $report = true) {
		if(file_exists($toFile)){
			if($report) new ErrorDev("File $toFile already exists!");
			return;
		}
		self::getExternalFile($url, $toFile, $onSuccessJs, $checksum);
	}

	public static function doUnzip($zipfile, $targetDir, $delete = true) {
		$ok = ServiceArchives::doUnzip($zipfile, $targetDir, $delete);
		$filename = basename($zipfile);
		if($ok){
			self::startWizardConfirm("Successfully unzipped file '$filename'.");
		}else{
			self::startWizardConfirm("Error when unzipping file '$filename'!", Message::TYPE_ERROR);
		}
	}

	public static function getExternalFile($url, $toFile, $onSuccessJs = "", $checksum = false) {
		if (!ServiceEnv::requestCmd(self::CMD_GetExternalFile)) {

			$msg = "Downloading <b>$url</b>...";
			$m = new Message(Message::TYPE_INFO, $msg);
			$msg = $m->toHtml();
			$msg = "<div id='" . self::DIVID_download_status_div . "'>$msg</div>";

			self::startWizard(
				$msg
				. "<script>" . Js::ajaxPostToId(self::DIVID_download_status_div, self::CMD_GetExternalFile, InstallerApi::API_ID, array(
					"url" => $url,
					"to_file" => $toFile,
					"checksum" => $checksum,
				), "html", "
				
					if(data.ok){
						$onSuccessJs
					}
				
				") . "</script>"
			);
		}

		return true;
	}

	public static function doGetExternalFile2($url, $toFile, $checksum = false) {

		$stream = fopen($url, 'r');
		if ($stream === false) {
			new ErrorDev("Could not open URL '$url'!");
		}
		ServiceFiles::save($toFile, $stream);

		$filename = basename($toFile);

		$msg = "Successfully stored file '$filename'.";
		$msg = Message::messageToHtml(Message::TYPE_CONFIRM, $msg);
		$form = new Form(null, "", false);
		$form->addButton(
			"<input type='submit' id='focus' value='OK'>"
		);
		$msg .= $form;

		if ($checksum !== false) {
			$hash = hash_file("md5", $toFile);
			if ($hash !== $checksum) {
				$msg = Message::messageToHtml(Message::TYPE_ERROR, "Stored file '$filename', but hash doesn't match!"
					." ".$hash
				);
			}
		}

		return new JsonResponse(array(
			"ok" => true,
			"html" => $msg,
		));

	}

	public static function requireConfigServer() {
		$file = Config::get(Config::CFG_SERVER_INIT_FILE);
		if($file===false)return;

		if (!file_exists($file)) {
			self::promptConfigServer($file);
		}

		require_once $file;
	}

	private static function promptConfigServer($file) {
		if (ServiceEnv::requestCmd('createConfigServer')) {
			self::createConfigServer($file);
			return;
		}
		$form = new Form("createConfigServer", "", "Create ConfigServer.php");

		$suggest = dirname($_SERVER['SCRIPT_NAME']);
		if (($p = strpos($suggest, '/TToolbox')) !== false) {
			$suggest = substr($suggest, 0, $p);
		}
		$form->addField(new FormfieldText("HTTP_ROOT", "Web root path", $suggest, true, array("id" => "focus")));

		$form->addField($ff = new FormfieldText("DB_HOST", "DB host", "localhost"));
		$ff->setTooltip("leave empty for installation without database");

		$form->addField(new FormfieldText("DB_NAME", "DB name", "mytt"));
		$form->addField(new FormfieldText("DB_USER", "DB user", "root"));
		$form->addField(new FormfieldPassword("DB_PASS", "DB pass"));

		$form->addField(new FormfieldRadio("DEVMODE", array(
			new FormfieldRadioOption("on", "Development"),
			new FormfieldRadioOption("off", "Production"),
		), "on"));

		self::startWizard(
			Message::messageToHtml(Message::TYPE_INFO,
				"The file <b>$file</b> contains server specific settings."
			)
			. $form
		);
	}

	private static function createConfigServer($file) {

		$platform = "PLATFORM_UNKNOWN";
		if (PHP_OS == 'WINNT') $platform = "PLATFORM_WINDOWS";
		if (PHP_OS == 'Linux') $platform = "PLATFORM_LINUX";

		$servername = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : "myserver";

		ServiceTemplates::create_file($file, __DIR__ . '/templates/ConfigServerTemplate.php', array(
			"<?php" . PHP_EOL . PHP_EOL . "/*" => "<?php" . PHP_EOL . "/*",

			"#HTTP_ROOT" => $_REQUEST["HTTP_ROOT"],

			PHP_EOL . "Database::init" => PHP_EOL . ($_REQUEST["DB_HOST"] ? "" : "#") . "Database::init",
			"#DB_HOST" => $_REQUEST["DB_HOST"],
			"#DB_NAME" => $_REQUEST["DB_NAME"],
			"#DB_USER" => $_REQUEST["DB_USER"],
			"#DB_PASS" => $_REQUEST["DB_PASS"],
			"'#DEVMODE'" => $_REQUEST["DEVMODE"] == 'on' ? 'true' : 'false',

			"'#PLATFORM'" => 'Config::' . $platform,
			"#SERVERNAME" => $servername,
		));
	}

	public static function requireConfigProject($file) {
		if (!file_exists($file)) {
			self::promptConfigProject($file);
		}

		require_once $file;
	}

	private static function promptConfigProject($file) {
		require_once dirname(__DIR__) . '/core/Autoloader.php';
		Autoloader::init();

		if (!ServiceEnv::requestCmd('cmdCreateInit')) {
			$form = new Form("cmdCreateInit", "", "Create " . basename($file));

			$suggest = "dirname(__DIR__) . '/" . basename(dirname(__DIR__)) . "'";
			$form->addField(new FormfieldText("TToolbox", "Path to TToolbox", $suggest, true, array(
				"id" => "focus",
			)));

			$suggest = strtolower(basename(dirname(dirname(__DIR__))));
			$form->addField(new FormfieldText("PROJ_NAMESPACE_ROOT", "Project's root namespace", $suggest));

			self::startWizard(
				Message::messageToHtml(Message::TYPE_INFO,
					"The file <b>$file</b> contains project specific settings."
				)
				. $form
			);
		}

		$ttroot = "Config::get(Config::HTTP_ROOT) . '/" . basename(dirname(__DIR__)) . "'";

		ServiceTemplates::create_file($file, __DIR__ . '/templates/ConfigProjectTemplate.php', array(
			"//TPL:namespace" => "namespace",
			"'#TToolbox'" => $_REQUEST["TToolbox"],
			"#PROJ_NAMESPACE_ROOT" => $_REQUEST["PROJ_NAMESPACE_ROOT"],
			"'#HTTP_TTROOT'" => $ttroot,
		));

		$form = new Form(null, "", false);
		$form->addButton(
			"<input type='submit' id='focus' value='OK'>"
		);
		self::startWizard(
			Message::messageToHtml(Message::TYPE_CONFIRM,
				"File '<b>$file</b>' has been created."
			)
			. $form
		);

	}

	public static function initDatabaseGui($dbname, $host, $user, $password) {
		if (!ServiceEnv::requestCmd('cmdInitDatabase')) {
			$form = new Form("cmdInitDatabase", "", false);
			$form->addButton(
				"<input type='submit' id='focus' value='"
				. ServiceStrings::escape_value_html("Create database '$dbname'")
				. "'>"
			);

			self::startWizard(
				Message::messageToHtml(Message::TYPE_INFO,
					"Database '<b>$dbname</b>' needs to be created."
				)
				. $form
			);
		}

		$init_response = self::initDatabaseDo($dbname, $host, $user, $password);

		$form2 = new Form(null, "", false);
		$form2->addButton(
			"<input type='submit' id='focus' value='OK'>"
		);
		self::startWizard(
			Message::messageToHtml(Message::TYPE_CONFIRM,
				"Database '<b>$dbname</b>' has been created. $init_response"
			)
			. $form2
		);

	}

	private static function initDatabaseDo($dbname, $host, $user, $password) {
		$dbh = new \PDO("mysql:host=" . $host, $user, $password);
		$dbh->exec(
			"CREATE DATABASE `" . $dbname . "` CHARACTER SET utf8 COLLATE utf8_general_ci;"
		) or die("Error240! " . print_r($dbh->errorInfo(), true));

		CoreModule2::init($host, $dbname, $user, $password);

		//Initialize installed modules:
		$msg = implode("<br>", Modules::updateAll());

		return $msg;
	}

	/**
	 * @deprecated [A__DE]
	 */
	public static function initApiClass($classname, $filename) {
		if (!ServiceEnv::requestCmd('cmdInitApiClass')) {
			$form = new Form("cmdInitApiClass", "", false);
			$form->addButton(
				"<input type='submit' id='focus' value=\"Create API class '$classname'\">"
			);

			self::startWizard(
				Message::messageToHtml(Message::TYPE_INFO,
					"API class '<b>$filename</b>' needs to be created."
				)
				. $form
			);
		}

		$api_class_content = "<?php\n\nnamespace tt\\api;\n\nclass $classname extends \\tt\\core\\api_default\\$classname {\n}";

		ServiceFiles::save($filename, $api_class_content);
	}

	public static function startWizardConfirm($msg, $type = Message::TYPE_CONFIRM) {

		$msgHtml = Message::messageToHtml($type, $msg);

		$form = new Form(null, "", false);
		$form->addButton(
			"<input type='submit' id='focus' value='OK'>"
		);

		self::startWizard( $msgHtml . $form );
	}

	public static function startWizard($html, $api = false) {

		if ($api || ServiceEnv::isSapiCLI()) {
			new ErrorDev("Start wizard: Not possible! Please call via Browser.");
		}

		echo self::wizardHtml($html);
		exit;
	}

	public static function onloadFocusJs() {
		return "e=document.getElementById('focus');if(e)e.focus();";
	}

	public static function wizardHtml($body) {
		$css = file_get_contents(__DIR__ . "/wizard.css");
		$js = file_get_contents(__DIR__ . "/wizard.js");

		$head = "<style>$css</style>";
		$head .= "<script>$js</script>";
		$head .= (self::$WizardHeadJquerySource?:"<script src=\"".Config::get(Config::HTTP_3RDPARTY)."/jquery341/jquery.min.js"."\"></script>");

		if ($ttroot = Config::get(Config::HTTP_TTROOT, false))
			$head .= ServiceHtml::htmlScript($ttroot . '/service/js/core.js');

		$head = "<head>$head</head>";

		$html = ServiceHtml::H1("Install wizard");
		$html .= "<div id='tt_pg_messages'></div>";
		$html .= $body;

		$html = "<body onload='" . ServiceStrings::escape_value_html(self::onloadFocusJs()) . "'>$html</body>";

		$html = $head . $html;
		$html = "<!DOCTYPE html><html>$html</html>";

		return $html;
	}

}