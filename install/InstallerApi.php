<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\install;

use tt\core\page\Message;
use tt\core\view\Api;
use tt\core\view\InputData2;
use tt\core\view\JsonResponse;

class InstallerApi extends Api {

	const API_ID = "core/installer2";

	/**
	 * @inheritdoc
	 */
	public function run(InputData2 $inputData) {

		$data = $inputData->getData();

		list($cmd) = $this->requiredFieldsFromData($data,
			array("cmd"), false);

		switch ($cmd) {
			case Installer::CMD_GetExternalFile:
				list($url, $toFile) = $this->requiredFieldsFromData($data,
					array("url", "to_file"), false);
				$checksum = (isset($data["checksum"]) && $data["checksum"] !== 'false') ? $data["checksum"] : false;
				return Installer::doGetExternalFile2($url, $toFile, $checksum);
				break;
			default:
				return new JsonResponse(array(
					"ok" => true,
					"html" => Message::messageToHtml(Message::TYPE_ERROR, "InstallerApi: Unknown command \"$cmd\"!"),
				));
				break;
		}

	}

	/**
	 * @inheritdoc
	 */
	public function getApiId() {
		return self::API_ID;
	}

	public static function getClass() {
		return \tt\service\polyfill\Php5::get_class();
	}

	public function getClass_() {
		return self::getClass();
	}
}