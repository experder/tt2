<?php
/**TPLDOCSTART
 * Template for the file "config_pointer.php" (ignored in git).
 * @see \tt\install\Installer::requireConfigPointer()
 */
/** @noinspection PhpUnreachableStatementInspection */
/** @noinspection PhpIncludeInspection */
if (true) exit;/*
 * TPLDOCEND*/

#require_once dirname(__DIR__).'/TTconfig/ConfigProject.php';
#require_once '#INIT_WEB_PATH';

use tt\install\Installer;

require_once __DIR__ . '/install/Installer.php';
Installer::requireConfigProject('#INIT_WEB_PATH');
