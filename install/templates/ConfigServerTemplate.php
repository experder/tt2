<?php
/**TPLDOCSTART
 * Template for the file CFG_SERVER_INIT_FILE (e.g. "ConfigServer.php").
 * @see \tt\install\Installer::requireConfigServer()
 */
if (true) exit;/*
 * TPLDOCEND*/

/*
 * Server specific settings
 *
 * Server: #SERVERNAME
 *
 */

use tt\coremodule\dbmodell\core_config;
use tt\core\database\Database;
use tt\core\Config;

Config::set(Config::DEVMODE, '#DEVMODE');

Config::set(Config::DB_CORE_PREFIX, 'core');

core_config::setTableName(Config::get(Config::DB_CORE_PREFIX) . '_config');

Config::set(Config::HTTP_ROOT, '#HTTP_ROOT');

Database::init('#DB_HOST', '#DB_NAME', '#DB_USER', '#DB_PASS');

Config::set(Config::CFG_PLATFORM, '#PLATFORM');

Config::set(Config::CSS_NOCACHE, false);
