<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace tt\install;

use tt\core\Config;
use tt\core\page\Message;
use tt\core\view\HtmlPage;
use tt\service\ServiceArchives;
use tt\service\ServiceStrings;

abstract class ThirdpartyInstaller {

	/**
	 * @var string $id Used for:
	 * - Naming the thirdparty-subdirectory
	 * - Identifying the CSS- and JS-references
	 */
	protected $id = null;

	protected $resourceUrl = null;
	/**
	 * @var string|false $checksum md5-hash of the resource or false if no check required.
	 */
	protected $checksum = false;
	protected $unzip = null;

	protected $includeUrl = null;
	protected $includeCss = null;
	protected $includeJs = null;

	protected $checkFile = null;
	protected $targetDir = null;

	private static $singletons = array();

	private function getUnzip() {
		if($this->unzip===null){
			return ServiceStrings::endsWith(".zip", $this->resourceUrl,false);
		}
		return $this->unzip;
	}

	protected function getResourceUrl(){
		return $this->resourceUrl;
	}

	protected function getCheckFile(){
		if($this->checkFile===null){
			$this->checkFile=$this->getTargetDir().'/'.($this->getIncludeJs()?:$this->includeCss);
		}
		return $this->checkFile;
	}

	protected function getTargetDir() {
		if($this->targetDir===null){
			$this->targetDir=Config::getChecked(Config::DIR_3RDPARTY).'/'.$this->id;
		}
		return $this->targetDir;
	}

	protected function getIncludeUrl() {
		if($this->includeUrl===null){
			$this->includeUrl=Config::getChecked(Config::HTTP_3RDPARTY).'/'.$this->id;
		}
		return $this->includeUrl;
	}

	protected function getDownloadTarget() {
		return $this->getTargetDir().'/'.basename($this->getResourceUrl());
	}

	protected function getIncludeCss() {
		return $this->includeCss;
	}

	protected function getIncludeJs() {
		if($this->includeJs===null){
			if (ServiceStrings::endsWith(".js", $this->resourceUrl,false)){
				$this->includeJs = basename($this->resourceUrl);
			}
		}
		return $this->includeJs;
	}

	/**
	 * @return $this
	 */
	public static function getSingleton(){

		$classname = get_called_class();

		if(!isset(self::$singletons[$classname])){
			self::$singletons[$classname] = new $classname();
		}

		return self::$singletons[$classname];
	}

	/**
	 * @deprecated [AB__E]
	 */
	public function doInclude(HtmlPage $htmlPage){
		if (!$this->checkExists()) $this->doDownload();

		$css = $this->getIncludeCss();
		if($css!==null){
			$htmlPage->addCssAbsolute($this->getIncludeUrl().'/'.$css, $this->id);
		}
		$js = $this->getIncludeJs();
		if($js!==null){
			$htmlPage->addJsLink($this->getIncludeUrl().'/'.$js, $this->id);
		}
	}

	/**
	 * @return array|null
	 */
	public function getJsLink(){
		if (!$this->checkExists()) $this->doDownload();
		$js = $this->getIncludeJs();
		if($js===null) return null;
		return array($this->id => $this->getIncludeUrl().'/'.$js);
	}

	public function getCssLink(){
		if (!$this->checkExists()) $this->doDownload();
		$css = $this->getIncludeCss();
		if($css===null) return null;
		return array($this->id => $this->getIncludeUrl().'/'.$css);
	}

	protected function checkExists(){
		return file_exists($this->getCheckFile());
	}

	protected function hookAfterDownload(){}

	protected function doDownload(){

		Installer::getExternalFileChecked($this->getResourceUrl(), $this->getDownloadTarget(), Installer::onloadFocusJs(), $this->checksum, false);

		$zip_ok = !$this->getUnzip() || ServiceArchives::doUnzip($this->getDownloadTarget(), $this->getTargetDir());

		$filename = basename($this->getDownloadTarget());
		if(!$zip_ok){
			Installer::startWizardConfirm("Error when unzipping file '$filename'!", Message::TYPE_ERROR);
		}

		$this->hookAfterDownload();

		Installer::startWizardConfirm("Successfully installed file '$filename'.");

	}

}