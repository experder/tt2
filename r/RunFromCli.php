<?php

namespace tt\r;

interface RunFromCli
{

	function __construct();

	/**
	 * @param string[] $args
	 * @return void
	 */
	function runFromCli($args);

}