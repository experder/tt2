<?php
//TODO:Documentation(.md)

namespace tt\r;

use tt\features\debug\DebugHandler;
use tt\features\frontcontroller\bycode\FrontcontrollerByCode;

require_once dirname(__DIR__).'/features/debug/DebugHandler.php';
DebugHandler::setStart();

if(php_sapi_name() !== 'cli') {
	echo "This is a commandline entrypoint.";
	exit;
}
if(!isset($_SERVER)||!is_array($_SERVER)||!isset($_SERVER['argv'])
	||!is_array($_SERVER['argv'])||count($_SERVER['argv'])<2) {
	echo "Please specify class.";
	exit;
}

$serverArgs = $_SERVER['argv'];

array_shift($serverArgs);//First element: clirun.php
$target = array_shift($serverArgs);

require_once dirname(__DIR__).'/features/frontcontroller/bycode/FrontcontrollerByCode.php';
FrontcontrollerByCode::cliCallTarget($target, $serverArgs);
