<?php

namespace tt\r;

use tt\features\debug\DebugHandler;
use tt\features\frontcontroller\bycode\FrontcontrollerByCode;

require_once dirname(__DIR__).'/features/debug/DebugHandler.php';
DebugHandler::setStart();

if(!isset($_GET['tt_route'])){
	echo "<hr><pre style='white-space: pre-wrap;color:darkred;'>⛔ Not set: 'tt_route'!"
		."\nPlease 'AllowOverride' for folder '".basename(__DIR__)."' or call route via 'index.php?tt_route=...'</pre><hr>";
	exit;
}
$route = $_GET['tt_route'];

require_once dirname(__DIR__).'/features/frontcontroller/bycode/FrontcontrollerByCode.php';
FrontcontrollerByCode::callRoute($route);
