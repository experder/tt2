<?php

namespace tt\services;

use tt\features\debug\errorhandler\v1\Error;
use tt\services\polyfill\Php7;

class ServiceFinancial
{

	public static function centsToEuro($cents, $posSign = true, $suffix = true)
	{
		if (!$cents) $cents = "0";
		if (!is_numeric($cents)) new Error("Malformatted amount '$cents'!");
		if($suffix===true)$suffix=Php7::mb_chr(UnicodeIcons::non_breaking_space)."€";
		return ($posSign && $cents > 0 ? "+" : "")
			. number_format($cents / 100, 2, ',', '.')
			. $suffix;
	}

	public static function euroToCents($euro_string)
	{
		if (!$euro_string) return 0;
		if (!preg_match("/^([-+]? ?[.\\d]*,?\\d+)/", $euro_string, $matches)) {
			new Error("Malformatted amount '$euro_string'!");
		}
		$euro_bereinigt = $matches[1];
		$euro_bereinigt = str_replace(".", "", $euro_bereinigt);
		$euro_bereinigt = str_replace(",", ".", $euro_bereinigt);
		return round($euro_bereinigt * 100);
	}

}