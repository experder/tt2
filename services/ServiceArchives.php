<?php

namespace tt\services;

class ServiceArchives {

	public static function doUnzip($zipfile, $targetDir, $delete = true) {

		$zip = new \ZipArchive();
		$res = $zip->open($zipfile);
		if ($res !== true) return false;
		$ok = $zip->extractTo($targetDir);
		$zip->close();
		if ($ok && $delete) {
			unlink($zipfile);
		}
		return $ok;

	}

}