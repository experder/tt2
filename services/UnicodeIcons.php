<?php

namespace tt\services;

use tt\services\polyfill\Php7;

class UnicodeIcons
{

	const AS_CHAR = 0xfe0e;//Variation Selector 15, text style (monochrome)
	const AS_EMOJI = 0xfe0f;//Variation Selector 16, emoji-style (with color)
	const AS_SKINTONE_LIGHT = 0x1F3FB;
	const AS_SKINTONE_MEDIUM_LIGHT = 0x1F3FC;
	const AS_SKINTONE_MEDIUM = 0x1F3FD;
	const AS_SKINTONE_MEDIUM_DARK = 0x1F3FE;
	const AS_SKINTONE_DARK = 0x1F3FF;
	const MODIFIER_AND = 0x200D;//zero width joiner

	public static $all_modifiers = array(
		self::AS_EMOJI => "Emoji-style",
		self::AS_CHAR => "Text-style",
		self::AS_SKINTONE_LIGHT => "Light skintone",
		self::AS_SKINTONE_MEDIUM_LIGHT => "Medium-light skintone",
		self::AS_SKINTONE_MEDIUM => "Medium skintone",
		self::AS_SKINTONE_MEDIUM_DARK => "Medium-dark skintone",
		self::AS_SKINTONE_DARK => "Dark skintone",
		self::MODIFIER_AND => "AND",
	);

	public static $modifiers_short = array(
		self::AS_EMOJI => "EMO",
		self::AS_CHAR => "TXT",
		self::MODIFIER_AND => "AND",
	);

	//2190-21ff Arrows
	const Leftwards_Arrow_with_Hook = 0x21a9;//↩

	//2600-26ff Miscellaneous Symbols
	const Warning_Sign = 0x26a0;//⚠
	const High_Voltage_Sign = 0x26a1;//⚡
	const No_Entry = 0x26d4;//⛔

	//2700—27bf Dingbats
	const White_Heavy_Check_Mark = 0x2705;//✅
	const Pencil = 0x270f;//✏
	const Heavy_Check_Mark = 0x2714;//✔
	const Heavy_Ballot_X = 0x2718;//✘

	//1f300-1f5ff Miscellaneous Symbols and Pictographs
	const White_Left_Pointing_Backhand_Index = 0x1f448;//👈
	const White_Right_Pointing_Backhand_Index = 0x1f449;//👉
	const Open_Mailbox_with_Raised_Flag = 0x1f4ec;//📬
	const Wastebasket = 0x1F5D1;//🗑
	const Put_Litter_In_Its_Place_Symbol = 0x1F6AE;//🚮
	const Black_Universal_Recycling_Symbol = 0x267B;//♻

	const Magic_Wand = 0x1FA84;//🪄
	const ballot_box_with_bold_check = 0x1F5F9;//🗹
	const ballot_box = 0x2610;//☐
	const hammer_and_wrench = 0x1F6E0;//🛠
	const earth_globe_europe_africa = 0x1F30D;//🌍
	const house_with_garden = 0x1F3E1;//🏡
	const brick= 0x1F9F1;//🧱
	const t_rex = 0x1F996;//🦖
	const thumbs_up_sign=0x1F44D;//👍
	const thumbs_down_sign=0x1F44E;//👎
	const regional_indicator_symbol_letter_b=0x1F1E7;//🇧
	const hourglass_with_flowing_sand=0x23F3;//⏳
	const fire_engine=0x1F692;//🚒
	const adult=0x1F9D1;//🧑
	const cross_mark=0x274C;//❌
	const non_breaking_space=0xA0;

	/**
	 * Variation Selector 16, emoji-style (with color)
	 * Example: ✅️ (✅)
	 * @param string $num Codepoint
	 * @return string Character variation
	 */
	public static function asEmoji($num){
		return Php7::mb_chr($num).Php7::mb_chr(UnicodeIcons::AS_EMOJI);
	}
	/** @deprecated */
	public static function emoji($num){return self::asEmoji($num);}

	/**
	 * Variation Selector 15, text style (monochrome)
	 * Example: 📬️ (📬)
	 * @param string $num Codepoint
	 * @return string Character variation
	 */
	public static function asText($num){
		return Php7::mb_chr($num).Php7::mb_chr(UnicodeIcons::AS_CHAR);
	}

	public static function joiner($codepoint1, $codepoint2){
		return Php7::mb_chr($codepoint1).Php7::mb_chr(self::MODIFIER_AND).Php7::mb_chr($codepoint2);
	}

}