<?php

namespace tt\services;

use tt\features\config\v1\CFG_S;
use tt\features\debug\errorhandler\v1\Error;
use tt\features\debug\errorhandler\v2\Warning;

class ServiceFiles
{

	public static function includeIfExists($file)
	{
		if (file_exists($file) && is_file($file)) {
			require_once $file;
			return true;
		}
		return false;
	}

	public static function download($url, $file_target)
	{
		if(file_exists($file_target))return;
		$stream = fopen($url, 'r');
		if ($stream === false) {
			new Error("Could not open URL '$url'!");
		}
		ServiceFiles::save($file_target, $stream);
	}

	public static function mkdir($dirname) {
		if (!is_dir($dirname)) {
			@mkdir($dirname, 0755, true);
		}
		return is_dir($dirname);
	}

	/**
	 * @param string $filename
	 * @param string|resource $content
	 * @param bool $append
	 * @return bool
	 */
	public static function save($filename, $content, $append = false) {
		if(!self::mkdir(dirname($filename))){
			return false;
		}

		$success = false;

		if (is_resource($content)) {
			$success = @file_put_contents($filename, $content, $append ? FILE_APPEND : 0);
		} else {
			$file = @fopen($filename, $append ? "a" : "w");
			if ($file !== false) {
				$success = fwrite($file, $content);
				fclose($file);
			}
		}

		if ($success === false) {
			new Error("Couldn't store file \"$filename\".");
		}

		return true;
	}

	public static function get_contents($file, $utf8 = true)
	{
		if (!$file || !file_exists($file)) {
			new Error("File not found!" . (CFG_S::$DEVMODE ? " '$file'" : ""));
		}
		$content = @file_get_contents($file);
		if ($content === false) {
			new Error("Could not read file" . (CFG_S::$DEVMODE ? " '$file'" : "") . ".");
		}
		if (!$utf8) $content = utf8_encode($content);
		return $content;
	}

	/**
	 * @param string $deletefile
	 * @return bool
	 */
	public static function unlink_file($deletefile, $report = true)
	{
		$ok = @unlink($deletefile);
		if ($report && !$ok) new Warning("Could not delete '" . (CFG_S::$DEVMODE ? $deletefile : basename($deletefile)) . "'!");
		return $ok;
	}

	public static function checkHashOfFile($file, $hash, $die=false)
	{
		$hashCalc = md5_file($file);
		$ok = ($hashCalc===$hash);
		if(!$ok){
			$msg = "Hash value of '".basename($file)."' is not correct.".(CFG_S::$DEVMODE?" ".$hashCalc:"");
			if($die){new Error($msg);}
			new Warning($msg);
		}
		return $ok;
	}

	public static function rename($from, $to)
	{
		if(!self::mkdir(dirname($to))){
			return false;
		}
		rename($from, $to);
	}

	public static function unlinkDirectoryRecursively($dir) {
		self::delDirContents($dir);
		rmdir($dir);
	}

	public static function delDirContents($dir, $recursive = true) {
		$files = glob("$dir/{,.}*", GLOB_BRACE);
		$ok_count = 0;
		foreach ($files as $file) {
			if (basename($file) == '.' || basename($file) == '..') continue;
			if (is_file($file)) {
				if (@unlink($file) && ($ok_count !== false)) {
					$ok_count++;
				} else {
					$ok_count = false;
				}
			} else if (is_dir($file) && $recursive) {
				$ok_sub = self::delDirContents($file);
				if ($ok_sub === false) {
					$ok_count = false;
				} else {
					rmdir($file);
					$ok_count += ++$ok_sub;
				}
			} else {
				$ok_count = false;
			}
		}
		return $ok_count;
	}

}