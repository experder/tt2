<?php

namespace tt\services;

use tt\features\htmlpage\components\HtmlComponent;
use tt\services\polyfill\Php7;

class ServiceHtml
{

	public static function arrayToList(array $array)
	{
		return "<ul>\n\t<li>" . implode("</li>\n\t<li>", $array) . "</li>\n</ul>";
	}

	public static function arrayToList_(array $array)
	{
		return "<ul><li>" . implode("</li><li>", $array) . "</li></ul>";
	}

	public static function htmlentitiesArray(array $array)
	{
		$escaped = array();
		foreach ($array as $string) {
			$escaped[] = htmlentities($string);
		}
		return $escaped;
	}

	/**
	 * Escapes quotes (single, double) and ampersand with htmlentities.
	 * Example:
	 *      "<tag value = '".escape_value_html($value)."' />"
	 * @param string $value
	 * @return string
	 */
	public static function escape_value_html($value)
	{
		return ServiceStrings::replace_byArray($value, array(
			"&" => "&amp;",
			"\"" => "&quot;",
			"'" => "&apos;",
		));
	}

	public static function keyValueHtml($key, $value = "")
	{
		if (is_array($key)) {
			$html = array();
			foreach ($key as $k => $val) {
				$html[] = self::keyValueHtml($k, $val);
			}
			return implode("", $html);
		}
		return " " . $key . "='"
			. ($value === null ? "" : ServiceHtml::escape_value_html($value))
			. "'";
	}

	public static function buildNode($name, $keyVals, HtmlComponent $component, $inner_before = "", $inner_after = "", $void = false)
	{
		$css = $component->getCssHtml();

		$id = $component->getId();

		$keyVals_html = $keyVals ? ServiceHtml::keyValueHtml($keyVals) : "";

		$tag = $name . " id='$id'" . $keyVals_html . $css;

		if ($void) {
			return "<$tag />";
		}

		$children = $component->getChildrenHtml();

		return "<$tag>$inner_before$children$inner_after</$name>";
	}

	public static function checkAction($onClick){
		return "<a class='checkAction button' onclick='".ServiceHtml::escape_value_html($onClick)."'>"
			.Php7::mb_chr(UnicodeIcons::ballot_box)."</a>";
	}

}