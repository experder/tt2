<?php

namespace tt\services;

use tt\services\polyfill\Php5;

class ServiceResponse
{

	public static function permanentRedirect301($url)
	{
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: " . $url);
		exit;
	}

	public static function respondJson($object, $pretty = false)
	{
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($object, ($pretty ? Php5::JSON_PRETTY_PRINT : 0));
		exit;
	}

}