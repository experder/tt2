<?php

namespace tt\services;

use tt\features\debug\errorhandler\v1\Error;
use tt\features\htmlpage\components\Form;

class ServiceEnv
{

	const DEFAULT_QUIT_WITH_ERROR = "tt_quit_with_error_!";

	private static $mobile_agent = null;

	public static function isSapiCLI()
	{
		return php_sapi_name() === 'cli';
	}

	public static function isRequestAjax()
	{
		$headers = getallheaders();
		if (!isset($headers['X-TT-Source'])) return false;
		if ($headers['X-TT-Source'] === 'coreJs/ajaxJson') return true;
		return false;
	}

	public static function valueFromGet($key, $default = null)
	{
		if (isset($_GET[$key])) return $_GET[$key];
		if ($default === self::DEFAULT_QUIT_WITH_ERROR) {
			new Error("Required GET value not set: $key");
		}
		return $default;
	}

	public static function updateUrlParam($key, $value, $prefix = '?')
	{
		return self::updateUrlParams(array($key=>$value), $prefix);
	}

	public static function updateUrlParams($keyValues, $prefix = '?')
	{
		$urlParams = $_GET;
		unset($urlParams['tt_route']);
		foreach ($keyValues as $key=>$value){
			$urlParams[$key] = $value;
			if($value===false)unset($urlParams[$key]);
		}
		return ServiceStrings::buildQueryString($urlParams, $prefix);
	}

	public static function valueFromPost($key, $default = null)
	{
		if (isset($_POST[$key])) return $_POST[$key];
		return $default;
	}

	public static function isCmd($cmd){
		return self::valueFromPost(Form::COMMAND_STRING)===$cmd;
	}

	public static function normalizeLinebreaks($text)
	{
		return str_replace("\r\n", "\n", $text);
	}

	public static function isMobileAgent(){
		if(self::$mobile_agent===null){
			self::$mobile_agent = !ServiceEnv::isSapiCLI() && (strpos(strtolower($_SERVER["HTTP_USER_AGENT"]), "mobile")!==false);
		}
		return self::$mobile_agent;
	}

}