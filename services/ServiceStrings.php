<?php

namespace tt\services;

use tt\features\templates\tt_template\ServiceTtTemplate;

class ServiceStrings
{

	/**
	 * @param string $string
	 * @param string $additionalChars e.g. isClassname2($string, "\\") for namespaces
	 * @return bool
	 */
	public static function isClassname2($string, $additionalChars = "")
	{
		//https://www.php.net/manual/en/language.oop5.basic.php
		return !preg_match("/[^" . preg_quote($additionalChars, '/') . "a-zA-Z\\d_\\x80-\\xff]/", $string);
	}

	public static function ttMarkup($string)
	{

		//URLs:
		/** @noinspection HtmlUnknownTarget */
		$string = preg_replace("/( |^|\\n)(https?:\\/\\/.+?)( |\$|\\n)/",
			"\$1<a href='\$2' target='_blank'>\$2</a>\$3", $string);

		//Lists:
		if (preg_match_all("/(?:\\n|^)(\\* .*?)(\\n[^*]|\$)/s", $string, $matches)) {
			for ($i = 0; $i < count($matches[1]); $i++) {
				$list = $matches[1][$i];
				$listHtml = "<ul><li>"
					. str_replace("\n* ", "</li><li>", substr($list, 2)/*Strip firt bulletpoint*/)
					. "</li></ul>";
				$string = str_replace($list
					. ($matches[2][$i] !== '' ? "\n" : "")//If not end of string replace trailing linebreak as UL comes as a block.
					, $listHtml, $string);
			}
		}

		return $string;
	}

	public static function startsWith($needle, $haystack, $caseSensitive = true)
	{
		if ($caseSensitive) {
			return mb_substr($haystack, 0, mb_strlen($needle)) === $needle;
		}
		return mb_stripos($haystack, $needle) === 0;
	}

	public static function endsWith($needle, $haystack, $caseSensitive = true)
	{
		$haystack_part = mb_substr($haystack, mb_strlen($haystack) - mb_strlen($needle));
		if (!$caseSensitive) {
			$haystack_part = mb_strtolower($haystack_part);
			$needle = mb_strtolower($needle);
		}
		return $haystack_part === $needle;
	}

	/**
	 * Other syntax for the str_replace function.
	 *
	 * !!! WARNING! Keys that are a subset of other keys must be defined AFTER the other:
	 * Example 1:
	 * ServiceStrings::replace_byArray("dev dev1", array("dev"=>"A","dev1"=>"B"));
	 * => results in "A A1"
	 * Example 2:
	 * ServiceStrings::replace_byArray("dev dev1", array("dev1"=>"B","dev"=>"A"));
	 * => results in "A B"
	 *
	 * @param array $substitutions An associative array containing the substitutions.
	 * @param string $string
	 * @return string
	 */
	public static function replace_byArray($string, $substitutions)
	{
		return str_replace(
			array_keys($substitutions),
			array_values($substitutions),
			$string
		);
	}

	public static function eliminate_windows_linebreaks($string)
	{
		return str_replace("\r\n", "\n", $string);
	}

	public static function prefixValues($prefix, $valuesArray, $suffix = '')
	{
		$resulting = array();
		foreach ($valuesArray as $key => $val) {
			$resulting[$key] = $prefix . $val . $suffix;
		}
		return $resulting;
	}

	/**
	 * @param string[] $params
	 * @param string $prefix
	 * @return string
	 */
	public static function buildQueryString($params, $prefix = '?')
	{
		if (!$params || !is_array($params)) return "";
		$parts = array();
		foreach ($params as $key => $value) {
			$parts[] = urlencode($key) . '=' . urlencode($value);
		}
		return $prefix . implode('&', $parts);
	}

	public static function prefixKeys($prefix, $dataArray, $suffix = '')
	{
		$resulting = array();
		foreach ($dataArray as $key => $val) {
			$resulting[$prefix . $key . $suffix] = $val;
		}
		return $resulting;
	}

	public static function cutAndEllipsis($text, $maxlength, $ellipsis = "…", $ellipsis_length = true)
	{
		if (!$text) return $text;
		if (mb_strlen($text) > $maxlength) {
			if ($ellipsis_length === true) $ellipsis_length = mb_strlen($ellipsis);
			$text = mb_substr($text, 0, $maxlength - $ellipsis_length) . $ellipsis;
		}
		return $text;
	}

	public static function ttTemplates($string)
	{
		/** @noinspection PhpUnnecessaryLocalVariableInspection */
		$string = ServiceTtTemplate::template_dateTime($string);
		return $string;
	}

}