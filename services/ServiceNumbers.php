<?php

namespace tt\services;

class ServiceNumbers
{

	public static function leadingZeros($number, $length){
		return str_pad($number, $length, '0', STR_PAD_LEFT);
	}

}