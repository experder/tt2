<?php

namespace tt\services;

use tt\features\database\v1\Model;

class ServiceArrays
{

	public static function compareArrays($array1, $array2)
	{
		if (!is_array($array1) || !is_array($array2)) return false;
		if (count($array1) != count($array2)) return false;
		for ($i = 0; $i < count($array1); $i++) {
			if ($array1[$i] !== $array2[$i]) return false;
		}
		return true;
	}

	/**
	 * @param array $array [{"foo":"bar","hello":"world",...},...]
	 * @param string $key Placeholders in curly brackets: "{foo}". Default: true = "{id}" - see Model::FIELD_id
	 * @param string $value Placeholders in curly brackets: "{hello}"
	 * @return array Example: ["bar"=>"world",...]
	 * @see Model::FIELD_id
	 */
	public static function keyValueArray($array, $value, $key = true)
	{
		if ($key === true) $key = "{" . Model::FIELD_id . "}";
		$result = array();
		foreach ($array as $row_raw) {
			$row = ServiceStrings::prefixKeys("{", $row_raw, "}");
			$result_value = ServiceStrings::replace_byArray($value, $row);
			if($key===false){
				$result[] = $result_value;
			}else{
				$result_key = ServiceStrings::replace_byArray($key, $row);
				$result[$result_key] = $result_value;
			}
		}
		return $result;
	}

}