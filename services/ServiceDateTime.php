<?php

namespace tt\services;

use DateInterval;
use DateTime;
use Exception;
use tt\features\debug\errorhandler\v1\Error;
use tt\features\i18n\Trans;

class ServiceDateTime
{

	private static $month_names = null;

	public static function getMonthNames(){
		if(self::$month_names===null){
			self::$month_names=array(
				Trans::late("January"),
				Trans::late("February"),
				Trans::late("March"),
				Trans::late("April"),
				Trans::late("May"),
				Trans::late("June"),
				Trans::late("July"),
				Trans::late("August"),
				Trans::late("September"),
				Trans::late("October"),
				Trans::late("November"),
				Trans::late("December"),
			);
		}
		return self::$month_names;
	}

	public static function monthAusgeschrieben($dateString)
	{
		$ts = strtotime($dateString);
		if(!$ts)new Error("Invalid date format: $dateString");
		return (self::getMonthNames())[date('n', $ts)-1];
	}

	public static function dateAusgeschrieben($sql_date)
	{
		preg_match("/^(\\d{2,4})-(\\d{2})(?:-(\\d{2}))?\$/", $sql_date, $matches);
		if (!$matches) new Error("Invalid date format: $sql_date");
		$ausgabe = count($matches) > 3 ? ($matches[3] * 1) . ". " : "";
		$ausgabe .= (self::getMonthNames())[$matches[2] - 1];
		$ausgabe .= " " . $matches[1];
		return $ausgabe;
	}

	public static function folgemonat($from, $format="Y-m"){
		try {
			return date($format, date_add(new DateTime($from), new DateInterval("P1M"))->getTimestamp());
		} catch (Exception $e) {
			Error::fromException($e);
		}
		return false;
	}

	public static function vormonat($from, $format="Y-m"){
		try {
			return date($format, date_sub(new DateTime($from), new DateInterval("P1M"))->getTimestamp());
		} catch (Exception $e) {
			Error::fromException($e);
		}
		return false;
	}

}