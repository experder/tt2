<?php

namespace tt\services\polyfill;

use PDO;

class Php8
{

	public static function pdo_init(PDO $pdo){
		//Since PHP 8 this is the default behaviour:
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}

}