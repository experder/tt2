<?php

namespace tt\services\polyfill;

/**
 * Features that have been introduced in PHP 7
 */
class Php7 {

	/**
	 * Example: $session_id = bin2hex(\tt\services\polyfill\Php7::random_bytes(10));
	 *
	 * Syntax since PHP 7: $session_id = bin2hex(random_bytes(10));
	 *
	 * @see http://php.net/manual/en/function.random-bytes.php (Since 7.0)
	 *
	 * @param int $length
	 * @return string
	 */
	public static function random_bytes($length) {
		$string = "";
		for ($i = 0; $i < $length; $i++) {
			$string .= chr(rand(0, 255));
		}

		//Syntax since PHP 7:
//		try {
//			$string = random_bytes($length);
//		} catch (\Exception $e) {
//			\tt\features\debug\errorhandler\v1\Error::fromException($e);
//		}

		return $string;
	}

	/**
	 * Example: $emoji = \tt\services\polyfill\Php7::mb_chr(0x1f4ec);
	 *
	 * Syntax since PHP 7.2: $emoji = mb_chr(0x1f4ec);
	 *
	 * @see https://www.php.net/manual/en/function.mb-chr.php
	 *
	 * @param int $codepoint
	 * @return string
	 */
	public static function mb_chr($codepoint) {
		if ($codepoint <= 0x7F) return chr($codepoint);
		if ($codepoint <= 0x7FF) return chr(($codepoint >> 6) + 192) . chr(($codepoint & 63) + 128);
		if ($codepoint <= 0xFFFF) return chr(($codepoint >> 12) + 224) . chr((($codepoint >> 6) & 63) + 128) . chr(($codepoint & 63) + 128);
		if ($codepoint <= 0x1FFFFF) return chr(($codepoint >> 18) + 240) . chr((($codepoint >> 12) & 63) + 128) . chr((($codepoint >> 6) & 63) + 128) . chr(($codepoint & 63) + 128);
		return '';
	}

	public static function mb_ord($character, $encoding='UTF-8') {
		$charUCS4 = mb_convert_encoding($character, 'UCS-4BE', $encoding);
		$byte1 = ord(substr($charUCS4, 0, 1));
		$byte2 = ord(substr($charUCS4, 1, 1));
		$byte3 = ord(substr($charUCS4, 2, 1));
		$byte4 = ord(substr($charUCS4, 3, 1));
		return ($byte1 << 32) + ($byte2 << 16) + ($byte3 << 8) + $byte4;
	}

	public static function mb_str_split($unicode_character){
		$chr = Php7::mb_chr(Php7::mb_ord($unicode_character));
		if($chr===$unicode_character || strlen($chr)<=1 )return array($unicode_character);
		return array_merge(array($chr), self::mb_str_split(substr($unicode_character,strlen($chr))));
	}

}