<?php

namespace tt\services\polyfill;

/**
 * Features that have been introduced in PHP 5.4, 5.5 and 5.6
 */
class Php5
{

	/**
	 * Added in PHP 5.4:
	 * https://www.php.net/manual/en/function.json-encode.php
	 */
	const JSON_PRETTY_PRINT = 128;

	/**
	 * Determines classname of the calling class.
	 *
	 * Can be used instead of the constant "class" (Since 5.5):
	 * PHP 5.3:
	 *   Myclass::getClass();
	 *   class Myclass {
	 *     public static function getClass() {
	 *       return Php5::get_class();
	 *     }
	 *   }
	 * PHP 5.5:
	 *   Myclass::class;
	 *
	 * @return string name of the calling class
	 */
	public static function get_class()
	{
		$backtrace = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 2);
		return $backtrace[1]["class"];
	}

}