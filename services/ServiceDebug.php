<?php

namespace tt\services;

use PDOStatement;
use tt\features\config\v1\CFG_S;
use tt\features\debug\errorhandler\v2\Warning;
use tt\features\messages\v2\Message;

class ServiceDebug
{

	/** @noinspection PhpUnused */
	public static function out_continue($mixed = null)
	{
		self::out($mixed, true, false);
	}

	public static function out($mixed = null, $continue = false, $backtrace = true, $html = false)
	{
		ServiceEnv::isSapiCLI()
			?self::out_cli($mixed, $continue, $backtrace)
			:self::out_cgi($mixed, $continue, $backtrace, $html);
	}
	public static function out_cgi($mixed, $continue, $backtrace, $html)
	{
		if (!$continue) echo Message::getMessagesHtml();
		$output = print_r($mixed, 1);
		if (!$html) $output = htmlentities($output);
		echo "<pre style='white-space: pre-wrap'>" . $output . "</pre>";
		if ($backtrace) echo DEBUG::backtraceHtml(1);
		if (!$continue) exit;
	}
	public static function out_cli($mixed, $continue, $backtrace)
	{
		echo "\n-----------------\n";
		print_r($mixed);
		echo "\n-----------------\n";
		if ($backtrace){
			echo "Backtrace:\n* ".implode("\n* ",DEBUG::backtrace());
		}
		if (!$continue) exit;
		echo "\n-----------------\n";
	}

	/**
	 * Returns $message but adds warning to the changelog (and frontend in DEVMODE)
	 * @param string $message Fallback string
	 * @return string void
	 */
	public static function invalidState($message)
	{
		if(CFG_S::$DEVMODE)new Warning($message);
		return $message;
	}

	/**
	 * @param PDOStatement $statement
	 * @return false|string
	 */
	public static function debugDumpParams(PDOStatement $statement)
	{
		if (ob_get_status()) {
			ob_flush();
		}
		ob_start();
		$statement->debugDumpParams();
		return ob_get_clean();
	}

	public static function backtraceHtml($cut = 0)
	{
		return "<h2>Backtrace</h2>"
			. ServiceHtml::arrayToList(DEBUG::backtrace($cut + 1));
	}

	/**
	 * @return boolean
	 */
	public static function verbose()
	{
		return isset($_GET['debug']);
	}

	public static function backtrace($cut = 0)
	{
		$backtrace = debug_backtrace();
		if (!$backtrace) {
			return array("unknown caller");
		}
		$caller = array();
		foreach ($backtrace as $row) {
			if (!isset($row["file"]) || !isset($row["line"])) continue;
			$file = preg_replace(
				"/^" . preg_quote(CFG_S::$DIR_PROJ_ROOT ?: "", '/') . "/",
				"", $row["file"]);
			$caller[] = $file . ':' . $row["line"]
				. " (" . $row["function"] . ")";
		}
		for ($i = 0; $i < $cut; $i++) {
			array_shift($caller);
		}
		if (!$caller) {
			return array("unknown_caller");
		}
		return $caller;
	}

}