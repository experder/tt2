Integrate TToolbox into your project using
```
git submodule add https://github.com/experder/TToolbox.git TToolbox
git commit -m "Add TT as submodule 'TToolbox'."
```

When cloning your project's repository use this command:
```
git clone --recurse-submodules https://github.com/experder/ttDemo.git .
```
