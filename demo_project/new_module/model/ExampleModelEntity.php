<?php

namespace myproject\new_module\model;

use tt\core\Model;

class ExampleModelEntity extends Model {

	private static $table_name = "tt_example_customer";

	public function getTableName() {
		return self::$table_name;
	}

	public static function getClass() {
		return \tt\service\polyfill\Php5::get_class();
	}

}