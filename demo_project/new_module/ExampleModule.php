<?php

namespace myproject\new_module;

use tt\coremodule\dbmodell\core_routes;

class ExampleModule extends \tt\core\modules\Module {

	const moduleId = "example_module";

	/**
	 * @inheritdoc
	 */
	function getModuleId() {
		return self::moduleId;
	}

	public function doUpdateDatabase() {

		$view = new core_routes(array(
			"route_id" => "example_root",
			"title" => "Example module",
		));
		$this->q(1, $view->getSqlInsert());

		$view = new \myproject\new_module\views\ExampleView();
		$this->q(2, $view->get_navi_entry_sql2("example_root"));

	}

}