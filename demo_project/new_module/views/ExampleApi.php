<?php

namespace myproject\new_module\views;

use tt\core\view\Api;
use tt\core\view\InputData2;
use tt\core\view\JsonResponse;

class ExampleApi extends Api {

	const ROUTE_ID = "example_api";

	/**
	 * @inheritdoc
	 */
	public function run(InputData2 $inputData) {
		return new JsonResponse(array(
			"html" => "Hello world!",
		));
	}

	/**
	 * @inheritdoc
	 */
	public function getApiId() {
		return self::ROUTE_ID;
	}

	public static function getClass() {
		return \tt\service\polyfill\Php5::get_class();
	}

	public function getClass_() {
		return self::getClass();
	}

}