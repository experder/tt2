<?php

namespace myproject\new_module\views;

use tt\core\view\HtmlPage;
use tt\core\view\InputData2;
use tt\core\view\View;

class ExampleView extends View {

	const ROUTE_ID = "example_view";
	protected $title = "Example view";

	public static function getClass() {
		return \tt\service\polyfill\Php5::get_class();
	}

	/**
	 * @inheritdoc
	 */
	public function runWeb(InputData2 $inputData) {
		return new HtmlPage("Example!");
	}

	public function getClass_() {
		return self::getClass();
	}

	/**
	 * @inheritdoc
	 */
	public function getViewId() {
		return self::ROUTE_ID;
	}

}
