<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace myproject\demo_module\views;

use tt\components\tables\Datatable;
use tt\core\Config;
use tt\core\page\Message;
use tt\core\view\HtmlPage;
use tt\core\view\InputData2;
use tt\core\view\View;
use tt\service\form\Fieldset;
use tt\service\form\Form;
use tt\service\form\FormfieldCheckbox;
use tt\service\form\FormfieldHeader;
use tt\service\form\FormfieldHidden;
use tt\service\form\FormfieldPassword;
use tt\service\form\FormfieldRadio;
use tt\service\form\FormfieldRadioOption;
use tt\service\form\FormfieldSelect;
use tt\service\form\FormfieldText;
use tt\service\form\FormfieldTextarea;
use tt\service\ServiceHtml;

class CssDemo extends View {

	const VIEW_ID = "cssdemo";

	public static function getClass() {
		return \tt\service\polyfill\Php5::get_class();
	}

	/**
	 * @inheritdoc
	 */
	public function runWeb(InputData2 $inputData) {
		$components = array();

		$components[] = $this->demoText();
		$components[] = $this->demoForm();
		$components[] = $this->demoTable();

		$page = new HtmlPage($components);
		$this->demoAlerts($page);
		$page->setFocus(false);
		$page->addCssAbsolute(Config::getChecked(Config::HTTP_TTROOT) . "/demo_project/demo_module/views/cssdemo.css");

		return $page;
	}

	public function getClass_() {
		return self::getClass();
	}

	/**
	 * @inheritdoc
	 */
	public function getViewId() {
		return self::VIEW_ID;
	}


	private function demoText() {
		$html = array();

		$html[] = ServiceHtml::H1("Header 1");
		$html[] = ServiceHtml::H2("Header 2");
		$html[] = ServiceHtml::H3("Header 3");
		$html[] = ServiceHtml::H4("Header 4");

		$html[] = ServiceHtml::A("link", "") . " " . ServiceHtml::A_external("external link", "");

		return $html;
	}

	private function demoForm() {

		$form = new Form();

		$form->addField(new FormfieldHidden("hidden", "value"));

		$form->addField($ff = new FormfieldText("text", "Text"));
		$ff->setTooltip("Hint1");

		$form->addField($fieldset = new Fieldset("Fieldset"));
		$fieldset->addField(new FormfieldPassword("password", "Password"));

		$form->addField(new FormfieldHeader("Header", "header"));

		$form->addField($ff = new FormfieldCheckbox("checkbox1", "Checkbox1"));
		$ff->setTooltip("Hint2");
		$form->addField(new FormfieldCheckbox("checkbox2", "Checkbox2", "foo"));

		$form->addField(new FormfieldRadio("radio", array(
			new FormfieldRadioOption("value1", "Option1", "Hint3"),
			new FormfieldRadioOption("value2", "Option2"),
		), "value2"));

		$form->addField(new FormfieldTextarea("textarea", "Textarea", null, true,
			array("placeholder" => "Placeholder")
		));

		$form->addField(new FormfieldSelect("select",
			array(
				"one" => "bir",
				"two" => "İki",
				"three" => "Üç",
			),
			"Select"
		));

		$form->addButton(ServiceHtml::BUTTON("Option 2", "tt_tools.spinnerStart();"));

		return array($form);
	}

	private function demoTable() {
		$table = new Datatable(array(
			array("one" => "Foo", "two" => "Bar"),
			array("one" => "Foobar", "two" => "Foofoo"),
		));

		return array($table);
	}

	private function demoAlerts(HtmlPage $page) {

		$page->addMessage2(Message::TYPE_INFO, "Info");
		$page->addMessage2(Message::TYPE_QUESTION, "Question ('ask')");
		$page->addMessage2(Message::TYPE_ERROR, "Error");
		$page->addMessage2(Message::TYPE_CONFIRM, "Confirm ('ok')");

	}

}