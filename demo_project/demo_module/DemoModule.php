<?php
/*
 * This file is part of the TT toolbox;
 * Copyright (C) 2014-2022 Fabian Perder (t2@qnote.de) and contributors
 * TT comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under
 * certain conditions. See the GNU General Public License (file 'LICENSE' in the root directory) for more details.
 */

namespace myproject\demo_module;

use tt\core\modules\Module;
use tt\coremodule\dbmodell\core_routes;

class DemoModule extends Module {

	const moduleId = "demo_module";

	/**
	 * @inheritdoc
	 */
	function getModuleId() {
		return self::moduleId;
	}

	public function doUpdateDatabase() {

		$view = new core_routes(array(
			"route_id" => "core_demos",
			"title" => "Core demos",
			#"parent"=>$parent,
			#"visible"=>$visible?'1':'0',
			#"target"=>"http://google.de",
			#"orderby"=>$orderby,
		));
		$this->q(1, $view->getSqlInsert());

		$view = new \myproject\demo_module\views\CssDemo();
		$this->q(2, $view->get_navi_entry_sql("CSS demo", "core_demos"));

	}

}