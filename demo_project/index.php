<?php
/**
 * Starts TToolbox installer to initialize project settings
 * @see Installer
 */

#require_once __DIR__ . '/TToolbox/install/Installer.php';
require_once dirname(__DIR__) . '/install/Installer.php';
\tt\install\Installer::requireConfigPointer();

$page = new \tt\core\view\HtmlPage(null);

$page->addMessage2(\tt\core\page\Message::TYPE_CONFIRM, "Welcome!");

$page->deliver();
